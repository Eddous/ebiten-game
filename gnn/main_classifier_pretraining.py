import json
import random
import torch
from torch_geometric.data import Data
from supervised import train
import models
import torch.nn.functional as F
import sys


dataset_path = "./data/dataset2.json"
epochs = 50
# model = models.SGC_class(19, 6, 4)
model = models.GAT_class(19, 32, 6, 4, 3, 4)
output = "models/gat.pt"

batch_size = 256
lr = 0.001

loss = lambda outputs, targets: torch.nn.KLDivLoss(reduction="batchmean")(
    F.log_softmax(outputs, dim=1), targets
)
#loss = torch.nn.CrossEntropyLoss()

####################################################################

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

with open(dataset_path, "r") as file:
    json_data = json.load(file)

dataset = []
for d in json_data:
    dataset.append(
        Data(
            x=torch.tensor(d["X"]).float(),
            edge_index=torch.tensor(d["EdgeIndex"]),
            y=torch.tensor(d["Result"]).float().unsqueeze(0),
        )
    )
random.shuffle(dataset)

train(model, loss, dataset, epochs, batch_size, lr, device)
torch.save(model.state_dict(), output)
