import torch
import copy
import models
import torch.nn.functional as F
from torch.nn import Linear
from torch_geometric.nn import GCNConv
from torch_geometric.nn import global_mean_pool
import torch.nn.functional as F

# Implementation of Twin Delayed Deep Deterministic Policy Gradients (TD3)
# Paper: https://arxiv.org/abs/1802.09477

input_dim = 24
hidden_dim = 128
actor_layers = 6
heads = 4
critic_gat_layers = 4
critic_fc_layers = 2


# returns tensor of a shape [node, 1]
# this is important because this tensor is then concatenated
class Actor(torch.nn.Module):
    def __init__(self):
        super().__init__()
        self.net = models.GAT_act(input_dim, hidden_dim, actor_layers, heads)

    def forward(self, x, edge_index, batch):
        return self.net(x, edge_index, batch)


# returns tensor of a shape (batch)
class Critic(torch.nn.Module):
    def __init__(self):
        super().__init__()
        self.q1 = models.GAT_class(
            input_dim + 1, hidden_dim, 1, critic_gat_layers, critic_fc_layers, heads
        )
        self.q2 = models.GAT_class(
            input_dim + 1, hidden_dim, 1, critic_gat_layers, critic_fc_layers, heads
        )

    def forward(self, x, actor_output, edge_index, batch):
        return self.Q1(x, actor_output, edge_index, batch), self.Q2(
            x, actor_output, edge_index, batch
        )

    def Q1(self, x, actor_output, edge_index, batch):
        x = torch.cat((x, actor_output), dim=1)
        x = self.q1(x, edge_index, batch)
        x = x.squeeze()
        return x

    def Q2(self, x, actor_output, edge_index, batch):
        x = torch.cat((x, actor_output), dim=1)
        x = self.q2(x, edge_index, batch)
        x = x.squeeze()
        return x


class TD3(object):
    def __init__(
        self,
        device,
        discount=0.99,
        tau=0.005,
        policy_noise=0.2,
        noise_clip=0.5,
        policy_freq=2,
    ):
        self.device = device

        self.actor = Actor().to(device)
        self.actor_target = copy.deepcopy(self.actor)
        self.actor_optimizer = torch.optim.Adam(self.actor.parameters(), lr=3e-4)

        self.critic = Critic().to(device)
        self.critic_target = copy.deepcopy(self.critic)
        self.critic_optimizer = torch.optim.Adam(self.critic.parameters(), lr=3e-4)

        self.discount = discount
        self.tau = tau
        self.policy_noise = policy_noise
        self.noise_clip = noise_clip
        self.policy_freq = policy_freq

        self.total_it = 0

    def select_action(self, x, edge_index):
        x = torch.tensor(x).float().to(self.device)
        edge_index = torch.tensor(edge_index).to(self.device)
        return self.actor(x, edge_index, None).flatten().tolist()

    def train(self, b):
        self.total_it += 1

        with torch.no_grad():
            # Select action according to policy and add clipped noise
            noise = (torch.randn_like(b.action) * self.policy_noise).clamp(
                -self.noise_clip, self.noise_clip
            )

            next_action = self.actor_target(b.next_x, b.edge_index, b.batch) + noise

            # Compute the target Q value
            target_Q1, target_Q2 = self.critic_target(
                b.next_x, next_action, b.edge_index, b.batch
            )
            target_Q = torch.min(target_Q1, target_Q2)
            target_Q = b.reward + b.not_done * self.discount * target_Q

        # Get current Q estimates
        current_Q1, current_Q2 = self.critic(b.x, b.action, b.edge_index, b.batch)

        # Compute critic loss
        critic_loss = F.mse_loss(current_Q1, target_Q) + F.mse_loss(
            current_Q2, target_Q
        )

        # Optimize the critic
        self.critic_optimizer.zero_grad()
        critic_loss.backward()
        self.critic_optimizer.step()

        if self.total_it % self.policy_freq != 0:
            return None, None, critic_loss.item()

        # Delayed policy updates
        action = self.actor(b.x, b.edge_index, b.batch)

        # Compute actor loss
        actor_loss = -self.critic.Q1(b.x, action, b.edge_index, b.batch).mean()

        # Adding penalty for action drift
        # if this is enabled the actor learns to stay at a place :)
        action_drift_loss = F.l1_loss(action, b.action_used)
        # actor_loss += action_drift_loss

        # Optimize the actor
        self.actor_optimizer.zero_grad()
        actor_loss.backward()
        self.actor_optimizer.step()

        # Update the frozen target models
        for param, target_param in zip(
            self.critic.parameters(), self.critic_target.parameters()
        ):
            target_param.data.copy_(
                self.tau * param.data + (1 - self.tau) * target_param.data
            )

        for param, target_param in zip(
            self.actor.parameters(), self.actor_target.parameters()
        ):
            target_param.data.copy_(
                self.tau * param.data + (1 - self.tau) * target_param.data
            )

        return actor_loss.item(), action_drift_loss.item(), critic_loss.item()

    def save(self, filename, directory):
        torch.save(self.actor.state_dict(), "%s/%s_actor.pth" % (directory, filename))
        torch.save(self.critic.state_dict(), "%s/%s_critic.pth" % (directory, filename))

    def load(self, filename, directory):
        self.actor.load_state_dict(
            torch.load("%s/%s_actor.pth" % (directory, filename))
        )
        self.critic.load_state_dict(
            torch.load("%s/%s_critic.pth" % (directory, filename))
        )
