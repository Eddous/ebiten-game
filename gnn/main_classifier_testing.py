import torch
import models
import socket
import json
import time

host = "127.0.0.1"
port = 54775
buffer_size = 10000
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")


def receive_decode(socket):
    bytes = socket.recv(buffer_size)
    if not bytes:
        return None
    msg = json.loads(bytes.decode())
    return msg


def encode_send(socket, response):
    response_data = json.dumps(response).encode()
    socket.sendall(response_data)


def connect_to_socket():
    while True:
        try:
            sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            sock.connect((host, port))
            print("Connection successful.")
            return sock
        except (ConnectionRefusedError, socket.error) as e:
            print(f"Failed to connect to the server: {e}. Retrying in 1 second...")
            time.sleep(1)

def play_match(model, model_path):
    # sockets
    sock = connect_to_socket()

    # loading model
    model.load_state_dict(torch.load(model_path))
    model.to(device)

    # playing game until the end of a world
    while True:
        msg = receive_decode(sock)
        if msg == None:
            break
        encode_send(sock, models.classify(model, msg["X"], msg["EdgeIndex"], device))

if __name__ == "__main__": 
    # model = model = models.SGC_class(19, 6, 4)
    # path = "models/sgc.pt"
    model = models.GAT_class(19, 32, 6, 4, 3, 4)
    path = "models/gat.pt"
    
    play_match(model, path)
