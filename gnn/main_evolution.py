import torch
import socket
import json
import time
from evotorch.neuroevolution import NEProblem
from evotorch.algorithms import Cosyne
from evotorch.algorithms import CMAES
from evotorch.logging import PandasLogger, StdOutLogger
import matplotlib.pyplot as plt
import models

host = "127.0.0.1"
port = 54775
buffer_size = 10000


def receive_decode(sock):
    bytes = sock.recv(buffer_size)
    msg = json.loads(bytes.decode())
    return msg


def encode_send(sock, response):
    response_data = json.dumps(response).encode()
    sock.sendall(response_data)


time_sum = 0
fitness_runs = 0


def fitness(network):
    global time_sum
    global fitness_runs

    # if the socket is global, it is for some reson closed...
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.connect((host, port))

    start_time = time.time()
    while True:
        msg = receive_decode(sock)

        t = msg["T"]
        if t == 1:
            sock.close()
            time_sum += time.time() - start_time
            fitness_runs += 1
            return msg["Score"]

        edge_index = torch.IntTensor(msg["EdgeIndex"])
        x = torch.FloatTensor(msg["X"])
        x = models.classify(network, x, edge_index, device)
        encode_send(sock, x)


def fitness20(network):
    sum = 0
    for _ in range(20):
        sum += fitness(network)
    return sum / 20


# ============================================== #

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
device = "cpu" # faster on cpu with num_actors=8

model = models.SGC_class(21)
saved_model_path = "models/run4.pt"
saved_dataframe_path = "dataframes/run4.csv"

problem = NEProblem(
    objective_sense="max",
    network=model,
    network_eval_func=fitness20,
    device=device,
    num_actors=8,
)

searcher = CMAES(problem, stdev_init=0.01)

# loggers
StdOutLogger(searcher)
logger = PandasLogger(searcher)

for i in range(50):
    searcher.run(1)
    if searcher.status["pop_best"].evals.item()>= 0:
        pop_best = problem.make_net(searcher.status["pop_best"].values)
        torch.save(pop_best.state_dict(), "models/" + str(i) + ".pt")

# saving is here the pain in the ass
# if the problem has only 1 actor, there is no "best" keyword
# best = problem.make_net(searcher.status["best"].values)
# torch.save(best.state_dict(), saved_model_path)

dataframe = logger.to_dataframe()
dataframe.to_csv(saved_dataframe_path)

# just to please my eyes
# dataframe.mean_eval.plot()
# plt.show()
