import torch
import environment
import random
import models
import TD3 as TD3

# plays for pid=0 against AI1 with specified noise
# returns winrate, action_drift, reward
def test_run(env, model, device, noise, test_steps):
    winrate = []
    action_drifts = []
    rewards = []
    for _ in range(test_steps):
        msg = env.start_game(random.randint(0, 4294967296), False, noise)
        edge_index, x = msg["EdgeIndex"], msg["X"]
        while True:
            actions = {0: models.select_action(model, x, edge_index, device)}
            msg = env.step(actions)
            action_drifts.append(
                environment.action_drift(actions[0], msg["ActionsUsed"][0])
            )
            rewards.append(msg["Rewards"][0])
            if msg["Scores"] != None:
                break
            x = msg["X"]
        winrate.append(environment.decode_score(msg["Scores"][0]))

    return sum(winrate) / len(winrate), sum(action_drifts) / len(action_drifts), sum(rewards) / len(rewards)


if __name__ == "__main__":
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    env = environment.Env()

    policy = TD3.TD3(device)
    policy.load("DDPG_TD3_100000", "models")
    model = policy.actor
    
    # model = models.GAT_act(24, 128, 6, 4)
    # model.load_state_dict(torch.load("models/DDPG_TD3_100000_actor.pth"))
    # model.to(device)

    win_rate, _, _ = test_run(env, model, device, 0, 20)
    print(win_rate)

    env.close_env()
