import torch
import random
import math
import models
import environment
from torch.utils.tensorboard import SummaryWriter
from torchrl.data import ListStorage, PrioritizedReplayBuffer
import os
from main_actor_testing import test_run


# if self_play then noise does not matter
# returns points of winner or can return [] in case of !self_play or too lengthy game
def train_run(env, model, device, self_play, noise):
    msg = env.start_game(random.randint(0, 4294967296), self_play, noise)
    edge_index, x, random_actions, playing = (
        msg["EdgeIndex"],
        msg["X"],
        msg["RandomActions"],
        msg["PlayingPlayers"],
    )

    game_actions = {}
    last_seen_states = {}
    last_seen_actions = {}
    while True:
        actions = {}
        for pid in playing:
            if random.random() < 0.1:
                actions[pid] = random_actions[pid]
            else:
                permutation = environment.find_permutation(pid)
                t_x = environment.transform_x(x, permutation)
                t_action = models.select_action(model, t_x, edge_index, device)
                actions[pid] = t_action
            last_seen_states[pid] = x
            last_seen_actions[pid] = actions[pid]

        msg = env.step(actions)

        # points will transform x and next_x
        for pid, point in environment.Points(
            last_seen_states, edge_index, last_seen_actions, msg
        ).items():
            if not pid in game_actions:
                game_actions[pid] = []
            game_actions[pid].append(point)

        if msg["Scores"] != None:
            break

        x, random_actions, playing = (
            msg["X"],
            msg["RandomActions"],
            msg["PlayingPlayers"],
        )

    for pid, res in msg["Scores"].items():
        if res == "Win" and pid in game_actions:
            return game_actions[pid]

    return []


def train_step(model, criterion, optimizer, batch):
    action = model(batch.x, batch.edge_index, batch.batch)
    loss = criterion(action, batch.action_used)
    optimizer.zero_grad()
    loss.backward()
    optimizer.step()
    return loss.item()


def output(i, writer, wr1, wr2, ad, rew1, rew2, test_counter, buffer_size):
    print(
        "i:",
        i,
        "  wr1:",
        wr1,
        "  wr2:",
        wr2,
        "  rew1:",
        rew1,
        "  rew2:",
        rew2,
        "  ad:",
        round(ad, 2),
        "  buffer:",
        buffer_size,
        sep="",
    )

    writer.add_scalar("Winrate(Random)", wr1, test_counter)
    writer.add_scalar("Winrate(AI1)", wr2, test_counter)
    writer.add_scalar("Reward(Random)", rew1, test_counter)
    writer.add_scalar("Reward(AI1)", rew2, test_counter)
    writer.add_scalar("ActionDrift", ad, test_counter)
    writer.flush()


if __name__ == "__main__":
    print("STARTING!")

    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    self_play, noise = (True, -1)
    episodes = 10000  # just large number
    test_after = 40
    test_steps = 20
    save_model_after = 10000
    train_steps = 1
    lr = 0.001
    replay_buffer = PrioritizedReplayBuffer(
        storage=ListStorage(max_size=10**6),
        batch_size=64,
        collate_fn=environment.collate_fn,
        alpha=1,
        beta=0,
    )

    model = models.GAT_act(24, 128, 6, 4)
    # model.load_state_dict(torch.load("models/actor.pt"))
    model.to(device)

    criterion = torch.nn.MSELoss()
    optimizer = torch.optim.Adam(model.parameters(), lr=lr)
    env = environment.Env()
    writer = SummaryWriter()

    train_step_counter = 0
    test_counter = 0

    wr1, ad1, _ = test_run(env, model, device, 1, test_steps)
    wr2, ad2, _ = test_run(env, model, device, 0, test_steps)
    output(
        i=0,
        writer=writer,
        wr1=wr1,
        wr2=wr2,
        rew1=0,
        rew2=0,
        ad=(ad1 + ad2) / 2,
        test_counter=0,
        buffer_size=0,
    )
    test_counter += 1

    for i in range(1, episodes + 1):
        # data gathering
        points = train_run(env, model, device, self_play, noise)
        if len(points) <= 50 and len(points) != 0:
            indices = replay_buffer.extend(points)
            replay_buffer.update_priority(indices, math.exp(-len(points)))

        # training
        for j in range(train_steps):
            if len(replay_buffer) == 0:
                break
            batch = replay_buffer.sample(return_info=False).to(device)
            loss = train_step(model, criterion, optimizer, batch)
            writer.add_scalar("Loss", loss, train_step_counter)
            train_step_counter += 1

        # testing
        if i % test_after == 0:
            wr1, ad1, _ = test_run(env, model, device, 1, test_steps)
            wr2, ad2, _ = test_run(env, model, device, 0, test_steps)
            output(
                i=i,
                writer=writer,
                wr1=wr1,
                wr2=wr2,
                ad=(ad1 + ad2) / 2,
                rew1=0,
                rew2=0,
                test_counter=test_counter,
                buffer_size=len(replay_buffer),
            )
            test_counter += 1

        if i % save_model_after == 0:
            directory = "models"
            if not os.path.exists(directory):
                os.makedirs(directory)
            torch.save(model.state_dict(), directory + "/model_" + str(i) + ".pt")

    writer.close()
    env.close_env()
