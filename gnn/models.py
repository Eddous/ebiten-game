import torch
import torch.nn.functional as F
from torch.nn import Linear, Sequential as Seq, ReLU
from torch_geometric.nn import (
    GCNConv,
    GINConv,
    SGConv,
    GATConv,
    global_mean_pool,
)


#######################
##### CLASSIFIERS #####


class GAT_class(torch.nn.Module):
    def __init__(
        self,
        input_dim,
        hidden_dim,
        output_dim,
        num_gat_layers,
        num_fc_layers,
        num_heads,
    ):
        super().__init__()
        self.gat_layers = torch.nn.ModuleList()
        self.gat_layers.append(
            GATConv(input_dim, hidden_dim, heads=num_heads, concat=True)
        )
        for _ in range(1, num_gat_layers - 1):
            self.gat_layers.append(
                GATConv(
                    hidden_dim * num_heads, hidden_dim, heads=num_heads, concat=True
                )
            )
        self.gat_layers.append(
            GATConv(hidden_dim * num_heads, hidden_dim, heads=num_heads, concat=False)
        )

        self.fc_layers = torch.nn.ModuleList()
        for _ in range(num_fc_layers - 1):
            self.fc_layers.append(torch.nn.Linear(hidden_dim, hidden_dim))
        self.fc_layers.append(torch.nn.Linear(hidden_dim, output_dim))

    def forward(self, x, edge_index, batch):
        for gat_layer in self.gat_layers[:-1]:
            x = F.elu(gat_layer(x, edge_index))
        x = F.elu(self.gat_layers[-1](x, edge_index))

        x = global_mean_pool(x, batch)

        for fc_layer in self.fc_layers[:-1]:
            x = F.relu(fc_layer(x))
        x = self.fc_layers[-1](x)

        return x


class GIN_class(torch.nn.Module):
    def __init__(
        self, input_dim, hidden_dim, output_dim, num_gin_layers, num_fc_layers
    ):
        super().__init__()
        self.gin_layers = torch.nn.ModuleList()
        for _ in range(num_gin_layers):
            self.gin_layers.append(
                GINConv(
                    Seq(
                        torch.nn.Linear(
                            input_dim if _ == 0 else hidden_dim, hidden_dim
                        ),
                        torch.nn.ReLU(),
                        torch.nn.Linear(hidden_dim, hidden_dim),
                    )
                )
            )
        self.fc_layers = torch.nn.ModuleList()
        for _ in range(num_fc_layers - 1):
            self.fc_layers.append(torch.nn.Linear(hidden_dim, hidden_dim))
        self.fc_layers.append(torch.nn.Linear(hidden_dim, output_dim))

    def forward(self, x, edge_index, batch):
        for gin_layer in self.gin_layers:
            x = F.relu(gin_layer(x, edge_index))
        x = global_mean_pool(x, batch)
        for fc_layer in self.fc_layers[:-1]:
            x = F.relu(fc_layer(x))
        x = self.fc_layers[-1](x)
        return x


class GCN_class(torch.nn.Module):
    def __init__(
        self, input_dim, hidden_dim, output_dim, num_gcn_layers, num_fc_layers
    ):
        super().__init__()
        self.gcn_layers = torch.nn.ModuleList()
        self.gcn_layers.append(GCNConv(input_dim, hidden_dim))
        for _ in range(1, num_gcn_layers):
            self.gcn_layers.append(GCNConv(hidden_dim, hidden_dim))
        self.fc_layers = torch.nn.ModuleList()
        for _ in range(num_fc_layers - 1):
            self.fc_layers.append(torch.nn.Linear(hidden_dim, hidden_dim))
        self.fc_layers.append(torch.nn.Linear(hidden_dim, output_dim))

    def forward(self, x, edge_index, batch):
        for gcn_layer in self.gcn_layers:
            x = F.relu(gcn_layer(x, edge_index))
        x = global_mean_pool(x, batch)
        for fc_layer in self.fc_layers[:-1]:
            x = F.relu(fc_layer(x))
        x = self.fc_layers[-1](x)
        return x


class SGC_class(torch.nn.Module):
    def __init__(self, input_dim, output_dim, K):
        super().__init__()
        self.simpleConv = SGConv(input_dim, output_dim, K=K)

    def forward(self, x, edge_index, batches):
        x = self.simpleConv(x, edge_index)
        x = global_mean_pool(x, batches)
        return x


def classify(model, x, edge_index, device):
    x = torch.tensor(x).float().to(device)
    edge_index = torch.tensor(edge_index).to(device)
    ret = model(x, edge_index, None)
    ret = ret.squeeze()
    ret = F.softmax(ret, dim=0)
    return ret.tolist()


##################
##### Actors #####

# all return tensor of a shape [node, 1]
# this is important because this tensor is then concatenated
# ^ only in the case of actor-critic, in other cases it is only keeping it the same


class GCN_act(torch.nn.Module):
    def __init__(self, input_dim, hidden_dim, layers):
        super().__init__()

        self.layers = torch.nn.ModuleList()
        self.layers.append(GCNConv(input_dim, hidden_dim))

        for _ in range(1, layers - 1):
            self.layers.append(GCNConv(hidden_dim, hidden_dim))
        self.layers.append(GCNConv(hidden_dim, 1))

    def forward(self, x, edge_index, batch):
        for gcn in self.layers[:-1]:
            x = gcn(x, edge_index)
            x = F.relu(x)
        x = self.layers[-1](x, edge_index)
        return x


class SGC_act(torch.nn.Module):
    def __init__(self, features, steps):
        super().__init__()
        self.simpleConv = SGConv(features, 1, K=steps)

    def forward(self, x, edge_index, batch):
        x = self.simpleConv(x, edge_index)
        return x


class GAT_act(torch.nn.Module):
    def __init__(self, input_dim, hidden_dim, num_gat_layers, num_heads):
        super().__init__()
        self.layers = torch.nn.ModuleList()
        self.layers.append(GATConv(input_dim, hidden_dim, heads=num_heads, concat=True))
        for _ in range(1, num_gat_layers - 1):
            self.layers.append(
                GATConv(
                    hidden_dim * num_heads, hidden_dim, heads=num_heads, concat=True
                )
            )
        self.layers.append(
            GATConv(hidden_dim * num_heads, 1, heads=num_heads, concat=False)
        )

    def forward(self, x, edge_index, batch):
        for gat in self.layers[:-1]:
            x = gat(x, edge_index)
            x = F.elu(x)
        x = self.layers[-1](x, edge_index)
        return x


class GIN_act(torch.nn.Module):
    def __init__(self, input_dim, hidden_dim, num_gin_layers):
        super().__init__()
        self.gin_layers = torch.nn.ModuleList()

        self.gin_layers.append(
            GINConv(
                Seq(
                    Linear(input_dim, hidden_dim),
                    ReLU(),
                    Linear(hidden_dim, hidden_dim),
                )
            )
        )
        for _ in range(1, num_gin_layers):
            self.gin_layers.append(
                GINConv(
                    Seq(
                        Linear(hidden_dim, hidden_dim),
                        ReLU(),
                        Linear(hidden_dim, hidden_dim),
                    )
                )
            )
        self.gin_layers.append(
            GINConv(
                Seq(
                    Linear(hidden_dim, hidden_dim),
                    ReLU(),
                    Linear(hidden_dim, 1),
                )
            )
        )

    def forward(self, x, edge_index, batch):
        for gin in self.gin_layers[:-1]:
            x = gin(x, edge_index)
            x = F.relu(x)
        x = self.gin_layers[-1](x, edge_index)
        return x


def select_action(model, x, edge_index, device):
    x = torch.tensor(x).float().to(device)
    edge_index = torch.tensor(edge_index).to(device)
    return model(x, edge_index, None).flatten().tolist()
