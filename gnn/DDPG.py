import copy
import torch
import models
import torch.nn.functional as F
from torch.nn import Linear
from torch_geometric.nn import GCNConv
from torch_geometric.nn import global_mean_pool

# Implementation of Deep Deterministic Policy Gradients (DDPG)
# Paper: https://arxiv.org/abs/1509.02971
# NOT MINE


input_dim = 24
hidden_dim = 128
actor_layers = 6
heads = 4
critic_gat_layers = 4
critic_fc_layers = 2


# returns tensor of a shape [node, 1]
# this is important because this tensor is then concatenated
class Actor(torch.nn.Module):
    def __init__(self):
        super().__init__()
        self.net = models.GAT_act(input_dim, hidden_dim, actor_layers, heads)

    def forward(self, x, edge_index, batch):
        return self.net(x, edge_index, batch)


# returns tensor of a shape (batch)
class Critic(torch.nn.Module):
    def __init__(self):
        super().__init__()
        self.net = models.GAT_class(
            input_dim + 1, hidden_dim, 1, critic_gat_layers, critic_fc_layers, heads
        )

    def forward(self, x, actor_output, edge_index, batch):
        x = torch.cat((x, actor_output), dim=1)
        x = self.net(x, edge_index, batch)
        x = x.squeeze()
        return x


# actor is nn.Module with forward : state -> actions
# critic is nn.Module with forward : state, actions -> q value
class DDPG(object):
    def __init__(self, device, discount=0.99, tau=0.005):

        self.device = device
        self.actor = Actor().to(device)
        self.actor_target = copy.deepcopy(self.actor)
        self.actor_optimizer = torch.optim.Adam(self.actor.parameters(), lr=1e-4)

        self.critic = Critic().to(device)
        self.critic_target = copy.deepcopy(self.critic)
        self.critic_optimizer = torch.optim.Adam(
            self.critic.parameters(), weight_decay=1e-2
        )

        self.discount = discount
        self.tau = tau

    def select_action(self, x, edge_index):
        x = torch.tensor(x).float().to(self.device)
        edge_index = torch.tensor(edge_index).to(self.device)
        return self.actor(x, edge_index, None).flatten().tolist()

    def train(self, b):

        # Compute the target Q value
        target_Q = self.critic_target(
            b.next_x,
            self.actor_target(b.next_x, b.edge_index, b.batch),
            b.edge_index,
            b.batch,
        )
        target_Q = b.reward + (b.not_done * self.discount * target_Q).detach()

        # Get current Q estimate
        current_Q = self.critic(b.x, b.action, b.edge_index, b.batch)

        # Compute critic loss
        critic_loss = F.mse_loss(current_Q, target_Q)

        # Optimize the critic
        self.critic_optimizer.zero_grad()
        critic_loss.backward()
        self.critic_optimizer.step()

        # Computing the action to plug it into loss function/s
        action = self.actor(b.x, b.edge_index, b.batch)

        # Compute actor loss
        actor_loss = -self.critic(b.x, action, b.edge_index, b.batch).mean()

        # Adding penalty for action drift
        # if this is enabled the actor learns to stay at a place :)
        action_drift_loss = F.mse_loss(action, b.action_used)
        # actor_loss += action_drift_loss

        # Optimize the actor
        self.actor_optimizer.zero_grad()
        actor_loss.backward()
        self.actor_optimizer.step()

        # Update the frozen target models
        for param, target_param in zip(
            self.critic.parameters(), self.critic_target.parameters()
        ):
            target_param.data.copy_(
                self.tau * param.data + (1 - self.tau) * target_param.data
            )

        for param, target_param in zip(
            self.actor.parameters(), self.actor_target.parameters()
        ):
            target_param.data.copy_(
                self.tau * param.data + (1 - self.tau) * target_param.data
            )
        return actor_loss.item(), action_drift_loss.item(), critic_loss.item()

    def save(self, filename):
        torch.save(self.critic.state_dict(), filename + "_critic")
        torch.save(self.critic_optimizer.state_dict(), filename + "_critic_optimizer")

        torch.save(self.actor.state_dict(), filename + "_actor")
        torch.save(self.actor_optimizer.state_dict(), filename + "_actor_optimizer")

    def load(self, filename):
        self.critic.load_state_dict(torch.load(filename + "_critic"))
        self.critic_optimizer.load_state_dict(
            torch.load(filename + "_critic_optimizer")
        )
        self.critic_target = copy.deepcopy(self.critic)

        self.actor.load_state_dict(torch.load(filename + "_actor"))
        self.actor_optimizer.load_state_dict(torch.load(filename + "_actor_optimizer"))
        self.actor_target = copy.deepcopy(self.actor)
