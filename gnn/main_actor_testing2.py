import torch
import random
import models
import TD3 as TD3
import json
import socket
from environment import find_permutation, transform_x

def receive_decode(socket):
    bytes = socket.recv(buffer_size)
    if not bytes:
        return None
    msg = json.loads(bytes.decode())
    return msg


def encode_send(socket, response):
    response_data = json.dumps(response).encode()
    socket.sendall(response_data)


host = "127.0.0.1"
port = 54776
buffer_size = 10000
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sock.connect((host, port))

policy = TD3.TD3(device)
policy.load("DDPG_TD3_50000", "models")
model = policy.actor

while True:
    msg = receive_decode(sock)
    edge_index, x = msg["EdgeIndex"], transform_x(msg["X"], find_permutation(2))
    action = models.select_action(model, x, edge_index, device)
    encode_send(sock, action)
