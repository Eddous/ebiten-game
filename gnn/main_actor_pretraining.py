import random
import json
import torch
import models
from torch_geometric.data import Data

from supervised import train

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

dataset_path = "./data/actor_S_10000.json"
with open(dataset_path, "r") as file:
    json_data = json.load(file)

dataset = []
for d in json_data:
    dataset.append(
        Data(
            x=torch.tensor(d["X"]).float(),
            edge_index=torch.tensor(d["EdgeIndex"]),
            y=torch.tensor(d["Action"]).float().reshape([-1, 1]),
        )
    )
random.shuffle(dataset)

epochs = 40
# model = models.GCN_act(21, 128, 6)
# model = models.SGC_act(5, 2)
model = models.GAT_act(21, 128, 6, 4)
# model = models.GIN_act(21, 128, 6)

batch_size = 256
lr = 0.001

train(model, torch.nn.MSELoss(), dataset, epochs, batch_size, lr, device)
torch.save(model.state_dict(), "models/pretrained.pt")
