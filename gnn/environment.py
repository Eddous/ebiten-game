import socket
import json
import torch
import random
import copy
from torch_geometric.data import Data, Batch

host = "127.0.0.1"
port = 54775
buffer_size = 10000


class Env:
    def __init__(self):
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.connect((host, port))

    def start_game(self, seed, self_play, noise):
        self.__encode_send(
            {"T": 0, "Seed": seed, "SelfPlay": self_play, "Noise": noise}
        )
        msg = self.__receive_decode()
        return msg

    # returns (data, score), where data is a tuple
    def step(self, actions):
        self.__encode_send({"T": 1, "Moves": actions})
        msg = self.__receive_decode()
        return msg

    def close_env(self):
        msg = {"T": 2}
        self.__encode_send(msg)
        self.sock.close()

    def __receive_decode(self):
        bytes = self.sock.recv(buffer_size)
        msg = json.loads(bytes.decode())
        self.__fix_keys(msg)
        return msg

    def __encode_send(self, response):
        response_data = json.dumps(response).encode()
        self.sock.sendall(response_data)

    def __fix_keys(self, msg):
        self.__fix_key(msg, "RandomActions")
        self.__fix_key(msg, "Rewards")
        self.__fix_key(msg, "ActionsUsed")
        self.__fix_key(msg, "Scores")

    def __fix_key(self, msg, key):
        if msg[key] == None:
            return
        dict = msg[key]
        new = {}
        for k, v in dict.items():
            new[int(k)] = v
        msg[key] = new


# actions are not the used actions in the last seen state!
# it is the last recorded actions for the players because of the delayed feedback
def Points(last_seen_states, edge_index, last_seen_actions, msg):
    points = {}
    # using PlayingPlayers is unrelible because in the last msg it is empty
    for pid in msg["ActionsUsed"].keys():
        if not pid in last_seen_actions:
            pass
        map = find_permutation(pid)
        p = Point()
        p.x = transform_x(last_seen_states[pid], map)
        p.edge_index = edge_index
        p.action_used = msg["ActionsUsed"][pid]
        p.action = last_seen_actions[pid]
        p.next_x = transform_x(msg["X"], map)
        p.reward = msg["Rewards"][pid]
        p.not_done = 1.0 - msg["Done"]
        points[pid] = p
    return points


class Point:
    x = None
    edge_index = None
    action = None
    action_used = None
    next_x = None
    reward = None
    not_done = None

    def __str__(self):
        return (
            f"Point(x={self.x}, edge_index={self.edge_index}, action_used={self.action_used}, "
            f"next_x={self.next_x}, reward={self.reward}, not_done={self.not_done}, newcomer={self.newcomer})"
        )

def collate_fn(points):
    data_list = [
        Data(
            x=torch.tensor(p.x).float(),
            edge_index=torch.tensor(p.edge_index),
            action_used=torch.tensor(p.action_used).reshape(-1, 1).float(),
            action=torch.tensor(p.action).reshape(-1, 1).float(),
            next_x=torch.tensor(p.next_x).float(),
            reward=p.reward,
            not_done=p.not_done,
        )
        for p in points
    ]

    return Batch.from_data_list(data_list)


def action_drift(action, action_used):
    return sum([abs(a - b) for a, b in zip(action, action_used)])


def decode_score(score):
    if score == "Lost":
        return 0
    if score == "Draw":
        return 0.5
    if score == "Win":
        return 1
    raise Exception("Incorrect data")


def find_identity():
    return {0:0, 1:1, 2:2, 3:3, 4:4, 5:5}

def find_permutation(pid):
    image = [i for i in range(1, 6)]
    random.shuffle(image)
    map = {pid: 0}
    for i in range(0, 6):
        if i == pid:
            continue
        map[i] = image.pop()
    return map


# this is dependent on how the state is encoded!
def transform_x(x, permutation):
    new = copy.deepcopy(x)
    for nid in range(len(x)):
        for i in range(6):
            new[nid][permutation[i] + 6] = x[nid][i + 6]
    return new
