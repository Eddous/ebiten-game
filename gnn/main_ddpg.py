import numpy as np
import torch
import random
import os

import DDPG as DDPG
import TD3 as TD3
import environment
from torch.utils.tensorboard import SummaryWriter
from torchrl.data import ReplayBuffer, ListStorage
from main_actor_testing import test_run
from main_cross_entropy import output


if __name__ == "__main__":
    print("STARTING!")

    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    self_play, noise = (True, -1)
    episodes = 2**32-1  # just large number
    test_after = 200
    test_steps = 50
    save_model_after = 10**4
    lr = 0.001
    replay_buffer = ReplayBuffer(
        storage=ListStorage(max_size=10**6),
        batch_size=64,
        collate_fn=environment.collate_fn,
    )
    # policy = DDPG.DDPG(device)
    policy = TD3.TD3(device)
    #policy.load("NAME", "models")

    env = environment.Env()
    writer = SummaryWriter()

    train_step_counter = 0
    test_counter = 0
    wr1, ad1, rew1 = test_run(env, policy.actor, device, 1, test_steps)
    wr2, ad2, rew2 = test_run(env, policy.actor, device, 0, test_steps)
    output(
        i=0,
        writer=writer,
        wr1=wr1,
        wr2=wr2,
        rew1=rew1,
        rew2=rew2,
        ad=(ad1 + ad2) / 2,
        test_counter=0,
        buffer_size=0,
    )
    test_counter += 1

    for i in range(1, episodes + 1):
        
        # playing and training at once
        msg = env.start_game(random.randint(0, 4294967296), self_play, noise)
        edge_index, x, random_actions, playing = (
            msg["EdgeIndex"],
            msg["X"],
            msg["RandomActions"],
            msg["PlayingPlayers"],
        )
        last_seen_states = {}
        last_seen_actions = {}
        while True:
            actions = {}
            for pid in playing:
                if random.random() < 0.1:
                    actions[pid] = random_actions[pid]
                else:
                    permutation = environment.find_permutation(pid)
                    t_x = environment.transform_x(x, permutation)
                    actions[pid] = policy.select_action(t_x, edge_index)
                last_seen_states[pid] = x
                last_seen_actions[pid] = actions[pid]

            msg = env.step(actions)

            # points will transform x and next_x
            for point in environment.Points(last_seen_states, edge_index, last_seen_actions, msg).values():
                replay_buffer.add(point)

            if msg["Scores"] != None:
                break

            x, random_actions, playing = (
                msg["X"],
                msg["RandomActions"],
                msg["PlayingPlayers"],
            )

            # training
            s = replay_buffer.sample().to(device)
            actor_loss, action_drift_loss, critic_loss = policy.train(s)

            if actor_loss != None:
                writer.add_scalar("LossActor", actor_loss, train_step_counter)
            if action_drift_loss != None:
                writer.add_scalar("LossActionDrift", action_drift_loss, train_step_counter)
            writer.add_scalar("LossCritic", critic_loss, train_step_counter)

            train_step_counter += 1


        # testing
        if i % test_after == 0:
            wr1, ad1, rew1 = test_run(env, policy.actor, device, 1, test_steps)
            wr2, ad2, rew2 = test_run(env, policy.actor, device, 0, test_steps)
            output(
                i=i,
                writer=writer,
                wr1=wr1,
                wr2=wr2,
                rew1=rew1,
                rew2=rew2,
                ad=(ad1 + ad2) / 2,
                test_counter=test_counter,
                buffer_size=len(replay_buffer),
            )
            test_counter += 1

        if i % save_model_after == 0:
            policy.save("DDPG_TD3_"+str(i), "models")

    writer.close()
    env.close_env()
