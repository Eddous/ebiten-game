import torch
from torch_geometric.data import Data, Batch

class Point:
    def __init__(self, x, edge_index, action, action_used, next_x, reward, done):
        self.x = x
        self.edge_index = edge_index
        self.action = action
        self.action_used = action_used
        self.next_x = next_x
        self.reward = reward
        if done:
            print("gotchya")
        self.not_done = 1.0 - done

    def action_drift(self):
        return sum([abs(a - b) for a, b in zip(self.action, self.action_used)])

    def __str__(self):
        return (
            f"Point(x={self.x}, edge_index={self.edge_index}, action_used={self.action_used}, "
            f"next_x={self.next_x}, reward={self.reward}, not_done={self.not_done})"
        )

def collate_fn(points):
    data_list = [
        Data(
            x=torch.tensor(p.x).float(),
            edge_index=torch.tensor(p.edge_index),
            action=torch.tensor(p.action).reshape(-1, 1),
            action_used=torch.tensor(p.action_used).reshape(-1, 1).float(),
            next_x=torch.tensor(p.next_x).float(),
            reward=p.reward,
            not_done=p.not_done,
        )
        for p in points
    ]

    return Batch.from_data_list(data_list)
