import torch
from torch_geometric.loader import DataLoader


def train_run(model, loader, criterion, optimizer, device):
    model.train()
    for data in loader:
        data = data.to(device)
        out = model(data.x, data.edge_index, data.batch)
        loss = criterion(out, data.y.to(device))
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()


def test_run(model, loader, criterion, device):
    model.eval()
    loss = 0
    for data in loader:
        data = data.to(device)
        out = model(data.x, data.edge_index, data.batch)
        loss += criterion(out, data.y.to(device)).item()
    return loss / len(loader)


def output_performance(model, epoch, train_loader, test_loader, criterion, device):
    train = test_run(model, train_loader, criterion, device)
    test = test_run(model, test_loader, criterion, device)
    print(f"Epoch: {epoch:03d}, Train: {train:.4f}, Test: {test:.4f}")


def print_gradient_norms(model):
    print("Gradient norms for each layer:")
    for name, param in model.named_parameters():
        if param.grad is not None:
            grad_norm = param.grad.norm()
            print(f"Layer: {name}, Gradient Norm: {grad_norm:.4f}")
        else:
            print(f"Layer: {name}, Gradient Norm: None (No gradient)")


def train(model, criterion, dataset, epochs, batch_size, lr, device):
    border = int(len(dataset) * 0.8)
    train_dataset = dataset[:border]
    test_dataset = dataset[border:]
    train_loader = DataLoader(train_dataset, batch_size=batch_size, shuffle=True)
    test_loader = DataLoader(test_dataset, batch_size=batch_size, shuffle=False)

    optimizer = torch.optim.Adam(model.parameters(), lr=lr)

    model.to(device)

    output_performance(model, -1, train_loader, test_loader, criterion, device)
    for epoch in range(epochs):
        train_run(model, train_loader, criterion, optimizer, device)
        output_performance(model, epoch, train_loader, test_loader, criterion, device)
        # print_gradient_norms(model)
