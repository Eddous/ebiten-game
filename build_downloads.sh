#!/bin/bash

cd go/main_pc || { echo "you are in a wrong path"; exit 1; }

GOOS=windows GOARCH=amd64 go build -o gra-amd64.exe
GOOS=windows GOARCH=386 go build -o gra-386.exe
GOOS=linux GOARCH=amd64 go build -o gra-linux

mv gra-amd64.exe ../../web/downloads/
mv gra-386.exe ../../web/downloads/
mv gra-linux ../../web/downloads/

# waiting till the ebitengine is purego on macOS
# GOOS=darwin GOARCH=amd64 go build -o gra-darwin
# mv gra-darwin ../../web/downloads/

