//go:build !js && !wasm

package board

import (
	ai "gra/core2/ai"
	state "gra/core2/state"

	"github.com/barweiss/go-tuple"
)

func (b *Board) runAI(s *state.State, playingAIs []*ai.AI, ch chan tuple.T2[int, string]) {
	for _, ai := range playingAIs {
		str, _ := ai.Action(s)
		ch <- tuple.New2(ai.Pid, str)
	}
	close(ch)
}
