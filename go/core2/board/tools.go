package board

import (
	"gra/resources"

	"github.com/barweiss/go-tuple"
	"github.com/hajimehoshi/ebiten/v2"
)

const toolMove = "move"
const toolSpawn = "spawn"
const toolDisband = "disband"
const toolBuildFarm = "farm"
const toolBuildTower = "tower"
const toolBuildCity = "city"
const toolDestroy = "destroy"
const toolReset = "reset"

func (b *Board) Tools() []tuple.T2[string, *ebiten.Image] {
	return []tuple.T2[string, *ebiten.Image]{
		tuple.New2(toolMove, resources.Arrow3),
		tuple.New2(toolSpawn, resources.Spawn),
		tuple.New2(toolDisband, resources.Disband),
		tuple.New2(toolBuildFarm, resources.Farm),
		tuple.New2(toolBuildTower, resources.Tower),
		tuple.New2(toolBuildCity, resources.City),
		tuple.New2(toolDestroy, resources.Destroy),
		tuple.New2(toolReset, resources.Reset),
	}
}
