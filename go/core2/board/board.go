package board

import (
	ai "gra/core2/ai"
	state "gra/core2/state"
	"gra/coredrawutils"
	"gra/coreutils"
	"gra/datstruct"
	"gra/graphgen"
	"gra/gulrot"
	"gra/utils"
	"strconv"

	"github.com/barweiss/go-tuple"
	"github.com/hajimehoshi/ebiten/v2"
)

type transition struct {
	// killed and reseted are split because of viewing the history (surrender is move, having split territory is not)
	// reasons for new state
	killed     datstruct.Set[int] // nodes of killed players (surrender, time)
	negotation map[int][]int      // map[nid][]pid
	cmds       *state.Commands    // this is special command that has info from all players

	// only visual flow
	moves            [][]int // matrix, DOES NOT HAVE SELF MOVES (because it is for drawing)
	somethingHappend datstruct.Set[int]
	reseted          datstruct.Set[int] // reseted nodes due to split territory
}

type Board struct {
	// arguments
	seed    int64
	players int
	mode    int

	// things that does not change
	nodes  []utils.VecF
	matrix [][]bool

	history     []*state.State
	transitions []transition

	// for CoreInterface (the support for gui)
	negotationNodes datstruct.Set[int]
	commands        state.Commands

	// this is for creating moves
	selectedNid      int
	selectedSoldiers int
	posibleMoves     datstruct.Set[int]

	bannedNodes datstruct.Set[int]

	helperArrowsPositions []float64

	ais       []*ai.AI
	defaultAI *ai.DefaultAI
}

func (b *Board) last() *state.State {
	return b.history[len(b.history)-1]
}

// ======================
// # High level interface

func NewBoard(seed int64, players []int, nodes int) *Board {
	gameMap := graphgen.GenerateGenerate(seed, nodes)

	b := &Board{
		seed:    seed,
		players: len(players),
		mode:    nodes,

		history:     []*state.State{state.NewState(len(players), gameMap)},
		transitions: []transition{},
		ais:         make([]*ai.AI, 0),
		defaultAI:   ai.NewDefaultAI(seed),
	}

	// initialization of constants for this particular map
	s := b.history[0]
	b.nodes = make([]utils.VecF, len(s.Nodes))
	for i, n := range s.Nodes {
		b.nodes[i] = n.Pos
	}
	b.matrix = b.history[0].Matrix

	b.helperArrowsPositions = coreutils.HelperArrowsPositions(b.nodes, b.matrix)

	// settings player types
	for pid, playerType := range players {
		if playerType != 0 {
			b.ais = append(b.ais, ai.NewAI(pid, seed+int64(pid), playerType))
		}
	}

	b.ResetActionBuffer()
	return b
}

// seed, players, mode
func (b *Board) Args() (int64, int, int) {
	return b.seed, b.players, b.mode
}

// ===================
// # TurnGameInterface

func (b *Board) Result() map[int]coreutils.DeathInfo {
	ret := map[int]coreutils.DeathInfo{}
	for id, p := range b.last().Players {
		if p.Dead {
			ret[id] = p.DeathInfo
		}
	}
	return ret
}

func (b *Board) KillPlayers(ids []int, deathText string) (playing []int) {
	old := b.last()
	new := old.Kill(ids, deathText)
	b.history = append(b.history, new)

	set := datstruct.NewSet[int]()
	for i := range old.Nodes {
		if old.Nodes[i].Owner != new.Nodes[i].Owner {
			set.Add(i)
		}
	}

	b.transitions = append(b.transitions, transition{killed: set})
	return new.AlivePlayers()
}

func (b *Board) InputCheck(pid int, buffer string) bool {
	s := b.last()

	switch s.MoveType {
	case state.Negotation:
		nids, ok := coreutils.StrToNids(buffer)
		if !ok {
			return false
		}
		return s.CheckNegotationInput(pid, nids)
	case state.Moves:
		moves, ok := state.StrToCommands(buffer)
		if !ok {
			return false
		}
		return s.CheckMoveInput(pid, moves)
	}
	panic("no such option")
}

func (b *Board) Move(input map[int]string) (playing []int) {
	last := b.last()
	var new *state.State
	var trans transition

	switch last.MoveType {
	case state.Negotation:
		move := coreutils.FromStrBatch(input, coreutils.StrToNids)
		new = last.Negotation(move)
		trans = transition{negotation: coreutils.NegotationToTrans(move)}
	case state.Moves:
		move := coreutils.FromStrBatch(input, state.StrToCommands)
		var reseted datstruct.Set[int]
		new, reseted = last.Move2(move)
		matrix, somethingHappend := state.MovesToTransition(len(new.Matrix), move)
		cmds := state.ConcatCommands(move)
		trans = transition{cmds: &cmds, moves: matrix, somethingHappend: somethingHappend, reseted: reseted}
	}
	b.history = append(b.history, new)
	b.transitions = append(b.transitions, trans)
	return new.AlivePlayers()
}

func (b *Board) DefaultMove(pid int) string {
	return b.defaultAI.Action(b.last(), pid)
}

func (b *Board) StartAI() <-chan tuple.T2[int, string] {
	s := b.last()
	if s.MoveType == state.End {
		return nil
	}

	playingAIs := make([]*ai.AI, 0)
	for _, ai := range b.ais {
		if !s.Players[ai.Pid].Dead {
			playingAIs = append(playingAIs, ai)
		}
	}
	if len(playingAIs) == 0 {
		return nil
	}

	// if the user leave game, the AI still runs in the background
	ch := make(chan tuple.T2[int, string], len(b.ais))
	// this is compilation magic
	go b.runAI(s, playingAIs, ch)
	return ch
}

// # TurnGameInterface
// ===================

// ===============
// # CoreInterface

func (b *Board) GetNodesAsVecF() []utils.VecF {
	return b.nodes
}

func (b *Board) GetActionBuffer(pid int) (string, bool) {
	s := b.last()
	switch s.MoveType {
	case state.Negotation:
		neg := b.negotationNodes.ToSlice()
		if s.CheckNegotationInput(pid, neg) {
			return coreutils.NidsToStr(neg), true
		}
	case state.Moves:
		if s.CheckMoveInput(pid, b.commands) {
			return b.commands.String(), true
		}
	}
	return "", false
}
func (b *Board) ResetActionBuffer() {
	b.negotationNodes = datstruct.NewSet[int]()
	b.bannedNodes = datstruct.NewSet[int]()
	b.commands = state.NewCommands()
	b.resetMoveBuffer()
}

func (b *Board) resetMoveBuffer() {
	b.selectedNid = -1
	b.selectedSoldiers = 0
	b.posibleMoves = datstruct.NewSet[int]()
}

// resets are done using a tool
func (b *Board) Holdable(nid int) bool { return false }
func (b *Board) Holded(nid int)        {}

func (b *Board) Tap(pid, nid int, tool string) {
	s := b.last()
	if s.MoveType == state.Negotation {
		b.negTap(nid)
	} else if s.MoveType == state.Moves {
		b.movTap(pid, nid, tool)
	}
}
func (b *Board) MissTap() {
	b.resetMoveBuffer()
}

func (b *Board) negTap(nid int) {
	s := b.last()
	if b.bannedNodes.Has(nid) {
		return
	}
	if b.negotationNodes.Has(nid) {
		b.negotationNodes.Remove(nid)
	} else if len(s.AlivePlayers()) > b.negotationNodes.Size() {
		b.negotationNodes.Add(nid)
	} else {
		return
	}

	b.bannedNodes = datstruct.NewSet[int]()
	for nid1 := range b.negotationNodes.Iterator() {
		for nid2 := range s.Nodes {
			if !b.negotationNodes.Has(nid2) && s.NodesTooClose(nid1, nid2) {
				b.bannedNodes.Add(nid2)
			}
		}
	}
}

// the branching is annoying
func (b *Board) movTap(pid, nid int, tool string) {
	s := b.last()
	n := s.Nodes[nid]
	switch tool {
	case toolMove:
		if b.selectedNid == -1 {
			// first click
			if n.Owner != pid || b.commands.SoldiersUsed(nid) == n.Soldiers {
				return
			}
			b.selectedNid = nid
			b.selectedSoldiers = 1
			for _, nid2 := range n.Neighbours {
				b.posibleMoves.Add(nid2)
			}
			return
		} else {
			// not first click
			if nid == b.selectedNid {
				// the same node
				if b.commands.SoldiersUsed(nid)+b.selectedSoldiers < n.Soldiers {
					b.selectedSoldiers++
				}
				return
			}
			if !b.posibleMoves.Has(nid) {
				// the same as miss tap
				b.resetMoveBuffer()
				return
			}
			// finishing the move
			b.commands.AddMove(b.selectedNid, nid, b.selectedSoldiers)
			b.resetMoveBuffer()
			return
		}
	case toolSpawn:
		b.resetMoveBuffer()
		if n.Owner == pid &&
			n.Building == state.City &&
			s.Players[pid].CurrSoldiers+b.commands.SpawningSoldiers() < s.Players[pid].MaxSoldiers {
			b.commands.AddSpawning(nid, 1)
		}
		return
	case toolDisband:
		b.resetMoveBuffer()
		if n.Owner == pid && b.commands.SoldiersUsed(nid) < n.Soldiers {
			b.commands.AddDisbanding(nid, 1)
		}
		return

	case toolBuildFarm:
		b.resetMoveBuffer()
		if n.Owner == pid && n.Building != state.Farm && (b.commands.SoldiersUsed(nid) < n.Soldiers || b.commands.IsBuilding(nid)) {
			b.commands.Build(nid, state.Farm)
		}
		return

	case toolBuildTower:
		b.resetMoveBuffer()
		if n.Owner == pid && n.Building != state.Tower && (b.commands.SoldiersUsed(nid) < n.Soldiers || b.commands.IsBuilding(nid)) {
			b.commands.Build(nid, state.Tower)
		}
		return

	case toolBuildCity:
		b.resetMoveBuffer()
		if n.Owner == pid && n.Building != state.City && (b.commands.SoldiersUsed(nid) < n.Soldiers || b.commands.IsBuilding(nid)) {
			b.commands.Build(nid, state.City)
		}
		return

	case toolDestroy:
		b.resetMoveBuffer()
		if n.Owner == pid && n.Building != state.None && (b.commands.SoldiersUsed(nid) < n.Soldiers || b.commands.IsBuilding(nid)) {
			b.commands.Build(nid, state.None)
		}
		return

	case toolReset:
		b.resetMoveBuffer()
		b.commands.ResetNode(nid)
	default:
		panic("not an option: " + tool)
	}

	if s.Nodes[nid].Owner != pid {
		return
	}

}

func (b *Board) PlayerText(pid int) ([]string, []string) {
	player := b.last().Players[pid]
	return []string{"Territory", "Soldiers"}, []string{strconv.Itoa(player.Territory), strconv.Itoa(player.CurrSoldiers) + "/" + strconv.Itoa(player.MaxSoldiers)}
}

func (b *Board) Territory(pid int) string {
	player := b.last().Players[pid]
	return strconv.Itoa(player.Territory)
}

func (b *Board) Soldiers(pid int) string {
	player := b.last().Players[pid]
	return strconv.Itoa(player.CurrSoldiers) + "/" + strconv.Itoa(player.MaxSoldiers)
}

func (b *Board) TutorialText() []string {
	switch b.last().MoveType {
	case state.Negotation:
		players := len(b.last().AlivePlayers())
		minimalDist := b.last().MinimalDistanceBetweenNodes[players]
		return []string{
			"Select " + strconv.Itoa(players) + " nodes, at least " + strconv.Itoa(minimalDist) + " apart, single one will be selected for you to start on.",
		}
	case state.Moves:
		return []string{
			"TODO",
		}
	case state.End:
		return nil
	}
	panic("dont happend")
}

func (c *Board) HistoryBoardStates() int {
	return len(c.history)
}

func (b *Board) ShowTools(pid, historyState int) bool {
	if historyState != len(b.history)-1 {
		return false
	}
	if b.last().Players[pid].Dead {
		return false
	}
	return b.last().MoveType == state.Moves
}

func (b *Board) StateStr() (string, bool) {
	t := b.history[len(b.history)-1].MoveType
	return t.String(), t != state.Moves
}

func (b *Board) Update() {}

func (b *Board) Draw(screen *ebiten.Image, camera *gulrot.Camera, historyState int, drawTrans bool) {
	coredrawutils.DrawEdges(screen, camera, b.nodes, b.matrix)
	/*
		0 - draw state
		1 - draw state && transition
		2 - draw state && current buffers && visual flow
	*/

	var t byte
	if historyState == len(b.history)-1 {
		t = 2
	} else if drawTrans {
		t = 1
	} else {
		t = 0
	}

	for nid, n := range b.history[historyState].Nodes {
		outline, alpha := b.outline(nid, t, historyState)
		soldiers, super, outgoing := b.soldiers(nid, t, historyState)
		selectedSoldiers := 0
		if nid == b.selectedNid {
			selectedSoldiers = b.selectedSoldiers
		}
		coredrawutils.DrawNode(
			screen,
			camera,
			coredrawutils.DrawNodeOptions{
				Pos:                n.Pos,
				Pid:                n.Owner,
				Outline:            outline,
				OutlineAlpha:       alpha,
				Soldiers:           soldiers,
				SuperScript:        super,
				Selected:           b.selected(nid, t, historyState),
				SelectedMoreTimes:  selectedSoldiers,
				Banned:             b.banned(nid, t, historyState),
				Outgoing:           outgoing,
				WhoChosePlayers:    b.whoChoseNode(nid, t, historyState),
				PointingArrowAngle: b.helperArrowsPositions[nid],
				ShowArrow:          b.helperArrow(nid, t, historyState),
			},
		)
	}
	if t == 2 && historyState != 0 {
		trans := b.transitions[historyState-1]
		if trans.moves != nil {
			coredrawutils.DrawHelpArrows(screen, camera, b.nodes, trans.moves)
		}
	}

}

// # core driver interface
// =======================

// ============
// Draw helpers

// horrible horrible function
func (b *Board) outline(nid int, t byte, i int) (byte, float64) {
	node := b.history[i].Nodes[nid]
	var cmds state.Commands

	switch t {
	case 0:
		return buildingToOutline(node.Building), 1
	case 1:
		if b.transitions[i].cmds == nil {
			return buildingToOutline(node.Building), 1
		}
		cmds = *b.transitions[i].cmds
	case 2:
		cmds = b.commands
	default:
		panic("not an option")
	}

	cmd := cmds.GetNodeCmd(nid)
	if !cmd.BuildFlag {
		return buildingToOutline(node.Building), 1
	}
	if cmd.Building == state.None {
		return buildingToOutline(node.Building), 0.5
	}
	return buildingToOutline(cmd.Building), 0.5
}

func buildingToOutline(building state.BuildingType) byte {
	switch building {
	case state.None:
		return 0
	case state.City:
		return 1
	case state.Tower:
		return 2
	case state.Farm:
		return 3
	}
	panic("not implemented")
}

func (b *Board) selected(nid int, t byte, i int) bool {
	switch t {
	case 0:
		return false
	case 1:
		return false
	case 2:
		return b.negotationNodes.Has(nid) || b.posibleMoves.Has(nid)
	}
	panic("dont happen")
}

func (b *Board) banned(nid int, t byte, i int) bool {
	switch t {
	case 0:
		return false
	case 1:
		return b.transitions[i].killed.Has(nid)
	case 2:
		if b.bannedNodes.Has(nid) {
			return true
		}
		if i == 0 {
			return false
		}
		return b.transitions[i-1].reseted.Has(nid) || b.transitions[i-1].killed.Has(nid)
	}
	panic("dont happen")
}

// for the first phase
func (b *Board) whoChoseNode(nid int, t byte, i int) []int {
	switch t {
	case 0:
		return nil
	case 1:
		if t := b.transitions[i]; t.negotation != nil {
			return t.negotation[nid]
		}
		return nil
	case 2:
		return nil
	}
	panic("dont happen")
}

// returns soldiers, superscript, outgoing
func (b *Board) soldiers(nid int, t byte, i int) (int, int, []coredrawutils.Outgoing) {
	node := b.history[i].Nodes[nid]
	if t == 0 {
		return node.Soldiers, 0, nil
	}

	var cmd state.NodeCommand
	if t == 1 {
		if b.transitions[i].cmds == nil {
			return node.Soldiers, 0, nil
		}
		cmd = b.transitions[i].cmds.GetNodeCmd(nid)
	} else {
		cmd = b.commands.GetNodeCmd(nid)
	}

	soldiers := node.Soldiers
	super := 0
	outgoing := make([]coredrawutils.Outgoing, 0)
	if cmd.BuildFlag {
		soldiers--
	}
	if cmd.DisbandFlag {
		soldiers -= cmd.Soldiers
		super = -cmd.Soldiers
	}
	if cmd.SpawnFlag {
		super = cmd.Soldiers
	}
	if cmd.MoveFlag {
		for nid2, sol := range cmd.Moving {
			outgoing = append(outgoing, coredrawutils.NewOutgoing(b.history[0].Nodes[nid2].Pos, sol))
			soldiers -= sol
		}
	}
	return soldiers, super, outgoing
}

func (b *Board) helperArrow(nid int, t byte, i int) bool {
	if t != 2 || i == 0 {
		return false
	}
	return b.transitions[i-1].somethingHappend.Has(nid)
}

// Draw helpers
// ============
