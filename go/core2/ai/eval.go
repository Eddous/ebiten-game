package ai

import (
	"gra/core1/ai"
	state "gra/core2/state"
	"gra/coreutils"
	"gra/datstruct"
)

// this is quite stupid heuristics
func eval(s *state.State) []float64 {
	if s.MoveType == state.End {
		results := make([]coreutils.Result, len(s.Players))
		for i, p := range s.Players {
			results[i] = p.DeathInfo.Result
		}
		return coreutils.ResultsToFloat(results)
	}

	return ai.LinearNormalization(territoryAndDistEvalFunc(s, 1, 1, 1, 1))
}

func territoryAndDistEvalFunc(s *state.State, soldierVal, cityVal, towerVal, farmVal float64) []float64 {
	avgDist := averageDistOfSoldiersFromBorder(s)
	score := territorySum(s, soldierVal, cityVal, towerVal, farmVal)

	for i := range score {
		score[i] -= avgDist[i] / (avgDist[i] + 1)
	}
	return score
}

func territorySum(s *state.State, soldierVal, cityVal, towerVal, farmVal float64) []float64 {
	ret := make([]float64, len(s.Players))
	for _, node := range s.Nodes {
		if pid := node.Owner; pid != -1 {
			ret[pid] += 1 + float64(node.Soldiers)*soldierVal
			switch node.Building {
			case state.City:
				ret[pid] += cityVal
			case state.Tower:
				ret[pid] += towerVal
			case state.Farm:
				ret[pid] += farmVal
			}
		}
	}
	return ret
}

func averageDistOfSoldiersFromBorder(s *state.State) []float64 {
	soldiers := make([]float64, len(s.Players))
	distanceSum := make([]float64, len(s.Players))
	for nid, node := range s.Nodes {
		pid := node.Owner
		sold := float64(node.Soldiers)
		if sold != 0 {
			soldiers[pid] += sold
			distanceSum[pid] += sold * float64(shortestDistToEnemyNode(s, nid, pid))
		}
	}
	for i := range soldiers {
		if soldiers[i] != 0 {
			distanceSum[i] = distanceSum[i] / soldiers[i]
		}
	}
	return distanceSum
}

func shortestDistToEnemyNode(s *state.State, nid int, pid int) int {
	type elem struct {
		nid  int
		dist int
	}
	seen := datstruct.NewSet[int]()
	seen.Add(nid)
	fifo := []elem{{nid: nid, dist: 0}}
	for i := 0; i < len(fifo); i++ {
		curr := fifo[i]
		for neighbour := 0; neighbour < len(s.Matrix); neighbour++ {
			if s.Matrix[curr.nid][neighbour] && !seen.Has(neighbour) {
				if s.Nodes[neighbour].Owner != pid {
					return curr.dist + 1
				}
				fifo = append(fifo, elem{nid: neighbour, dist: curr.dist + 1})
				seen.Add(neighbour)
			}
		}
	}
	return 0
}
