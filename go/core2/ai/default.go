package ai

import (
	"gra/ai"
	"gra/core2/state"
	"gra/coreutils"
	"math/rand"
)

type DefaultAI struct {
	rnd *rand.Rand
	neg func(*state.State, int, *rand.Rand) []int
	mov func(*state.State, int, *rand.Rand) []ai.NodeActionTuple[state.NodeCommand]
}

func NewDefaultAI(seed int64) *DefaultAI {
	return &DefaultAI{
		rnd: rand.New(rand.NewSource(seed)),
		neg: RandomNegotation,
		mov: ai.Bean1(
			stateInterface(),
			2,
			ai.LocalSearch,
			eval,
		),
	}
}

func (ai *DefaultAI) Action(s *state.State, pid int) string {
	switch s.MoveType {
	case state.Negotation:
		return coreutils.NidsToStr(ai.neg(s, pid, ai.rnd))
	case state.Moves:
		cmds := state.NewCommands()
		for _, nat := range ai.mov(s, pid, ai.rnd) {
			cmds[nat.Nid] = nat.NodeAction
		}
		return cmds.String()
	}
	panic("what?!")
}
