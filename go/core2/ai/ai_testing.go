package ai

import (
	"math/rand"
)

// this is for testing of AIs
func NewAITesting(
	pid int,
	seed int64,
	testingType int,
) *AI {
	return &AI{
		Pid:  pid,
		rnd:  rand.New(rand.NewSource(seed)),
		Type: -1,
	}
}

// for external stuff use special function
// func NewAITestingExternalEval(evalFunc)
