package ai

import (
	"gra/ai"
	"gra/core2/state"
	"gra/coreutils"
	"math/rand"
)

// high level interface + implementation of StateInterface

type AI struct {
	Pid  int
	Type int

	rnd *rand.Rand
	neg func(*state.State, int, *rand.Rand) []int
	mov func(*state.State, int, *rand.Rand) []ai.NodeActionTuple[state.NodeCommand]
}

func NewAI(pid int, seed int64, t int) *AI {
	ret := &AI{
		Pid:  pid,
		rnd:  rand.New(rand.NewSource(seed)),
		Type: t,
		neg:  RandomNegotation,
	}
	switch t {
	case 1:
		ret.mov = ai.Bean1(
			stateInterface(),
			2,
			ai.LocalSearch,
			eval,
		)
	case 2:
		ret.mov = ai.Bean3(
			stateInterface(),
			3,
			ai.LocalSearch,
			eval,
		)
	case 3:
		ret.mov = ai.Bean3(
			stateInterface(),
			6,
			ai.LocalSearch,
			eval,
		)
	default:
		panic("invalid")
	}
	return ret
}

// when false it means that the AI does not play
func (ai *AI) Action(s *state.State) (string, bool) {
	if s.Players[ai.Pid].Dead {
		return "", false
	}
	switch s.MoveType {
	case state.Negotation:
		return coreutils.NidsToStr(ai.neg(s, ai.Pid, ai.rnd)), true
	case state.Moves:
		cmds := state.NewCommands()
		for _, nat := range ai.mov(s, ai.Pid, ai.rnd) {
			cmds[nat.Nid] = nat.NodeAction
		}
		return cmds.String(), true
	}
	panic("what?!")
}

func stateInterface() ai.StateInterface[*state.State, state.NodeCommand] {
	return ai.StateInterface[*state.State, state.NodeCommand]{
		Playing:             playing,
		RandomActions:       randomActions,
		Neighbourhood:       neighbourhood,
		GlobalNeighbourhood: globalNeighbourhood,
		Step:                step,
	}
}

func playing(s *state.State) []int {
	return s.AlivePlayers()
}

// all missing will be spawned + ignoring disbanding + ignoring destroying
func randomActions(s *state.State, pid int, rnd *rand.Rand) []ai.NodeActionTuple[state.NodeCommand] {
	// I want use the methods
	cmds := state.NewCommands()

	nidUsed := []int{}
	spawningNids := []int{}
	toCreateSoldiers := s.Players[pid].MaxSoldiers - s.Players[pid].CurrSoldiers
	if toCreateSoldiers < 0 {
		// in case of more soldiers than territory allows
		toCreateSoldiers = 0
	}
	for nid, node := range s.Nodes {
		canSpawn := (node.Building == state.City && toCreateSoldiers > 0)
		if node.Owner == pid && (node.Soldiers > 0 || canSpawn) {
			cmd := &ai.NodeActionTuple[state.NodeCommand]{Nid: nid, NodeAction: state.NodeCommand{}}
			cmds[nid] = cmd.NodeAction

			nidUsed = append(nidUsed, nid)
			if canSpawn {
				spawningNids = append(spawningNids, nid)
			}
		}
	}

	// global info
	for _, nid := range coreutils.CombinationWithRepetetion2(rnd, spawningNids, toCreateSoldiers) {
		cmds.AddSpawning(nid, 1)
	}

	// local actions
	const probOfBuilding = 0.1
	for _, nid := range nidUsed {
		n := &s.Nodes[nid]
		if n.Soldiers != 0 && n.Building == state.None && rnd.Float64() < probOfBuilding {
			switch rnd.Intn(3) {
			case 0:
				cmds.Build(nid, state.Farm)
			case 1:
				cmds.Build(nid, state.Tower)
			case 2:
				cmds.Build(nid, state.City)
			}
		}
		for _, nid2 := range coreutils.CombinationWithRepetetion2(rnd, n.Neighbourhood, n.Soldiers-cmds.SoldiersUsed(nid)) {
			if nid2 != nid {
				cmds.AddMove(nid, nid2, 1)
			}
		}
	}

	ret := []ai.NodeActionTuple[state.NodeCommand]{}
	for _, nid := range nidUsed {
		ret = append(ret, ai.NodeActionTuple[state.NodeCommand]{Nid: nid, NodeAction: cmds[nid]})
	}
	return ret
}

// if the node is spawning then it is locked for disbanding
func neighbourhood(s *state.State, nid int, cmd state.NodeCommand) []state.NodeCommand {
	ret := []state.NodeCommand{}
	inter := toIntermediate(s, nid, cmd)
	for i := range inter.all {
		for j := range inter.all {
			if i == j || inter.all[i] == 0 {
				continue
			}
			if next := inter.transfer(i, j); next != nil {
				ret = append(ret, fromIntermediate(s, nid, next))
			}
		}
	}
	return ret
}

// disbanding will disable the spawning
// will spawn all posible
// assumes that all possible spawning soldiers are there
func globalNeighbourhood(s *state.State, na []ai.NodeActionTuple[state.NodeCommand]) [][]ai.NodeActionTuple[state.NodeCommand] {

	// to intermediate
	intermediate := []int{}
	back := map[int]int{}
	for i, nat := range na {
		if s.Nodes[nat.Nid].Building == state.City && !nat.NodeAction.DisbandFlag {
			intermediate = append(intermediate, nat.NodeAction.Soldiers)
			back[len(intermediate)-1] = i
		}
	}

	ret := [][]ai.NodeActionTuple[state.NodeCommand]{}
	for i := range intermediate {
		for j := range intermediate {
			if i != j && intermediate[i] != 0 {
				// copying
				new := make([]ai.NodeActionTuple[state.NodeCommand], len(na))
				copy(new, na)

				// making the local change
				from := &new[back[i]].NodeAction
				to := &new[back[j]].NodeAction
				from.Soldiers -= 1
				to.Soldiers += 1
				from.SpawnFlag = from.Soldiers != 0
				to.SpawnFlag = to.Soldiers != 0

				ret = append(ret, new)
			}
		}
	}
	return ret
}

func step(s *state.State, actions map[int][]ai.NodeActionTuple[state.NodeCommand]) *state.State {
	if s.MoveType != state.Moves {
		panic("wrong state")
	}
	cmds := map[int]state.Commands{}
	for pid, nats := range actions {
		cmds[pid] = state.NewCommands()
		for _, nat := range nats {
			cmds[pid][nat.Nid] = nat.NodeAction
		}
	}
	return s.Move(cmds)
}

// == helpers ==

type intermediate struct {
	all []int // examples(not all examples): [neighbour1, neighbour2, ..., disbanding, buildingNone], [n1, n2, ..., bf, bt, bc]

	nidToIndex      map[int]int
	disbandingIndex int
	buildToIndex    map[state.BuildingType]int

	bulding bool
}

// this returns false when it breaches building constraint
func (r *intermediate) transfer(i1, i2 int) *intermediate {
	if i1 <= r.disbandingIndex && i2 > r.disbandingIndex && r.bulding {
		return nil
	}
	ret := &intermediate{
		all:             make([]int, len(r.all)),
		nidToIndex:      r.nidToIndex,
		disbandingIndex: r.disbandingIndex,
		buildToIndex:    r.buildToIndex,
		bulding:         r.bulding,
	}
	copy(ret.all, r.all)

	if i1 > r.disbandingIndex && i2 <= r.disbandingIndex {
		ret.bulding = false
	}
	if i1 <= r.disbandingIndex && i2 > r.disbandingIndex {
		ret.bulding = true
	}
	ret.all[i1]--
	ret.all[i2]++
	return ret
}

func toIntermediate(s *state.State, nid int, cmd state.NodeCommand) *intermediate {
	n := s.Nodes[nid]

	i := &intermediate{
		nidToIndex:      map[int]int{},
		disbandingIndex: -1,
		buildToIndex:    map[state.BuildingType]int{},
	}

	size := len(n.Neighbourhood)
	for idx, nid2 := range n.Neighbourhood {
		i.nidToIndex[nid2] = idx
	}
	if !cmd.SpawnFlag {
		i.disbandingIndex = size
		size++
	}
	if n.Building == state.None {
		i.buildToIndex[state.Farm] = size
		i.buildToIndex[state.Tower] = size + 1
		i.buildToIndex[state.City] = size + 2
		size += 3
	} else {
		i.buildToIndex[state.None] = size
		size += 1
	}
	i.all = make([]int, size)

	freeSoldiers := n.Soldiers
	if cmd.MoveFlag {
		for nid2, soldiers := range cmd.Moving {
			freeSoldiers -= soldiers
			i.all[i.nidToIndex[nid2]] = soldiers
		}
	}
	if cmd.DisbandFlag {
		freeSoldiers -= cmd.Soldiers
		i.all[i.disbandingIndex] = cmd.Soldiers
	}
	if cmd.BuildFlag {
		freeSoldiers -= 1
		i.all[i.buildToIndex[cmd.Building]] = 1
		i.bulding = true
	}
	i.all[i.nidToIndex[nid]] = freeSoldiers
	return i
}

func fromIntermediate(s *state.State, nid int, cmd *intermediate) state.NodeCommand {
	n := s.Nodes[nid]
	ret := state.NodeCommand{}

	for i := 0; i < cmd.disbandingIndex; i++ {
		if cmd.all[i] != 0 && n.Neighbourhood[i] != nid {
			if !ret.MoveFlag {
				ret.MoveFlag = true
				ret.Moving = map[int]int{}
			}
			ret.Moving[n.Neighbourhood[i]] = cmd.all[i]
		}
	}
	if cmd.disbandingIndex != -1 && cmd.all[cmd.disbandingIndex] != 0 {
		ret.DisbandFlag = true
		ret.Soldiers = cmd.all[cmd.disbandingIndex]
	}
	for building, index := range cmd.buildToIndex {
		if cmd.all[index] == 1 {
			ret.BuildFlag = true
			ret.Building = building
		}
	}
	return ret
}
