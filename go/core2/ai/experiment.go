package ai

import (
	"gra/core2/state"
	"gra/coreutils"
	"gra/graphgen"
)

func RunAIExperiment(seed int64, nodes int, AIs []*AI, limit int) ([]coreutils.Result, []map[int]string) {
	gameMap := graphgen.GenerateGenerate(seed, nodes)
	s := state.NewState(len(AIs), gameMap)
	history := []map[int]string{}

	for s.MoveType != state.End {
		actions := map[int]string{}
		for _, ai := range AIs {
			str, ok := ai.Action(s)
			if ok {
				actions[ai.Pid] = str
			}
		}
		s = s.Step(actions)
		history = append(history, actions)
		if len(history) == limit {
			s = s.Kill(s.AlivePlayers(), "limit was reached")
		}
	}

	results := []coreutils.Result{}
	for _, p := range s.Players {
		results = append(results, p.DeathInfo.Result)
	}
	return results, history
}
