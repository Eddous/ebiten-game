package ai

import (
	"gra/core2/state"
	"gra/coreutils"
	"math/rand"
)

func RandomNegotation(s *state.State, pid int, rnd *rand.Rand) []int {
	alive := len(s.AlivePlayers())
	for {
		rndChoice := coreutils.RandomTuple(rnd, alive, len(s.Nodes))
		if s.CheckNegotationInput(pid, rndChoice) {
			return rndChoice
		}
	}
}
