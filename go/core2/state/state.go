package state

import (
	"gra/coreutils"
	"gra/datstruct"
	"gra/graphgen"
	"gra/utils"
	"math"
)

// read only pls
type State struct {
	Matrix                      [][]bool
	GraphGenData                *graphgen.Data
	MinimalDistanceBetweenNodes []int // the index is number of players - if you play in two, then you will use [2] and rest you will ignore

	MoveType MoveType
	Players  []Player
	Nodes    []Node
}

func NewState(players int, gameMap graphgen.Data) *State {
	if !utils.IsInInt(players, 2, 6) {
		return nil
	}

	plrs := make([]Player, players)
	for pid := 0; pid < players; pid++ {
		plrs[pid] = Player{} // everything is default
	}
	nodes := make([]Node, 0)
	for nid, n := range gameMap.Nodes {

		neighbours := []int{}
		neighbourhood := []int{} // calculating it that way to have the neighbourhood sorted by nid (IDK if it is needed)

		for nid2 := range gameMap.Matrix[nid] {
			if gameMap.Matrix[nid][nid2] {
				neighbours = append(neighbours, nid2)
				neighbourhood = append(neighbourhood, nid2)
			}
			if nid == nid2 {
				neighbourhood = append(neighbourhood, nid2)
			}
		}

		nodes = append(nodes,
			Node{
				Pos: n,

				Neighbours:    neighbours,
				Neighbourhood: neighbourhood,
				Owner:         -1,
				// others are default
			},
		)
	}
	return &State{
		Matrix:                      gameMap.Matrix,
		GraphGenData:                &gameMap,
		MinimalDistanceBetweenNodes: coreutils.MinDistanceBetweenNodes(gameMap, players),
		MoveType:                    Negotation,
		Players:                     plrs,
		Nodes:                       nodes,
	}
}

func (s *State) CheckNegotationInput(pid int, nids []int) bool {
	if len(nids) != len(s.AlivePlayers()) {
		return false
	}
	for _, n := range nids {
		if !utils.IsInInt(n, 0, len(s.Nodes)-1) {
			return false
		}
	}
	for i := 0; i < len(nids)-1; i++ {
		for j := i + 1; j < len(nids); j++ {
			if s.NodesTooClose(nids[i], nids[j]) {
				return false
			}
		}
	}
	return true
}

func (s *State) CheckMoveInput(pid int, commands Commands) bool {
	return CheckCommands(s, pid, commands)
}

func (s *State) Kill(ids []int, deathText string) *State {
	new := s.copy()

	// get points to assign
	alive := 0
	for _, p := range new.Players {
		if !p.Dead {
			alive++
		}
	}

	var result coreutils.Result
	if len(ids) != alive {
		result = coreutils.Lost
	} else {
		result = coreutils.Draw
	}

	// kill players
	set := datstruct.NewSet[int]()
	for _, id := range ids {
		new.Players[id].kill(result, deathText)
		set.Add(id)
	}

	// reset nodes
	for i := range new.Nodes {
		n := &new.Nodes[i]
		if set.Has(n.Owner) {
			n.Reset()
		}
	}

	if len(new.AlivePlayers()) <= 1 {
		new.MoveType = End
	}

	return new
}

func (old *State) Step(actions map[int]string) *State {
	switch old.MoveType {
	case Negotation:
		return old.Negotation(coreutils.FromStrBatch(actions, coreutils.StrToNids))
	case Moves:
		return old.Move(coreutils.FromStrBatch(actions, StrToCommands))
	}
	panic("Panic panic")
}

// map[pid][]nid
func (old *State) Negotation(nids map[int][]int) *State {
	if old.MoveType != Negotation {
		panic("wrong state")
	}

	new := old.copy()
	for pid, nid := range coreutils.MaxMinDistance(nids, new.GraphGenData.MinimalPathDistance) {
		node := &new.Nodes[nid]
		player := &new.Players[pid]

		node.Owner = pid
		node.Soldiers = 1
		player.CurrSoldiers = 1
		player.MaxSoldiers = 1
		player.Territory = 1
		new.Nodes[nid].Building = City
	}
	new.MoveType = Moves
	return new
}

func (old *State) Move(commands map[int]Commands) *State {
	new, _ := old.Move2(commands)
	return new
}

// returns reseted nodes, IT DOES NOT CHANGE COMMANDS! <- crucial because of AI
func (old *State) Move2(commands map[int]Commands) (*State, datstruct.Set[int]) {
	if old.MoveType != Moves {
		panic("wrong state")
	}

	reseted := datstruct.NewSet[int]()
	new := old.copy()

	newSoldiers, newBuildings, moveMatrix := new.preprocessCommands(commands)
	new.fight(moveMatrix) // fighting, setting ownership
	new.finishBuilding(old, newSoldiers, newBuildings)
	new.trimSplitedTerritory(reseted)
	new.recalculatePlayers()
	new.killPlayers()
	return new, reseted

}

// returns where_build_soldiers, new_building, move_matrix (diagonal is not empty)
func (s *State) preprocessCommands(playerCommands map[int]Commands) (map[int]int, map[int]BuildingType, [][]int) {
	newSoldiers := map[int]int{}
	newBuildings := map[int]BuildingType{}
	moveMatrix := coreutils.NewMoveMatrix(len(s.Matrix))

	for _, commands := range playerCommands {
		for nid, command := range commands {
			if command.BuildFlag {
				s.Nodes[nid].Soldiers--
				newBuildings[nid] = command.Building
			}
			if command.SpawnFlag {
				// it is not compatible with disband
				newSoldiers[nid] = command.Soldiers
			}
			if command.DisbandFlag {
				s.Nodes[nid].Soldiers -= command.Soldiers
				// it is not compatible with spawn

			}
			if command.MoveFlag {
				for nid2, num := range command.Moving {
					moveMatrix[nid][nid2] += num
					s.Nodes[nid].Soldiers -= num
				}
			}
		}
	}

	// the rest of soldiers does not evaporate
	for nid := range s.Nodes {
		moveMatrix[nid][nid] += s.Nodes[nid].Soldiers
		s.Nodes[nid].Soldiers = 0
	}

	return newSoldiers, newBuildings, moveMatrix
}

// this will edit given state
// the matrix has soldiers on diagonal
func (s *State) fight(moves [][]int) {

	// ================
	// # fight in edges

	for d1 := range moves {
		for d2 := range moves[d1] {
			if s.Nodes[d1].Owner != s.Nodes[d2].Owner {
				min := int(utils.Min(moves[d1][d2], moves[d2][d1]))
				moves[d1][d2] -= min
				moves[d2][d1] -= min
			}
		}
	}

	// ==================
	// # calculate armies

	nodeArmies := make([]map[int]int, len(s.Nodes)) // this holds the maximum of soldiers the player can have
	nodeStrengths := make([]map[int]float64, len(s.Nodes))
	for i := range s.Nodes {
		nodeArmies[i] = make(map[int]int)
		nodeStrengths[i] = make(map[int]float64)

		// calculate armies
		for j, from := range s.Nodes {
			if moves[j][i] == 0 {
				continue
			}
			nodeArmies[i][from.Owner] += moves[j][i]
			nodeStrengths[i][from.Owner] += float64(moves[j][i])

			// bonus
			if j == i {
				nodeStrengths[i][from.Owner] += 0.5
				if s.Nodes[i].Building == Tower {
					nodeStrengths[i][from.Owner] += 0.5
				}
			}
		}
	}

	// ==================
	// # set up the nodes

	for nid := range s.Nodes {
		node := &s.Nodes[nid]

		if len(nodeStrengths[nid]) == 0 {
			continue
		}
		if len(nodeStrengths[nid]) == 1 {
			for pid, army := range nodeArmies[nid] {
				node.Owner = pid
				node.Soldiers = army
			}
			continue
		}

		p1, a1 := findlargest(nodeStrengths[nid])
		delete(nodeStrengths[nid], p1)
		_, a2 := findlargest(nodeStrengths[nid])

		soldiers := int(math.Ceil(a1 - a2))
		if soldiers != 0 {
			node.Owner = p1
			node.Soldiers = utils.Min(nodeArmies[nid][p1], soldiers)
		}
	}
}

// helper function for move method
func findlargest(armies map[int]float64) (int, float64) {
	var pMax int
	var aMax float64
	for p, army := range armies {
		if army > aMax {
			aMax = army
			pMax = p
		}
	}
	return pMax, aMax
}

func (s *State) finishBuilding(old *State, soldiers map[int]int, buildings map[int]BuildingType) {
	for nid, sol := range soldiers {
		if s.Nodes[nid].Owner == old.Nodes[nid].Owner {
			s.Nodes[nid].Soldiers += sol
		}
	}
	for nid, b := range buildings {
		s.Nodes[nid].Building = b
	}
}

func (s *State) trimSplitedTerritory(reseted datstruct.Set[int]) {
	seen := datstruct.NewSet[int]()
	for nid, node := range s.Nodes {
		if node.Building == City && !seen.Has(nid) {
			s.DfsByOwner(nid, seen)
		}
	}
	for nid := range s.Nodes {
		if !seen.Has(nid) && s.Nodes[nid].Owner != -1 {
			s.Nodes[nid].Reset()
			reseted.Add(nid)
		}
	}
}

func (s *State) recalculatePlayers() {
	for i := range s.Players {
		p := &s.Players[i]

		p.CurrSoldiers = 0
		p.MaxSoldiers = 0
		p.Territory = 0
	}
	for _, n := range s.Nodes {
		if n.Owner != -1 {
			p := &s.Players[n.Owner]
			if n.Building == Farm {
				p.Territory += 2
			}
			p.Territory += 1
			p.CurrSoldiers += n.Soldiers
		}
	}
	for i := range s.Players {
		p := &s.Players[i]
		if !p.Dead {
			p.MaxSoldiers, _ = coreutils.MaxSoldiers2(p.Territory)
		}
	}
}

// kill all players without terriroty
func (s *State) killPlayers() {
	var lastLiving *Player // this is used only if there is one last player
	toBeKilled := []int{}
	alivePlayers := 0
	for pid, p := range s.Players {
		if !p.Dead {
			if p.Territory == 0 {
				toBeKilled = append(toBeKilled, pid)
			} else {
				alivePlayers++
				lastLiving = &s.Players[pid]
			}
		}
	}

	var res coreutils.Result
	var text string

	// normal case
	if alivePlayers >= 2 {
		res = coreutils.Lost
		text = "Killed"
	}
	// one player won
	if alivePlayers == 1 {
		lastLiving.kill(coreutils.Win, "Won")
		res = coreutils.Lost
		text = "Killed"
		s.MoveType = End
	}
	// draw case
	if alivePlayers == 0 {
		res = coreutils.Draw
		text = "Drew"
		s.MoveType = End
	}

	// all others stuff are already set up
	for _, pid := range toBeKilled {
		s.Players[pid].kill(res, text)
	}
}

// =========
// # Helpers

func (s *State) NodesTooClose(n1, n2 int) bool {
	return s.GraphGenData.MinimalPathDistance[n1][n2] < s.MinimalDistanceBetweenNodes[len(s.AlivePlayers())]
}

func (old *State) copy() *State {
	new := &State{
		Matrix:                      old.Matrix,
		MinimalDistanceBetweenNodes: old.MinimalDistanceBetweenNodes,
		GraphGenData:                old.GraphGenData,

		MoveType: old.MoveType,
		Players:  make([]Player, len(old.Players)),
		Nodes:    make([]Node, len(old.Nodes)),
	}
	copy(new.Nodes, old.Nodes)
	copy(new.Players, old.Players)
	return new
}

func (s *State) DfsByOwner(curr int, seen datstruct.Set[int]) {
	if seen.Has(curr) {
		return
	}
	seen.Add(curr)
	for next, exists := range s.Matrix[curr] {
		if exists && s.Nodes[curr].Owner == s.Nodes[next].Owner {
			s.DfsByOwner(next, seen)
		}
	}
}

func (s *State) AlivePlayers() []int {
	ret := []int{}
	for pid, p := range s.Players {
		if !p.Dead {
			ret = append(ret, pid)
		}
	}
	return ret
}
