package state

import (
	"encoding/json"
	"gra/coreutils"
	"gra/datstruct"
	"gra/utils"
)

// for each nid it holds list of commands
// this is only data structure that does answer questions like howManySoldiersGoThatWay?, areYouPossible?, isThisNodeBuilding?...
// the command that is builded is holded by board structure
type Commands map[int]NodeCommand

func NewCommands() Commands {
	return Commands{}
}

type NodeCommand struct {
	// these indicate what other fields are valid
	BuildFlag   bool
	SpawnFlag   bool
	DisbandFlag bool
	MoveFlag    bool

	Building BuildingType
	Moving   map[int]int // does not allow the self moves
	Soldiers int         // the interpreation is based on the flag
}

func CheckCommands(
	s *State,
	pid int,
	commands Commands,
) bool {
	if commands == nil {
		return false
	}
	matrix := s.Matrix
	nodes := len(matrix)

	newSoldiers := 0 // player can not make more soldiers than a limit
	for nid, cmd := range commands {

		// basic checks
		if !utils.IsInInt(nid, 0, nodes-1) {
			return false
		}
		node := s.Nodes[nid]
		if node.Owner != pid {
			return false
		}

		// incorect flag combinations
		// removed because of AI
		/*if !cmd.BuildFlag && !cmd.SpawnFlag && !cmd.DisbandFlag && !cmd.MoveFlag {
			return false
		}*/
		if cmd.DisbandFlag && cmd.SpawnFlag {
			return false
		}

		// are data ok?
		if cmd.BuildFlag && !utils.IsInInt(int(cmd.Building), 0, 3) {
			return false
		}
		if (cmd.DisbandFlag || cmd.SpawnFlag) && cmd.Soldiers < 0 {
			return false
		}
		if cmd.MoveFlag && cmd.Moving == nil {
			return false
		}

		// illegal stuff
		if cmd.SpawnFlag && node.Building != City {
			return false
		}
		if cmd.BuildFlag && cmd.Building == node.Building {
			return false
		}

		// does the player has soldiers?
		soldiers := node.Soldiers
		if cmd.BuildFlag {
			soldiers--
		}
		if cmd.DisbandFlag {
			soldiers -= cmd.Soldiers
		}
		if cmd.SpawnFlag {
			newSoldiers += cmd.Soldiers
		}
		if cmd.MoveFlag {
			for nid2, s := range cmd.Moving {
				if !matrix[nid][nid2] {
					return false
				}
				if s <= 0 {
					return false
				}
				soldiers -= s
			}
		}
		if soldiers < 0 {
			return false
		}
	}
	// if true return true (-: but here it makes sense
	if newSoldiers != 0 && newSoldiers > s.Players[pid].MaxSoldiers-s.Players[pid].CurrSoldiers {
		return false
	}

	// fuck the rules
	goto end
end:

	return true
}

// this call assumes that the data are correct
func (c Commands) String() string {
	return string(utils.ToBytes(c))
}

// this call DOST NOT assumes that the data are correct
func StrToCommands(in string) (Commands, bool) {
	var cmd Commands
	err := json.Unmarshal([]byte(in), &cmd)
	return cmd, err == nil
}

func MovesToTransition(matrixSize int, moves map[int]Commands) ([][]int, datstruct.Set[int]) {
	matrix := coreutils.NewMoveMatrix(matrixSize)
	set := datstruct.NewSet[int]()
	for _, move := range moves {
		for nid, nAction := range move {
			if nAction.MoveFlag {
				for nid2, s := range nAction.Moving {
					matrix[nid][nid2] = s
				}
			}
			if nAction.BuildFlag || nAction.DisbandFlag || nAction.SpawnFlag {
				set.Add(nid)
			}
		}
	}
	return matrix, set

}

// == getting values ==

func (c Commands) GetNodeCmd(nid int) NodeCommand {
	val, ok := c[nid]
	if ok {
		return val
	}
	return NodeCommand{}
}

func (c Commands) SoldiersUsed(nid int) int {
	cmd := c.GetNodeCmd(nid)

	sum := 0
	if cmd.BuildFlag {
		sum++
	}
	for _, s := range cmd.Moving {
		sum += s
	}
	if cmd.DisbandFlag {
		sum += cmd.Soldiers
	}
	return sum
}

// global
func (c Commands) SpawningSoldiers() int {
	ret := 0
	for _, cmd := range c {
		if cmd.SpawnFlag {
			ret += cmd.Soldiers
		}
	}
	return ret
}

func (c Commands) IsBuilding(nid int) bool {
	cmd := c.GetNodeCmd(nid)
	return cmd.BuildFlag
}

// == adding commands ==

// assume correct call

func (c Commands) ResetNode(nid int) {
	delete(c, nid)
}

func (c Commands) AddMove(nid1, nid2, soldiers int) {
	cmd := c.GetNodeCmd(nid1)
	if !cmd.MoveFlag {
		cmd.MoveFlag = true
		cmd.Moving = make(map[int]int)
	}
	cmd.Moving[nid2] += soldiers
	c[nid1] = cmd
}

func (c Commands) AddSpawning(nid int, soldiers int) {
	cmd := c.GetNodeCmd(nid)
	if cmd.DisbandFlag {
		cmd.DisbandFlag = false
		cmd.Soldiers = 0
	}
	cmd.SpawnFlag = true
	cmd.Soldiers += soldiers
	c[nid] = cmd
}

func (c Commands) AddDisbanding(nid int, soldiers int) {
	cmd := c.GetNodeCmd(nid)
	if cmd.SpawnFlag {
		cmd.SpawnFlag = false
		cmd.Soldiers = 0
	}
	cmd.DisbandFlag = true
	cmd.Soldiers += soldiers
	c[nid] = cmd
}

func (c Commands) Build(nid int, building BuildingType) {
	cmd := c.GetNodeCmd(nid)
	cmd.BuildFlag = true
	cmd.Building = building
	c[nid] = cmd
}

// == helpers ==

func ConcatCommands(allPlayerMoves map[int]Commands) Commands {
	ret := NewCommands()
	for _, nidCmds := range allPlayerMoves {
		for nid, cmd := range nidCmds {
			ret[nid] = cmd
		}
	}
	return ret
}
