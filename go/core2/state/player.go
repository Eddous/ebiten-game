package state

import "gra/coreutils"

type Player struct {
	Territory    int
	CurrSoldiers int
	MaxSoldiers  int

	Dead      bool // if the player won the game, he is still Dead
	DeathInfo coreutils.DeathInfo
}

func (p *Player) kill(result coreutils.Result, deathText string) {
	p.Territory = 0
	p.CurrSoldiers = 0
	p.MaxSoldiers = 0

	p.Dead = true
	p.DeathInfo = coreutils.NewDeathInfo(result, deathText)
}
