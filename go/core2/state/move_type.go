package state

type MoveType byte

const (
	Negotation MoveType = iota
	Moves
	End
)

func (s MoveType) String() string {
	switch s {
	case Negotation:
		return "Negotation"
	case Moves:
		return "Moves"
	case End:
		return "End"
	}
	panic("state doesn't exists")
}
