package gulw

import (
	"gra/gulrot"
	"image/color"

	"github.com/hajimehoshi/ebiten/v2"
)

type HoldButton struct {
	clicked      func()
	released     func()
	isMouseAbove bool

	image *ebiten.Image
	Bopt  *gulrot.Bopt

	touch *gulrot.LTouch

	hover, clicking color.Color
}

func NewHoldButton(image *ebiten.Image, clicked, released func(), bopt *gulrot.Bopt, hover, clicking color.Color) *HoldButton {
	return &HoldButton{
		clicked:  clicked,
		released: released,

		image: image,
		Bopt:  bopt,

		hover:    hover,
		clicking: clicking,
	}
}

// ===== LeafInterface ===============

func (b *HoldButton) Layout(w int, h int) gulrot.Opt {
	return b.Bopt.Layout(w, h)
}

func (b *HoldButton) Update(in gulrot.Input) {
	b.isMouseAbove = in.IsMouseAbove()

	touches := in.Touches()
	if b.touch == nil && len(touches) != 0 {
		b.touch = &touches[0]
		b.clicked()
	}
	if b.touch != nil && b.touch.Released {
		b.released()
		b.touch = nil
	}
}

func (b *HoldButton) Draw(dst *ebiten.Image) {
	if b.touch == nil && b.isMouseAbove {
		dst.Fill(b.hover)
	}
	if b.touch != nil {
		dst.Fill(b.clicking)
	}
	gulrot.Draw4(dst, b.image)
}
