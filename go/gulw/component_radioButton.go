package gulw

import (
	"gra/gulrot"
	"gra/utils"
	"image/color"
	"math"

	"github.com/hajimehoshi/ebiten/v2"
)

// This is really horrible

// # ================================

type RadioButtonsMode byte

const (
	One RadioButtonsMode = iota
	AnyNotZero
	Any
)

// ================================ #

type RadioButton[T comparable] struct {
	coords   utils.VecF
	value    T
	image    *ebiten.Image
	selected bool
}

type RadioButtons[T comparable] struct {
	Bopt               *gulrot.Bopt
	buttons            map[T]*RadioButton[T]
	singleButtonScaler func(int, int) (int, int)
	mode               RadioButtonsMode
	colorPixel         *ebiten.Image

	buttonW, buttonH float64 // tmp variable
}

// This is really horrible.
// Size from bopt is replaced by calculation using singleButtonScaler
func NewRadioButtons[T comparable](bopt *gulrot.Bopt, singleButtonScaler func(int, int) (int, int), mode RadioButtonsMode, buttons ...*RadioButton[T]) *RadioButtons[T] {
	if len(buttons) == 0 {
		panic("must have at least one button")
	}
	m := make(map[T]*RadioButton[T])
	for _, b := range buttons {
		m[b.value] = b
	}

	return &RadioButtons[T]{
		Bopt:               bopt,
		buttons:            m,
		singleButtonScaler: singleButtonScaler,
		mode:               mode,
		colorPixel:         gulrot.ColorPixel(color.RGBA{150, 150, 150, 255}),
	}
}

func NewRadioButton[T comparable](x, y float64, value T, image *ebiten.Image, selected bool) *RadioButton[T] {
	return &RadioButton[T]{
		coords:   utils.NewVecF(x, y),
		value:    value,
		image:    image,
		selected: selected,
	}
}

func (rbs *RadioButtons[T]) Values() []T {
	ret := make([]T, 0)
	for _, rb := range rbs.buttons {
		if rb.selected {
			ret = append(ret, rb.value)
		}
	}
	switch rbs.mode {
	case One:
		if len(ret) != 1 {
			panic("mode is One have different return number")
		}
	case AnyNotZero:
		if len(ret) == 0 {
			panic("mode is AnyNotZero have different return number")
		}
	}
	return ret
}

// this works the same way as clicking
// the function depends on mode
func (rbs *RadioButtons[T]) SelectValue(val T) {
	switch rbs.mode {
	case One:
		rbs.selectZeroOrOne(val)
	case AnyNotZero:
		rbs.selectAnyNotZero(val, true)
	case Any:
		rbs.selectAnyNotZero(val, false)
	}
}

func (rbs *RadioButtons[T]) selectZeroOrOne(val T) {
	for _, rb := range rbs.buttons {
		if rb.value == val {
			rb.selected = true
		} else {
			rb.selected = false
		}
	}
}

func (rbs *RadioButtons[T]) selectAnyNotZero(val T, forbidZero bool) {
	vals := rbs.Values()
	if forbidZero && len(vals) == 1 && vals[0] == val {
		return
	}
	rbs.buttons[val].selected = !rbs.buttons[val].selected
}

// ===============
// # LeafInterface

func (rbs *RadioButtons[T]) Layout(outW, outH int) gulrot.Opt {
	w, h := rbs.singleButtonScaler(outW, outH)
	rbs.buttonW, rbs.buttonH = float64(w), float64(h)

	var x, y float64
	for _, rb := range rbs.buttons {
		x = math.Max(x, rb.coords.X)
		y = math.Max(y, rb.coords.Y)
	}
	opt := rbs.Bopt.Layout(outW, outH)
	return gulrot.Opt{
		X:         opt.X,
		Y:         opt.Y,
		Width:     int((x + 1) * rbs.buttonW),
		Height:    int((y + 1) * rbs.buttonH),
		Anchor:    opt.Anchor,
		Border:    opt.Border,
		FillColor: opt.FillColor,
		Visible:   opt.Visible,
	}
}

func (rbs *RadioButtons[T]) Update(in gulrot.Input) {

	ts := in.Touches()

	if len(ts) == 0 {
		return
	}

	x := float64(ts[0].X()) / rbs.buttonW
	y := float64(ts[0].Y()) / rbs.buttonH
	for _, rb := range rbs.buttons {
		bx, by := rb.coords.XY()
		if utils.IsIn(x, bx, bx+1, 0) && utils.IsIn(y, by, by+1, 0) {
			rbs.SelectValue(rb.value)
			return
		}
	}
}

func (rbs *RadioButtons[T]) Draw(dst *ebiten.Image) {
	for _, rb := range rbs.buttons {
		x, y := rb.coords.XY()
		x, y = x*rbs.buttonW, y*rbs.buttonH

		if rb.selected {
			dio := gulrot.Dio(rbs.colorPixel, x, y, gulrot.LT, rbs.buttonW, rbs.buttonH, 0)
			dio.Filter = ebiten.FilterNearest
			gulrot.Draw1(dst, rbs.colorPixel, dio)
		}

		gulrot.Draw2(dst, rb.image, x, y, gulrot.LT, rbs.buttonW, rbs.buttonH, 0)
	}
}
