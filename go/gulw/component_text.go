package gulw

import (
	"gra/gulrot"
	"image/color"

	"github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/text/v2"
)

type Text struct {
	Text   string
	font   *text.GoTextFaceSource
	size   float64
	anchor gulrot.Anchor
	sx, sy float64
	Color  color.Color

	Bopt *gulrot.Bopt
}

func NewText(txt string, font *text.GoTextFaceSource, size float64, anchor gulrot.Anchor, sx, sy float64, color color.Color, bopt *gulrot.Bopt) *Text {
	return &Text{
		Text:   txt,
		font:   font,
		size:   size,
		anchor: anchor,
		sx:     sx,
		sy:     sy,
		Color:  color,
		Bopt:   bopt,
	}
}

// ===== LeafInterface =====

func (t *Text) Layout(outW int, outH int) gulrot.Opt {
	return t.Bopt.Layout(outW, outH)
}

func (t *Text) Update(gulrot.Input) {}

func (t *Text) Draw(dst *ebiten.Image) {

	var x, y float64
	switch t.anchor {
	case gulrot.LT:
		x = 0
		y = 0
	case gulrot.MT:
		x = 0.5
		y = 0
	case gulrot.RT:
		x = 1
		y = 0
	case gulrot.LM:
		x = 0
		y = 0.5
	case gulrot.MM:
		x = 0.5
		y = 0.5
	case gulrot.RM:
		x = 1
		y = 0.5
	case gulrot.LB:
		x = 0
		y = 1
	case gulrot.MB:
		x = 0.5
		y = 1
	case gulrot.RB:
		x = 1
		y = 1
	}
	w, h := gulrot.ImgSize(dst)
	gulrot.Text(
		dst,
		t.Text,
		t.font,
		float64(h)*t.size,
		int(x*float64(w)),
		int(y*float64(h)),
		t.anchor,
		0,
		0,
		t.Color,
	)
}
