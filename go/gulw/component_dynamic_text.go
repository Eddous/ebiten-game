package gulw

import (
	"gra/gulrot"
	"image/color"
	"math"
	"strings"

	"github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/text/v2"
)

type DynamicText struct {
	font     *text.GoTextFaceSource
	fontSize float64
	pad      float64
	Bopt     *gulrot.Bopt

	Text string

	lastOutHeight      int
	lastWidth          int
	face               *text.GoTextFace
	computedText       string
	computedTextHeight int
	computedPad        int
	lastText           string
}

// fontSize is relative to height of a parent
// h returned from the bopt is replaced by calculated height
// pad is relative to the font size
func NewDynamicText(font *text.GoTextFaceSource, fontSize, pad float64, bopt *gulrot.Bopt) *DynamicText {
	return &DynamicText{
		font:     font,
		fontSize: fontSize,
		pad:      pad,
		Bopt:     bopt,
	}
}

func (t *DynamicText) recalculateText(lineWidth int) {
	em, _ := text.Measure("MMMMMMMMMM", t.face, 0)
	em = em / 10

	t.computedText = ""
	chars := int(float64(lineWidth) / em)
	for _, line := range strings.Split(t.Text, "\n") {
		t.computedText += splitLineToFit(chars, line) + "\n"
	}
	t.computedText = t.computedText[:len(t.computedText)-1]

	_, h := text.Measure(t.computedText, t.face, gulrot.GetFontSpacing(t.face))
	t.computedTextHeight = int(math.Round(h))
}

func splitLineToFit(chars int, lineStr string) string {
	if chars < 2 {
		return "X"
	}
	if len(lineStr) == 0 {
		return ""
	}
	ret := make([]rune, 0)
	counter := 0
	i := 0
	line := []rune(lineStr)

	for i < len(line) {
		if counter == chars-1 {
			if line[i] == ' ' {
				ret = append(ret, '\n')
				i++
			} else if line[i-1] == ' ' {
				ret = append(ret, '\n')
			} else {
				ret = append(ret, '-', '\n')
			}
			counter = 0
		} else {
			ret = append(ret, line[i])
			counter++
			i++
		}
	}
	if ret[len(ret)-1] == '\n' {
		ret = ret[:len(ret)-1]
	}
	return string(ret)
}

// ===== LeafInterface =====

func (t *DynamicText) Layout(outW int, outH int) gulrot.Opt {
	flag := false

	// recalculating face
	if outH != t.lastOutHeight {
		newFontSize := float64(outH) * t.fontSize
		t.face = &text.GoTextFace{
			Source: t.font,
			Size:   newFontSize,
		}
		t.computedPad = int(newFontSize * t.pad)

		t.lastOutHeight = outH
		flag = true
	}
	opt := t.Bopt.Layout(outW, outH)

	if opt.Width != t.lastWidth {
		t.lastWidth = opt.Width
		flag = true
	}

	if t.lastText != t.Text {
		flag = true
	}

	if flag {
		t.recalculateText(t.lastWidth - 2*t.computedPad)
	}
	opt.Height = t.computedTextHeight + 2*t.computedPad
	return opt
}

func (t *DynamicText) Update(in gulrot.Input) {}

func (t *DynamicText) Draw(dst *ebiten.Image) {
	gulrot.Text(dst, t.computedText, t.face.Source, t.face.Size, t.computedPad, t.computedPad, gulrot.LT, 0, 0, color.Black)
}
