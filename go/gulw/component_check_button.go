package gulw

import (
	"gra/gulrot"
	"image/color"

	"github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/text/v2"
)

type CheckButton struct {
	Value   bool
	Focused bool // read out value
	Bopt    *gulrot.Bopt

	isMouseAbove bool

	touch *gulrot.LTouch

	t byte // 0-image, 1-text

	image *ebiten.Image

	Text string
	font *text.GoTextFaceSource
	size float64
}

func NewCheckImageButton(image *ebiten.Image, bopt *gulrot.Bopt) *CheckButton {
	return &CheckButton{
		Bopt:  bopt,
		t:     0,
		image: image,
	}
}

func NewCheckTextButton(text string, font *text.GoTextFaceSource, size float64, bopt *gulrot.Bopt) *CheckButton {
	return &CheckButton{
		Bopt: bopt,
		t:    0,
		Text: text,
		font: font,
		size: size,
	}
}

// ===== LeafInterface ===============

func (b *CheckButton) Layout(w int, h int) gulrot.Opt {
	return b.Bopt.Layout(w, h)
}

func (b *CheckButton) Update(in gulrot.Input) {
	b.isMouseAbove = in.IsMouseAbove()
	b.Focused = in.IsFocused()

	touches := in.Touches()
	if b.touch == nil && len(touches) != 0 {
		b.touch = &touches[0]
	}
	if b.touch != nil && b.touch.Released {
		if b.touch.InMe() {
			b.Value = !b.Value
		}
		b.touch = nil
	}
}

func (b *CheckButton) Draw(dst *ebiten.Image) {
	if b.touch != nil || b.Value {
		dst.Fill(Grey2)
	}
	if b.touch == nil && b.isMouseAbove {
		dst.Fill(Grey1)
	}
	switch b.t {
	case 0:
		gulrot.Draw4(dst, b.image)
	case 1:
		w, h := gulrot.ImgSize(dst)
		gulrot.Text(
			dst,
			b.Text,
			b.font,
			float64(h)*b.size,
			w/2,
			h/2,
			gulrot.MM,
			0,
			0,
			color.Black,
		)
	}
}
