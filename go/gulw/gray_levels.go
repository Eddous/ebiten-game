package gulw

import "image/color"

var Grey1 = color.Gray{Y: 220}
var Grey2 = color.Gray{Y: 180}
var Grey3 = color.Gray{Y: 100}
