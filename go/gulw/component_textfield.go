package gulw

import (
	"gra/gulrot"
	"image/color"
	"strings"

	"github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/inpututil"
	"github.com/hajimehoshi/ebiten/v2/text/v2"
)

/*
This code is for special cases
*/

type TextFieldGroup struct {
	fields []*TextField
	active int

	gulCon      gulrot.GulrotController
	enterButton *Button
}

func NewTextFieldGroup(gulCon gulrot.GulrotController, enterButton *Button) *TextFieldGroup {
	return &TextFieldGroup{
		active:      -1,
		gulCon:      gulCon,
		enterButton: enterButton,
	}
}

// constructor is here because TextField without TextFieldGroup does not make any sense
func (g *TextFieldGroup) AddField(txt string, font *text.GoTextFaceSource, size float64, defaultText string, password bool, bopt *gulrot.Bopt) *TextField {
	f := &TextField{
		Text:        txt,
		font:        font,
		size:        size,
		defaultText: defaultText,
		password:    password,
		bopt:        bopt,
		group:       g,
		index:       len(g.fields), // index within the text group fields
	}
	g.fields = append(g.fields, f)
	return f
}

func (g *TextFieldGroup) Update() {
	if inpututil.IsKeyJustPressed(ebiten.KeyEnter) {
		if g.enterButton.Activated() {
			g.enterButton.clicked()
		}
		return
	}

	if g.active == -1 {
		return
	}
	active := g.fields[g.active]

	for _, r := range ebiten.AppendInputChars(nil) {
		if 32 <= r && r <= 126 {
			active.Text += string(r)
		}
	}
	if inpututil.IsKeyJustPressed(ebiten.KeyTab) {
		g.gulCon.SetFocused(g.fields[(g.active+1)%len(g.fields)])
		return
	}
	if inpututil.IsKeyJustPressed(ebiten.KeyBackspace) {
		if len(active.Text) != 0 {
			active.Text = active.Text[:len(active.Text)-1]
		}
	}

	// this horrible horribleness is because on mobile I cannot directly use keyboard.
	msg, typ := g.gulCon.IncomingMsg()
	switch typ {
	case 1:
		new := ""
		for _, r := range msg {
			if 32 <= r && r <= 126 {
				new += string(r)
			}
		}
		active.Text = new
		g.gulCon.SetFocused(nil)
	case 2:
		g.gulCon.SetFocused(nil)
	}
}

// ===== TextField =====

type TextField struct {
	Text string

	font        *text.GoTextFaceSource
	size        float64
	defaultText string
	password    bool

	bopt *gulrot.Bopt

	group        *TextFieldGroup
	index        int
	isMouseAbove bool
}

// ===== LeafInterface =====

func (t *TextField) Layout(outW int, outH int) gulrot.Opt {
	return t.bopt.Layout(outW, outH)
}

func (t *TextField) Update(in gulrot.Input) {
	t.isMouseAbove = in.IsMouseAbove()
	if t.group.active != t.index && in.IsFocused() {
		t.group.gulCon.OpenKeyboard(t.Text, t.password)
		t.group.active = t.index
		return
	}
	if t.group.active == t.index && !in.IsFocused() {
		t.group.active = -1
	}
}

func (t *TextField) Draw(dst *ebiten.Image) {
	txt := t.Text
	if t.password {
		txt = strings.Repeat("*", len(txt))
	}
	if t.group.active == t.index {
		dst.Fill(Grey2)
		txt += "|"
	}
	if t.group.active != t.index && t.isMouseAbove {
		dst.Fill(Grey1)
	}

	_, h := gulrot.ImgSize(dst)
	fontSize := float64(h) * t.size
	if txt == "" {
		gulrot.Text(dst, t.defaultText, t.font, fontSize, 0, h/2, gulrot.LM, 0, 0, Grey2)
	} else {
		gulrot.Text(dst, txt, t.font, fontSize, 0, h/2, gulrot.LM, 0, 0, color.Black)
	}
}
