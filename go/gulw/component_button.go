package gulw

import (
	"gra/gulrot"
	"image/color"

	"github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/text/v2"
)

type Button struct {
	Bopt *gulrot.Bopt

	clicked      func()
	activated    bool
	isMouseAbove bool

	t     byte // 0-image, 1-text
	image *ebiten.Image
	Text  string

	font *text.GoTextFaceSource
	size float64

	touch *gulrot.LTouch
}

func NewImageButton(image *ebiten.Image, clicked func(), bopt *gulrot.Bopt) *Button {
	return &Button{
		Bopt: bopt,

		clicked:   clicked,
		activated: true,

		t:     0,
		image: image,
	}
}

func NewTextButton(text string, font *text.GoTextFaceSource, size float64, clicked func(), bopt *gulrot.Bopt) *Button {
	return &Button{
		Bopt:      bopt,
		clicked:   clicked,
		activated: true,

		t:    1,
		Text: text,
		font: font,
		size: size,
	}
}

func (rb *Button) SetActived(val bool) {
	rb.activated = val
	if !val {
		rb.touch = nil
	}
}

func (rb *Button) Activated() bool {
	return rb.activated
}

// ===== LeafInterface =====

func (b *Button) Layout(w int, h int) gulrot.Opt {
	return b.Bopt.Layout(w, h)
}

func (b *Button) Update(in gulrot.Input) {
	if !b.activated {
		return
	}

	b.isMouseAbove = in.IsMouseAbove()

	touches := in.Touches()
	if b.touch == nil && len(touches) != 0 {
		b.touch = &touches[0]
	}
	if b.touch != nil && b.touch.Released {
		if b.touch.InMe() {
			b.clicked()
		}
		b.touch = nil
	}
}

func (b *Button) Draw(dst *ebiten.Image) {
	if !b.activated {
		dst.Fill(Grey3)
	}
	if b.touch != nil {
		dst.Fill(Grey2)
	}
	if b.touch == nil && b.activated && b.isMouseAbove {
		dst.Fill(Grey1)
	}
	switch b.t {
	case 0:
		gulrot.Draw4(dst, b.image)
	case 1:
		w, h := gulrot.ImgSize(dst)
		gulrot.Text(
			dst,
			b.Text,
			b.font,
			float64(h)*b.size,
			w/2,
			h/2,
			gulrot.MM,
			0,
			0,
			color.Black,
		)
	}
}
