package gulw

import (
	"gra/gulrot"
	"gra/utils"

	"github.com/hajimehoshi/ebiten/v2"
)

type StateSwitch[T comparable] struct {
	Bopt   *gulrot.Bopt
	vals   []T
	images []*ebiten.Image

	i int
}

func NewStateSwitch[T comparable](bopt *gulrot.Bopt, vals []T, images []*ebiten.Image, i int) *StateSwitch[T] {
	if len(images) != len(vals) || len(images) == 0 || !utils.IsInInt(i, 0, len(images)-1) {
		panic("incorrect call")
	}
	return &StateSwitch[T]{
		Bopt:   bopt,
		vals:   vals,
		images: images,
		i:      i,
	}
}

func (s *StateSwitch[T]) SetVal(value T) {
	for i, v := range s.vals {
		if v == value {
			s.i = i
		}
	}
}

func (s *StateSwitch[T]) Val() T {
	return s.vals[s.i]
}

// ===== LeafInterface =====

func (s *StateSwitch[T]) Layout(ow int, oh int) gulrot.Opt {
	return s.Bopt.Layout(ow, oh)
}

func (s *StateSwitch[T]) Update(in gulrot.Input) {
	ts := in.Touches()
	if len(ts) == 0 {
		return
	}
	s.i++
	if s.i == len(s.images) {
		s.i = 0
	}
}

func (s *StateSwitch[T]) Draw(dst *ebiten.Image) {
	gulrot.Draw4(dst, s.images[s.i])
}
