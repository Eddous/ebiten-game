package gui

import (
	"gra/corewrapper"
	"gra/game"
	"gra/gulrot"
	"gra/utils"
	"math/rand"
	"time"

	"github.com/barweiss/go-tuple"
)

const aiSpeed int64 = 1000

func (c *Controller) loadOffGameScn(gameMode corewrapper.GameMode, nodes int, playerTypes []int) {
	names := []string{"Turing", "Neumann", "Dijkstra", "GOTO", "BigONotation", "Algorithms", "Ritchie", "C", "Capablanca", "MagnusCarlsen", "AdamOndra", "Chess", "Dumbledore", "IfStatement", "FordPrefect", "Gandalf", "Smaug"}
	rand.Shuffle(len(names), func(i, j int) { names[i], names[j] = names[j], names[i] })

	eloes := make([]float64, len(playerTypes))
	for i := range eloes {
		eloes[i] = utils.RandomFloat2(-100, 100)
	}

	wrapper := corewrapper.NewOffline(gameMode, c.nextGameSeedAndUpdate(), nodes, playerTypes)

	// finding first human player
	var player *corewrapper.Player
	selfPlayPlayers := []*corewrapper.Player{}
	for i, t := range playerTypes {
		if t == 0 {
			selfPlayPlayers = append(selfPlayPlayers, wrapper.AllPlayers[i])
			if player == nil {
				player = wrapper.AllPlayers[i]
			}
		}
	}
	// making the selfPlayPlayers nil to disable self-play mode
	if len(selfPlayPlayers) <= 1 {
		selfPlayPlayers = nil
	}

	scn := &offGameScn{
		w:               wrapper,
		selfPlayPlayers: selfPlayPlayers,
		aiChan:          wrapper.TGame.StartAI(),
		aiSpeed:         int64(aiSpeed),
		lastUpdateTime:  time.Now().UnixMilli(),
	}

	var root *gulrot.Root
	scn.gameScn, root = game.NewGame(
		c.gulrotController,
		wrapper,
		player,
		func(p *corewrapper.Player, pf corewrapper.PlayerFeelings) {
			p.SetFeelings(pf)
		},
		func(p *corewrapper.Player, buffer string) {
			p.SetBuffer(buffer)
			if scn.selfPlayPlayers != nil {
				scn.gameScn.SetYou(scn.findNextSelfPlayPlayer())
			}
		},
		c.loadIndexScn,
		true,
		true,
	)

	c.loadNextScene(nil, scn, root)
}

type offGameScn struct {
	w       *corewrapper.Wrapper
	gameScn *game.Game

	// if this is not nil then the "self play" mode is active
	// these are pids of players which does play on the same machine
	selfPlayPlayers []*corewrapper.Player
	currIndex       int // for selfPlayPlayers

	timeStepCounter int

	aiChan <-chan tuple.T2[int, string]

	aiSpeed        int64
	lastUpdateTime int64
}

func (s *offGameScn) update(c *Controller) {
	if s.w.End() {
		s.gameScn.GuiUpdate()
		return
	}
	s.timeStepCounter++
	if s.timeStepCounter == 60 {
		s.w.TimeStep()
		s.timeStepCounter = 0
	}

	// disable fast move when the player is still playing but not this turn
	time := time.Now().UnixMilli()
	timeOk := time-s.lastUpdateTime > s.aiSpeed

	// reading AI (but not too fast)
	if s.aiChan != nil && timeOk {
		select {
		case m, ok := <-s.aiChan:
			if !ok {
				s.aiChan = nil
				break
			}
			if s.w.AllPlayers[m.V1].SetBuffer(m.V2) == nil {
				panic("wrong AI buffer: " + m.V2)
			}
		default:
			// empty
		}
	}

	// s.aiChan is nil when all ai players already have played (every turn new aiChan is created, and new AI goroutines run)
	// POTENTIAL BUG for non-inf time management
	if s.aiChan == nil {
		if _, next := s.w.TryStep(); next {
			s.lastUpdateTime = time
			s.aiChan = s.w.TGame.StartAI()
			s.gameScn.NewStateDetected()
			if s.selfPlayPlayers != nil {
				s.currIndex = 0
				s.gameScn.SetYou(s.findNextSelfPlayPlayer())
			}

		}
	}
	s.gameScn.GuiUpdate()
}

func (s *offGameScn) findNextSelfPlayPlayer() *corewrapper.Player {
	for ; s.currIndex < len(s.selfPlayPlayers); s.currIndex++ {
		player := s.selfPlayPlayers[s.currIndex]
		if !player.Ready() {
			return player
		}
	}
	return nil
}
