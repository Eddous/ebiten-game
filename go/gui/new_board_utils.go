package gui

import (
	"gra/gulrot"
	"gra/gulw"
	"gra/resources"
	"gra/utils"
	"image/color"
)

// this is also used for button computations
var layoutScaler = gulrot.NiceScaler(0.75, 0.1, 0.2)

// function which creates selector
func (c *Controller) newSelector(layoutOffset float64, text string, values []string, i int) (*gulrot.Layout, func() string) {
	if len(values) == 0 || !utils.IsInInt(i, 0, len(values)-1) {
		panic("wrongly initialized")
	}

	const fontSize = 0.3
	const buttonFontSize = 0.7
	const buttonSize = 0.7
	const buttonBorder = 1
	const layoutBorder = 0

	valueField := gulw.NewText(
		text+values[i],
		resources.GuiFont,
		fontSize,
		gulrot.LM,
		0,
		0,
		color.Black,
		gulrot.NewBopt(
			func(ow, oh int) (int, int, int, int) {
				return 0, 0, ow, oh
			},
			gulrot.LT,
			0,
			color.White,
			true,
		),
	)

	var rightB *gulw.Button
	var leftB *gulw.Button

	rightB = gulw.NewTextButton(
		">",
		resources.GuiFont,
		buttonFontSize,
		func() {
			i++
			valueField.Text = text + values[i]
			leftB.Bopt.Visible = true
			if i == len(values)-1 {
				rightB.Bopt.Visible = false
			}
		},
		gulrot.NewBopt(
			func(ow, oh int) (int, int, int, int) {
				bs := int(buttonSize * float64(oh))
				return ow - 1, oh / 2, bs, bs
			},
			gulrot.RM,
			buttonBorder,
			color.White,
			true,
		),
	)

	leftB = gulw.NewTextButton(
		"<",
		resources.GuiFont,
		buttonFontSize,
		func() {
			i--
			valueField.Text = text + values[i]
			rightB.Bopt.Visible = true
			if i == 0 {
				leftB.Bopt.Visible = false
			}
		},
		gulrot.NewBopt(
			func(ow, oh int) (int, int, int, int) {
				bs := int(buttonSize * float64(oh))
				return ow - 1 - bs + buttonBorder, oh / 2, bs, bs
			},
			gulrot.RM,
			buttonBorder,
			color.White,
			true,
		),
	)

	leftB.Bopt.Visible = i >= 1
	rightB.Bopt.Visible = i < len(values)-1

	lay := gulrot.NewLayout(
		gulrot.NewBopt(
			func(ow, oh int) (int, int, int, int) {
				w, h := layoutScaler(ow, oh)
				y := oh/2 + int(layoutOffset*float64(h))
				return ow / 2, y, w, h
			},
			gulrot.MM,
			layoutBorder,
			color.White,
			true,
		),
	)
	lay.AddLeaf(valueField)
	lay.AddLeaf(leftB)
	lay.AddLeaf(rightB)

	return lay, func() string { return values[i] }
}

func (c *Controller) newBoardNextButton(layoutOffset float64, click func()) *gulw.Button {
	const fontSize = 0.6
	const widthFactor = 0.8

	return gulw.NewTextButton(
		"Start",
		resources.GuiFont,
		fontSize,
		click,
		gulrot.NewBopt(
			func(ow, oh int) (int, int, int, int) {
				w, h := layoutScaler(ow, oh)
				y := oh/2 + int(layoutOffset*float64(h))
				return ow / 2, y, int(widthFactor * float64(w)), h
			},
			gulrot.MM,
			3,
			color.White,
			true,
		),
	)
}
