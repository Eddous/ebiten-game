package gui

import (
	"gra/gulw"
	"gra/server"
)

func (c *Controller) loadChangeEmailScn() {
	var emailField *gulw.TextField

	g := c.loadGeneralFormScn(
		func(c *Controller, msg server.ServerToClient, setErrorText func(string, bool)) {
			if msg.T != server.GeneralResT {
				panic("not supported msg")
			}
			setErrorText("Email is changed", true)
		},
		c.loadUserScn,
		[]textFieldSettings{
			{
				defaultString: "new email",
				password:      false,
			},
		},
		[]buttonSettings{
			newButtonSettings(
				"Send",
				func() {
					if c.client.Mode() != server.Pending {
						return
					}
					c.client.Send(
						server.ClientToServer{
							T:           server.ChangeEmailT,
							ChangeEmail: emailField.Text,
						},
					)
				},
				[]int{},
			),
		},
		0,
	)
	emailField = g.fields[0]
}
