package gui

import (
	"gra/gulw"
	"gra/server"
	"strconv"
)

func (c *Controller) loadJoinPrivateLobbyScn() {
	var codeField *gulw.TextField
	var errField *gulw.Text

	g := c.loadGeneralFormScn(
		func(c *Controller, msg server.ServerToClient, setErrorText func(string, bool)) {
			switch msg.T {
			case server.LobbyErrT:
				setErrorText("Wrong code", false)
			case server.LobbyMsgT:
				c.loadLobbyScn(msg.LobbyMsg)
			default:
				panic("wrong type of msg")
			}
		},
		c.loadOnlineHomeScn,
		[]textFieldSettings{
			{
				defaultString: "code",
				password:      false,
			},
		},
		[]buttonSettings{
			newButtonSettings(
				"Send",
				func() {
					if num, err := strconv.Atoi(codeField.Text); err == nil {
						if c.client.Mode() != server.Pending {
							return
						}
						c.client.Send(
							server.ClientToServer{
								T:                   server.JoinPrivateLobbyReqT,
								JoinPrivateLobbyReq: server.LobbyId(num),
							},
						)
					} else {
						errField.Text = "Use only numbers."
					}
				},
				[]int{0},
			),
		},
		0,
	)
	codeField = g.fields[0]
	errField = g.errorField
}
