package gui

import (
	"gra/corewrapper"
	"gra/game"
	"gra/gulrot"
	"gra/gulw"
	"gra/resources"
	"gra/utils"
	"image/color"
)

// this is not ideal, but it will be completely reworked

func (c *Controller) loadSavedGamesScn() {
	root := gulrot.NewRoot(color.White)
	root.AddLeaf(c.makeBackButton(c.loadIndexScn))

	enterButton := &gulw.Button{}
	group := gulw.NewTextFieldGroup(c.gulrotController, enterButton)

	textFieldScaler := gulrot.NiceScaler(0.8, 0.1, 0.2)
	textField := group.AddField(
		"",
		resources.GuiFont,
		0.5,
		"file name",
		false,
		gulrot.NewBopt(
			func(ow, oh int) (int, int, int, int) {
				w, h := textFieldScaler(ow, oh)
				return ow / 2, oh/2 - 2*h/3, w, h
			},
			gulrot.MM,
			3,
			color.White,
			true,
		),
	)

	root.AddLeaf(textField)

	*enterButton = *gulw.NewTextButton(
		"Load",
		resources.GuiFont,
		0.5,
		func() {
			data := utils.ReadBytesFromFile(c.savedGamesPath + "/" + textField.Text)
			wrapper := corewrapper.Deserialize(data) // TODO: can possibly fail
			if wrapper == nil {
				panic("wrapper is nil")
			}
			c.loadSavedGameScn(wrapper)
		},
		gulrot.NewBopt(
			func(ow, oh int) (int, int, int, int) {
				w, h := textFieldScaler(ow, oh)
				return ow / 2, oh/2 + 2*h/3, w, h
			},
			gulrot.MM,
			3,
			color.White,
			true,
		),
	)
	root.AddLeaf(enterButton)
	c.loadNextScene(nil, &savedGamesScn{group}, root)
}

type savedGamesScn struct {
	text *gulw.TextFieldGroup
}

func (scn savedGamesScn) update(c *Controller) {
	scn.text.Update()
}

type savedGameScn struct {
	*game.Game
}

func (c *Controller) loadSavedGameScn(wrapper *corewrapper.Wrapper) {
	gameScn, root := game.NewGame(
		c.gulrotController,
		wrapper,
		nil,
		nil,
		nil,
		c.loadSavedGamesScn,
		false,
		false,
	)
	c.loadNextScene(nil, savedGameScn{gameScn}, root)
}

func (scn savedGameScn) update(c *Controller) {
	scn.Game.GuiUpdate()
}
