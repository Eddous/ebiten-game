package gui

import (
	"gra/corewrapper"
	"gra/gulrot"
	"gra/server"
	"gra/utils"
	"image/color"
	"strconv"
)

func (c *Controller) loadNewMultiScn() {

	gameModeSelector, getGameMode := c.newSelector(-3.5, "Mode    : ", []string{classicalString, citiesString}, 0)
	privPub, gPrivPub := c.newSelector(-2.5, "Game    : ", []string{"Private"}, 0)
	size, gSize := c.newSelector(-1.5, "Size    : ", []string{"S", "M", "L", "XL"}, 0)
	players, gPlayers := c.newSelector(-0.5, "Players : ", []string{"2", "3", "4", "5", "6"}, 0)
	timeMode, gTimeMode := c.newSelector(0.5, "Timer   : ", modesKeys, 0)

	// on the same y, because only one is visible
	time0, gTime0 := c.newSelector(1.5, "Time      : ", t0Keys, 0)
	time1, gTime1 := c.newSelector(1.5, "Time      : ", t1Keys, 0)
	time2, gTime2 := c.newSelector(1.5, "Time      : ", t2Keys, 0)

	multiScn := &newMultiScn{
		timeFields: []*gulrot.Layout{
			time0,
			time1,
			time2,
		},
		gTimeMode: gTimeMode,
	}

	root := gulrot.NewRoot(color.White)
	root.AddLeaf(
		c.makeBackButton(
			func() {
				if c.client.Mode() == server.Pending {
					c.loadOnlineHomeScn()
				}
			},
		),
	)
	root.AddLayout(gameModeSelector)
	root.AddLayout(privPub)
	root.AddLayout(size)
	root.AddLayout(players)
	root.AddLayout(timeMode)

	root.AddLayout(time0)
	root.AddLayout(time1)
	root.AddLayout(time2)

	root.AddLeaf(c.newBoardNextButton(3.5, func() {
		var mode int
		switch gSize() {
		case "S":
			mode = 0
		case "M":
			mode = 1
		case "L":
			mode = 2
		case "XL":
			mode = 3
		default:
			panic("not an option")
		}
		p, err := strconv.Atoi(gPlayers())
		if err != nil {
			panic("find me")
		}

		// getting game Mode
		var gameMode corewrapper.GameMode
		switch getGameMode() {
		case classicalString:
			gameMode = corewrapper.Classical
		case citiesString:
			gameMode = corewrapper.Cities
		default:
			panic("not an option")
		}

		mti := parseTime(gTimeMode(), gTime0(), gTime1(), gTime2())

		bytes := utils.ToBytes(
			corewrapper.WrapperArgs{
				GameMode:  gameMode,
				MapMode:   mode,
				Seed:      c.nextGameSeedAndUpdate(),
				Players:   p,
				TimeMode:  mti.m,
				Time:      mti.t,
				Increment: mti.i,
			},
		)
		if gPrivPub() == "Private" {
			if c.client.Mode() == server.Pending {
				c.client.Send(
					server.ClientToServer{
						T:                     server.CreatePrivateLobbyReqT,
						CreatePrivateLobbyReq: bytes,
					},
				)
			}
		} else {
			panic("is not implemented")
			/*c.client.Send(
				server.ClientToServer{
					T:              server.PublicLobbyReqT,
					PublicLobbyReq: bytes,
				},
			)*/
		}
	}))

	c.loadNextScene(multiScn, multiScn, root)
}

type newMultiScn struct {
	timeFields []*gulrot.Layout
	gTimeMode  func() string
}

func (s *newMultiScn) msgUpdate(c *Controller, msg server.ServerToClient) {
	switch msg.T {
	case server.LobbyErrT:
		c.loadOnlineHomeScn()
	case server.LobbyMsgT:
		c.loadLobbyScn(msg.LobbyMsg)
	default:
		panic("not expected msg")
	}
}

func (s *newMultiScn) update(c *Controller) {
	keep := modes[s.gTimeMode()]
	for i, c := range s.timeFields {
		tmp := c.Layouter.(*gulrot.Bopt)
		tmp.Visible = i == keep
		c.Layouter = tmp
	}
}

func parseTime(timeModeStr string, time0, time1, time2 string) mti {
	switch modes[timeModeStr] {
	case 0:
		return t0[time0]
	case 1:
		return t1[time1]
	case 2:
		return t2[time2]
	case 3:
		return mti{corewrapper.Inf, 0, 0}
	}
	panic("not an option")
}

var modesKeys []string = []string{"Normal", "Increment", "Per-Round", "Inf"}
var modes map[string]int = map[string]int{
	"Normal":    0,
	"Increment": 1,
	"Per-Round": 2,
	"Inf":       3,
}

var t0Keys []string = []string{"5", "10", "15", "30", "60"}
var t1Keys []string = []string{"5+3", "10+5", "15+10", "30+15", "60+30"}
var t2Keys []string = []string{"0:15", "0:30", "1", "2", "3", "5", "10"}

var t0 map[string]mti = map[string]mti{
	"5":  {corewrapper.Normal, 5 * 60, 0},
	"10": {corewrapper.Normal, 10 * 60, 0},
	"15": {corewrapper.Normal, 15 * 60, 0},
	"30": {corewrapper.Normal, 30 * 60, 0},
	"60": {corewrapper.Normal, 60 * 60, 0},
}
var t1 map[string]mti = map[string]mti{
	"5+3":   {corewrapper.Normal, 5 * 60, 3},
	"10+5":  {corewrapper.Normal, 10 * 60, 5},
	"15+10": {corewrapper.Normal, 15 * 60, 10},
	"30+15": {corewrapper.Normal, 30 * 60, 15},
	"60+30": {corewrapper.Normal, 60 * 60, 30},
}
var t2 map[string]mti = map[string]mti{
	"0:15": {corewrapper.PerRound, 15, 0},
	"0:30": {corewrapper.PerRound, 30, 0},
	"1":    {corewrapper.PerRound, 60, 0},
	"2":    {corewrapper.PerRound, 2 * 60, 0},
	"3":    {corewrapper.PerRound, 3 * 60, 0},
	"5":    {corewrapper.PerRound, 5 * 60, 0},
	"10":   {corewrapper.PerRound, 10 * 60, 0},
}

type mti struct {
	m corewrapper.TimeMode
	t int
	i int
}
