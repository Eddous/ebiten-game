package gui

import (
	"gra/corewrapper"
	"gra/gulrot"
	"gra/server"
	"gra/utils"
	"image/color"
	"strconv"
)

var stringToTimeMode map[string]corewrapper.TimeMode = map[string]corewrapper.TimeMode{
	"Per-Round": corewrapper.PerRound,
	"Inf":       corewrapper.Inf,
}

var stringToTime map[string]int = map[string]int{
	"0:15":  15,
	"0:30":  30,
	"1:00":  60,
	"2:00":  2 * 60,
	"3:00":  3 * 60,
	"5:00":  5 * 60,
	"10:00": 10 * 60,
}

func (c *Controller) loadNewMultiScn() {

	gameModeSelector, getGameMode := c.newSelector(-3.5, "Mode    : ", []string{classicalString, citiesString}, 0)
	privPub, gPrivPub := c.newSelector(-2.5, "Game    : ", []string{"Private"}, 0)
	size, gSize := c.newSelector(-1.5, "Size    : ", []string{"20", "30", "45", "60", "80", "100"}, 0)
	players, gPlayers := c.newSelector(-0.5, "Players : ", []string{"2", "3", "4", "5", "6"}, 0)
	timeMode, gTimeMode := c.newSelector(0.5, "Timer   : ", []string{"Per-Round", "Inf"}, 0)
	time, gTime := c.newSelector(1.5, "Time    : ", []string{"0:15", "0:30", "1:00", "2:00", "3:00", "5:00", "10:00"}, 0)

	multiScn := &newMultiScn{
		timeField: time,
		gTimeMode: gTimeMode,
	}

	root := gulrot.NewRoot(color.White)
	root.AddLeaf(
		c.makeBackButton(
			func() {
				if c.client.Mode() == server.Pending {
					c.loadOnlineHomeScn()
				}
			},
		),
	)
	root.AddLayout(gameModeSelector)
	root.AddLayout(privPub)
	root.AddLayout(size)
	root.AddLayout(players)
	root.AddLayout(timeMode)

	root.AddLayout(time)

	root.AddLeaf(c.newBoardNextButton(3.5, func() {
		nodes, err := strconv.Atoi(gSize())
		if err != nil {
			panic(err)
		}
		p, err := strconv.Atoi(gPlayers())
		if err != nil {
			panic(err)
		}

		// getting game Mode
		var gameMode corewrapper.GameMode
		switch getGameMode() {
		case classicalString:
			gameMode = corewrapper.Classical
		case citiesString:
			gameMode = corewrapper.Cities
		default:
			panic("not an option")
		}

		// getting time
		timeMode := stringToTimeMode[gTimeMode()]
		time := 0
		if timeMode == corewrapper.PerRound {
			time = stringToTime[gTime()]
		}

		bytes := utils.ToBytes(
			corewrapper.WrapperArgs{
				GameMode: gameMode,
				MapNodes: nodes,
				Seed:     c.nextGameSeedAndUpdate(),
				Players:  p,
				TimeMode: timeMode,
				Time:     time,
			},
		)
		if gPrivPub() == "Private" {
			if c.client.Mode() == server.Pending {
				c.client.Send(
					server.ClientToServer{
						T:                     server.CreatePrivateLobbyReqT,
						CreatePrivateLobbyReq: bytes,
					},
				)
			}
		} else {
			panic("is not implemented")
			/*c.client.Send(
				server.ClientToServer{
					T:              server.PublicLobbyReqT,
					PublicLobbyReq: bytes,
				},
			)*/
		}
	}))

	c.loadNextScene(multiScn, multiScn, root)
}

type newMultiScn struct {
	timeField *gulrot.Layout
	gTimeMode func() string
}

func (s *newMultiScn) msgUpdate(c *Controller, msg server.ServerToClient) {
	switch msg.T {
	case server.LobbyErrT:
		c.loadOnlineHomeScn()
	case server.LobbyMsgT:
		c.loadLobbyScn(msg.LobbyMsg)
	default:
		panic("not expected msg")
	}
}

func (s *newMultiScn) update(c *Controller) {
	s.timeField.Bopt.Visible = stringToTimeMode[s.gTimeMode()] == corewrapper.PerRound
}
