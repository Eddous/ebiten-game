package gui

import (
	"gra/gulrot"
	"gra/gulw"
	"gra/resources"
	"image/color"
)

func (c *Controller) getInitScn() *gulrot.Root {

	scaler := gulrot.NiceScaler(0.8, 0.1, 0.2)
	heightPad := 1.5
	fonterSize := 0.5
	border := 3

	root := gulrot.NewRoot(color.White)

	root.AddLeaf(
		gulw.NewTextButton(
			"Offline",
			resources.GuiFont,
			fonterSize,
			func() {
				c.loadNewSingleScn()
			},
			gulrot.NewBopt(
				func(ow, oh int) (int, int, int, int) {
					w, h := scaler(ow, oh)
					y := oh/2 - int(2*float64(h)*heightPad)
					return ow / 2, y, w, h
				},
				gulrot.MM,
				border,
				color.White,
				true,
			),
		),
	)

	online := gulw.NewTextButton(
		"Online",
		resources.GuiFont,
		fonterSize,
		func() {
			c.loadConnectingScn()
		},
		gulrot.NewBopt(
			func(ow, oh int) (int, int, int, int) {
				w, h := scaler(ow, oh)
				y := oh/2 - int(float64(h)*heightPad)
				return ow / 2, y, w, h
			},
			gulrot.MM,
			border,
			color.White,
			true,
		),
	)
	online.SetActived(c.serverAddress != "")
	root.AddLeaf(online)

	savedGames := gulw.NewTextButton(
		"Saved Games",
		resources.GuiFont,
		fonterSize,
		func() {
			c.loadSavedGamesScn()
		},
		gulrot.NewBopt(
			func(ow, oh int) (int, int, int, int) {
				w, h := scaler(ow, oh)
				y := oh / 2
				return ow / 2, y, w, h
			},
			gulrot.MM,
			border,
			color.White,
			true,
		),
	)
	savedGames.SetActived(c.savedGamesPath != "")
	root.AddLeaf(savedGames)

	rules := gulw.NewTextButton(
		"Rules↪",
		resources.GuiFont,
		fonterSize,
		func() {
			c.openBrowser(c.rulesUrl)
		},
		gulrot.NewBopt(
			func(ow, oh int) (int, int, int, int) {
				w, h := scaler(ow, oh)
				y := oh/2 + int(float64(h)*heightPad)
				return ow / 2, y, w, h
			},
			gulrot.MM,
			border,
			color.White,
			true,
		),
	)
	rules.SetActived(c.rulesUrl != "")
	root.AddLeaf(rules)

	website := gulw.NewTextButton(
		"Website↪",
		resources.GuiFont,
		fonterSize,
		func() {
			c.openBrowser(c.webUrl)
		},
		gulrot.NewBopt(
			func(ow, oh int) (int, int, int, int) {
				w, h := scaler(ow, oh)
				y := oh/2 + int(2*float64(h)*heightPad)
				return ow / 2, y, w, h
			},
			gulrot.MM,
			border,
			color.White,
			true,
		),
	)
	website.SetActived(c.rulesUrl != "")
	root.AddLeaf(website)

	return root
}

func (c *Controller) loadInitScn() {
	c.loadNextScene(
		nil,
		nil,
		c.getInitScn(),
	)
}
