package gui

import (
	"gra/gulrot"
	"gra/gulw"
	"gra/resources"
	"image/color"
)

func (c *Controller) loadOnlineHomeScn() {
	scaler := gulrot.NiceScaler(0.8, 0.1, 0.2)
	heightPad := 1.5
	fonterSize := 0.5
	border := 3

	root := gulrot.NewRoot(color.White)
	root.AddLeaf(
		gulw.NewTextButton(
			"New Game",
			resources.GuiFont,
			fonterSize,
			func() {
				c.loadNewMultiScn()
			},
			gulrot.NewBopt(
				func(ow, oh int) (int, int, int, int) {
					w, h := scaler(ow, oh)
					return ow / 2, oh/2 - int(heightPad*float64(h)), w, h
				},
				gulrot.MM,
				border,
				color.White,
				true,
			),
		),
	)

	root.AddLeaf(
		gulw.NewTextButton(
			"Join Private",
			resources.GuiFont,
			fonterSize,
			c.loadJoinPrivateLobbyScn,
			gulrot.NewBopt(
				func(ow, oh int) (int, int, int, int) {
					w, h := scaler(ow, oh)
					return ow / 2, oh / 2, w, h
				},
				gulrot.MM,
				border,
				color.White,
				true,
			),
		),
	)

	root.AddLeaf(
		gulw.NewTextButton(
			"User",
			resources.GuiFont,
			fonterSize,
			c.loadUserScn,
			gulrot.NewBopt(
				func(ow, oh int) (int, int, int, int) {
					w, h := scaler(ow, oh)
					return ow / 2, oh/2 + int(heightPad*float64(h)), w, h
				},
				gulrot.MM,
				border,
				color.White,
				true,
			),
		),
	)

	root.AddLeaf(
		c.makeBackButton(
			func() {
				c.client.Kill()
				c.loadIndexScn()
			},
		),
	)

	c.loadNextScene(nil, nil, root)
}
