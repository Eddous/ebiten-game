package gui

import (
	"gra/gulrot"
	"gra/gulw"
	"gra/resources"
	"image/color"
)

func (c *Controller) loadConnectingScn() {
	txt := gulw.NewDynamicText(
		resources.GuiFont,
		0.025,
		1,
		gulrot.NewBopt(
			func(ow, oh int) (int, int, int, int) {
				return ow / 2, oh / 2, 2 * ow / 3, -1
			},
			gulrot.MM,
			2,
			color.White,
			true,
		),
	)

	if err := c.client.TryConnect(); err == nil {
		c.loadLoginScn2()
		return
	} else {
		txt.Text = err.Error()
		// log.Println(err)
	}

	root := gulrot.NewRoot(color.White)
	root.AddLeaf(txt)
	root.AddLeaf(c.makeBackButton(c.loadIndexScn))

	c.loadNextScene(nil, nil, root)
}
