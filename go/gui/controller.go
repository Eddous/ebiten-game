package gui

import (
	"fmt"
	"gra/gulrot"
	"gra/gulw"
	"gra/resources"
	"gra/server"
	"gra/utils"
	"image/color"
)

type updaterInterface interface {
	update(*Controller)
}

type msgUpdaterInterface interface {
	msgUpdate(*Controller, server.ServerToClient)
}

type Controller struct {
	// inited before init function
	client         *server.ClientController
	openBrowser    func(string)
	nextGameSeed   int64 // this way I can before each game measure seed
	serverAddress  string
	savedGamesPath string
	webUrl         string

	gulrotController gulrot.GulrotController

	msgUpdater msgUpdaterInterface
	updater    updaterInterface
	newLayout  *gulrot.Root

	user server.User

	// loginScn
	login    string
	password string
}

func (c *Controller) nextGameSeedAndUpdate() int64 {
	seed := c.nextGameSeed
	c.nextGameSeed = utils.NextSeed(seed)
	fmt.Println("seed: ", seed)
	return seed
}

func (c *Controller) loadNextScene(msgUpdater msgUpdaterInterface, newUpdater updaterInterface, newLayout *gulrot.Root) {
	c.msgUpdater = msgUpdater
	c.updater = newUpdater
	c.newLayout = newLayout
}

func (c *Controller) makeBackButton(foo func()) *gulw.Button {
	scaler := gulrot.QuadraticScaler(0.008, 1)
	return gulw.NewImageButton(
		resources.BackArrow,
		foo,
		gulrot.NewBopt(
			func(ow, oh int) (int, int, int, int) {
				w, h := scaler(ow, oh)
				return 0, 0, w, h
			},
			gulrot.LT,
			2,
			color.White,
			true,
		),
	)
}

func (c *Controller) receiveMsg(msg server.ServerToClient) {
	if c.msgUpdater == nil {
		panic("no receiver!")
	}
	c.msgUpdater.msgUpdate(c, msg)
}

type ControllerArgs struct {
	Seed           int64
	ServerAdress   string
	SavedGamesPath string // if empty, the saved games won't be usable
	TestingEnv     bool
	WebUrl         string // if empty, won't be usable

	// for mobile support
	OpenBrowser func(url string)
}

// openBrowser and rotate are for mobile support
func NewController(args ControllerArgs) *Controller {
	return &Controller{
		client:         server.NewClientController(args.ServerAdress, args.TestingEnv),
		serverAddress:  args.ServerAdress,
		openBrowser:    args.OpenBrowser,
		nextGameSeed:   args.Seed,
		savedGamesPath: args.SavedGamesPath,
		webUrl:         args.WebUrl,
	}
}

func (c *Controller) Init(gulrotController gulrot.GulrotController) *gulrot.Root {
	resources.Init()
	c.gulrotController = gulrotController
	return c.getIndexScn()
}

func (c *Controller) Update() *gulrot.Root {
	if c.newLayout != nil {
		layout := c.newLayout
		c.newLayout = nil
		return layout
	}
	if c.client.Mode() != server.Dead {
		if !c.client.Update(c.receiveMsg) {
			c.loadConnectingScn()
		}
	}
	if c.updater != nil {
		c.updater.update(c)
	}
	if c.newLayout != nil {
		layout := c.newLayout
		c.newLayout = nil
		return layout
	}
	return nil
}
