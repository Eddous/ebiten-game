package gui

import (
	"gra/gulw"
	"gra/server"
)

func (c *Controller) loadLoginScn2() {
	var name, password *gulw.TextField

	g := c.loadGeneralFormScn(
		func(c *Controller, msg server.ServerToClient, setErrorText func(string, bool)) {
			if msg.T != server.LoginResT {
				panic("unexpected message type")
			}
			switch msg.LoginRes.T {
			case server.Err:
				setErrorText(msg.LoginRes.Error, false)
			case server.Success:
				c.user = msg.LoginRes.You
				c.loadOnlineHomeScn()
			case server.SuccessReconnected:
				c.user = msg.LoginRes.You
				c.loadOnGameScn(msg.LoginRes.InitRoomMsg)
			default:
				panic("unknown type of LoginRes")
			}
		},
		func() {
			c.client.Kill()
			c.loadIndexScn()
		},
		[]textFieldSettings{
			{
				defaultString: "username",
				password:      false,
			},
			{
				defaultString: "password",
				password:      true,
			},
		},
		[]buttonSettings{
			newButtonSettings(
				"Login",
				func() {
					c.login = name.Text
					c.password = password.Text
					if c.client.Mode() != server.Pending {
						return
					}
					c.client.Send(
						server.ClientToServer{
							T: server.LoginReqT,
							LoginReq: server.LoginReq{
								Name:     name.Text,
								Password: password.Text,
							},
						},
					)
				},
				[]int{0},
			),
			newButtonSettings(
				"Register",
				func() {
					if c.client.Mode() != server.Pending {
						return
					}
					c.loadRegisterScn()
				},
				[]int{},
			),
			newButtonSettings(
				"Forget Password",
				func() {
					if c.client.Mode() != server.Pending {
						return
					}
					c.loadForgetPasswordScn()
				},
				[]int{},
			),
		},
		0,
	)

	name = g.fields[0]
	password = g.fields[1]
	name.Text = c.login
	password.Text = c.password
}
