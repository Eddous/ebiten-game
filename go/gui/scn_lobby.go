package gui

import (
	"gra/gulrot"
	"gra/gulw"
	"gra/resources"
	"gra/server"
	"image/color"
	"strconv"
)

func (c *Controller) loadLobbyScn(msg server.LobbyMsg) {

	textScaler := gulrot.StandardScaler(1, 0.8)
	buttonScaler := gulrot.NiceScaler(0.7, 0.1, 0.2)

	root := gulrot.NewRoot(color.White)

	text := gulw.NewText(
		"",
		resources.GuiFont,
		0.05,
		gulrot.MM,
		0,
		0,
		color.Black,
		gulrot.NewBopt(
			func(ow, oh int) (int, int, int, int) {
				w, h := textScaler(ow, oh)
				return 0, 0, w, h
			},
			gulrot.LT,
			0,
			color.White,
			true,
		),
	)
	root.AddLeaf(text)

	root.AddLeaf(
		gulw.NewTextButton(
			"Leave",
			resources.GuiFont,
			0.5,
			func() {
				// this is ok since the client is Streaming (getting msgs without sending requests)
				// server will ignore the additional request
				c.client.Send(
					server.ClientToServer{
						T: server.LeaveLobbyReqT,
					},
				)
			},
			gulrot.NewBopt(
				func(ow, oh int) (int, int, int, int) {
					_, textHeight := textScaler(ow, oh)

					w, h := buttonScaler(ow, oh)
					return ow / 2, textHeight, w, h
				},
				gulrot.MT,
				3,
				color.White,
				true,
			),
		),
	)

	s := &lobbyScn{text: text}
	s.updateText(msg)
	c.loadNextScene(s, nil, root)
}

type lobbyScn struct {
	text *gulw.Text
}

func (s *lobbyScn) msgUpdate(c *Controller, msg server.ServerToClient) {
	switch msg.T {
	case server.LeaveLobbyResT:
		c.loadOnlineHomeScn()
	case server.LobbyMsgT:
		s.updateText(msg.LobbyMsg)
	case server.InitRoomMsgT:
		c.loadOnGameScn(msg.InitRoomMsg)
	default:
		panic("unexpected message from server")
	}
}

func (s *lobbyScn) updateText(msg server.LobbyMsg) {
	if msg.LobbyId != 0 {
		s.text.Text = "Code: " + strconv.Itoa(int(msg.LobbyId))
	}
	for _, user := range msg.Names {
		s.text.Text += "\n" + user
	}
}
