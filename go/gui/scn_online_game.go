package gui

import (
	"gra/corewrapper"
	"gra/game"
	"gra/server"
)

func (c *Controller) loadOnGameScn(initRoom []byte) *onGameScn {
	wrapper := corewrapper.Deserialize(initRoom)
	you := wrapper.NameToPlayer[c.user.Name]
	gameScn, root := game.NewGame(
		c.gulrotController,
		wrapper,
		you,
		func(p *corewrapper.Player, pf corewrapper.PlayerFeelings) {
			c.client.Send(
				server.ClientToServer{
					T:             server.GameActionMsgT,
					GameActionMsg: p.SetFeelings(pf),
				},
			)
		},
		func(p *corewrapper.Player, buffer string) {
			c.client.Send(
				server.ClientToServer{
					T:             server.GameActionMsgT,
					GameActionMsg: p.SetBuffer(buffer)},
			)
		},
		func() {
			c.client.Send(
				server.ClientToServer{
					T: server.LeaveRoomReqT,
				},
			)
		},
		true,
		true,
	)

	scn := &onGameScn{wrapper, you, gameScn}
	c.loadNextScene(scn, scn, root)
	return scn
}

type onGameScn struct {
	*corewrapper.Wrapper
	you     *corewrapper.Player
	gameScn *game.Game
}

func (s *onGameScn) msgUpdate(c *Controller, msg server.ServerToClient) {
	switch msg.T {
	case server.GameMsgT:
		if s.Wrapper.ClientUpdate(msg.GameMsg, s.you.Name) {
			s.gameScn.NewStateDetected()
		}
	case server.LeaveRoomResT:
		c.user = msg.LeaveRoomRes
		c.loadOnlineHomeScn()
	default:
		panic("not implemented")
	}
}

func (s *onGameScn) update(c *Controller) {
	s.gameScn.GuiUpdate()
}
