package gui

import (
	"gra/gulw"
	"gra/server"
)

func (c *Controller) loadChangePasswordScn() {
	var passwdField *gulw.TextField

	g := c.loadGeneralFormScn(
		func(c *Controller, msg server.ServerToClient, setErrorText func(string, bool)) {
			if msg.T != server.GeneralResT {
				panic("not supported msg")
			}
			setErrorText("Password is changed", true)
		},
		c.loadUserScn,
		[]textFieldSettings{
			{
				defaultString: "new password",
				password:      true,
			},
		},
		[]buttonSettings{
			newButtonSettings(
				"Send",
				func() {
					if c.client.Mode() != server.Pending {
						return
					}
					c.client.Send(
						server.ClientToServer{
							T:              server.ChangePasswordT,
							ChangePassword: passwdField.Text,
						},
					)
				},
				[]int{},
			),
		},
		0,
	)
	passwdField = g.fields[0]
}
