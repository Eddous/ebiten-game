package gui

import (
	"gra/gulw"
	"gra/server"
)

func (c *Controller) loadRegisterScn() {
	var name *gulw.TextField
	var password *gulw.TextField
	var email *gulw.TextField

	generalScn := c.loadGeneralFormScn(
		func(c *Controller, msg server.ServerToClient, setErrorText func(string, bool)) {
			if msg.T != server.GeneralResT {
				panic("unexpected message type")
			}
			if msg.GeneralRes.Ok {
				setErrorText("Registration was sucessful", true)
			} else {
				setErrorText(msg.GeneralRes.Err, false)
			}
		},
		c.loadLoginScn2,
		[]textFieldSettings{
			{
				defaultString: "username",
				password:      false,
			},
			{
				defaultString: "password",
				password:      true,
			},
			{
				defaultString: "email",
				password:      false,
			},
		},
		[]buttonSettings{
			newButtonSettings(
				"Register",
				func() {
					c.login = name.Text
					c.password = password.Text
					if c.client.Mode() != server.Pending {
						return
					}
					c.client.Send(
						server.ClientToServer{
							T: server.RegisterReqT,
							RegisterReq: server.RegisterReq{
								Name:     name.Text,
								Password: password.Text,
								Mail:     email.Text,
							},
						},
					)
				},
				[]int{0},
			),
		},
		0,
	)
	name = generalScn.fields[0]
	password = generalScn.fields[1]
	email = generalScn.fields[2]
}
