package gui

import (
	"gra/corewrapper"
	"gra/gulrot"
	"gra/gulw"
	"gra/resources"
	"image/color"
	"strconv"

	"github.com/hajimehoshi/ebiten/v2/text/v2"
)

const classicalString = "Classic"
const citiesString = "Cities"

const humanString = "Human"
const ai1String = "AI:1"
const ai2String = "AI:2"
const ai3String = "AI:3"

type newSingleScn struct {
	getPlayers func() string
	buttons    []*gulw.Button
}

func (c *Controller) loadNewSingleScn() {

	scn := &newSingleScn{}

	gameModeSelector, getGameMode := c.newSelector(-3, "Mode    : ", []string{classicalString, citiesString}, 0)
	sizeSelector, getSize := c.newSelector(-2, "Size    : ", []string{"20", "30", "45", "60", "80", "100"}, 0)
	playersSelector, getPlayers := c.newSelector(-1, "Players : ", []string{"2", "3", "4", "5", "6"}, 0)

	scn.getPlayers = getPlayers

	root := gulrot.NewRoot(color.White)
	root.AddLeaf(c.makeBackButton(c.loadIndexScn))
	root.AddLayout(gameModeSelector)
	root.AddLayout(sizeSelector)
	root.AddLayout(playersSelector)
	root.AddLeaf(c.newBoardNextButton(2.5, func() {

		// getting game Mode
		var gameMode corewrapper.GameMode
		switch getGameMode() {
		case classicalString:
			gameMode = corewrapper.Classical
		case citiesString:
			gameMode = corewrapper.Cities
		default:
			panic("not an option")
		}

		// getting board Mode
		nodes, err := strconv.Atoi(getSize())
		if err != nil {
			panic(err)
		}

		// getting numbers of players
		players, err := strconv.Atoi(getPlayers())
		if err != nil {
			panic(err)
		}

		// getting type of players
		playerTypes := make([]int, players)
		for i := range playerTypes {
			switch scn.buttons[i].Text {
			case humanString:
				playerTypes[i] = 0
			case ai1String:
				playerTypes[i] = 1
			case ai2String:
				playerTypes[i] = 2
			case ai3String:
				playerTypes[i] = 3
			default:
				panic(scn.buttons[i].Text + " is not known as player type")
			}
		}
		c.loadOffGameScn(gameMode, nodes, playerTypes)
	}))
	root.AddLayout(scn.buttonsForChoosingPlayers(playersSelector.Bopt, resources.GuiFont))

	c.loadNextScene(nil, scn, root)
}

func (scn *newSingleScn) update(c *Controller) {
	players, _ := strconv.Atoi(scn.getPlayers())
	for i, b := range scn.buttons {
		b.Bopt.Visible = i < players
	}
}

// will load scn buttons and returns layout to be added to the root
func (scn *newSingleScn) buttonsForChoosingPlayers(bopt *gulrot.Bopt, font *text.GoTextFaceSource) *gulrot.Layout {

	const buttonFontSize = 0.45
	const relativeShiftY = 1.6 // how the layout is shifted from closest stuff right above this layout
	const relativeWidth = 1    // relative to the layouter
	const relativeHeight = 1.3 // relative to the layouter
	const relativePad = 0.01   // relative to the width of layout
	const border = 1

	// the calculation depends on last layout, it is quite ugly
	lay := gulrot.NewLayout(
		gulrot.NewBopt(
			func(ow, oh int) (int, int, int, int) {
				opt := bopt.Layout(ow, oh)
				return opt.X,
					opt.Y + int(float64(opt.Height)*relativeShiftY),
					int(float64(opt.Width) * relativeWidth),
					int(float64(opt.Height) * relativeHeight)
			},
			gulrot.MM,
			0,
			color.White,
			true,
		),
	)

	scn.buttons = make([]*gulw.Button, 6)

	buttonScaler := func(ow, oh int) (int, int) {
		pad := int(float64(ow) * relativePad)
		return (ow - 2*pad) / 3, (oh - pad) / 2
	}

	rotation := map[string]string{
		humanString: ai1String,
		ai1String:   ai2String,
		ai2String:   ai3String,
		ai3String:   humanString,
	}
	rotateText := func(i int) {
		button := scn.buttons[i]
		button.Text = rotation[button.Text]
	}

	// this is horrible horriblness
	scn.buttons[0] = gulw.NewTextButton(
		humanString,
		font,
		buttonFontSize,
		func() { rotateText(0) },
		gulrot.NewBopt(
			func(ow, oh int) (int, int, int, int) {
				w, h := buttonScaler(ow, oh)
				return 0, 0, w, h
			},
			gulrot.LT,
			border,
			resources.RgbsOwners[0],
			true,
		),
	)
	scn.buttons[1] = gulw.NewTextButton(
		ai1String,
		font,
		buttonFontSize,
		func() { rotateText(1) },
		gulrot.NewBopt(
			func(ow, oh int) (int, int, int, int) {
				w, h := buttonScaler(ow, oh)
				return ((ow - 1) / 2), 0, w, h
			},
			gulrot.MT,
			border,
			resources.RgbsOwners[1],
			true,
		),
	)
	scn.buttons[2] = gulw.NewTextButton(
		ai1String,
		font,
		buttonFontSize,
		func() { rotateText(2) },
		gulrot.NewBopt(
			func(ow, oh int) (int, int, int, int) {
				w, h := buttonScaler(ow, oh)
				return ow - 1, 0, w, h
			},
			gulrot.RT,
			border,
			resources.RgbsOwners[2],
			false,
		),
	)
	scn.buttons[3] = gulw.NewTextButton(
		ai1String,
		font,
		buttonFontSize,
		func() { rotateText(3) },
		gulrot.NewBopt(
			func(ow, oh int) (int, int, int, int) {
				w, h := buttonScaler(ow, oh)
				return 0, oh - 1, w, h
			},
			gulrot.LB,
			border,
			resources.RgbsOwners[3],
			false,
		),
	)
	scn.buttons[4] = gulw.NewTextButton(
		ai1String,
		font,
		buttonFontSize,
		func() { rotateText(4) },
		gulrot.NewBopt(
			func(ow, oh int) (int, int, int, int) {
				w, h := buttonScaler(ow, oh)
				return (ow - 1) / 2, oh - 1, w, h
			},
			gulrot.MB,
			border,
			resources.RgbsOwners[4],
			false,
		),
	)
	scn.buttons[5] = gulw.NewTextButton(
		ai1String,
		font,
		buttonFontSize,
		func() { rotateText(5) },
		gulrot.NewBopt(
			func(ow, oh int) (int, int, int, int) {
				w, h := buttonScaler(ow, oh)
				return ow - 1, oh - 1, w, h
			},
			gulrot.RB,
			border,
			resources.RgbsOwners[5],
			false,
		),
	)

	for _, b := range scn.buttons {
		lay.AddLeaf(b)
	}

	return lay
}
