package gui

import (
	"gra/gulrot"
	"gra/gulw"
	"gra/resources"
	"gra/server"
	"image/color"
	"math"
)

type buttonSettings struct {
	text       string
	foo        func()
	dependency []int // the index of the textfields which must to be filled
}

func newButtonSettings(text string, foo func(), dependency []int) buttonSettings {
	return buttonSettings{
		text:       text,
		foo:        foo,
		dependency: dependency,
	}
}

type textFieldSettings struct {
	defaultString string
	password      bool
}

type generalFormScn struct {
	// dont touch these
	msgUpdateFoo      func(*Controller, server.ServerToClient, func(string, bool))
	dependencies      map[*gulw.Button][]int
	textFields        *gulw.TextFieldGroup
	errorField        *gulw.Text
	errorFieldOpacity float64 // maximum is 255, if larger then it is capt to 255, reason for values >255 is making larger time when the color is full

	// touch these
	fields []*gulw.TextField
}

func (c *Controller) loadGeneralFormScn(msgUpdateFoo func(*Controller, server.ServerToClient, func(string, bool)), backFunc func(), textFields []textFieldSettings, buttons []buttonSettings, enterButtonIndex int) *generalFormScn {

	// ===== consts =====

	border := 3
	topPad := 0.2
	textFieldScaler := gulrot.NiceScaler(0.8, 0.1, 0.15)
	buttonScaler := func(ow, oh int) (int, int) {
		w, h := textFieldScaler(ow, oh)
		return w / 2, 3 * h / 4
	}
	paddingBetweenElem := func(h int) int {
		return h / 2
	}

	// ===== general =====

	// fill this latter
	enterButton := &gulw.Button{}

	data := &generalFormScn{
		msgUpdateFoo: msgUpdateFoo,
		dependencies: make(map[*gulw.Button][]int),
		textFields:   gulw.NewTextFieldGroup(c.gulrotController, enterButton),
		fields:       make([]*gulw.TextField, len(textFields)),
	}

	root := gulrot.NewRoot(color.White)

	// back button
	backButton := c.makeBackButton(
		func() {
			if c.client.Mode() == server.Pending {
				backFunc()
			}
		},
	)
	root.AddLeaf(backButton)

	// ===== text fields =====

	for i, tf := range textFields {
		iSaver := i
		data.fields[i] = data.textFields.AddField(
			"",
			resources.GuiFont,
			0.5,
			tf.defaultString,
			tf.password,
			gulrot.NewBopt(
				func(ow, oh int) (int, int, int, int) {
					w, h := textFieldScaler(ow, oh)
					y := int(topPad * float64(oh))
					y += iSaver * h
					y += iSaver * paddingBetweenElem(h) // pading between fields
					return ow / 2, y, w, h
				},
				gulrot.MT,
				border,
				color.White,
				true,
			),
		)
		root.AddLeaf(data.fields[i])
	}

	// ===== error field =====

	data.errorField = gulw.NewText(
		"",
		resources.GuiFont,
		0.3,
		gulrot.MM,
		0,
		0,
		color.Black,
		gulrot.NewBopt(
			func(ow, oh int) (int, int, int, int) {
				_, textFieldH := textFieldScaler(ow, oh)
				y := int(topPad * float64(oh))
				y += len(textFields) * textFieldH
				y += (len(textFields) - 1) * paddingBetweenElem(textFieldH)
				return ow / 2, y, ow, textFieldH
			},
			gulrot.MT,
			0,
			color.White,
			true,
		),
	)
	root.AddLeaf(data.errorField)

	// ===== buttons =====

	for i, gb := range buttons {
		iSaver := i
		button := gulw.NewTextButton(
			gb.text,
			resources.GuiFont,
			0.5,
			gb.foo,
			gulrot.NewBopt(
				func(ow, oh int) (int, int, int, int) {
					_, textFieldH := textFieldScaler(ow, oh)
					y := int(topPad * float64(oh))
					y += (len(textFields) + 1) * textFieldH
					y += (len(textFields) - 1) * paddingBetweenElem(textFieldH) // padding

					w, h := buttonScaler(ow, oh)
					y += iSaver * h
					y += iSaver * paddingBetweenElem(h)
					return ow / 2, y, w, h
				},
				gulrot.MT,
				border,
				color.White,
				true,
			),
		)
		data.dependencies[button] = gb.dependency
		if i == enterButtonIndex {
			*enterButton = *button
		}

		root.AddLeaf(button)
	}
	c.loadNextScene(data, data, root)
	return data
}

func (s *generalFormScn) msgUpdate(c *Controller, msg server.ServerToClient) {
	s.msgUpdateFoo(c, msg, s.setErrorTxt)
}

func (s *generalFormScn) update(c *Controller) {
	s.textFields.Update()

	for b, dependencies := range s.dependencies {
		active := true
		for _, i := range dependencies {
			active = active && s.fields[i].Text != ""
		}
		b.SetActived(active)
	}

	// updating errorField opacity
	if s.errorFieldOpacity > 0 {
		s.errorFieldOpacity -= 1

		errorFieldColor := s.errorField.Color.(color.RGBA)
		errorFieldColor.A = uint8(
			math.Min(255, s.errorFieldOpacity),
		)
		s.errorField.Color = errorFieldColor

		if s.errorFieldOpacity < 0 {
			s.errorFieldOpacity = 0
		}
	}
}

func (s *generalFormScn) setErrorTxt(txt string, ok bool) {
	s.errorField.Text = txt
	if ok {
		s.errorField.Color = color.RGBA{
			R: 0,
			G: 204,
			B: 0,
			A: 255,
		}
	} else {
		s.errorField.Color = color.RGBA{
			R: 204,
			G: 0,
			B: 0,
			A: 255,
		}
	}

	s.errorFieldOpacity = 300
}
