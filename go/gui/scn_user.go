package gui

import (
	"fmt"
	"gra/gulrot"
	"gra/gulw"
	"gra/resources"
	"gra/server"
	"image/color"
	"strconv"
)

func (c *Controller) loadUserScn() {
	topBotPad := 0.2
	scaler := gulrot.NiceScaler(0.7, 0.1, 0.2)
	heightPad := 1.5

	buttonBorder := 3
	butonFontSize := 0.45

	root := gulrot.NewRoot(color.White)

	root.AddLeaf(c.makeBackButton(c.loadOnlineHomeScn))

	dynText := gulw.NewDynamicText(
		resources.GuiFont,
		0.0225,
		1,
		gulrot.NewBopt(
			func(ow, oh int) (int, int, int, int) {
				return ow / 2, int(topBotPad * float64(oh)), int(0.8 * float64(ow)), 0
			},
			gulrot.MT,
			2,
			color.White,
			true,
		),
	)
	dynText.Text = userToString(c.user)
	root.AddLeaf(dynText)

	root.AddLeaf(
		gulw.NewTextButton(
			"Change Email",
			resources.GuiFont,
			butonFontSize,
			c.loadChangeEmailScn,
			gulrot.NewBopt(
				func(ow, oh int) (int, int, int, int) {
					w, h := scaler(ow, oh)
					y := oh - int(topBotPad*float64(oh)) - int(heightPad*float64(h))
					return ow / 2, y, w, h
				},
				gulrot.MB,
				buttonBorder,
				color.White,
				true,
			),
		),
	)
	root.AddLeaf(
		gulw.NewTextButton(
			"Change Password",
			resources.GuiFont,
			butonFontSize,
			c.loadChangePasswordScn,
			gulrot.NewBopt(
				func(ow, oh int) (int, int, int, int) {
					w, h := scaler(ow, oh)
					y := oh - int(topBotPad*float64(oh))
					return ow / 2, y, w, h
				},
				gulrot.MB,
				buttonBorder,
				color.White,
				true,
			),
		),
	)
	c.loadNextScene(nil, nil, root)
}

func userToString(u server.User) string {
	tmp := "Name: " + u.Name + "\n"
	tmp += "Email: " + u.Email + "\n"
	tmp += "EmailWasVadidated: " + strconv.FormatBool(u.WasEmailValidated) + "\n"
	tmp += "Eloes: " + fmt.Sprintln(u.Eloes)
	return tmp
}
