package gui

import (
	"gra/gulw"
	"gra/server"
)

func (c *Controller) loadForgetPasswordScn() {
	var nameField *gulw.TextField
	var emailField *gulw.TextField

	g := c.loadGeneralFormScn(
		func(c *Controller, msg server.ServerToClient, setErrorText func(string, bool)) {
			if msg.T != server.GeneralResT {
				panic("not supported msg")
			}
			setErrorText("New password was send to the email\n(if the email was in the database)", true)
		},
		c.loadLoginScn2,
		[]textFieldSettings{
			{
				defaultString: "name",
				password:      false,
			},
			{
				defaultString: "email",
				password:      false,
			},
		},
		[]buttonSettings{
			newButtonSettings(
				"Send",
				func() {
					if c.client.Mode() != server.Pending {
						return
					}
					c.client.Send(
						server.ClientToServer{
							T: server.ForgotPasswordT,
							ForgotPassword: server.ForgotPassword{
								Name: nameField.Text,
								Mail: emailField.Text,
							},
						},
					)
				},
				[]int{0, 1},
			),
		},
		0,
	)
	nameField = g.fields[0]
	emailField = g.fields[1]
}
