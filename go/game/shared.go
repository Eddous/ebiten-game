package game

import (
	"gra/corewrapper"
	"gra/gulrot"
	"gra/gulw"
)

type shared struct {
	wrapper          *corewrapper.Wrapper
	you              *corewrapper.Player // can be nil, for example when viewing saved game, or viewing AI only play
	coreDriver       CoreInterface
	camera           *gulrot.Camera
	currState        int
	gulrotController gulrot.GulrotController
	toolButtons      *gulw.RadioButtons[string]
}

func (s shared) lastState() bool {
	return s.currState == 2*s.coreDriver.HistoryBoardStates()-2
}
