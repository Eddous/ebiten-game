package game

import (
	"fmt"
	"gra/coreutils"
	"gra/corewrapper"
	"gra/gulrot"
	"gra/resources"
	"image/color"
	"strconv"

	"github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/text/v2"
)

type info struct {
	data *shared

	text         string
	fontSize     float64
	pad          float64
	imgSize      float64
	innerImgSize float64
	imgStart     int
}

func newInfoText(data *shared) *info {
	return &info{data: data}
}

// ===============
// # LeafInterface

func (i *info) Layout(ow, oh int) gulrot.Opt {
	i.updateText()

	base := textScaler(ow, oh)
	w, h := i.measure(base)
	for w > ow {
		base -= 0.5
		w, h = i.measure(base)
	}

	y, _ := buttonScaler(ow, oh)

	return gulrot.Opt{
		X:         ow / 2,
		Y:         int(i.fontSize*0.25) + y,
		Width:     int(w),
		Height:    int(h),
		Anchor:    gulrot.MT,
		Border:    0,
		FillColor: nil,
		Visible:   true,
	}
}

func (i *info) Update(in gulrot.Input) {}

func (i *info) Draw(dst *ebiten.Image) {

	if i.data.you != nil {
		w, h := text.Measure(i.data.you.Name, &text.GoTextFace{Source: resources.TextFont, Size: i.fontSize}, 0)
		gulrot.Draw3(dst, resources.ColoredImages[i.data.you.Id], 0, 0, gulrot.LT, w, float64(h))
	}
	gulrot.Text(
		dst,
		i.text,
		resources.TextFont,
		i.fontSize,
		0,
		0,
		gulrot.LT,
		0,
		0,
		color.Black,
	)

	_, h := gulrot.ImgSize(dst)
	x := i.imgStart + int(i.imgSize/2)
	y := float64(h / 2)
	for idx, p := range i.data.wrapper.AllPlayers {
		gulrot.Draw3(dst, resources.ColoredFrames[idx], float64(x), y, gulrot.MM, i.imgSize, i.imgSize)
		if img := getInnerImg(p); img != nil {
			gulrot.Draw3(dst, img, float64(x), y, gulrot.MM, float64(i.innerImgSize), float64(i.innerImgSize))
		}
		if p.Feelings == corewrapper.Draw {
			gulrot.Draw3(dst, resources.WantDraw, float64(x), y, gulrot.MM, float64(i.imgSize), float64(i.imgSize))
		}
		x += int(i.imgSize + i.pad)
	}

}

func (i *info) updateText() {
	var str string
	p := i.data.you
	if p != nil {
		str += p.Name + " | "
		if p.Alive() {
			if p.Time > 0 {
				str += fmt.Sprintf("%02d:%02d ", p.Time/60, p.Time%60)
			}
			str += i.data.coreDriver.Soldiers(p.Id) + " (" + i.data.coreDriver.Territory(p.Id) + ")" + " | "
		}
	}
	maxState := i.data.coreDriver.HistoryBoardStates()
	if i.data.lastState() {
		stateStr, show := i.data.coreDriver.StateStr()
		if show {
			str += stateStr + " | "
		}
	} else {
		str += strconv.Itoa(i.data.currState / 2)
		if i.data.currState%2 == 1 {
			str += "+"
		}
		str += "/" + strconv.Itoa(maxState-1) + " | "
	}
	i.text = str
}

func (i *info) measure(fontSize float64) (int, int) {
	i.fontSize = fontSize
	i.pad = i.fontSize * 0.1
	i.imgSize = i.fontSize * 1.3
	i.innerImgSize = i.fontSize * 0.8

	w, _ := text.Measure(i.text, &text.GoTextFace{
		Source: resources.TextFont,
		Size:   i.fontSize,
	}, 10)
	i.imgStart = int(w)

	ps := float64(len(i.data.wrapper.AllPlayers))
	w += ps*i.imgSize + (ps-1)*i.pad
	return int(w), int(i.imgSize) + 2 // the images are otherwise croped
}

func getInnerImg(p *corewrapper.Player) *ebiten.Image {
	if (p.Alive() && p.Ready()) || (!p.Alive() && p.DeathInfo.Result == coreutils.Win) {
		return resources.Ok
	}
	if !p.Alive() && p.DeathInfo.Result == coreutils.Lost {
		return resources.Nok
	}
	if !p.Alive() && p.DeathInfo.Result == coreutils.Draw {
		return resources.Equals
	}
	return nil
}
