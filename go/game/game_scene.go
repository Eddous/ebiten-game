package game

import (
	"gra/corewrapper"
	"gra/gulrot"
	"gra/gulw"
	"gra/resources"
	"gra/utils"
	"image/color"
)

var buttonScaler = gulrot.NiceScaler(0.115, 0.06, 1)
var textScaler = gulrot.TextHeightScaler(0.022)

// flashing button
const flashingDuration = 15 // seconds
const flashingSpeed = 30    // frames
var blinkColor = color.RGBA{R: 255, G: 140}
var tutorialButtonClickColor = color.RGBA{R: 255, G: 100}

type Game struct {
	data *shared

	nextButton          *gulw.Button
	openMenu            *gulw.CheckButton
	menuLayout          *gulrot.Layout
	offerDrawButton     *gulw.Button
	surrenderButton     *gulw.Button
	tutorialButton      *gulw.HoldButton
	tutorialBox         *gulw.DynamicText
	players             *players
	backwards, forwards *gulw.Button

	tutorialButtonAnimation int
}

func NewGame(
	gulrotController gulrot.GulrotController,
	tgame *corewrapper.Wrapper,
	you *corewrapper.Player,

	setFeelings func(*corewrapper.Player, corewrapper.PlayerFeelings),
	setBuffer func(*corewrapper.Player, string),
	exitGame func(),

	showTime bool,
	showElo bool,
) (*Game, *gulrot.Root) {

	g := &Game{
		data: &shared{
			wrapper:          tgame,
			you:              you,
			coreDriver:       tgame.TGame.(CoreInterface),
			gulrotController: gulrotController,
		},
	}

	// ===== initialization of the root layout =====

	root := gulrot.NewRoot(color.White)

	// map leaf
	root.AddLeaf(newMapLeaf(g.data))

	// top text
	root.AddLeaf(newInfoText(g.data))

	// players
	g.players = newPlayers(g.data, showTime, showElo)
	root.AddLeaf(g.players)

	// tutorial
	g.tutorialBox = gulw.NewDynamicText(
		resources.TextFont,
		0.02,
		1,
		gulrot.NewBopt(
			func(ow, oh int) (int, int, int, int) {
				return ow / 2, oh / 2, int(float64(ow) * 0.8), -1
			},
			gulrot.MM,
			2,
			color.White,
			false,
		),
	)
	root.AddLeaf(g.tutorialBox)

	// ===== top panel =====

	toolLayout := gulrot.NewLayout(
		gulrot.NewBopt(
			func(ow, oh int) (int, int, int, int) {
				_, buttonSize := buttonScaler(ow, oh)
				return -2, -2, ow + 4, buttonSize + 2
			},
			gulrot.LT,
			2,
			color.White,
			true,
		),
	)
	root.AddLayout(toolLayout)

	// menu button
	g.openMenu = gulw.NewCheckImageButton(
		resources.ButtonOpenMenu,
		gulrot.NewBopt(
			func(ow, oh int) (int, int, int, int) {
				return 0, 0, oh, oh
			},
			gulrot.LT,
			0,
			nil,
			true,
		),
	)
	toolLayout.AddLeaf(g.openMenu)

	// tutorial opener
	g.tutorialButton = gulw.NewHoldButton(
		resources.Help,
		func() {
			g.tutorialBox.Bopt.Visible = true
			g.tutorialButton.Bopt.FillColor = color.White
			g.tutorialButtonAnimation = 60 * flashingDuration
		},
		func() {
			g.tutorialBox.Bopt.Visible = false
		},
		gulrot.NewBopt(
			func(ow, oh int) (int, int, int, int) {
				return oh, 0, oh, oh
			},
			gulrot.LT,
			0,
			nil,
			true,
		),
		blinkColor,
		tutorialButtonClickColor,
	)
	toolLayout.AddLeaf(g.tutorialButton)

	// backwards
	g.backwards = gulw.NewImageButton(
		resources.Previous,
		func() {
			if g.data.currState > 0 {
				g.data.currState -= 1
			}
		},
		gulrot.NewBopt(
			func(ow, oh int) (int, int, int, int) {
				return 2 * oh, 0, oh, oh
			},
			gulrot.LT,
			0,
			color.White,
			true,
		),
	)
	toolLayout.AddLeaf(g.backwards)

	// forwards
	g.forwards = gulw.NewImageButton(
		resources.Next,
		func() {
			if !g.data.lastState() {
				g.data.currState += 1
			}
		},
		gulrot.NewBopt(
			func(ow, oh int) (int, int, int, int) {
				return 3 * oh, 0, oh, oh
			},
			gulrot.LT,
			0,
			color.White,
			true,
		),
	)
	toolLayout.AddLeaf(g.forwards)

	// minus
	toolLayout.AddLeaf(
		gulw.NewImageButton(
			resources.ZoomMinus,
			func() { g.data.camera.Zoom(1 / zoomSpeed) },
			gulrot.NewBopt(
				func(ow, oh int) (int, int, int, int) {
					return 4 * oh, 0, oh, oh
				},
				gulrot.LT,
				0,
				nil,
				true,
			),
		),
	)

	// plus
	toolLayout.AddLeaf(
		gulw.NewImageButton(
			resources.ZoomPlus,
			func() { g.data.camera.Zoom(zoomSpeed) },
			gulrot.NewBopt(
				func(ow, oh int) (int, int, int, int) {
					return 5 * oh, 0, oh, oh
				},
				gulrot.LT,
				0,
				nil,
				true,
			),
		),
	)

	// players button
	toolLayout.AddLeaf(
		gulw.NewHoldButton(
			resources.Players,
			func() {
				g.players.visible = true
			},
			func() {
				g.players.visible = false
			},
			gulrot.NewBopt(
				func(ow, oh int) (int, int, int, int) {
					return 6 * oh, 0, oh, oh
				},
				gulrot.LT,
				0,
				nil,
				true,
			),
			gulw.Grey1,
			gulw.Grey2,
		),
	)

	// next button
	g.nextButton = gulw.NewTextButton(
		"Next",
		resources.TextFont,
		0.6,
		func() {
			if buffer, ok := g.data.coreDriver.GetActionBuffer(g.data.you.Id); ok {
				setBuffer(g.data.you, buffer)
			}
			// switching of users in human vs human is in gui update and new state detected function
		},
		gulrot.NewBopt(
			func(ow, oh int) (int, int, int, int) {
				return ow - 1, 0, 2 * oh, oh
			},
			gulrot.RT,
			0,
			color.White,
			true,
		),
	)
	toolLayout.AddLeaf(g.nextButton)

	// ===== tools =====

	if tools := g.data.coreDriver.Tools(); len(tools) != 0 {
		rbs := []*gulw.RadioButton[string]{}
		for i, tool := range tools {
			rbs = append(rbs, gulw.NewRadioButton(float64(i), 0, tool.V1, tool.V2, i == 0))
		}
		g.data.toolButtons = gulw.NewRadioButtons(
			gulrot.NewBopt(
				func(ow, oh int) (int, int, int, int) {
					return ow / 2, oh - 1, 0, 0
				},
				gulrot.MB,
				0,
				color.White,
				false,
			),
			func(ow, oh int) (int, int) {
				buttonSize, _ := buttonScaler(ow, oh)
				return buttonSize, buttonSize
			},
			gulw.One,
			rbs...,
		)
		root.AddLeaf(g.data.toolButtons)
	}

	// ===== menuLayout =====

	menuButtonsScaler := gulrot.FixedRationScalerH(0.15, 4, 1)
	menuLayoutTextSize := 0.6
	tr := utils.Get1Transformator(0, 0.27, 2, 0.73)

	menuScaler := gulrot.NiceScaler(0.5, 0.5, 1)
	g.menuLayout = gulrot.NewLayout(
		gulrot.NewBopt(
			func(ow, oh int) (int, int, int, int) {
				w, h := menuScaler(ow, oh)
				return ow / 2, oh / 2, w, h
			},
			gulrot.MM,
			2,
			color.White,
			false,
		),
	)
	root.AddLayout(g.menuLayout)

	// offer draw button
	g.offerDrawButton = gulw.NewTextButton(
		"Offer draw",
		resources.TextFont,
		menuLayoutTextSize,
		func() {
			if g.data.you.Feelings == corewrapper.WantBlood {
				setFeelings(g.data.you, corewrapper.Draw)
			} else if g.data.you.Feelings == corewrapper.Draw {
				setFeelings(g.data.you, corewrapper.WantBlood)
			}
		},
		gulrot.NewBopt(
			func(ow, oh int) (int, int, int, int) {
				w, h := menuButtonsScaler(ow, oh)
				return ow / 2, int(tr(0) * float64(oh)), w, h
			},
			gulrot.MM,
			2,
			color.White,
			true,
		),
	)
	g.menuLayout.AddLeaf(g.offerDrawButton)

	// surrender button
	g.surrenderButton = gulw.NewTextButton(
		"Surrender",
		resources.TextFont,
		menuLayoutTextSize,
		func() {
			setFeelings(g.data.you, corewrapper.Surrender)
		},
		gulrot.NewBopt(
			func(ow, oh int) (int, int, int, int) {
				w, h := menuButtonsScaler(ow, oh)
				return ow / 2, int(tr(1) * float64(oh)), w, h
			},
			gulrot.MM,
			2,
			color.White,
			true,
		),
	)
	g.menuLayout.AddLeaf(g.surrenderButton)

	// exit button
	g.menuLayout.AddLeaf(
		gulw.NewTextButton(
			"Exit",
			resources.TextFont,
			menuLayoutTextSize,
			func() {
				if setFeelings != nil && g.data.you != nil { // case of saved games
					setFeelings(g.data.you, corewrapper.Surrender)
				}
				exitGame()
			},
			gulrot.NewBopt(
				func(ow, oh int) (int, int, int, int) {
					w, h := menuButtonsScaler(ow, oh)
					return ow / 2, int(tr(2) * float64(oh)), w, h
				},
				gulrot.MM,
				2,
				color.White,
				true,
			),
		),
	)

	return g, root
}

// this could be checked in GuiUpdate (in offline this is always synchronize, in online it is unsynchronized)
func (g *Game) NewStateDetected() {
	g.data.coreDriver.ResetActionBuffer()
	// read implementation of lastState() in shared.go
	if g.data.currState == 2*g.data.coreDriver.HistoryBoardStates()-4 {
		g.data.currState += 2
	}
}

func (g *Game) SetYou(p *corewrapper.Player) {
	g.data.coreDriver.ResetActionBuffer()
	g.data.you = p
}

func (g *Game) GuiUpdate() {
	if g.data.toolButtons != nil {
		if g.data.you == nil {
			g.data.toolButtons.Bopt.Visible = false
		} else {
			g.data.toolButtons.Bopt.Visible = g.data.coreDriver.ShowTools(g.data.you.Id, g.data.currState/2)
		}
	}

	g.solveMenuLayoutVisibility()

	okBuffer := false
	if g.data.you != nil {
		_, okBuffer = g.data.coreDriver.GetActionBuffer(g.data.you.Id)
	}
	notLastState := !g.data.lastState()

	alive := g.data.you != nil && g.data.you.Alive() // messy variable, it is not used when the g.data.you == nil
	ready := g.data.you != nil && g.data.you.Ready() // messy variable, it is not used when the g.data.you == nil

	g.nextButton.SetActived(okBuffer && alive && !ready && !notLastState)
	g.surrenderButton.SetActived(alive)
	g.offerDrawButton.SetActived(alive)

	text := g.getTutorialText(notLastState, g.data.wrapper.End(), alive, ready)
	g.tutorialBox.Text = text

	g.backwards.SetActived(g.data.currState > 0)
	g.forwards.SetActived(notLastState)

	g.data.coreDriver.Update()

	if g.tutorialButtonAnimation < flashingDuration*60 {
		if g.tutorialButtonAnimation/flashingSpeed%2 == 0 {
			g.tutorialButton.Bopt.FillColor = color.White
		} else {
			g.tutorialButton.Bopt.FillColor = blinkColor
		}
		g.tutorialButtonAnimation++
		if g.tutorialButtonAnimation == flashingDuration*60 {
			g.tutorialButton.Bopt.FillColor = color.White
		}
	}
}

// this is horrible
func (g *Game) solveMenuLayoutVisibility() {
	visibility := false

	if !g.openMenu.Value {
		visibility = false
	} else {
		if g.openMenu.Focused || g.menuLayout.IsMeOrMyDescandantFocused() {
			visibility = true
		} else {
			visibility = false
			g.openMenu.Value = false
		}
	}
	g.menuLayout.Bopt.Visible = visibility
}

func (g *Game) getTutorialText(viewingHistory, end, alive, ready bool) string {
	var ret string
	if viewingHistory {
		ret = "Now you are viewing the history.\n\n"
		ret += "Repeatedly click on the double right arrow to return to the actual state.\n\n"
		if alive && !ready {
			ret += "YOU ARE NOW PLAYING!, return to the actual state to submit actions."
		}
		return ret
	}
	stateStr, _ := g.data.coreDriver.StateStr()
	ret = "The current game phase is " + stateStr + ".\n\n"
	if end {
		ret += "The game ends here."
		return ret
	}

	if !alive {
		ret += "You are dead the game ends here for you.\n\n"
	} else if ready {
		ret += "You are ready, wait till other players submit their actions.\n\n"
	} else {
		for _, str := range g.data.coreDriver.TutorialText() {
			ret += str + "\n\n"
		}
		ret += "To submit the move click Next button."
	}

	return ret
}
