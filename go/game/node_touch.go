package game

import (
	"gra/gulrot"
)

const holdingStarts = 20
const holdingStops = 60

type nodeTouch struct {
	touch gulrot.LTouch

	t       byte // 0 not decided, 1 holding
	nid     int
	holding int // holding the same node
}

func newNodeTouch(lt gulrot.LTouch, nid int) *nodeTouch {
	return &nodeTouch{
		touch: lt,
		nid:   nid,
	}
}

// returns (continue in updating, nid (if it was clicked))
func (nt *nodeTouch) update(pid int, driver CoreInterface, newNid int, tool string) bool {
	if newNid != nt.nid {
		return false
	}
	nt.holding++

	switch nt.t {
	case 0:
		if nt.touch.Released {
			driver.Tap(pid, nt.nid, tool)
			return false
		}
		if nt.holding == holdingStarts && driver.Holdable(nt.nid) {
			nt.t = 1
		}
		return true
	case 1:
		if nt.touch.Released {
			return false
		}
		if nt.holding == holdingStops {
			driver.Holded(nt.nid)
			return false
		}
		return true
	}
	panic("never happen")
}
