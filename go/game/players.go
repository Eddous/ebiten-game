package game

import (
	"fmt"
	"gra/coreutils"
	"gra/corewrapper"
	"gra/gulrot"
	"gra/resources"
	"gra/utils"
	"image/color"
	"strings"

	"github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/text/v2"
)

type players struct {
	data          *shared
	showTime      bool
	showEloChange bool
	visible       bool

	text      string
	fontSize  float64
	pad       float64
	face      *text.GoTextFace
	nameSizes []utils.VecF
	maxName   float64
}

func newPlayers(data *shared, showTime bool, showEloChange bool) *players {
	return &players{data: data, showTime: showTime, showEloChange: showEloChange}
}

// ===============
// # LeafInterface

func (ps *players) Layout(ow, oh int) gulrot.Opt {
	raw := make([][]string, 1)
	if ps.showTime {
		raw[0] = []string{"Name", "Time", "Terr", "Sold", "Elo", "Death"}
	} else {
		raw[0] = []string{"Name", "Terr", "Sold", "Elo", "Death"}
	}

	for _, p := range ps.data.wrapper.AllPlayers {
		raw = append(raw, ps.formatPlayer(p))
	}
	table := padText(raw)

	ps.text = ""
	for i, row := range table {
		for j, item := range row {
			ps.text += item
			if j == 0 {
				ps.text += "  "
			}
			if j != len(row)-1 {
				ps.text += "  "
			}
		}
		if i != len(table)-1 {
			ps.text += "\n"
		}
	}

	ps.fontSize = textScaler(ow, oh)
	ps.pad = ps.fontSize
	ps.face = &text.GoTextFace{Source: resources.TextFont, Size: ps.fontSize}
	w, h := gulrot.TextSize(ps.text, ps.face)
	for int(w+2*ps.pad) > ow || int(h+2*ps.pad) > oh {
		ps.fontSize -= 0.5
		ps.pad = ps.fontSize
		ps.face = &text.GoTextFace{Source: resources.TextFont, Size: ps.fontSize}
		w, h = gulrot.TextSize(ps.text, ps.face)
	}

	// measuring names
	ps.nameSizes = make([]utils.VecF, len(raw)-1)
	for i := 1; i < len(raw); i++ {
		tw, th := gulrot.TextSize(raw[i][0], ps.face)
		ps.nameSizes[i-1] = utils.NewVecF(tw, th)
	}

	// measure max string
	ps.maxName, _ = gulrot.TextSize(table[0][0], ps.face)

	return gulrot.Opt{
		X:         ow / 2,
		Y:         oh / 2,
		Width:     int(w + 2*ps.pad),
		Height:    int(h + 2*ps.pad),
		Anchor:    gulrot.MM,
		Border:    2,
		FillColor: color.White,
		Visible:   ps.visible,
	}
}

func (p *players) Update(in gulrot.Input) {}

func (ps *players) Draw(dst *ebiten.Image) {
	increment := gulrot.GetFontSpacing(ps.face)
	y := ps.pad + increment
	for i, p := range ps.data.wrapper.AllPlayers {
		gulrot.Draw3(dst, resources.ColoredImages[i], ps.pad, y, gulrot.LT, ps.nameSizes[i].X, ps.nameSizes[i].Y)
		if p.Feelings == corewrapper.Draw {
			gulrot.Draw3(dst, resources.WantDraw, ps.pad+ps.maxName, y, gulrot.LT, ps.fontSize, ps.fontSize)
		}
		if img := getRightmostImage(p); img != nil {
			gulrot.Draw3(dst, img, ps.pad+ps.fontSize+ps.maxName, y, gulrot.LT, ps.fontSize, ps.fontSize)
		}

		y += increment
	}

	gulrot.Text(
		dst,
		ps.text,
		resources.TextFont,
		ps.fontSize,
		int(ps.pad),
		int(ps.pad),
		gulrot.LT,
		0,
		0,
		color.Black,
	)
}

func (ps *players) formatPlayer(p *corewrapper.Player) []string {
	ret := make([]string, 0)
	ret = append(ret, p.Name)
	if ps.showTime {
		ret = append(ret, fmt.Sprintf("%02d:%02d", p.Time/60, p.Time%60))
	}
	ret = append(ret, ps.data.coreDriver.Territory(p.Id))
	ret = append(ret, ps.data.coreDriver.Soldiers(p.Id))

	elo := fmt.Sprintf("%+.2f", p.Elo)
	if ps.showEloChange && !p.Alive() {
		elo += fmt.Sprintf("%+.2f", p.UpdatedElo-p.Elo)
	}
	ret = append(ret, elo)

	ret = append(ret, p.DeathInfo.Text)
	return ret
}

func padText(text [][]string) [][]string {
	ret := make([][]string, len(text))
	for i := range ret {
		ret[i] = make([]string, len(text[0]))
	}

	for col := range text[0] {
		maxSize := 0
		for row := range text {
			if len(text[row][col]) > maxSize {
				maxSize = len(text[row][col])
			}
		}
		for row := range text {
			ret[row][col] = text[row][col] + strings.Repeat(" ", maxSize-len(text[row][col]))
		}
	}

	return ret
}

func getRightmostImage(p *corewrapper.Player) *ebiten.Image {
	if (p.Alive() && p.Ready()) || (!p.Alive() && p.DeathInfo.Result == coreutils.Win) {
		return resources.Ready
	}
	if !p.Alive() && p.DeathInfo.Result == coreutils.Lost {
		return resources.NotReady
	}
	if !p.Alive() && p.DeathInfo.Result == coreutils.Draw {
		return resources.Equals
	}
	return nil
}
