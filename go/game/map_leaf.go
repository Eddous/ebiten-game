package game

import (
	"gra/coredrawutils"
	"gra/gulrot"
	"gra/resources"
	"gra/utils"
	"image/color"
	"math"

	"github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/vector"
)

const zoomSpeed = 1.1

type mapLeaf struct {
	data *shared

	nodes []utils.VecF
	minX  float64
	minY  float64
	maxX  float64
	maxY  float64

	nodeSize float64

	cameraTouch *gulrot.CameraTouch
	nodeTouch   *nodeTouch

	w int
	h int
}

func newMapLeaf(data *shared) *mapLeaf {
	return &mapLeaf{
		data:     data,
		nodes:    data.coreDriver.GetNodesAsVecF(),
		nodeSize: coredrawutils.NodeSize,
	}
}

// ===============
// # LeafInterface

func (ml *mapLeaf) Layout(w, h int) gulrot.Opt {
	if ml.data.camera == nil {
		ml.initCamera(w, h)
	}

	ml.w = w
	ml.h = h
	return gulrot.Opt{
		X:         0,
		Y:         0,
		Width:     w,
		Height:    h,
		Anchor:    gulrot.LT,
		Border:    0,
		FillColor: color.White,
		Visible:   true,
	}
}

func (ml *mapLeaf) Update(in gulrot.Input) {

	for _, t := range in.Touches() {
		if ml.cameraTouch != nil || ml.nodeTouch != nil {
			break
		}
		// both cameraTouch and nodeTouch are nil
		node := ml.getPotentialNode(t)
		if node == -1 {
			ml.data.coreDriver.MissTap()
			ml.cameraTouch = gulrot.NewCameraTouch(ml.data.camera, t)
		} else if ml.data.you != nil && !ml.data.you.Ready() && ml.data.lastState() {
			ml.nodeTouch = newNodeTouch(t, node)
		}
	}

	// this may be written in one if but it is unreadable
	if ml.nodeTouch != nil { // this implies that the ml.data.you != nil
		// this has to come first, because boar does not have knowledge if the player is ready => even ready player can update buffers
		if ml.data.you.Ready() {
			ml.nodeTouch = nil
		} else if !ml.nodeTouch.update(ml.data.you.Id, ml.data.coreDriver, ml.getPotentialNode(ml.nodeTouch.touch), ml.getTool()) {
			ml.nodeTouch = nil
		}
	}

	if ml.cameraTouch != nil && !ml.cameraTouch.Update() {
		ml.cameraTouch = nil
	}

	wf, hf := float64(ml.w), float64(ml.h)
	paddingNodes := 5.
	minX := ml.minX + paddingNodes
	minY := ml.minY + paddingNodes
	maxX := ml.maxX - paddingNodes
	maxY := ml.maxY - paddingNodes

	x1, y1 := ml.data.camera.GameToPixels1(minX, minY)
	x2, y2 := ml.data.camera.GameToPixels1(maxX, maxY)
	if x1 > wf {
		ml.data.camera.Jump(
			utils.NewVecF(minX, minY),
			utils.NewVecF(wf, y1),
		)
		x1, y1 = ml.data.camera.GameToPixels1(minX, minY)
		x2, y2 = ml.data.camera.GameToPixels1(maxX, maxY)
	}
	if y1 > hf {
		ml.data.camera.Jump(
			utils.NewVecF(minX, minY),
			utils.NewVecF(x1, hf),
		)
		x2, y2 = ml.data.camera.GameToPixels1(maxX, maxY)
	}
	if x2 < 0 {
		ml.data.camera.Jump(
			utils.NewVecF(maxX, maxY),
			utils.NewVecF(0, y2),
		)
		x2, y2 = ml.data.camera.GameToPixels1(maxX, maxY)
	}
	if y2 < 0 {
		ml.data.camera.Jump(
			utils.NewVecF(maxX, maxY),
			utils.NewVecF(x2, 0),
		)
	}
}

func (ml *mapLeaf) Draw(screen *ebiten.Image) {
	ml.data.coreDriver.Draw(screen, ml.data.camera, ml.data.currState/2, ml.data.currState%2 == 1)
	if ml.nodeTouch != nil && ml.nodeTouch.t == 1 {
		ml.drawHolding(screen)
	}
}

func (ml *mapLeaf) drawHolding(screen *ebiten.Image) {
	if b := screen.Bounds(); b.Min.X != 0 || b.Min.Y != 0 {
		panic("BUG!!, the code assumes that the image starts at (0,0) - DrawTriangles")
	}
	xf64, yf64 := ml.data.camera.GameToPixels2(ml.nodes[ml.nodeTouch.nid]).XY()
	r := ml.data.camera.GameToPixelsSize(ml.nodeSize)
	x, y := float32(xf64), float32(yf64)

	var path vector.Path

	theta := utils.Get1Transformator(holdingStarts, -math.Pi/2, holdingStops, 2*math.Pi)(float64(ml.nodeTouch.holding))

	path.MoveTo(x, y)
	path.Arc(x, y, float32(r/2), -math.Pi/2, float32(theta), vector.Clockwise)

	op := &ebiten.DrawTrianglesOptions{
		FillRule: ebiten.FillRuleFillAll,
	}
	vs, is := path.AppendVerticesAndIndicesForFilling(nil, nil)
	for i := range vs {
		vs[i].ColorA = 0.5
		vs[i].ColorR = 1
		vs[i].ColorG = 0
		vs[i].ColorB = 0
	}
	screen.DrawTriangles(vs, is, resources.WhitePixel, op)
}

// # LeafInterface
// ===============

// it will center and resize the level to match HEIGHT of the window
func (ml *mapLeaf) initCamera(w, h int) {
	x1, y1 := math.Inf(1), math.Inf(1)
	x2, y2 := math.Inf(-1), math.Inf(-1)
	for _, n := range ml.nodes {
		x1 = math.Min(x1, n.X)
		y1 = math.Min(y1, n.Y)
		x2 = math.Max(x2, n.X)
		y2 = math.Max(y2, n.Y)
	}

	ml.minX = x1
	ml.minY = y1
	ml.maxX = x2
	ml.maxY = y2

	zoomByWidth := float64(w) / (x2 - x1 + 10)
	zoomByHeight := float64(h) / (y2 - y1 + 10) // screen height / (logical height + padding nodes)

	ml.data.camera = gulrot.NewCamera(
		utils.NewVecF((x1+x2)/2, (y1+y2)/2),
		utils.NewVecF(float64(w)/2, float64(h)/2),
		math.Min(zoomByWidth, zoomByHeight),
	)
}

func (ml *mapLeaf) getPotentialNode(lt gulrot.LTouch) int {
	mousePosition := ml.data.camera.PixelsToGame2(lt.VecF())

	dist := math.Inf(1)
	selected := -1
	for i, n := range ml.nodes {
		new := n.GetDistance(mousePosition)
		if new < dist {
			selected = i
			dist = new
		}
	}

	// correct is float64(ml.nodeSize)/2, but it is hard to hit it
	if dist > float64(ml.nodeSize) {
		return -1
	}
	return selected
}

func (ml *mapLeaf) getTool() string {
	if ml.data.toolButtons == nil {
		return ""
	}
	return ml.data.toolButtons.Values()[0]
}
