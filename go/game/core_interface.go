package game

import (
	"gra/gulrot"
	"gra/utils"

	"github.com/barweiss/go-tuple"
	"github.com/hajimehoshi/ebiten/v2"
)

type CoreInterface interface {
	// to know whether the user is interacting with node
	GetNodesAsVecF() []utils.VecF

	// concerning only last state
	GetActionBuffer(int) (string, bool)
	ResetActionBuffer() // for over the board play

	// the pids are not here because the way how it is used
	Holdable(nid int) bool
	Holded(nid int)

	Tools() []tuple.T2[string, *ebiten.Image]
	ShowTools(int, int) bool
	Tap(pid, nid int, tool string)
	MissTap()

	PlayerText(pid int) ([]string, []string)
	Territory(pid int) string
	Soldiers(pid int) string

	TutorialText() []string

	// history control
	HistoryBoardStates() int // returns number of already seen states, thus it starts at 1
	Draw(screen *ebiten.Image, camera *gulrot.Camera, historyState int, drawBuffers bool)
	StateStr() (string, bool)

	// for updating of Font Faces and animations
	Update()
}
