package server

// ====================
// # server to database

type sdType byte

const (
	sdRegisterReqT sdType = iota
	sdLoginReqT

	sdLogoutMsgT
	sdForgotPasswordMsgT
)

type sd struct {
	t sdType

	sdForgotPasswordMsg ForgotPassword
	sdRegisterReq
	sdLoginReq
	sd_logoutMsg User
}

type sdRegisterReq struct {
	c2   *client2
	data RegisterReq
}

type sdLoginReq struct {
	c2   *client2
	data LoginReq
}

// ====================
// # database to server

type dsType byte

const (
	dsRegisterResT dsType = iota
	dsLoginResT
)

type ds struct {
	t dsType

	dsRegisterRes
	dsLoginRes
}

type dsRegisterRes struct {
	c2   *client2
	data GeneralRes
}

type dsLoginRes struct {
	c2   *client2
	ok   bool
	user User
}

// ===================
// # client1 to server

type c1sType byte

const (
	c1sRegisterT c1sType = iota
	c1sC0MsgT
	c1sCloseT
)

type c1s struct {
	t          c1sType
	fromServer chan ServerToClient

	msg ClientToServer
}

// ================ //
// DATABASE TO MAIL //

type dm struct {
	email       string
	newPassword string
}
