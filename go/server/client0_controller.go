package server

type ClientControllerMode byte

const (
	Dead ClientControllerMode = iota
	Pending
	Waiting
	Streaming
)

type ClientController struct {
	client     *Client
	mode       ClientControllerMode
	address    string
	testingEnv bool // if use wss or ws
}

func NewClientController(address string, testingEnv bool) *ClientController {
	return &ClientController{
		address:    address,
		mode:       Dead,
		testingEnv: testingEnv,
	}
}

func (c *ClientController) Mode() ClientControllerMode {
	return c.mode
}

func (c *ClientController) TryConnect() error {
	if !c.dead() {
		panic("already existing connection")
	}
	var err error
	c.client, err = RunClient0(c.address, c.testingEnv)
	if c.client == nil {
		return err
	}
	c.mode = Pending
	return nil
}

func (c *ClientController) Kill() {
	if c.dead() {
		panic("the client is dead")
	}
	c.client.Kill()
	c.client = nil
	c.mode = Dead
}

func (c *ClientController) Send(msg ClientToServer) {
	switch c.mode {
	case Dead:
		panic("the client is dead")
	case Pending:
		c.mode = Waiting
	case Waiting:
		panic("dont send if already waiting")
	}
	c.client.Send(msg)
}

func (c *ClientController) Update(receiver func(ServerToClient)) bool {
	if c.dead() {
		panic("the client is dead")
	}
	for {
		msg, code := c.client.Receive()
		switch code {
		case 0:
			switch c.mode {
			case Pending:
				panic("something went wrong")
			case Waiting:
				c.mode = Pending
				if msg.T == LobbyMsgT {
					c.mode = Streaming
				} else if msg.T == LoginResT && msg.LoginRes.T == SuccessReconnected {
					c.mode = Streaming
				}
				receiver(msg)
			case Streaming:
				if msg.T == LeaveLobbyResT {
					c.mode = Pending
				} else if msg.T == LeaveRoomResT {
					c.mode = Pending
				}
				receiver(msg)
			}
		case 1:
			return true
		case 2:
			c.Kill()
			return false
		}
	}
}

func (c *ClientController) dead() bool {
	return c.client == nil
}
