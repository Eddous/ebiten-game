package server

import (
	"context"
	"errors"
	"fmt"
	"gra/utils"
	"gra/version"
	"log"
	"net/http"
	"os"
	"strconv"
	"sync/atomic"
	"time"

	"github.com/coder/websocket"
	"github.com/coder/websocket/wsjson"
)

type Config struct {
	Address         string
	Port            string
	LogDirPath      string
	CertificatePath string // empty = not secured connections
	KeyPath         string // empty = not secured connections
	EmailPassword   string // empty = will not send emails
}

var c1Id uint64 = 0

func getId() uint64 {
	return atomic.AddUint64(&c1Id, 1)
}

const bufferSize = 100

func RunServer(config Config, gameFactory GenericGameFactoryInterface) {
	// Creating log file
	utils.CheckDirectory(config.LogDirPath)
	numName := utils.NextFileNumber(config.LogDirPath)

	logDirPath := config.LogDirPath + "/" + strconv.Itoa(numName) + "/"
	utils.CheckErr(os.Mkdir(logDirPath, os.ModePerm))

	clientToServer := make(chan c1s)
	serverToDatabase := make(chan sd, bufferSize)
	databaseToServer := make(chan ds)
	databaseToMail := make(chan dm, bufferSize)

	fmt.Println("Starting server")
	go newServer(gameFactory, clientToServer, serverToDatabase, databaseToServer, time.NewTicker(time.Second).C).run()

	fmt.Println("Connecting to DB")
	go runDatabase(serverToDatabase, databaseToServer, databaseToMail)

	if config.EmailPassword == "" {
		fmt.Println("Running mail agend (without password!!!)")
	} else {
		fmt.Println("Running mail agend")
	}
	go runMailAgend(config.EmailPassword, databaseToMail)

	fmt.Println("Starting accepting connection")

	fn := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		wc, err := websocket.Accept(w, r, &websocket.AcceptOptions{OriginPatterns: []string{config.Address}})
		if err != nil {
			log.Println(err)
			return
		}
		file, err := os.Create(logDirPath + strconv.FormatUint(getId(), 10) + ".log")
		utils.CheckErr(err)
		runClient1(wc, clientToServer, make(chan ServerToClient, bufferSize), file)
	})
	var err error
	if config.CertificatePath == "" || config.KeyPath == "" {
		err = http.ListenAndServe(config.Address+":"+config.Port, fn)
	} else {
		err = http.ListenAndServeTLS(config.Address+":"+config.Port, config.CertificatePath, config.KeyPath, fn)
	}
	log.Fatal(err)
}

func RunClient0(serverAddress string, testingEnv bool) (*Client, error) {
	ctx := context.Background()

	var wc *websocket.Conn
	var err error
	if testingEnv {
		wc, _, err = websocket.Dial(ctx, "ws://"+serverAddress, nil)
	} else {
		wc, _, err = websocket.Dial(ctx, "wss://"+serverAddress, nil)
	}

	if err != nil {
		return nil, err
	}

	// decoding the first msg (version)
	msg := ServerToClient{}
	err = wsjson.Read(ctx, wc, &msg)
	if err != nil {
		return nil, err
	}
	if msg.T != VersionT {
		return nil, errors.New("first msg from server was not VersionT")
	}
	if msg.Version != version.Version {
		return nil, errors.New("server(" + msg.Version + ") has a different version than client(" + version.Version + ")")
	}

	// running loop
	in := make(chan ServerToClient, 100)
	go func() {
	start:
		msg := ServerToClient{}
		err := wsjson.Read(ctx, wc, &msg)
		if err == nil {
			in <- msg
			goto start
		}
		// Dont logging error, because this goroutine runs until the connection is closed somewhere else
		wc.CloseNow() // TODO (CloseNow -> Close)
		close(in)
	}()
	return &Client{
		conn:    wc,
		context: ctx,
		in:      in,
	}, nil
}
