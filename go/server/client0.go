package server

import (
	"context"
	"log"

	"github.com/coder/websocket"
	"github.com/coder/websocket/wsjson"
)

type Client struct {
	conn    *websocket.Conn
	context context.Context
	in      chan ServerToClient
}

func (c *Client) Kill() {
	if err := c.conn.Close(websocket.StatusNormalClosure, "client went away"); err != nil {
		log.Println(err)
	}
}

// 0 - ok, 1 - not yet loaded, 2 - died
func (c *Client) Receive() (ServerToClient, byte) {
	select {
	case msg, ok := <-c.in:
		if !ok {
			return ServerToClient{}, 2 // already everything is cleaned up
		}
		return msg, 0
	default:
		return ServerToClient{}, 1
	}
}

func (c *Client) Send(out ClientToServer) {
	go func() {
		if err := wsjson.Write(c.context, c.conn, out); err != nil {
			log.Println(err)
			c.Kill()
		}
	}()
}
