package server

// game room updates client's gameroom and elo
// what if gameroom get stuck sending the ServerToClient
type gameRoom struct {
	factory GenericGameFactoryInterface
	game    GenericGameInterface
	clients []*client2

	args  []byte
	seed  int64
	names []string
	eloes []float64

	competetive bool
	eloType     string
}

// ===========================
// # interface for main server

func (g *gameRoom) time() {
	outMsg := g.game.Time()
	if outMsg != nil {
		sendMsgToC0s(g.clients, ServerToClient{T: GameMsgT, GameMsg: outMsg})
	}
}

// returns if the gameroom is dead
func (g *gameRoom) playerAction(c *client2, action []byte) {
	outMsg := g.game.PlayerMsg(c.user.Name, action)
	if outMsg != nil {
		sendMsgToC0s(g.clients, ServerToClient{T: GameMsgT, GameMsg: outMsg})
	}
}

// gameroom will remove players *gameroom and will update its elo
// returns if the gameroom have ended
// only way how the player can leave
func (g *gameRoom) playerWantLeaveOrLostConnection(c *client2, leave bool) bool {
	var text string
	if leave {
		text = "Player Left"
	} else {
		text = "Connection Lost"
	}

	outMsg := g.game.KillPlayers([]string{c.user.Name}, text)
	if outMsg != nil {
		sendMsgToC0s(g.clients, ServerToClient{T: GameMsgT, GameMsg: outMsg})
	}

	if g.competetive {
		c.user.Eloes[g.eloType] = g.game.PlayerElo(c.user.Name)
	}
	if leave {
		sendMsgToC0(c, ServerToClient{T: LeaveRoomResT, LeaveRoomRes: *c.user})
	}

	c.gameRoom = nil

	new := []*client2{}
	for _, c2 := range g.clients {
		if c2 != c {
			new = append(new, c2)
		}
	}
	g.clients = new
	return len(g.clients) == 0
}

func (g *gameRoom) reconnectingMsg() []byte {
	return g.game.Serialize()
}

// # interface for main server
// ===========================
