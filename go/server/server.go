package server

import (
	"fmt"
	"gra/datstruct"
	"gra/version"
	"math/rand"
	"strconv"
	"time"

	"golang.org/x/crypto/bcrypt"
)

const maxTimeToReconnect = 60

type server struct {
	gameFactory GenericGameFactoryInterface

	toDatabase   chan<- sd
	fromClient   <-chan c1s
	fromDatabase <-chan ds
	ticker       <-chan time.Time

	privLobbies map[LobbyId]*lobby
	pubLobbies  map[string]*lobby // key is string(incoming bytes)

	gameRooms datstruct.Set[*gameRoom]

	alive        map[chan ServerToClient]*client2 // players alive
	reconnecting map[*client2]int                 // server add, remove, check if gameroom is not over
	signedIn     map[string]*client2
}

func (s *server) String() string {
	var str string
	str += "toDatabase: " + strconv.Itoa(len(s.toDatabase)) + "\n"
	str += "fromClient: " + strconv.Itoa(len(s.fromClient)) + "\n"
	str += "fromDatabase: " + strconv.Itoa(len(s.fromDatabase)) + "\n"

	str += "privLobbies: " + strconv.Itoa(len(s.privLobbies)) + "\n"
	str += "pubCompLobbies: " + strconv.Itoa(len(s.pubLobbies)) + "\n"
	str += "gameRooms: " + strconv.Itoa(s.gameRooms.Size()) + "\n"

	str += "alive: " + strconv.Itoa(len(s.alive)) + "\n"
	str += "reconnecting: " + strconv.Itoa(len(s.reconnecting)) + "\n"
	str += "signedIn: " + strconv.Itoa(len(s.signedIn))
	return str
}

func newServer(gameFactory GenericGameFactoryInterface, fromClient <-chan c1s, toDatabase chan<- sd, fromDatabase <-chan ds, ticker <-chan time.Time) *server {
	return &server{
		gameFactory: gameFactory,

		toDatabase:   toDatabase,
		fromClient:   fromClient,
		fromDatabase: fromDatabase,
		ticker:       ticker,

		privLobbies: map[LobbyId]*lobby{},
		pubLobbies:  map[string]*lobby{},
		gameRooms:   datstruct.NewSet[*gameRoom](),

		alive:        map[chan ServerToClient]*client2{},
		reconnecting: make(map[*client2]int),
		signedIn:     map[string]*client2{},
	}
}

func (s *server) run() {
	for {
		select {
		case <-s.ticker:
			s.tEvent()
			// fmt.Println(s)
			// fmt.Println("=======================================")
		case msg := <-s.fromDatabase:
			switch msg.t {
			case dsRegisterResT:
				s.dRegister(msg.dsRegisterRes)
			case dsLoginResT:
				s.dLogin(msg.dsLoginRes)
			default:
				fmt.Println("no such option for database msg: ", msg)
			}
		case c1Msg := <-s.fromClient:
			switch c1Msg.t {
			case c1sRegisterT:
				s.c1Register(c1Msg.fromServer)
			case c1sCloseT:
				s.c1Close(c1Msg.fromServer)
			case c1sC0MsgT:
				c0Msg := c1Msg.msg
				c2 := s.alive[c1Msg.fromServer]
				switch c0Msg.T {
				case RegisterReqT:
					s.c0RegisterReq(c2, c0Msg.RegisterReq)
				case LoginReqT:
					s.c0LoginReq(c2, c0Msg.LoginReq)
				case LeaveLobbyReqT:
					s.c0LeaveLobbyReq(c2)
				case LeaveRoomReqT:
					s.c0LeaveRoomReq(c2)
				case GameActionMsgT:
					s.c0GameActionMsg(c2, c0Msg.GameActionMsg)
				case CreatePrivateLobbyReqT:
					s.c0CreatePrivateLobbyReq(c2, c0Msg.CreatePrivateLobbyReq)
				case JoinPrivateLobbyReqT:
					s.c0JoinPrivateLobbyReq(c2, c0Msg.JoinPrivateLobbyReq)
				case PublicLobbyReqT:
					s.c0PublicLobbyReq(c2, c0Msg.PublicLobbyReq)
				case ForgotPasswordT:
					s.c0ForgotPassword(c2, c0Msg.ForgotPassword)
				case ChangeEmailT:
					s.c0ChangeEmail(c2, c0Msg.ChangeEmail)
				case ChangePasswordT:
					s.c0ChangePassword(c2, c0Msg.ChangePassword)
				default:
					fmt.Println("no such option for c0 msg: ", c1Msg)
				}
			default:
				fmt.Println("no such option for c1 msg: ", c1Msg)
			}
		}
	}
}

// ==============
// # ticker event

// must check reconnecting
func (s *server) tEvent() {
	for room := range s.gameRooms.Iterator() {
		room.time()
	}
	for c2 := range s.reconnecting {
		s.reconnecting[c2]--
		if s.reconnecting[c2] <= 0 {
			room := c2.gameRoom
			if room.playerWantLeaveOrLostConnection(c2, false) {
				s.gameRooms.Remove(room)
			}
			s.removeUserFromServer(c2.user)
			delete(s.reconnecting, c2)
		}
	}
}

// =================
// # database events

func (s *server) dRegister(msg dsRegisterRes) {
	if msg.c2.state() != notAuth {
		return
	}
	sendMsgToC0(msg.c2,
		ServerToClient{
			T:          GeneralResT,
			GeneralRes: msg.data,
		},
	)
}

func (s *server) dLogin(msg dsLoginRes) {
	c2 := msg.c2
	user := msg.user
	if c2.state() != notAuth {
		// the connection can be already dead, in that case server does not register c2
		return
	}
	if !msg.ok {
		sendMsgToC0(
			c2,
			ServerToClient{
				T: LoginResT,
				LoginRes: LoginRes{
					T:     Err,
					Error: "Wrong credentials.",
				},
			},
		)
		return
	}
	if _, ok := s.signedIn[user.Name]; ok {
		sendMsgToC0(
			c2,
			ServerToClient{
				T: LoginResT,
				LoginRes: LoginRes{
					T:     Err,
					Error: "A player with this name already\nexists on the server.",
				},
			},
		)
		return
	}
	sendMsgToC0(
		c2,
		ServerToClient{
			T: LoginResT,
			LoginRes: LoginRes{
				T:   Success,
				You: user,
			},
		},
	)
	c2.user = &user
	s.signedIn[user.Name] = c2
}

// ================
// # client1 events

func (s *server) c1Register(fromServer chan ServerToClient) {
	c := newClient2(fromServer)
	s.alive[fromServer] = c

	// sending first msg, server DOES NOT CARE what it will do with a client,
	// thus there is no special client state
	fromServer <- ServerToClient{T: VersionT, Version: version.Version}
}

func (s *server) c1Close(fromServer chan ServerToClient) {
	c2 := s.alive[fromServer]
	state := c2.state()

	c2.fromServer = nil
	delete(s.alive, fromServer)
	close(fromServer)

	switch state {
	case notAuth:
		// nothing
	case inMain:
		s.removeUserFromServer(c2.user)
	case inLobby:
		s.removePlayerFromLobby(c2)
		s.removeUserFromServer(c2.user)
	case inGame:
		s.reconnecting[c2] = maxTimeToReconnect
	default:
		panic("not such state")
	}
}

// ================
// # client0 events

func (m *server) c0RegisterReq(c2 *client2, msg RegisterReq) {
	if c2.state() != notAuth {
		return
	}

	if ok, err := checkName(msg.Name); !ok {
		sendMsgToC0(c2,
			ServerToClient{
				T: GeneralResT,
				GeneralRes: GeneralRes{
					Ok:  false,
					Err: err,
				},
			},
		)
	} else {
		m.toDatabase <- sd{
			t: sdRegisterReqT,
			sdRegisterReq: sdRegisterReq{
				c2:   c2,
				data: msg,
			},
		}
	}
}

func (s *server) c0LoginReq(c2 *client2, msg LoginReq) {
	if c2.state() != notAuth {
		return
	}
	name := msg.Name
	password := msg.Password

	switch s.canC2Reconnect(c2, name, password) {
	case 0:
		s.toDatabase <- sd{
			t: sdLoginReqT,
			sdLoginReq: sdLoginReq{
				c2:   c2,
				data: msg,
			},
		}
		return
	case 1:
		sendMsgToC0(
			c2,
			ServerToClient{
				T: LoginResT,
				LoginRes: LoginRes{
					T:     Err,
					Error: "Wrong credentials.",
				},
			},
		)
		return
	case 2:
		sendMsgToC0(c2,
			ServerToClient{
				T: LoginResT,
				LoginRes: LoginRes{
					T:     Err,
					Error: "A player with this name already\nexists on the server.",
				},
			},
		)
		return
	case 3:
		oldC2 := s.signedIn[name]
		delete(s.reconnecting, oldC2)
		oldC2.fromServer = c2.fromServer
		s.alive[c2.fromServer] = oldC2

		sendMsgToC0(c2,
			ServerToClient{
				T: LoginResT,
				LoginRes: LoginRes{
					T:           SuccessReconnected,
					You:         *oldC2.user,
					InitRoomMsg: oldC2.gameRoom.reconnectingMsg(),
				},
			},
		)
		return
	}
}

func (s *server) c0LeaveLobbyReq(c2 *client2) {
	if c2.state() != inLobby {
		return
	}
	s.removePlayerFromLobby(c2)
	sendMsgToC0(c2, ServerToClient{T: LeaveLobbyResT})
}

func (s *server) c0LeaveRoomReq(c2 *client2) {
	if c2.state() != inGame {
		return
	}

	room := c2.gameRoom
	if room.playerWantLeaveOrLostConnection(c2, true) {
		s.gameRooms.Remove(room)
	}
}

func (s *server) c0GameActionMsg(c2 *client2, action []byte) {
	if c2.state() != inGame {
		return
	}
	c2.gameRoom.playerAction(c2, action)
}

func (s *server) c0CreatePrivateLobbyReq(c2 *client2, msg []byte) {
	if c2.state() != inMain {
		return
	}

	var newId LobbyId
	for {
		newId = LobbyId(rand.Uint32())
		if s.privLobbies[newId] == nil {
			break
		}
	}

	lobby := newLobby(false, newId, s.gameFactory, msg)
	if lobby == nil {
		sendMsgToC0(
			c2,
			ServerToClient{T: LobbyErrT},
		)
		return
	}
	s.privLobbies[lobby.lid] = lobby
	s.addC2ToLobby(c2, lobby)
}

func (s *server) c0JoinPrivateLobbyReq(c2 *client2, lid LobbyId) {
	if c2.state() != inMain {
		return
	}

	if _, ok := s.privLobbies[lid]; !ok {
		sendMsgToC0(
			c2,
			ServerToClient{
				T: LobbyErrT,
			},
		)
		return
	}
	s.addC2ToLobby(c2, s.privLobbies[lid])
}

func (s *server) c0PublicLobbyReq(c2 *client2, args []byte) {
	if c2.state() != inMain {
		return
	}
	key := string(args)
	if s.pubLobbies[key] == nil {
		l := newLobby(true, 0, s.gameFactory, args)
		if l == nil {
			sendMsgToC0(c2, ServerToClient{T: LobbyErrT})
			return
		}
		s.pubLobbies[key] = l
		s.addC2ToLobby(c2, s.pubLobbies[key])
	} else {
		s.addC2ToLobby(c2, s.pubLobbies[key])
	}
}

func (s *server) c0ChangePassword(c2 *client2, password string) {
	if c2.state() != inMain {
		return
	}
	encrypted, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	if err != nil {
		sendMsgToC0(
			c2,
			ServerToClient{
				T: GeneralResT,
				GeneralRes: GeneralRes{
					Ok:  false,
					Err: err.Error(),
				},
			},
		)
		return
	}
	c2.user.EncryptedPassword = encrypted
	sendMsgToC0(
		c2,
		ServerToClient{
			T: GeneralResT,
			GeneralRes: GeneralRes{
				Ok: true,
			},
		},
	)
}

func (s *server) c0ChangeEmail(c2 *client2, email string) {
	if c2.state() != inMain {
		return
	}
	c2.user.Email = email
	sendMsgToC0(
		c2,
		ServerToClient{
			T: GeneralResT,
			GeneralRes: GeneralRes{
				Ok: true,
			},
		},
	)
}

func (s *server) c0ForgotPassword(c2 *client2, msg ForgotPassword) {
	if c2.state() != notAuth {
		return
	}
	if _, ok := s.signedIn[msg.Name]; ok {
		sendMsgToC0(
			c2,
			ServerToClient{
				T: GeneralResT,
				GeneralRes: GeneralRes{
					Err: "The player is playing.",
					Ok:  false,
				},
			},
		)
		return
	}
	s.toDatabase <- sd{
		t:                   sdForgotPasswordMsgT,
		sdForgotPasswordMsg: msg,
	}
	sendMsgToC0(
		c2,
		ServerToClient{
			T: GeneralResT,
			GeneralRes: GeneralRes{
				Ok: true,
			},
		},
	)
}

// =========
// # helpers

func (s *server) removeUserFromServer(u *User) {
	delete(s.signedIn, u.Name)
	s.toDatabase <- sd{
		t:            sdLogoutMsgT,
		sd_logoutMsg: *u,
	}
}

func (s *server) addC2ToLobby(c2 *client2, l *lobby) {
	if g := l.addClient(c2); g != nil {
		s.gameRooms.Add(g)
		s.deleteLobby(l)
	}
}

func (s *server) removePlayerFromLobby(c2 *client2) {
	l := c2.lobby
	if l.untangleClient(c2) {
		s.deleteLobby(l)
	}
}

func (s *server) deleteLobby(l *lobby) {
	switch l.competetive {
	case false:
		delete(s.privLobbies, l.lid)
	case true:
		delete(s.pubLobbies, l.key)
	}
}

// 0 - c0 is not here,
// 1 - c0 is here but you have wrong credentials,
// 2 - c0 is here but not reconnecting,
// 3 - you can reconnect
func (s *server) canC2Reconnect(_ *client2, name, password string) byte {
	oldC2 := s.signedIn[name]
	if oldC2 == nil {
		return 0
	}
	err := bcrypt.CompareHashAndPassword(oldC2.user.EncryptedPassword, []byte(password))
	if err != nil {
		return 1
	}
	if oldC2.state() != reconnecting {
		return 2
	}
	return 3
}

func checkName(name string) (bool, string) {
	if len(name) == 0 {
		return false, "The name cannot be empty."
	}
	if len(name) > 20 {
		return false, "The name is too long."
	}
	for _, r := range name {
		if r <= 32 || r >= 127 {
			return false, "The name can only contain ASCII printable non-white characters."
		}
	}
	return true, ""
}
