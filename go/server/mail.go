package server

import (
	"fmt"
	"log"
	"net/smtp"
)

func runMailAgend(pass string, pipe <-chan dm) {
	for {
		msg := <-pipe
		send(pass, msg.email, msg.newPassword)
	}
}

func send(pass string, email string, newPassword string) {
	if pass == "" {
		fmt.Println("don't have a password for mail")
		return
	}

	from := "grrra.game@gmail.com"
	to := email
	msg := "From: " + from + "\n" +
		"To: " + to + "\n" +
		"Subject: Password reset\n\n" +
		"new password: " + newPassword + "\n"

	err := smtp.SendMail(
		"smtp.gmail.com:587",
		smtp.PlainAuth("", from, pass, "smtp.gmail.com"),
		from, []string{to}, []byte(msg),
	)

	if err != nil {
		log.Printf("smtp error: %s", err)
	}
}
