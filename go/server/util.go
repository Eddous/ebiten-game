package server

func namesAndEloes(c2s []*client2, eloType string, defaultElo float64) ([]string, []float64) {
	names := []string{}
	eloes := []float64{}
	for _, c := range c2s {
		names = append(names, c.user.Name)
		elo, ok := c.user.Eloes[eloType]
		if !ok {
			elo = defaultElo
		}
		eloes = append(eloes, elo)
	}
	return names, eloes
}

func sendMsgToC0s(c2s []*client2, msg ServerToClient) {
	for _, c2 := range c2s {
		if c2.isListening() {
			sendMsgToC0(c2, msg)
		}
	}
}

func sendMsgToC0(c2 *client2, msg ServerToClient) {
	if !c2.isListening() {
		panic("cant send msg to the dead c2")
	}
	c2.fromServer <- msg
}
