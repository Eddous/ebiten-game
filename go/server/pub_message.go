package server

import (
	"strconv"

	"github.com/davecgh/go-spew/spew"
)

// ===================
// # client0 to server

// each ClientToServerType denotes ONE function which is called on the server
// ClientToServer holds WHICH FUNCTION IS CALLED and the data WHICH THE FUNCTION NEEDS

type ClientToServerType byte

const (
	RegisterReqT    ClientToServerType = iota // -> GeneralResT
	LoginReqT                                 // -> LoginResT
	ForgotPasswordT                           // -> GeneralResT

	ChangeEmailT           // -> GeneralResT
	ChangePasswordT        // -> GeneralResT
	CreatePrivateLobbyReqT // -> LobbyErrT or LobbyMsg
	JoinPrivateLobbyReqT   // -> LobbyErrT or LobbyMsg
	PublicLobbyReqT        // -> LobbyErrT or LobbyMsg

	LeaveLobbyReqT // -> LeaveLobbyResT or continue streaming
	LeaveRoomReqT  // -> LeaveRoomResT or continue streaming
	GameActionMsgT // nothing
)

type ClientToServer struct {
	T ClientToServerType

	ForgotPassword        ForgotPassword
	ChangeEmail           string
	ChangePassword        string
	RegisterReq           RegisterReq
	AnonLoginReq          string
	LoginReq              LoginReq
	CreatePrivateLobbyReq []byte
	JoinPrivateLobbyReq   LobbyId
	PublicLobbyReq        []byte
	GameActionMsg         []byte
}

type ForgotPassword struct {
	Name, Mail string
}

type RegisterReq struct {
	Name     string
	Password string
	Mail     string
}

type LoginReq struct {
	Name     string
	Password string
}

func (msg ClientToServer) String() string {
	var str string
	switch msg.T {
	case RegisterReqT:
		str = spew.Sdump(msg.RegisterReq)
	case LoginReqT:
		str = spew.Sdump(msg.LoginReq)
	case CreatePrivateLobbyReqT:
		str = spew.Sdump(msg.CreatePrivateLobbyReq)
	case JoinPrivateLobbyReqT:
		str = spew.Sdump(msg.JoinPrivateLobbyReq)
	case PublicLobbyReqT:
		str = spew.Sdump(msg.PublicLobbyReq)
	case LeaveLobbyReqT:
		str = "LeaveLobbyReq\n"
	case LeaveRoomReqT:
		str = "LeaveRoomReq\n"
	case GameActionMsgT:
		str = spew.Sdump(msg.GameActionMsg)
	case ForgotPasswordT:
		str = spew.Sdump(msg.ForgotPassword)
	case ChangeEmailT:
		str = spew.Sdump(msg.ChangeEmail)
	case ChangePasswordT:
		str = spew.Sdump(msg.ChangePassword)
	default:
		str = "not a supported type of ServerToClient message" + strconv.Itoa(int(msg.T)) + "\n"
	}
	return str[:len(str)-1]
}

// ===================
// # server to client0

type ServerToClientType byte

const (
	VersionT ServerToClientType = iota

	GeneralResT
	LoginResT

	// inMain state
	LobbyErrT
	LeaveLobbyResT // if this message come, the leave was successful
	LeaveRoomResT

	LobbyMsgT // stream of messages
	InitRoomMsgT
	GameMsgT
)

type ServerToClient struct {
	T ServerToClientType

	Version      string
	GeneralRes   GeneralRes
	LoginRes     LoginRes
	LeaveRoomRes User
	InitRoomMsg  []byte
	GameMsg      []byte
	LobbyMsg     LobbyMsg
}

type GeneralRes struct {
	Ok  bool
	Err string
}

type LoginResType byte

const (
	Err LoginResType = iota
	Success
	SuccessReconnected
)

type LoginRes struct {
	T           LoginResType
	Error       string // Err
	You         User   // Success, SuccessReconnected
	InitRoomMsg []byte // SuccessReconnected
}

type LobbyMsg struct {
	LobbyId LobbyId // 0 - the request was not create private lobby

	Names []string
	Eloes []float64

	Players int
}

func (msg ServerToClient) String() string {
	var str string
	switch msg.T {
	case VersionT:
		str = "Server version msg, version: " + msg.Version + "\n"
	case GeneralResT:
		str = spew.Sdump(msg.GeneralRes)
	case LoginResT:
		str = spew.Sdump(msg.LoginRes)
	case LobbyErrT:
		str = "PrivateLobbyErr\n"
	case InitRoomMsgT:
		str = spew.Sdump(msg.InitRoomMsg)
	case GameMsgT:
		str = spew.Sdump(msg.GameMsg)
	case LobbyMsgT:
		str = spew.Sdump(msg.LobbyMsg)
	case LeaveLobbyResT:
		str = "LeaveLobbyRes\n"
	case LeaveRoomResT:
		str = spew.Sdump(msg.LeaveRoomRes)
	default:
		str = "not a supported type of ServerToClient message " + strconv.Itoa(int(msg.T)) + "\n"
	}
	return str[:len(str)-1]
}
