package server

type User struct {
	Name              string
	Eloes             map[string]float64
	Email             string
	WasEmailValidated bool
	EncryptedPassword []byte
}
