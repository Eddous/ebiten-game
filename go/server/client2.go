package server

type client2State int

const (
	notAuth client2State = iota
	inMain
	inLobby
	inGame

	reconnecting
	dead
)

type client2 struct {
	fromServer chan ServerToClient

	user     *User
	lobby    *lobby
	gameRoom *gameRoom
}

func newClient2(fromServer chan ServerToClient) *client2 {
	return &client2{fromServer: fromServer}
}

func (c *client2) isListening() bool {
	s := c.state()
	return s != dead && s != reconnecting
}

func (c *client2) state() client2State {
	if c.fromServer == nil && c.user == nil && c.lobby == nil && c.gameRoom == nil {
		return dead
	}
	if c.fromServer == nil && c.user != nil && c.lobby == nil && c.gameRoom == nil {
		return dead
	}
	if c.fromServer == nil && c.user != nil && c.lobby != nil && c.gameRoom == nil {
		return dead
	}
	if c.fromServer == nil && c.user != nil && c.lobby == nil && c.gameRoom != nil {
		return reconnecting
	}
	if c.fromServer != nil && c.user == nil && c.lobby == nil && c.gameRoom == nil {
		return notAuth
	}
	if c.fromServer != nil && c.user != nil && c.lobby == nil && c.gameRoom == nil {
		return inMain
	}
	if c.fromServer != nil && c.user != nil && c.lobby == nil && c.gameRoom != nil {
		return inGame
	}
	if c.fromServer != nil && c.user != nil && c.lobby != nil && c.gameRoom == nil {
		return inLobby
	}
	panic("bad state")
}
