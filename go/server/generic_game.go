package server

type GenericGameFactoryInterface interface {
	NewGame(generatorArgs []byte) (game GenericGameInterface, players int, eloType string, defaultElo float64)
}

type GenericGameInterface interface {
	Init(users []string, eloes []float64)

	// The events which has the effect on the Game, the Game can send synchronization msg
	Time() (gameMsg []byte)
	PlayerMsg(id string, playerMsg []byte) (gameMsg []byte)
	KillPlayers(ids []string, text string) (gameMsg []byte)

	// the method called by the server after the player left to calculate ELO => player is dead | the game is over
	PlayerElo(id string) (newElo float64)

	Serialize() []byte
}
