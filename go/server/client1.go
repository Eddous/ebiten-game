package server

import (
	"context"
	"log"
	"os"

	"github.com/coder/websocket"
	"github.com/coder/websocket/wsjson"
)

func runClient1(conn *websocket.Conn, toServer chan<- c1s, fromServer chan ServerToClient, logFile *os.File) {
	ctx := context.Background()
	logger := log.New(logFile, "", 0)

	logMsg(logger, "c1_info", "initialized")

	toServer <- c1s{
		t:          c1sRegisterT,
		fromServer: fromServer,
	}

	fromUser := make(chan ClientToServer)

	go func() {
		for {
			msg := ClientToServer{}
			err := wsjson.Read(ctx, conn, &msg)
			if err != nil {
				logMsg(logger, "c1_info", "decoding error "+err.Error())

				// since this is response to error I think that CloseNow() is correct
				conn.CloseNow()
				close(fromUser)
				return
			}
			fromUser <- msg
		}
	}()

	for {
		select {
		case msg, ok := <-fromUser:
			if !ok {
				logMsg(logger, "c1_info", "decoder goroutine closed connection")
				endCommunicationWithServer(toServer, fromServer, logger)

				logMsg(logger, "c1_info", "terminating")
				logFile.Close()
				return
			}

			toServer <- c1s{
				t:          c1sC0MsgT,
				fromServer: fromServer,
				msg:        msg,
			}
			logMsg(logger, "client -> server", msg.String())
		case msg, ok := <-fromServer:
			if !ok {
				logMsg(logger, "c1_info", "server goroutine closed communication, killing the connection")
				endCommunicationWithUser(conn, fromUser, logger)

				logMsg(logger, "c1_info", "terminating")
				logFile.Close()
				return
			}
			if err := wsjson.Write(ctx, conn, msg); err != nil {
				logMsg(logger, "c1_info", "encoding error "+err.Error())
				endCommunicationWithServer(toServer, fromServer, logger)
				endCommunicationWithUser(conn, fromUser, logger)

				logMsg(logger, "c1_info", "terminating")
				logFile.Close()
				return
			}
			logMsg(logger, "server -> client", msg.String())
		}
	}
}

func endCommunicationWithUser(conn *websocket.Conn, fromUser chan ClientToServer, logger *log.Logger) {
	conn.Close(websocket.StatusNormalClosure, "")
	for range fromUser {
	}
	logMsg(logger, "c1_info", "communication with receiver goroutine sucessfully closed")
}

func endCommunicationWithServer(toServer chan<- c1s, fromServer chan ServerToClient, logger *log.Logger) {
	toServer <- c1s{
		t:          c1sCloseT,
		fromServer: fromServer,
	}

	for range fromServer {
	}
	logMsg(logger, "c1_info", "communication with server goroutine sucessfully closed")
}

func logMsg(logger *log.Logger, header, body string) {
	logger.Println(header)
	logger.Println()
	logger.Println(body)
	logger.Println("===================================================================================")
}
