package server

import (
	"context"
	"encoding/json"
	"fmt"
	"gra/utils"
	"log"
	"math/rand"

	"github.com/redis/go-redis/v9"
	"golang.org/x/crypto/bcrypt"
)

type database struct {
	redis             *redis.Client
	toMail            chan<- dm
	passwordGenerator *rand.Rand
}

var database_ctx = context.Background()

func runDatabase(fromServer <-chan sd, toServer chan<- ds, toMail chan<- dm) {
	r := redis.NewClient(
		&redis.Options{
			Addr:     "localhost:6379",
			Password: "",
			DB:       0,
		},
	)

	db := database{
		redis:             r,
		toMail:            toMail,
		passwordGenerator: rand.New(rand.NewSource(0)),
	}

	// listening
	for {
		msg := <-fromServer
		switch msg.t {
		case sdRegisterReqT:
			res := db.register(msg.sdRegisterReq)
			toServer <- ds{
				t:             dsRegisterResT,
				dsRegisterRes: res,
			}
		case sdLoginReqT:
			res := db.login(msg.sdLoginReq)
			toServer <- ds{
				t:          dsLoginResT,
				dsLoginRes: res,
			}
		case sdLogoutMsgT:
			db.logout(msg.sd_logoutMsg)
		case sdForgotPasswordMsgT:
			db.forgotPassword(msg.sdForgotPasswordMsg)
		}
	}
}

func (db database) register(msg sdRegisterReq) dsRegisterRes {
	return dsRegisterRes{c2: msg.c2, data: db.registerHelp(msg)}
}

func (db database) registerHelp(msg sdRegisterReq) GeneralRes {
	if _, ok := db.get(dbUser, msg.data.Name); ok {
		return GeneralRes{Ok: false, Err: "The name already exist."}
	}
	encrypted, err := bcrypt.GenerateFromPassword([]byte(msg.data.Password), bcrypt.DefaultCost)
	if err != nil {
		return GeneralRes{Ok: false, Err: err.Error()}
	}
	user := User{
		Name:              msg.data.Name,
		Email:             msg.data.Mail,
		WasEmailValidated: true, // TODO
		EncryptedPassword: encrypted,
		Eloes:             map[string]float64{},
	}
	db.set(dbUser, user.Name, string(utils.ToBytes(user)))
	return GeneralRes{Ok: true}
}

func (db database) login(msg sdLoginReq) dsLoginRes {
	var user User
	val, ok := db.get(dbUser, msg.data.Name)
	if !ok {
		return dsLoginRes{c2: msg.c2, ok: false}
	}
	utils.CheckErr(json.Unmarshal([]byte(val), &user))
	err := bcrypt.CompareHashAndPassword(user.EncryptedPassword, []byte(msg.data.Password))
	if err != nil {
		return dsLoginRes{c2: msg.c2, ok: false}
	}
	return dsLoginRes{
		c2:   msg.c2,
		ok:   true,
		user: user,
	}
}

func (db database) logout(msg User) {
	db.set(dbUser, msg.Name, string(utils.ToBytes(msg)))
}

func (db database) forgotPassword(msg ForgotPassword) {
	var user User
	val, ok := db.get(dbUser, msg.Name)
	if !ok {
		return
	}
	utils.CheckErr(json.Unmarshal([]byte(val), &user))
	if user.Email != msg.Mail || !user.WasEmailValidated {
		return
	}

	// generating new password
	new := fmt.Sprintf("%04d", db.passwordGenerator.Intn(9999))
	var err error
	user.EncryptedPassword, err = bcrypt.GenerateFromPassword([]byte(new), bcrypt.DefaultCost)
	if err != nil {
		log.Println(err)
		return
	}
	db.set(dbUser, user.Name, string(utils.ToBytes(user)))
	db.toMail <- dm{email: msg.Mail, newPassword: new}
}

// =========
// # helpers

// I want to store oter things (like played games... and maybe something else)
type dbDataType string

const (
	dbUser dbDataType = "user:"
)

func (db database) set(t dbDataType, key, val string) {
	err := db.redis.Set(database_ctx, string(t)+key, val, 0).Err()
	if err != nil {
		panic(err)
	}
}

func (db database) get(t dbDataType, key string) (string, bool) {
	val, err := db.redis.Get(database_ctx, string(t)+key).Result()
	if err == redis.Nil {
		return "", false
	} else if err != nil {
		panic(err)
	} else {
		return val, true
	}
}
