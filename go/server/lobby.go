package server

import "fmt"

type LobbyId uint16

type lobby struct {
	competetive bool
	lid         LobbyId
	key         string // key used in finding already existings games

	seed int64
	args []byte

	factory    GenericGameFactoryInterface
	game       GenericGameInterface
	players    int
	eloType    string
	defaultElo float64

	clients []*client2
}

// lid is only for private lobbies
func newLobby(competetive bool, lid LobbyId, factory GenericGameFactoryInterface, args []byte) *lobby {
	game, players, eloType, defaultElo := factory.NewGame(args)
	if game == nil {
		fmt.Println("Could not generate the lobby with the args: ", args)
		return nil
	}
	return &lobby{
		competetive: competetive,
		lid:         lid,
		key:         string(args),

		args: args,

		factory:    factory,
		game:       game,
		players:    players,
		eloType:    eloType,
		defaultElo: defaultElo,

		clients: []*client2{},
	}
}

// generates messages for all clients
func (l *lobby) addClient(c *client2) *gameRoom {
	c.lobby = l
	l.clients = append(l.clients, c)
	l.sendLobbyMsg()
	if len(l.clients) != l.players {
		return nil
	}

	// creating gameroom
	names, eloes := namesAndEloes(l.clients, l.eloType, l.defaultElo)
	l.game.Init(names, eloes)
	sendMsgToC0s(l.clients,
		ServerToClient{
			T:           InitRoomMsgT,
			InitRoomMsg: l.game.Serialize(),
		},
	)
	g := &gameRoom{
		factory:     l.factory,
		game:        l.game,
		clients:     l.clients,
		args:        l.args,
		seed:        l.seed,
		names:       names,
		eloes:       eloes,
		competetive: l.competetive,
		eloType:     l.eloType,
	}
	for _, c := range l.clients {
		c.lobby = nil
		c.gameRoom = g
	}
	return g
}

// generates messages for all clients
// true -> delete lobby
func (l *lobby) untangleClient(cli *client2) bool {
	cli.lobby = nil
	newClients := make([]*client2, 0)
	for _, c := range l.clients {
		if c != cli {
			newClients = append(newClients, c)
		}
	}
	l.clients = newClients
	l.sendLobbyMsg()
	return len(l.clients) == 0
}

func (l *lobby) sendLobbyMsg() {
	names, eloes := namesAndEloes(l.clients, l.eloType, l.defaultElo)
	sendMsgToC0s(l.clients,
		ServerToClient{
			T: LobbyMsgT,
			LobbyMsg: LobbyMsg{
				LobbyId: l.lid,
				Names:   names,
				Eloes:   eloes,
				Players: l.players,
			},
		},
	)
}
