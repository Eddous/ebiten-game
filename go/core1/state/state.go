package state

import (
	"gra/coreutils"
	"gra/datstruct"
	"gra/graphgen"
	"gra/utils"
	"math"
)

// read only pls
type State struct {
	Matrix                      [][]bool
	MinimalDistanceBetweenNodes []int // the index is number of players - if you play in two, then you will use [2] and rest you will ignore

	MoveType MoveType
	Players  []Player
	Nodes    []Node

	// this hold additional info about the graph: centrality measures
	GraphGenData *graphgen.Data
}

func NewState(players int, gameMap graphgen.Data) *State {
	if !utils.IsInInt(players, 2, 6) {
		return nil
	}

	plrs := make([]Player, players)
	for pid := 0; pid < players; pid++ {
		plrs[pid] = Player{
			Playing: true,
			// others are default
		}
	}
	nodes := make([]Node, 0)
	for nid, n := range gameMap.Nodes {

		neighbours := []int{}
		neighbourhood := []int{} // calculating it that way to have the neighbourhood sorted by nid (IDK if it is needed)

		for nid2 := range gameMap.Matrix[nid] {
			if gameMap.Matrix[nid][nid2] {
				neighbours = append(neighbours, nid2)
				neighbourhood = append(neighbourhood, nid2)
			}
			if nid == nid2 {
				neighbourhood = append(neighbourhood, nid2)
			}
		}

		nodes = append(nodes,
			Node{
				Pos: n,

				Neighbours:    neighbours,
				Neighbourhood: neighbourhood,
				Owner:         -1,
				// others are default
			},
		)
	}
	return &State{
		Matrix:                      gameMap.Matrix,
		MinimalDistanceBetweenNodes: coreutils.MinDistanceBetweenNodes(gameMap, players),
		MoveType:                    Negotation,
		Players:                     plrs,
		Nodes:                       nodes,
		GraphGenData:                &gameMap,
	}
}

func (s *State) CheckNegotationInput(pid int, nids []int) bool {
	if len(nids) != len(s.Playing()) {
		return false
	}
	for _, n := range nids {
		if !utils.IsInInt(n, 0, len(s.Nodes)-1) {
			return false
		}
	}
	for i := 0; i < len(nids)-1; i++ {
		for j := i + 1; j < len(nids); j++ {
			if s.NodesTooClose(nids[i], nids[j]) {
				return false
			}
		}
	}
	return true
}

func (s *State) CheckMoveInput(pid int, moves [][]int) bool {
	if moves == nil || len(moves) != len(s.Nodes) {
		return false
	}
	for i := range moves {
		if moves[i] == nil || len(moves[i]) != len(s.Nodes) {
			return false
		}
		for j := range moves {
			if moves[i][j] < 0 {
				return false
			}
			if moves[i][j] > s.Nodes[i].Soldiers {
				return false
			}
			if moves[i][j] != 0 {
				// self moves are allowed
				if !(s.Matrix[i][j] || i == j) {
					return false
				}
				if s.Nodes[i].Owner != pid {
					return false
				}
			}
		}
	}
	return true
}

func (s *State) CheckTerritoryInput(pid int, nid int) bool {
	return utils.IsInInt(nid, 0, len(s.Nodes)-1) && s.Nodes[nid].Owner == pid
}

func (s *State) CheckSoldiersInput(pid int, nids []int) bool {
	if len(nids) < s.Players[pid].MaxSoldiers-s.Players[pid].CurrSoldiers {
		return false
	}
	for _, nid := range nids {
		if !utils.IsInInt(nid, 0, len(s.Nodes)-1) || s.Nodes[nid].Owner != pid {
			return false
		}
	}
	return true
}

func (s *State) Kill(ids []int, deathText string) *State {
	new := s.copy()

	// get points to assign
	alive := 0
	for _, p := range new.Players {
		if !p.Dead {
			alive++
		}
	}

	var result coreutils.Result
	if len(ids) != alive {
		result = coreutils.Lost
	} else {
		result = coreutils.Draw
	}

	// kill players
	set := datstruct.NewSet[int]()
	for _, id := range ids {
		new.Players[id].kill(result, deathText)
		set.Add(id)
	}

	// reset nodes
	for i := range new.Nodes {
		n := &new.Nodes[i]
		if set.Has(n.Owner) {
			n.Reset()
		}
	}
	new.loadNextState(new.MoveType)
	return new
}

// map[pid][]nid
func (old *State) Negotation(nids map[int][]int) *State {
	if old.MoveType != Negotation {
		panic("wrong state")
	}

	new := old.copy()
	for pid, nid := range coreutils.MaxMinDistance(nids, new.GraphGenData.MinimalPathDistance) {
		node := &new.Nodes[nid]
		player := &new.Players[pid]

		node.Owner = pid
		node.Soldiers = 1
		player.CurrSoldiers = 1
		player.MaxSoldiers = 1
		player.Territory = 1
		player.Playing = true
	}
	new.MoveType = Moves
	return new
}

// it allows self-moves (soldier is standing), it DOES NOT CHANGE THE MATRIX
func (old *State) Move(moves map[int][][]int) *State {
	if old.MoveType != Moves {
		panic("wrong state")
	}

	moveMatrix := coreutils.ConcatMatrix(moves)
	old.CheckMoveMatrix2(moveMatrix)

	// destroying diagonal
	for i := range moveMatrix {
		moveMatrix[i][i] = 0
	}

	// copying the state and removing the soldiers from the nodes they come from
	new := old.copy()
	for n1 := range moveMatrix {
		for n2 := range moveMatrix[n1] {
			new.Nodes[n1].Soldiers -= moveMatrix[n1][n2]
		}
	}

	// ================
	// # fight in edges

	for d1 := range moveMatrix {
		for d2 := range moveMatrix[d1] {
			if new.Nodes[d1].Owner != new.Nodes[d2].Owner {
				min := int(utils.Min(moveMatrix[d1][d2], moveMatrix[d2][d1]))
				moveMatrix[d1][d2] -= min
				moveMatrix[d2][d1] -= min
			}
		}
	}

	// ==================
	// # calculate armies

	nodeArmies := make([]map[int]float64, len(new.Nodes))
	for i, n := range new.Nodes {
		curr := make(map[int]float64)
		nodeArmies[i] = curr

		// sets your own army
		if n.Soldiers != 0 {
			curr[n.Owner] = float64(n.Soldiers) + 0.5
		}

		// calculate armies from others
		for j, from := range new.Nodes {
			jiSoldiers := moveMatrix[j][i]
			if jiSoldiers == 0 {
				continue
			}
			curr[from.Owner] = curr[from.Owner] + float64(jiSoldiers)
		}
	}

	// ==================
	// # set up the nodes

	for nid, armies := range nodeArmies {
		node := &new.Nodes[nid]
		if len(armies) == 1 {
			for pid, army := range armies {
				setNodeOwner(node, pid, int(math.Floor(army)))
			}
			continue
		}
		p1, a1 := findlargest(armies)
		delete(armies, p1)
		_, a2 := findlargest(armies)

		// setNodeOwner(node, p1, )
		soldiers := int(math.Ceil(a1 - a2))
		if soldiers != 0 {
			setNodeOwner(node, p1, soldiers)
		}

		new.Nodes[nid].Soldiers = int(math.Ceil(a1 - a2))
		if new.Nodes[nid].Soldiers != 0 {
			new.Nodes[nid].Owner = p1
		}
	}

	// =================================
	// # kills players with no territory

	new.recalculatePlayers()
	for i := range new.Players {
		p := &new.Players[i]
		if !p.Dead && p.Territory == 0 {
			// Last players CANNOT DIE at once in this variant of a game, hence the dead player is always loser
			p.kill(coreutils.Lost, "Killed")
		}
	}

	new.loadNextState(Territory)
	return new
}

// helper function for move method
func findlargest(armies map[int]float64) (int, float64) {
	var pMax int
	var aMax float64
	for p, army := range armies {
		if army > aMax {
			aMax = army
			pMax = p
		}
	}
	return pMax, aMax
}

// helper function for move method
// two opions: the node was the players or not
func setNodeOwner(node *Node, pid int, soldiers int) {
	if node.Owner == pid {
		node.Soldiers = soldiers
	} else {
		node.Owner = pid
		node.Soldiers = soldiers
		node.MakingSoldiers = 0
	}
}

func (old *State) Territory(input map[int]int) *State {
	if old.MoveType != Territory {
		panic("wrong state")
	}

	new := old.copy()

	for _, nid := range input {
		seen := datstruct.NewSet[int]()
		new.DfsByOwner(nid, seen, func(int) {})
		pid := new.Nodes[nid].Owner
		for nid := range new.Nodes {
			n := &new.Nodes[nid]
			if n.Owner == pid && !seen.Has(nid) {
				n.Reset()
			}
		}
	}
	new.finishBuildingSoldiers() // the soldiers must be ready before the next step
	new.recalculatePlayers()
	new.loadNextState(Soldiers)
	return new
}

// one nid can be there multiple times
func (old *State) Soldiers(input map[int][]int) *State {
	if old.MoveType != Soldiers {
		panic("wrong state")
	}

	new := old.copy()
	for _, nids := range input {
		for _, nid := range nids {
			new.Nodes[nid].MakingSoldiers++
		}
	}
	new.recalculatePlayers()
	new.loadNextState(Moves)
	return new
}

// =========
// # Helpers

// returns all players who are Playing this turn
func (s *State) Playing() []int {
	ret := make([]int, 0)
	for pid, p := range s.Players {
		if p.Playing {
			ret = append(ret, pid)
		}
	}
	return ret
}

func (s *State) NodesTooClose(n1, n2 int) bool {
	return s.GraphGenData.MinimalPathDistance[n1][n2] < s.MinimalDistanceBetweenNodes[len(s.Playing())]
}

func (old *State) copy() *State {
	new := &State{
		Matrix:                      old.Matrix,
		MinimalDistanceBetweenNodes: old.MinimalDistanceBetweenNodes,

		MoveType: old.MoveType,
		Players:  make([]Player, len(old.Players)),
		Nodes:    make([]Node, len(old.Nodes)),

		GraphGenData: old.GraphGenData,
	}
	copy(new.Nodes, old.Nodes)
	copy(new.Players, old.Players)
	return new
}

func (b *State) tryToEnd() {
	var lastLiving *Player
	for i, p := range b.Players {
		if !p.Dead {
			if lastLiving != nil {
				return
			}
			lastLiving = &b.Players[i]
		}
	}
	if lastLiving != nil {
		lastLiving.kill(coreutils.Win, "Won")
	}
	b.MoveType = End
}

func (s *State) loadNextState(moveType MoveType) {
	// check if the game has ended
	s.tryToEnd()
	if s.MoveType == End {
		return
	}

	// resets players
	for i := range s.Players {
		s.Players[i].Playing = false
	}

	// negotation is acyclical turn type
	if moveType == Negotation {
		for i, p := range s.Players {
			if !p.Dead {
				s.Players[i].Playing = true
			}
		}
		return
	}

	for {
		switch moveType {
		case Moves:
			if s.loadMovesPlaying() {
				s.MoveType = Moves
				return
			}
			moveType = Territory
		case Territory:
			if s.loadTerritoryPlaying() {
				s.MoveType = Territory
				return
			}
			s.finishBuildingSoldiers() // the soldiers must be ready before the next step
			s.recalculatePlayers()
			moveType = Soldiers
		case Soldiers:
			if s.loadSoldiersPlaying() {
				s.MoveType = Soldiers
				return
			}
			moveType = Moves

			// soldiers2
		}
	}
}

func (s *State) loadMovesPlaying() bool {
	ret := false
	for i := range s.Players {
		p := &s.Players[i]
		if p.CurrSoldiers != 0 { // !p.Dead is implicite
			p.Playing = true
			ret = true
		}
	}
	return ret
}

func (s *State) loadTerritoryPlaying() bool {
	splitted := datstruct.NewSet[int]()
	finished := make(map[int]datstruct.Set[int])
	for nid, n := range s.Nodes {
		if n.Owner == -1 {
			continue
		}
		set, ok := finished[n.Owner]
		if !ok {
			finished[n.Owner] = datstruct.NewSet[int]()
			s.DfsByOwner(nid, finished[n.Owner], func(int) {})
			continue
		}
		if !set.Has(nid) {
			splitted.Add(n.Owner)
		}
	}

	for pid := range splitted.Iterator() {
		s.Players[pid].Playing = true
	}

	return splitted.Size() > 0
}

func (s *State) loadSoldiersPlaying() bool {
	wantToPlace := datstruct.NewSet[int]()
	for id, p := range s.Players {
		if p.CurrSoldiers < p.MaxSoldiers { // !p.Dead is implicite
			wantToPlace.Add(id)
		}
	}
	if wantToPlace.Size() == 0 {
		return false
	}
	ret := false
	for _, node := range s.Nodes {
		if !wantToPlace.Has(node.Owner) {
			continue
		}
		s.Players[node.Owner].Playing = true
		wantToPlace.Remove(node.Owner)
		ret = true
	}
	return ret
}

func (s *State) recalculatePlayers() {
	for i := range s.Players {
		p := &s.Players[i]

		p.Territory = 0
		p.CurrSoldiers = 0
		p.MaxSoldiers = 0

		p.CreatingSoldiers = 0
		p.TerritoryUntilNextSoldier = 0
	}
	for _, n := range s.Nodes {
		if n.Owner != -1 {
			p := &s.Players[n.Owner]
			p.Territory += 1
			p.CurrSoldiers += n.Soldiers
			p.CreatingSoldiers += n.MakingSoldiers
		}
	}
	for i := range s.Players {
		p := &s.Players[i]
		if !p.Dead {
			p.MaxSoldiers, p.TerritoryUntilNextSoldier = coreutils.MaxSoldiers2(p.Territory)
		}
	}
}

func (s *State) DfsByOwner(curr int, seen datstruct.Set[int], foo func(int)) {
	if seen.Has(curr) {
		return
	}
	seen.Add(curr)
	if foo != nil {
		foo(curr)
	}
	for next, exists := range s.Matrix[curr] {
		if exists && s.Nodes[curr].Owner == s.Nodes[next].Owner {
			s.DfsByOwner(next, seen, foo)
		}
	}
}

func (s *State) finishBuildingSoldiers() {
	for i := range s.Nodes {
		n := &s.Nodes[i]
		n.Soldiers += n.MakingSoldiers
		n.MakingSoldiers = 0
	}
}

func (s *State) AlivePlayers() []int {
	ret := []int{}
	for pid, p := range s.Players {
		if !p.Dead {
			ret = append(ret, pid)
		}
	}
	return ret
}
