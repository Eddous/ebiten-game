package state

type MoveType byte

const (
	Negotation MoveType = iota
	Moves
	Territory
	Soldiers
	End
)

func (s MoveType) String() string {
	switch s {
	case Negotation:
		return "Negotation"
	case Soldiers:
		return "Soldiers"
	case Moves:
		return "Moves"
	case Territory:
		return "Territory"
	case End:
		return "End"
	}
	panic("state doesn't exists")
}
