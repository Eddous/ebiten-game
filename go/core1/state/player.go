package state

import (
	"gra/coreutils"
)

type Player struct {
	Playing      bool
	Territory    int
	CurrSoldiers int
	MaxSoldiers  int

	CreatingSoldiers          int
	TerritoryUntilNextSoldier int

	Dead      bool // if the player won the game, he is still Dead
	DeathInfo coreutils.DeathInfo
}

func (p *Player) kill(result coreutils.Result, deathText string) {
	p.Playing = false
	p.Dead = true
	p.DeathInfo = coreutils.NewDeathInfo(result, deathText)

	if result == coreutils.Lost {
		p.Territory = 0
		p.CurrSoldiers = 0
		p.MaxSoldiers = 0

		p.CreatingSoldiers = 0
		p.TerritoryUntilNextSoldier = 0
	}
}
