package state

import "gra/utils"

type Node struct {
	Pos           utils.VecF
	Neighbours    []int // the node itself is NOT HERE
	Neighbourhood []int // the node itself IS HERE, this is mainly used for random move generation where soldiers can stay at place

	Owner          int
	Soldiers       int
	MakingSoldiers int
}

func (n *Node) Reset() {
	n.Owner = -1
	n.MakingSoldiers = 0
	n.Soldiers = 0
}
