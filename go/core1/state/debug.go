package state

func (s *State) CheckMoveMatrix(matrix [][]int, pid int) {
	for i := range matrix {
		for j := range matrix {
			if matrix[i][j] != 0 && s.Nodes[i].Owner != pid {
				panic("WRONG OWNER!")
			}
		}
	}
	s.CheckMoveMatrix2(matrix)
}

func (s *State) CheckMoveMatrix2(matrix [][]int) {
	for i := range matrix {
		node := s.Nodes[i]
		sum := 0
		for j, outcoming := range matrix[i] {
			sum += outcoming
			if outcoming < 0 {
				panic("outcoming is less then 0")
			}
			if outcoming != 0 && !s.Matrix[i][j] && i != j {
				panic("wrong connection")
			}
		}
		if sum > node.Soldiers {
			panic("outcoming > soldiers")
		}
	}
}
