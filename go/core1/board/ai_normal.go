//go:build !js && !wasm

package board

import (
	state "gra/core1/state"
	"gra/coreutils"

	"github.com/barweiss/go-tuple"
)

func (b *Board) runAI(s *state.State, playingAIs []aiWrapper, ch chan tuple.T2[int, string]) {
	for _, ai := range playingAIs {
		switch s.MoveType {
		case state.Negotation:
			ch <- tuple.New2(ai.ai.Pid, coreutils.NidsToStr(ai.ai.Negotation(s)))
		case state.Moves:
			ch <- tuple.New2(ai.ai.Pid, coreutils.MovesToStr(ai.ai.Moves(s)))
		case state.Territory:
			ch <- tuple.New2(ai.ai.Pid, coreutils.NidToStr(ai.ai.Territory(s)))
		case state.Soldiers:
			ch <- tuple.New2(ai.ai.Pid, coreutils.NidsToStr(ai.ai.Soldiers(s)))
		default:
			panic("no such state")
		}
	}
	close(ch)
}
