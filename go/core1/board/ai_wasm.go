//go:build js && wasm

package board

import (
	"encoding/json"
	"fmt"
	state "gra/core1/state"
	"gra/utils"
	"sync/atomic"
	"syscall/js"
	"time"

	"github.com/barweiss/go-tuple"
)

type OutMsg struct {
	State  state.State
	AIType int
	Pid    int
}

type InMsg struct {
	Pid    int
	Action string
}

func (b *Board) runAI(s *state.State, playingAIs []aiWrapper, ch chan tuple.T2[int, string]) {
	var counter int64 = 0

	id := int(time.Now().UnixMilli())

	callback := js.FuncOf(func(this js.Value, args []js.Value) any {
		if args[0].Int() != id {
			fmt.Println("discarding old AI move")
			return js.Null()
		}
		inStr := args[1].String()
		var in InMsg
		json.Unmarshal([]byte(inStr), &in)
		ch <- tuple.New2(in.Pid, in.Action)

		atomic.AddInt64(&counter, 1)
		if counter == int64(len(playingAIs)) {
			close(ch)
		}

		return js.Null()
	})

	for _, ai := range playingAIs {
		msg := OutMsg{
			State:  *s,
			AIType: ai.t,
			Pid:    ai.ai.Pid,
		}
		js.Global().Get("webWorkerAI").Invoke(id, ai.ai.Pid, 1, string(utils.ToBytes(msg)), callback)
	}
}
