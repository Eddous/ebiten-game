package board

import (
	ai "gra/core1/ai"
	state "gra/core1/state"
	"gra/coredrawutils"
	"gra/coreutils"
	"gra/datstruct"
	"gra/graphgen"
	"gra/gulrot"
	"gra/utils"
	"strconv"

	"github.com/barweiss/go-tuple"
	"github.com/hajimehoshi/ebiten/v2"
)

type aiWrapper struct {
	ai *ai.AI
	t  int
}

// this is quite messy
type transition struct {
	killed     *datstruct.Set[int] // nids, can be nil to denote type of transition
	moves      [][]int
	territory  datstruct.Set[int] // this cannot be nil, but can be default
	soldiers   []int
	negotation map[int][]int // map[nid][]pid
}

type Board struct {
	// arguments
	seed    int64
	players int
	mode    int

	// things that does not change
	nodes  []utils.VecF
	matrix [][]bool

	history     []*state.State
	transitions []transition

	// for CoreInterface (the support for gui)
	negotationNodes datstruct.Set[int]
	soldiersNodes   []int
	territoryNode   int
	moves           [][]int // diagonal is empty

	// this is for creating moves
	selectedNid      int
	selectedSoldiers int
	posibleMoves     datstruct.Set[int]

	bannedNodes datstruct.Set[int]

	helperArrowsPositions []float64

	ais       []aiWrapper
	defaultAI *ai.DefaultAI // if the player is out of time
}

func (b *Board) last() *state.State {
	return b.history[len(b.history)-1]
}

// ======================
// # High level interface

func NewBoard(seed int64, players []int, nodes int) *Board {
	gameMap := graphgen.GenerateGenerate(seed, nodes)

	b := &Board{
		seed:    seed,
		players: len(players),
		mode:    nodes,

		history:     []*state.State{state.NewState(len(players), gameMap)},
		transitions: []transition{},
		ais:         make([]aiWrapper, 0),
		defaultAI:   ai.NewDefaultAI(seed),
	}

	// initialization of constants for this particular map
	s := b.history[0]
	b.nodes = make([]utils.VecF, len(s.Nodes))
	for i, n := range s.Nodes {
		b.nodes[i] = n.Pos
	}
	b.matrix = b.history[0].Matrix

	// settings player types
	for pid, playerType := range players {
		if playerType != 0 {
			ai := ai.AIBattery[playerType-1]()
			ai.Init(pid, seed+int64(pid))
			b.ais = append(b.ais, aiWrapper{ai: ai, t: playerType - 1})
		}
	}

	b.helperArrowsPositions = coreutils.HelperArrowsPositions(b.nodes, b.matrix)

	b.ResetActionBuffer()
	return b
}

// seed, players, mode
func (b *Board) Args() (int64, int, int) {
	return b.seed, b.players, b.mode
}

// ===================
// # TurnGameInterface

func (b *Board) Result() map[int]coreutils.DeathInfo {
	ret := map[int]coreutils.DeathInfo{}
	for id, p := range b.last().Players {
		if p.Dead {
			ret[id] = p.DeathInfo
		}
	}
	return ret
}

func (b *Board) KillPlayers(ids []int, deathText string) (playing []int) {
	old := b.last()
	new := old.Kill(ids, deathText)
	b.history = append(b.history, new)

	set := datstruct.NewSet[int]()
	for i := range old.Nodes {
		if old.Nodes[i].Owner != new.Nodes[i].Owner {
			set.Add(i)
		}
	}

	b.transitions = append(b.transitions, transition{killed: &set})
	return new.Playing()
}

func (b *Board) InputCheck(pid int, buffer string) bool {
	s := b.last()

	switch s.MoveType {
	case state.Negotation:
		nids, ok := coreutils.StrToNids(buffer)
		if !ok {
			return false
		}
		return s.CheckNegotationInput(pid, nids)
	case state.Soldiers:
		nids, ok := coreutils.StrToNids(buffer)
		if !ok {
			return false
		}
		return s.CheckSoldiersInput(pid, nids)
	case state.Territory:
		nid, ok := coreutils.StrToNid(buffer)
		if !ok {
			return false
		}
		return s.CheckTerritoryInput(pid, nid)
	case state.Moves:
		moves, ok := coreutils.StrToMoves(buffer)
		if !ok {
			return false
		}
		return s.CheckMoveInput(pid, moves)
	}
	panic("no such option")
}

func (b *Board) Move(input map[int]string) (playing []int) {
	last := b.last()
	var new *state.State
	var trans transition

	switch last.MoveType {
	case state.Negotation:
		move := coreutils.FromStrBatch(input, coreutils.StrToNids)
		new = last.Negotation(move)
		trans = transition{negotation: coreutils.NegotationToTrans(move)}
	case state.Territory:
		move := coreutils.FromStrBatch(input, coreutils.StrToNid)
		new = last.Territory(move)

		// trans
		set := datstruct.NewSet[int]()
		for _, nid := range move {
			set.Add(nid)
		}
		trans = transition{territory: set}
	case state.Soldiers:
		move := coreutils.FromStrBatch(input, coreutils.StrToNids)
		new = last.Soldiers(move)

		// trans
		tmp := make([]int, 0)
		for _, nids := range move {
			tmp = append(tmp, nids...)
		}
		trans = transition{soldiers: tmp}
	case state.Moves:
		move := coreutils.FromStrBatch(input, coreutils.StrToMoves)
		new = last.Move(move)

		// trans
		trans = transition{moves: coreutils.ConcatMatrix(move)}
	}
	b.history = append(b.history, new)
	b.transitions = append(b.transitions, trans)
	return new.Playing()
}

func (b *Board) DefaultMove(pid int) string {
	return b.defaultAI.Action(b.last(), pid)
}

func (b *Board) StartAI() <-chan tuple.T2[int, string] {
	s := b.last()
	if s.MoveType == state.End {
		return nil
	}

	playingAIs := make([]aiWrapper, 0)
	for _, ai := range b.ais {
		if s.Players[ai.ai.Pid].Playing {
			playingAIs = append(playingAIs, ai)
		}
	}
	if len(playingAIs) == 0 {
		return nil
	}

	// if the user leave game, the AI still runs in the background
	ch := make(chan tuple.T2[int, string], len(b.ais))
	// this is compilation magic
	go b.runAI(s, playingAIs, ch)
	return ch
}

// # TurnGameInterface
// ===================

// ===============
// # CoreInterface

func (b *Board) GetNodesAsVecF() []utils.VecF {
	return b.nodes
}

// this is also a check if the move is complete
func (b *Board) GetActionBuffer(pid int) (string, bool) {
	s := b.last()

	switch s.MoveType {
	case state.Negotation:
		neg := b.negotationNodes.ToSlice()
		if s.CheckNegotationInput(pid, neg) {
			return coreutils.NidsToStr(neg), true
		}
	case state.Moves:
		if s.CheckMoveInput(pid, b.moves) {
			return coreutils.MovesToStr(b.moves), true
		}
	case state.Territory:
		if s.CheckTerritoryInput(pid, b.territoryNode) {
			return coreutils.NidToStr(b.territoryNode), true
		}
	case state.Soldiers:
		if s.CheckSoldiersInput(pid, b.soldiersNodes) {
			return coreutils.NidsToStr(b.soldiersNodes), true
		}
	}
	return "", false
}

func (b *Board) ResetActionBuffer() {
	b.negotationNodes = datstruct.NewSet[int]()
	b.bannedNodes = datstruct.NewSet[int]()
	b.territoryNode = -1
	b.moves = coreutils.NewMoveMatrix(len(b.history[0].Nodes))
	b.soldiersNodes = make([]int, 0)
	b.resetMoveBuffer()
}

func (b *Board) resetMoveBuffer() {
	b.selectedNid = -1
	b.selectedSoldiers = 0
	b.posibleMoves = datstruct.NewSet[int]()
}

func (b *Board) Holdable(nid int) bool {
	if b.selectedNid == nid {
		return true
	}
	for _, n := range b.soldiersNodes {
		if n == nid {
			return true
		}
	}
	for i := range b.last().Nodes {
		if b.moves[nid][i] > 0 {
			return true
		}
	}
	return false
}

func (b *Board) Holded(nid int) {
	if !b.Holdable(nid) {
		return
	}

	b.resetMoveBuffer()

	new := make([]int, 0)
	for _, nid2 := range b.soldiersNodes {
		if nid != nid2 {
			new = append(new, nid2)
		}
	}
	b.soldiersNodes = new

	for i := range b.history[0].Nodes {
		b.moves[nid][i] = 0
	}

}

func (b *Board) Tools() []tuple.T2[string, *ebiten.Image] {
	return nil
}

func (b *Board) ShowTools(int, int) bool {
	return false
}

func (b *Board) Tap(pid, nid int, _ string) {
	s := b.last()
	switch s.MoveType {
	case state.Negotation:
		if b.bannedNodes.Has(nid) {
			return
		}
		if b.negotationNodes.Has(nid) {
			b.negotationNodes.Remove(nid)
		} else if len(s.AlivePlayers()) > b.negotationNodes.Size() {
			b.negotationNodes.Add(nid)
		} else {
			return
		}

		b.bannedNodes = datstruct.NewSet[int]()
		for nid1 := range b.negotationNodes.Iterator() {
			for nid2 := range s.Nodes {
				if !b.negotationNodes.Has(nid2) && s.NodesTooClose(nid1, nid2) {
					b.bannedNodes.Add(nid2)
				}
			}
		}
	case state.Territory:
		if s.CheckTerritoryInput(pid, nid) {
			b.territoryNode = nid
		}
	case state.Soldiers:
		if s.Nodes[nid].Owner == pid && len(b.soldiersNodes) < s.Players[pid].MaxSoldiers-s.Players[pid].CurrSoldiers {
			b.soldiersNodes = append(b.soldiersNodes, nid)
		}
	case state.Moves:
		n := s.Nodes[nid]
		soldiersUsed := 0
		for _, soldiers := range b.moves[nid] {
			soldiersUsed += soldiers
		}

		if b.selectedNid == -1 {
			// first click
			if n.Owner != pid || soldiersUsed == n.Soldiers {
				return
			}
			b.selectedNid = nid
			b.selectedSoldiers = 1
			for _, nid2 := range n.Neighbours {
				b.posibleMoves.Add(nid2)
			}
		} else {
			// not first click
			if nid == b.selectedNid {
				// the same node
				if soldiersUsed+b.selectedSoldiers < n.Soldiers {
					b.selectedSoldiers++
				}
				return
			}
			if !b.posibleMoves.Has(nid) {
				// the same as miss tap
				b.resetMoveBuffer()
				return
			}
			// finishing the move
			b.moves[b.selectedNid][nid] += b.selectedSoldiers
			b.resetMoveBuffer()
		}
	}
}

func (b *Board) MissTap() {
	b.resetMoveBuffer()
}

func (b *Board) PlayerText(pid int) ([]string, []string) {
	player := b.last().Players[pid]
	plus := ""
	if player.CreatingSoldiers != 0 {
		plus = "+"
	}
	return []string{"Territory", "Soldiers"}, []string{strconv.Itoa(player.Territory), strconv.Itoa(player.CurrSoldiers) + plus + "/" + strconv.Itoa(player.MaxSoldiers)}
}

func (b *Board) Territory(pid int) string {
	player := b.last().Players[pid]
	return strconv.Itoa(player.Territory)
}

func (b *Board) Soldiers(pid int) string {
	player := b.last().Players[pid]
	plus := ""
	if player.CreatingSoldiers != 0 {
		plus = "+"
	}
	return strconv.Itoa(player.CurrSoldiers) + plus + "/" + strconv.Itoa(player.MaxSoldiers)
}

func (b *Board) TutorialText() []string {
	switch b.last().MoveType {
	case state.Negotation:
		players := len(b.last().AlivePlayers())
		minimalDist := b.last().MinimalDistanceBetweenNodes[players]
		return []string{
			"Select " + strconv.Itoa(players) + " nodes, at least " + strconv.Itoa(minimalDist) + " apart, single one will be selected for you to start on.",
		}
	case state.Moves:
		return []string{
			"Click on soldier to plan its movement.",
			"Hold the node to reset.",
		}
	case state.Territory:
		return []string{
			"Select a node from a part of your territory you want to keep.",
		}
	case state.Soldiers:
		return []string{
			"Select nodes where to create a new soldiers.",
			"Hold the node to reset.",
		}
	case state.End:
		return nil
	}
	panic("dont happend")
}

func (c *Board) HistoryBoardStates() int {
	return len(c.history)
}

func (b *Board) StateStr() (string, bool) {
	return b.history[len(b.history)-1].MoveType.String(), true
}

func (b *Board) Update() {}

func (b *Board) Draw(screen *ebiten.Image, camera *gulrot.Camera, historyState int, drawTrans bool) {
	coredrawutils.DrawEdges(screen, camera, b.nodes, b.matrix)
	/*
		0 - draw state
		1 - draw state && transition
		2 - draw state && current buffers && visual flow
	*/
	var t byte
	if historyState == len(b.history)-1 {
		t = 2
	} else if drawTrans {
		t = 1
	} else {
		t = 0
	}

	for nid, n := range b.history[historyState].Nodes {
		soldiers, outgoing := b.outgoing(nid, t, historyState)
		coredrawutils.DrawNode(
			screen,
			camera,
			coredrawutils.DrawNodeOptions{
				Pos:                n.Pos,
				Pid:                n.Owner,
				Soldiers:           soldiers,
				SuperScript:        n.MakingSoldiers,
				Selected:           b.selected1(nid, t, historyState),
				SelectedMoreTimes:  b.selected2(nid, t, historyState),
				Banned:             b.banned(nid, t, historyState),
				Outgoing:           outgoing,
				WhoChosePlayers:    b.whoChoseNode(nid, t, historyState),
				PointingArrowAngle: b.helperArrowsPositions[nid],
				ShowArrow:          b.helperArrow(nid, t, historyState),
			},
		)
	}
	if t == 2 && historyState != 0 {
		trans := b.transitions[historyState-1]
		if trans.moves != nil {
			coredrawutils.DrawHelpArrows(screen, camera, b.nodes, trans.moves)
		}
	}
}

// # core driver interface
// =======================

// ============
// Draw helpers

func (b *Board) selected1(nid int, t byte, i int) bool {
	switch t {
	case 0:
		return false
	case 1:
		return b.transitions[i].territory.Has(nid)
	case 2:
		return b.territoryNode == nid || b.negotationNodes.Has(nid) || b.posibleMoves.Has(nid)
	}
	panic("dont happen")
}

func (b *Board) selected2(nid int, t byte, i int) int {
	switch t {
	case 0:
		return 0
	case 1:
		c := 0
		for _, nid2 := range b.transitions[i].soldiers {
			if nid2 == nid {
				c++
			}
		}
		return c
	case 2:
		c := 0
		for _, nid2 := range b.soldiersNodes {
			if nid == nid2 {
				c++
			}
		}
		if nid == b.selectedNid {
			c = b.selectedSoldiers
		}
		return c
	}
	panic("dont happen")
}

func (b *Board) banned(nid int, t byte, i int) bool {
	switch t {
	case 0:
		return false
	case 1:
		if t := b.transitions[i]; t.killed != nil {
			return t.killed.Has(nid)
		}
		return false
	case 2:
		if b.bannedNodes.Has(nid) {
			return true
		}
		if i == 0 {
			return false
		}
		return b.history[i-1].Nodes[nid].Owner != -1 && b.history[i].Nodes[nid].Owner == -1
	}
	panic("dont happen")
}

// returns how many are there and how many are outgoing
func (b *Board) outgoing(nid int, t byte, i int) (int, []coredrawutils.Outgoing) {
	node := b.history[i].Nodes[nid]

	var matrix [][]int
	switch t {
	case 0:
		return node.Soldiers, nil
	case 1:
		matrix = b.transitions[i].moves
	case 2:
		matrix = b.moves
	}

	if matrix == nil {
		return node.Soldiers, nil
	}

	out := make([]coredrawutils.Outgoing, 0)
	soldiers := node.Soldiers
	for nid2, sol := range matrix[nid] {
		if sol != 0 && nid != nid2 {
			out = append(out, coredrawutils.NewOutgoing(b.history[0].Nodes[nid2].Pos, sol))
			soldiers -= sol
		}
	}
	return soldiers, out
}

// for the first phase
func (b *Board) whoChoseNode(nid int, t byte, i int) []int {
	switch t {
	case 0:
		return nil
	case 1:
		if t := b.transitions[i]; t.negotation != nil {
			return t.negotation[nid]
		}
		return nil
	case 2:
		return nil
	}
	panic("dont happen")
}

func (b *Board) helperArrow(nid int, t byte, i int) bool {
	if t != 2 || i == 0 {
		return false
	}
	trans := b.transitions[i-1]
	if trans.territory.Has(nid) {
		return true
	}
	for _, nid2 := range trans.soldiers {
		if nid == nid2 {
			return true
		}
	}
	return false
}

// Draw helpers
// ============
