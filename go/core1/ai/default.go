package ai

import (
	"gra/core1/state"
	"gra/coreutils"
	"math/rand"
)

type DefaultAI struct {
	rnd *rand.Rand

	neg func(*state.State, *rand.Rand, int) []int
	mov func(*state.State, *rand.Rand, int) [][]int
	ter func(*state.State, *rand.Rand, int) int
	sol func(*state.State, *rand.Rand, int) []int
}

func NewDefaultAI(seed int64) *DefaultAI {
	return &DefaultAI{
		rnd: rand.New(rand.NewSource(seed)),
		neg: RandomNegotation,
		mov: BeanMoves1(2, LocalSearch, EvalBasic4(), LinearNormalization),
		ter: DefaultTerritory2(1),
		sol: DefaultSoldiers(1),
	}
}

func (ai *DefaultAI) Action(s *state.State, pid int) string {
	switch s.MoveType {
	case state.Negotation:
		return coreutils.NidsToStr(ai.neg(s, ai.rnd, pid))
	case state.Moves:
		return coreutils.MovesToStr(ai.mov(s, ai.rnd, pid))
	case state.Territory:
		return coreutils.NidToStr(ai.ter(s, ai.rnd, pid))
	case state.Soldiers:
		return coreutils.NidsToStr(ai.sol(s, ai.rnd, pid))
	}
	panic("not an option")
}
