package ai

import (
	state "gra/core1/state"
	"gra/coreutils"
	"gra/datstruct"
	"gra/utils"
	"math"
	"math/rand"
)

// I keep this because I think there is another way of calculation of distance
func EvalBasic1(soldierVal, makingSoldierVal float64) func(s *state.State) []float64 {
	return func(s *state.State) []float64 {
		return territorySum(s, soldierVal, makingSoldierVal)
	}
}

func EvalBasic2(soldierVal, makingSoldierVal float64) func(s *state.State) []float64 {
	return func(s *state.State) []float64 {
		return territoryAndDistEvalFunc(s, soldierVal, makingSoldierVal)
	}
}

// this one will roll out the territory step
func EvalBasic3(soldierVal, makingSoldierVal float64) func(s *state.State) []float64 {
	return func(s *state.State) []float64 {
		if s.MoveType == state.End {
			results := make([]coreutils.Result, len(s.Players))
			for i, p := range s.Players {
				results[i] = p.DeathInfo.Result
			}
			return coreutils.ResultsToFloat(results)
		}
		if s.MoveType == state.Territory {
			s = s.Territory(defaultTerritoryForAll(s))
		}
		return territoryAndDistEvalFunc(s, soldierVal, makingSoldierVal)
	}
}

// this one will roll out the territory step
func EvalBasic4() func(s *state.State) []float64 {
	return EvalWrapper2(EvalBasic2(1, 1))
}

// this does not work much or at all...
func EvalBasic5() func(s *state.State) []float64 {
	return EvalWrapper2(
		func(s *state.State) []float64 {
			return territoryAndDistEvalFunc2(s, 1, 0.5)
		},
	)
}

func EvalWrapper2(eval func(s *state.State) []float64) func(s *state.State) []float64 {
	return func(s *state.State) []float64 {
		for s.MoveType != state.Moves {
			switch s.MoveType {
			case state.Territory:
				s = s.Territory(DefaultTerritoryForAll2(s, 1))
			case state.Soldiers:
				s = s.Soldiers(DefaultSoldiersForAll(s, rand.New(rand.NewSource(0)), 1))
			case state.End:
				results := make([]coreutils.Result, len(s.Players))
				for i, p := range s.Players {
					results[i] = p.DeathInfo.Result
				}
				return coreutils.ResultsToFloat(results)
			}
		}
		return eval(s)
	}
}

// this function wraps some other eval function with benefits of unwrapping the territory state
// (it is meant for gnns)
func EvalWrapper(eval func(s *state.State) []float64) func(s *state.State) []float64 {
	return func(s *state.State) []float64 {
		if s.MoveType == state.End {
			results := make([]coreutils.Result, len(s.Players))
			for i, p := range s.Players {
				results[i] = p.DeathInfo.Result
			}
			return coreutils.ResultsToFloat(results)
		}
		if s.MoveType == state.Territory {
			s = s.Territory(defaultTerritoryForAll(s))
		}
		return eval(s)
	}
}

func EvalMonteCarlo() func(s *state.State) []float64 {

	// change this... if this code is ever run again
	ai := AIBattery[0]()
	ai.Init(-1, 0)

	games := 20
	depth := 3

	return func(s0 *state.State) []float64 {
		res := make([]float64, len(s0.Players))
		for range games {
			s := s0
			c := 0

			for s.MoveType != state.End && c != depth {
				switch s.MoveType {
				case state.Negotation:
					s = s.Negotation(ai.NegotationForAll(s))
				case state.Moves:
					s = s.Move(ai.MovesForAll(s))
				case state.Territory:
					s = s.Territory(ai.TerritoryForAll(s))
				case state.Soldiers:
					s = s.Soldiers(ai.SoldiersForAll(s))
				}
				c++
			}
			var values []float64
			if s.MoveType == state.End {
				res := []coreutils.Result{}
				for _, p := range s.Players {
					res = append(res, p.DeathInfo.Result)
				}
				values = coreutils.ResultsToFloat(res)
			} else {
				values = LinearNormalization(EvalBasic4()(s))
			}
			for pid := range res {
				res[pid] += values[pid]
			}
		}
		for pid := range res {
			res[pid] = res[pid] / float64(games)
		}
		return res
	}
}

// slowest slownes
// it may be change to run only to depth of 3 or whatever and then use EvalBasic
func EvalRec() func(s *state.State) []float64 {

	// change this... if this code is ever run again
	ai := AIBattery[0]()
	ai.Init(-1, 0)

	games := 10

	return func(s0 *state.State) []float64 {
		res := make([]float64, len(s0.Players))
		for range games {
			s := s0
			c := 0
			for s.MoveType != state.End {
				switch s.MoveType {
				case state.Negotation:
					s = s.Negotation(ai.NegotationForAll(s))
				case state.Moves:
					s = s.Move(ai.MovesForAll(s))
				case state.Territory:
					s = s.Territory(ai.TerritoryForAll(s))
				case state.Soldiers:
					s = s.Soldiers(ai.SoldiersForAll(s))
				}

				// safeguard
				c++
				if c > 100 {
					alive := s.AlivePlayers()
					for _, pid := range alive {
						res[pid] = 1 / float64(len(alive))
					}
				}
			}
			for pid, p := range s.Players {
				if p.DeathInfo.Result == coreutils.Win {
					res[pid] += 1
				}
			}
		}
		for pid := range res {
			res[pid] = res[pid] / float64(games)
		}
		return res
	}
}

func territoryAndDistEvalFunc(s *state.State, soldierVal, makingSoldierVal float64) []float64 {
	avgDist := averageDistOfSoldiersFromBorder(s)
	score := territorySum(s, soldierVal, makingSoldierVal)

	_, min := utils.SliceMin([]float64{1, soldierVal, makingSoldierVal})
	for i := range score {
		// the distance is only secondary atribute
		// IT MUST be smaller then 1 territory because then soldier could avoid capturing nodes
		score[i] -= min * avgDist[i] / (avgDist[i] + 1)
	}
	return score
}

func territoryAndDistEvalFunc2(s *state.State, soldierVal, makingSoldierVal float64) []float64 {
	avgDist := averageDistOfSoldiersFromBorder2(s)
	score := territorySum(s, soldierVal, makingSoldierVal)

	_, min := utils.SliceMin([]float64{1, soldierVal, makingSoldierVal})
	for i := range score {
		// the distance is only secondary atribute
		// IT MUST be smaller then 1 territory because then soldier could avoid capturing nodes
		score[i] -= min * avgDist[i] / (avgDist[i] + 1)
	}
	return score
}

func territorySum(s *state.State, soldierVal, makingSoldierVal float64) []float64 {
	ret := make([]float64, len(s.Players))
	for _, node := range s.Nodes {
		if pid := node.Owner; pid != -1 {
			ret[pid] += 1 + float64(node.Soldiers)*soldierVal + float64(node.MakingSoldiers)*makingSoldierVal
		}
	}
	return ret
}

// MakingSoldier = 1 soldier
func averageDistOfSoldiersFromBorder(s *state.State) []float64 {
	soldiers := make([]float64, len(s.Players))
	distanceSum := make([]float64, len(s.Players))
	for nid, node := range s.Nodes {
		pid := node.Owner
		sold := float64(node.Soldiers + node.MakingSoldiers)
		if sold != 0 {
			soldiers[pid] += sold
			distanceSum[pid] += sold * float64(shortestDistToEnemyNode(s, nid, pid, -1))
		}
	}
	for i := range soldiers {
		if soldiers[i] != 0 {
			distanceSum[i] = distanceSum[i] / soldiers[i]
		}
	}
	return distanceSum
}

// this is not deterministic - two players may have the same score
func averageDistOfSoldiersFromBorder2(s *state.State) []float64 {
	playerScores := make(map[int]float64)

	for nid, node := range s.Nodes {
		if _, ok := playerScores[node.Owner]; node.Owner == -1 || ok {
			continue
		}
		seen := datstruct.NewSet[int]()
		currScore := 0.0
		counterFunc := func(nid int) { currScore += 1 + float64(s.Nodes[nid].Soldiers+s.Nodes[nid].MakingSoldiers)*1 }
		s.DfsByOwner(nid, seen, counterFunc)
		playerScores[node.Owner] = currScore
	}

	maxPid := -1
	maxVal := -1.
	for pid, val := range playerScores {
		if val > maxVal {
			maxVal = val
			maxPid = pid
		}
	}

	soldiers := make([]float64, len(s.Players))
	distanceSum := make([]float64, len(s.Players))
	for nid, node := range s.Nodes {
		pid := node.Owner
		sold := float64(node.Soldiers + node.MakingSoldiers)
		if sold != 0 {
			var pid2 int
			if maxPid == pid {
				pid2 = -1
			} else {
				pid2 = maxPid
			}

			soldiers[pid] += sold
			distanceSum[pid] += sold * float64(shortestDistToEnemyNode(s, nid, pid, pid2))
		}
	}
	for i := range soldiers {
		if soldiers[i] != 0 {
			distanceSum[i] = distanceSum[i] / soldiers[i]
		}
	}
	return distanceSum
}

func shortestDistToEnemyNode(s *state.State, nid int, pid int, pid2 int) int {
	type elem struct {
		nid  int
		dist int
	}
	seen := datstruct.NewSet[int]()
	seen.Add(nid)
	fifo := []elem{{nid: nid, dist: 0}}
	for i := 0; i < len(fifo); i++ {
		curr := fifo[i]
		for neighbour := 0; neighbour < len(s.Matrix); neighbour++ {
			if s.Matrix[curr.nid][neighbour] && !seen.Has(neighbour) {
				if pid2 == -1 && s.Nodes[neighbour].Owner != pid {
					return curr.dist + 1
				}
				// searching for specific pid
				if pid2 != -1 && s.Nodes[neighbour].Owner == pid2 {
					return curr.dist + 1
				}
				fifo = append(fifo, elem{nid: neighbour, dist: curr.dist + 1})
				seen.Add(neighbour)
			}
		}
	}
	return 0
}

// ===== Flood 1 =====

func Flood1(s *state.State) []float64 {
	// fifo of nodes which were not seen
	playerFifo := make(map[int][]int)
	for _, pid := range s.AlivePlayers() {
		playerFifo[pid] = []int{}
	}

	// nodes got by some player
	ownership := make([]int, len(s.Nodes))
	for nid, node := range s.Nodes {
		ownership[nid] = -1
		if node.Soldiers != 0 {
			ownership[nid] = node.Owner
		}
	}

	// seting first seeds of flood
	for nid, node := range s.Nodes {
		if node.Soldiers != 0 {
			pid := node.Owner
			playerFifo[pid] = append(playerFifo[pid], setOwnership(s, ownership, nid, pid)...) // in this case the owner is already set
		}
	}
	updateFlood1(s, ownership, playerFifo)

	// setting second seed of flood (creating soldiers)
	for nid, node := range s.Nodes {
		if node.MakingSoldiers != 0 && ownership[nid] == -1 {
			pid := node.Owner
			playerFifo[pid] = append(playerFifo[pid], setOwnership(s, ownership, nid, pid)...)
		}
	}

	for !fifoEmpty(playerFifo) {
		updateFlood1(s, ownership, playerFifo)
	}

	ret := make([]float64, len(s.Players))
	for _, pid := range ownership {
		// some nodes are not taken
		if pid != -1 {
			ret[pid]++
		}
	}
	return ret
}

func fifoEmpty(playerFifo map[int][]int) bool {
	for _, fifo := range playerFifo {
		if len(fifo) != 0 {
			return false
		}
	}
	return true
}

// will update player fifo accordingly
func updateFlood1(s *state.State, ownership []int, playerFifo map[int][]int) {
	// checking whether more players want one node
	wanaGet := make(map[int]datstruct.Set[int])
	for pid, nids := range playerFifo {
		for _, nid := range nids {
			if ownership[nid] != -1 {
				continue
			}
			if _, ok := wanaGet[nid]; !ok {
				wanaGet[nid] = datstruct.NewSet[int]()
			}
			wanaGet[nid].Add(pid)
		}
		playerFifo[pid] = []int{}
	}

	for nid, pids := range wanaGet {
		if pids.Size() != 1 {
			continue
		}
		pid := pids.GetSingleton()
		playerFifo[pid] = append(playerFifo[pid], setOwnership(s, ownership, nid, pid)...)
	}
}

func setOwnership(s *state.State, ownership []int, nid int, pid int) []int {
	ownership[nid] = pid
	nonTakenNeighbours := []int{}
	for _, nid2 := range s.Nodes[nid].Neighbours {
		if ownership[nid2] == -1 {
			nonTakenNeighbours = append(nonTakenNeighbours, nid2)
		}
	}
	return nonTakenNeighbours
}

// ===== Flood 2 =====

func Flood2(soldierVal, makingSoldierVal float64) func(s *state.State) []float64 {
	return func(s *state.State) []float64 {
		if s.MoveType == state.Territory {
			s = s.Territory(defaultTerritoryForAll(s))
		}

		sum := territorySum(s, soldierVal, makingSoldierVal)
		for i, val := range Flood1(s) {
			sum[i] += val / (val + 1)
		}
		return sum
	}
}

// ===== Flood 3 =====

func Flood3(factor float64) func(s *state.State) []float64 {
	return func(s *state.State) []float64 {
		if s.MoveType == state.Territory {
			s = s.Territory(defaultTerritoryForAll(s))
		}

		sum := territorySum(s, 1, 1)
		for i, val := range flood3(s, factor) {
			sum[i] += val / (val + 1)
		}
		return sum
	}
}

// this will never be better then 4, because of the tendency to route the soldiers into the center of the map
// thus don't waste more time on it PLEASE
func flood3(s *state.State, factor float64) []float64 {
	nodeScores := make([][]float64, len(s.Nodes))
	for i := range nodeScores {
		nodeScores[i] = make([]float64, len(s.Players))
	}

	for nid, node := range s.Nodes {
		if node.Owner == -1 {
			continue
		}
		value := float64(node.Soldiers) + float64(node.MakingSoldiers)*factor
		if value != 0 {
			bfs(nid, node.Owner, value, factor, s.Matrix, nodeScores)
		}
	}

	playerScores := make([]float64, len(s.Players))
	for _, nodeScore := range nodeScores {
		pid, max1 := utils.SliceMax(nodeScore)
		nodeScore[pid] = 0
		_, max2 := utils.SliceMax(nodeScore)

		if max1 == max2 {
			continue
		}
		playerScores[pid]++
	}
	return playerScores
}

func bfs(nid, pid int, value, factor float64, matrix [][]bool, scores [][]float64) {
	type elem struct {
		nid  int
		dist int
	}
	seen := datstruct.NewSet[int]()
	seen.Add(nid)
	fifo := []elem{{nid: nid, dist: 0}}
	for i := 0; i < len(fifo); i++ {
		curr := fifo[i]
		scores[curr.nid][pid] += value * math.Pow(factor, float64(curr.dist))
		for neighbour := 0; neighbour < len(matrix); neighbour++ {
			if matrix[curr.nid][neighbour] && !seen.Has(neighbour) {
				fifo = append(fifo, elem{nid: neighbour, dist: curr.dist + 1})
				seen.Add(neighbour)
			}
		}
	}
}

func Flood4(soldierVal, makingSoldierVal float64, factor float64) func(s *state.State) []float64 {
	return func(s *state.State) []float64 {
		if s.MoveType == state.End {
			return exactPlayerScore(s)
		}
		if s.MoveType == state.Territory {
			s = s.Territory(defaultTerritoryForAll(s))
		}

		sum := territorySum(s, soldierVal, makingSoldierVal)
		for i, val := range flood4(s, factor) {
			sum[i] += val / (val + 1)
		}
		return sum
	}
}

func flood4(s *state.State, factor float64) []float64 {
	score := make([]float64, len(s.Players))
	territories := playerTerritories2(s)
	for nid1, n := range s.Nodes {
		if n.Owner == -1 || (n.Soldiers == 0 && n.MakingSoldiers == 0) {
			continue
		}
		for pid, nids := range territories {
			if pid == n.Owner {
				continue
			}
			for _, nid2 := range nids {
				score[n.Owner] += float64(n.Soldiers) * math.Pow(factor, float64(s.GraphGenData.MinimalPathDistance[nid1][nid2]))
			}
		}
	}
	return score
}

func exactPlayerScore(s *state.State) []float64 {
	if s.MoveType != state.End {
		panic("why are you calling?")
	}
	results := make([]coreutils.Result, len(s.Players))
	for pid, p := range s.Players {
		results[pid] = p.DeathInfo.Result
	}
	return coreutils.ResultsToFloat(results)
}
