package ai

import (
	state "gra/core1/state"
	"gra/coreutils"
	"gra/utils"
)

// this file is for computing rewards
// returns reward for only playing players
// the algorithm is responsible for collecting rewards - these algoritmhs can return rewards for players which did not play this turn

func GetReward1(curr *state.State, pid int) float64 {
	if curr.MoveType != state.End {
		return 0
	}

	switch curr.Players[pid].DeathInfo.Result {
	case coreutils.Lost:
		return 0
	case coreutils.Draw:
		return 0.5
	case coreutils.Win:
		return 1
	}
	panic("does not happen")
}

func GetReward2(curr *state.State, pid int) float64 {
	if curr.MoveType != state.End {
		return 0
	}

	switch curr.Players[pid].DeathInfo.Result {
	case coreutils.Lost:
		return -1
	case coreutils.Draw:
		return 0
	case coreutils.Win:
		return 1
	}
	panic("does not happen")
}

// just sum of territories
func GetReward3(curr *state.State, pid int) float64 {
	return float64(curr.Players[pid].Territory)
}

// difference of territories between last and new states
func GetReward4(s1, s2 *state.State, pid int) float64 {
	return float64(s2.Players[pid].Territory - s1.Players[pid].Territory)
}

// difference of territories between last and new states with huge bonus if player win
func GetReward5(s1, s2 *state.State, pid int) float64 {
	reward := GetReward4(s1, s2, pid)
	p := s2.Players[pid]
	if !p.Dead {
		return reward
	}
	if p.DeathInfo.Result == coreutils.Lost {
		reward += -100
	}
	if p.DeathInfo.Result == coreutils.Win {
		reward += 100
	}
	return reward
}

// reward 4 with bonus for win
func GetReward6(s1, s2 *state.State, pid int) float64 {
	return GetReward4(s1, s2, pid) + GetReward1(s2, pid)*10
}

// trying to make 4 better
func GetReward7(s1, s2 *state.State, pid int) float64 {
	return EvalBasic3(1, 1)(s2)[pid] - EvalBasic3(1, 1)(s1)[pid]
}

func GetReward8(curr *state.State, action []float64, actionUsed []float64, pid int) float64 {
	return GetReward1(curr, pid) - 1/(1+utils.L1Error(action, actionUsed))
}

func GetReward9(s1, s2 *state.State, action []float64, actionUsed []float64, pid int) float64 {
	return GetReward4(s1, s2, pid) - 1/(1+utils.L1Error(action, actionUsed))
}

func GetReward10(s1, s2 *state.State, action []float64, actionUsed []float64, pid int) float64 {
	return GetReward4(s1, s2, pid) - utils.L1Error(action, actionUsed)
}

// the best
func GetReward11(s1, s2 *state.State, action []float64, actionUsed []float64, pid int) float64 {
	return GetReward4(s1, s2, pid) + GetReward1(s2, pid)*10 - utils.L1Error(action, actionUsed)
}

func GetReward12(s1, s2 *state.State, action []float64, actionUsed []float64, pid int) float64 {
	return GetReward7(s1, s2, pid) + GetReward1(s2, pid)*10 - utils.L1Error(action, actionUsed)
}

func GetReward13(s1, s2 *state.State, action []float64, actionUsed []float64, pid int) float64 {
	return GetReward4(s1, s2, pid) + GetReward1(s2, pid)*10 - utils.L1Error(action, actionUsed)*0.1
}
