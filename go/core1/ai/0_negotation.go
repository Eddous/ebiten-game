package ai

import (
	state "gra/core1/state"
	"math/rand"
)

func RandomNegotation(s *state.State, rnd *rand.Rand, pid int) []int {
	return RandomNegotationForAll(s, rnd)[pid]
}
