package ai

import (
	"fmt"
	state "gra/core1/state"
	"gra/coreutils"
	"gra/utils"
	"math"
	"math/rand"
	"slices"
)

type DecodeMovesArgs struct {
	DestroyRate    float64
	Iters          int
	RecordToRecord float64 // 1.05 -> it takes 5% worse solutions then best
}

// desired is a vector that encodes how many soldiers should be where
func DecodeMove(s *state.State, pid int, desired []float64, args DecodeMovesArgs, seed int64) ([][]int, []float64, float64) {
	// vector which encodes how many soldiers are missing or are extra
	// -5: node needs another 5 soldiers, 3: node has 3 extra soldiers
	balance := make([]int, len(s.Nodes))
	for i, r := range desired {
		// does not make sense to have negative soldiers
		if r <= 0 {
			balance[i] = 0
		} else {
			balance[i] = -int(math.Round(r))
		}
	}

	// degrees of freedom - nodes the player can control
	dof := []int{}
	for nid, node := range s.Nodes {
		if node.Owner == pid && node.Soldiers != 0 {
			dof = append(dof, nid)
		}
	}

	rnd := rand.New(rand.NewSource(seed))
	matrix := coreutils.NewMoveMatrix(len(s.Nodes))
	for _, nid := range dof {
		solveRow(rnd, matrix, balance, nid, s.Nodes[nid].Neighbourhood, s.Nodes[nid].Soldiers)
	}

	// solving the problem
	best := newSolution(matrix, balance)
	curr := best
	for range args.Iters * s.Players[pid].CurrSoldiers {
		matrix, balance = curr.copy()

		destroyed := destroy(rnd, matrix, balance, dof, args.DestroyRate)
		utils.Shuffle(rnd, destroyed)
		for _, nidSol := range destroyed {
			solveRow(rnd, matrix, balance, nidSol.nid, s.Nodes[nidSol.nid].Neighbourhood, nidSol.soldiers)
		}

		new := newSolution(matrix, balance)
		if new.err == best.err && new.standing > best.standing {
			// BEWARE! this leads to no learning because all the soldiers stays at place
			// best = new
		}
		if new.err < best.err {
			best = new
		}
		if new.err <= int(math.Ceil(float64(best.err)*args.RecordToRecord)) {
			curr = new
		}
	}

	return best.matrix, GetDesiredFromMoveMatrix(best.matrix), float64(best.err) / float64(s.Players[pid].CurrSoldiers)
}

type solution struct {
	matrix   [][]int
	balance  []int
	err      int
	standing int
}

func newSolution(matrix [][]int, balance []int) solution {
	err := 0
	for _, bal := range balance {
		err += utils.Abs(bal)
	}

	standing := 0
	for i := range matrix {
		standing += matrix[i][i]
	}
	return solution{matrix: matrix, balance: balance, err: err, standing: standing}
}

func (s solution) copy() ([][]int, []int) {
	return utils.Copy2DSlice(s.matrix), slices.Clone(s.balance)
}

type nidSol struct {
	nid      int
	soldiers int
}

func destroy(rnd *rand.Rand, matrix [][]int, balance []int, dof []int, rate float64) []nidSol {
	ret := make([]nidSol, 0)
	for _, nid := range dof {
		if rnd.Float64() <= rate {
			sol := destroySingle(matrix, balance, nid)
			ret = append(ret, nidSol{nid: nid, soldiers: sol})
		}
	}
	return ret
}

func destroySingle(matrix [][]int, balance []int, nid int) int {
	ret := 0
	for col := range matrix {
		ret += matrix[nid][col]
		balance[col] -= matrix[nid][col]
		matrix[nid][col] = 0
	}
	return ret
}

func solveRow(rnd *rand.Rand, matrix [][]int, balance []int, nid int, neighbourhood []int, soldiers int) {
	que := []int{}
	for _, nid2 := range neighbourhood {
		if balance[nid2] >= 0 {
			continue
		}
		for range -balance[nid2] {
			que = append(que, nid2)
		}
	}
	utils.Shuffle(rnd, que)
	for i := 0; i < len(que) && i < soldiers; i++ {
		nid2 := que[i]
		matrix[nid][nid2]++
		balance[nid2]++
	}
	// all soldiers are already used
	if len(que) >= soldiers {
		return
	}
	for range soldiers - len(que) {
		nid2 := neighbourhood[rnd.Intn(len(neighbourhood))]
		matrix[nid][nid2]++
		balance[nid2]++
	}
}

var testDecoderArgs []DecodeMovesArgs = []DecodeMovesArgs{
	{Iters: 20, DestroyRate: 0.1, RecordToRecord: 1.0},
	{Iters: 20, DestroyRate: 0.1, RecordToRecord: 1.05},
	{Iters: 20, DestroyRate: 0.1, RecordToRecord: 1.1},
	{Iters: 20, DestroyRate: 0.1, RecordToRecord: 1.15},
	{Iters: 20, DestroyRate: 0.1, RecordToRecord: 1.20},
	{Iters: 20, DestroyRate: 0.1, RecordToRecord: 1.25},
}
var testDecoderErrors []float64 = make([]float64, len(testDecoderArgs))
var testDecoderIters int

// this function will create desired moves from single player move matrix and then test decode function
func TestDecoder(s *state.State, pid int, move [][]int) {
	desired := GetDesiredFromMoveMatrix(move)
	for i, args := range testDecoderArgs {
		_, _, err := DecodeMove(s, pid, desired, args, 0) // seed=0 is OK here
		testDecoderErrors[i] += err
	}
	testDecoderIters++

	if testDecoderIters%100 == 0 {
		fmt.Println()
		fmt.Println("AAAAAAAAA")
		for _, err := range testDecoderErrors {
			fmt.Println(err / float64(testDecoderIters))
		}
		fmt.Println()
	}
}

func GetDesiredFromMoveMatrix(move [][]int) []float64 {
	desired := make([]float64, len(move))
	for col := range move {
		for row := range move {
			desired[col] += float64(move[row][col])
		}
	}
	return desired
}
