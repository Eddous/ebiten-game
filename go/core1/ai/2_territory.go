package ai

import (
	state "gra/core1/state"
	"gra/datstruct"
	"math/rand"
)

// ===== Random =====

func RandomTerritory(s *state.State, rnd *rand.Rand, pid int) int {

	for _, nid := range RandomTerritoryForAll(s, rnd) {
		if s.Nodes[nid].Owner == pid {
			return nid
		}
	}
	panic("did not find my move")
}

// ===== Default =====

func DefaultTerritory(s *state.State, _ *rand.Rand, pid int) int {
	all := allTerritoryPossibilities(s)
	bestNid := -1
	score := 0
	for _, nid := range all[pid] {
		seen := datstruct.NewSet[int]()
		s.DfsByOwner(nid, seen, nil)
		if size := seen.Size(); size > score {
			bestNid = nid
			score = size
		}
	}
	return bestNid
}

func defaultTerritoryForAll(s *state.State) map[int]int {
	ret := map[int]int{}
	all := allTerritoryPossibilities(s)
	for _, pid := range s.Playing() {
		bestNid := -1
		score := 0
		for _, nid := range all[pid] {
			seen := datstruct.NewSet[int]()
			s.DfsByOwner(nid, seen, nil)
			if size := seen.Size(); size > score {
				bestNid = nid
				score = size
			}
		}
		ret[pid] = bestNid
	}
	return ret
}

// the soldierVal=spawningSoldierVal because the spawning will instantly bacome alive
func DefaultTerritory2(soldierVal float64) func(s *state.State, _ *rand.Rand, pid int) int {
	return func(s *state.State, _ *rand.Rand, pid int) int {
		all := allTerritoryPossibilities(s)
		bestNid := -1
		bestScore := 0.0

		for _, nid := range all[pid] {
			seen := datstruct.NewSet[int]()
			currScore := 0.0
			counterFunc := func(nid int) { currScore += 1 + float64(s.Nodes[nid].Soldiers+s.Nodes[nid].MakingSoldiers)*soldierVal }
			s.DfsByOwner(nid, seen, counterFunc)
			if currScore > bestScore {
				bestNid = nid
				bestScore = currScore
			}
		}
		return bestNid
	}
}

func DefaultTerritoryForAll2(s *state.State, soldierVal float64) map[int]int {
	ret := make(map[int]int)
	for _, pid := range s.Playing() {
		ret[pid] = DefaultTerritory2(soldierVal)(s, nil, pid)
	}
	return ret
}
