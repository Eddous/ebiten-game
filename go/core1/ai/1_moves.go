package ai

import (
	state "gra/core1/state"
	"gra/coreutils"
	"gra/utils"
	"maps"
	"math/rand"
)

// ===== Random =====

func randomMoves(s *state.State, rnd *rand.Rand, pid int) [][]int {
	return RandomMovesForAll(s, rnd)[pid]
}

// ===== LevelK =====

func LevelKThinking(
	passes int,
	search func(rnd *rand.Rand, move [][]int, measure func([][]int) float64) Solution,
	evaluate func(*state.State) []float64,
	normalize func([]float64) []float64,
) func(s *state.State, rnd *rand.Rand, pid int) [][]int {
	return func(s *state.State, rnd *rand.Rand, pid int) [][]int {
		return levelKThinking(s, rnd, pid, passes, search, evaluate, normalize)
	}
}

func levelKThinking(
	s *state.State,
	rnd *rand.Rand,
	pid int,
	passes int,
	search func(rnd *rand.Rand, move [][]int, measure func([][]int) float64) Solution,
	evaluate func(*state.State) []float64,
	normalize func([]float64) []float64,
) [][]int {
	var moves map[int][][]int
	moves = makeRandomMove(s, rnd)

	if passes == 0 {
		return moves[pid]
	}

	pids := make([]int, 0)
	pids = append(pids, s.Playing()...)

	for range passes - 1 {
		new := make(map[int][][]int)
		for _, pid := range pids {
			new[pid] = optimizeForPlayer(s, rnd, pid, search, evaluate, normalize, moves)
		}
		moves = new
	}
	return optimizeForPlayer(s, rnd, pid, search, evaluate, normalize, moves)
}

func levelKForAll(
	s *state.State,
	rnd *rand.Rand,
	passes int,
	search func(rnd *rand.Rand, move [][]int, measure func([][]int) float64) Solution,
	evaluate func(*state.State) []float64,
	normalize func([]float64) []float64,
) map[int][][]int {
	var moves map[int][][]int
	moves = makeRandomMove(s, rnd)

	if passes == 0 {
		return moves
	}

	pids := make([]int, 0)
	pids = append(pids, s.Playing()...)

	for range passes {
		new := make(map[int][][]int)
		for _, pid := range pids {
			new[pid] = optimizeForPlayer(s, rnd, pid, search, evaluate, normalize, moves)
		}
		moves = new
	}
	return moves
}

// ===== Bean (reimplemented) =====

func BeanMoves1(
	passes int,
	search func(rnd *rand.Rand, move [][]int, measure func([][]int) float64) Solution,
	evaluate func(*state.State) []float64,
	normalize func([]float64) []float64,
) func(s *state.State, rnd *rand.Rand, pid int) [][]int {
	return func(s *state.State, rnd *rand.Rand, pid int) [][]int {
		return beanMoves1(s, rnd, pid, passes, search, evaluate, normalize)
	}
}

func beanMoves1(
	s *state.State,
	rnd *rand.Rand,
	pid int,
	passes int,
	search func(rnd *rand.Rand, move [][]int, measure func([][]int) float64) Solution,
	evaluate func(*state.State) []float64,
	normalize func([]float64) []float64,
) [][]int {
	moves := beanMoves1ForAll(s, rnd, passes, search, evaluate, normalize)
	return moves[pid]
}

func beanMoves1ForAll(
	s *state.State,
	rnd *rand.Rand,
	passes int,
	search func(rnd *rand.Rand, move [][]int, measure func([][]int) float64) Solution,
	evaluate func(*state.State) []float64,
	normalize func([]float64) []float64,
) map[int][][]int {
	moves := makeRandomMove(s, rnd)
	pids := make([]int, 0)
	pids = append(pids, s.Playing()...)

	for range passes {
		utils.Shuffle(rnd, pids)
		for _, pid := range pids {
			moves[pid] = optimizeForPlayer(s, rnd, pid, search, evaluate, normalize, moves)
		}
	}
	return moves
}

// ====== Bean 2 ======

// passes2 is the number of found all-piece moves
func BeanMoves2(
	passes int,
	search func(rnd *rand.Rand, move [][]int, measure func([][]int) float64) Solution,
	evaluate func(*state.State) []float64,
	normalize func([]float64) []float64,
) func(s *state.State, rnd *rand.Rand, pid int) [][]int {
	return func(s *state.State, rnd *rand.Rand, pid int) [][]int {
		return beanMoves2(s, rnd, pid, passes, search, evaluate, normalize)
	}
}

// passes2 is the number of found all-piece moves
func beanMoves2(
	s *state.State,
	rnd *rand.Rand,
	pid int,
	passes2 int,
	search func(rnd *rand.Rand, move [][]int, measure func([][]int) float64) Solution,
	evaluate func(*state.State) []float64,
	normalize func([]float64) []float64,
) [][]int {
	moves := make([]map[int][][]int, passes2)
	for i := range passes2 {
		moves[i] = beanMoves1ForAll(s, rnd, 2, search, evaluate, normalize)
	}

	maxScoreIndx := -1
	maxScore := -1.
	for i := range passes2 {
		score := 0.
		for j := range passes2 {
			/*
					s1   s2   s3
				a1  0.3  0.1  0.5
				a2  1    0    0
				a3  0.5  0.5  0.5

				E(a1) = 1/3*0.3 + 1/3*0.1 + 1/3*0.5
				assuming that actions of other players are uniformly distributed and taking the best expected value
			*/
			copy := maps.Clone(moves[j])
			copy[pid] = moves[i][pid]
			score += normalize(evaluate(s.Move(copy)))[pid]
		}
		if score > maxScore {
			maxScore = score
			maxScoreIndx = i
		}
	}

	return moves[maxScoreIndx][pid]
}

// ===== Bean3 ======

func BeanMoves3(
	passes int,
	search func(rnd *rand.Rand, move [][]int, measure func([][]int) float64) Solution,
	evaluate func(*state.State) []float64,
	normalize func([]float64) []float64,
) func(s *state.State, rnd *rand.Rand, pid int) [][]int {
	return func(s *state.State, rnd *rand.Rand, pid int) [][]int {
		return beanMoves3(s, rnd, pid, passes, search, evaluate, normalize)
	}
}

/*
Here goes list of things that I tried

using K-Reasoning framework
random init / null init / beanMoves1ForAll
copying random actions from the history
*/
func beanMoves3(
	s *state.State,
	rnd *rand.Rand,
	pid int,
	passes int,
	search func(rnd *rand.Rand, move [][]int, measure func([][]int) float64) Solution,
	evaluate func(*state.State) []float64,
	normalize func([]float64) []float64,
) [][]int {
	assumption := []map[int][][]int{beanMoves1ForAll(s, rnd, 2, search, evaluate, normalize)}
	pids := append([]int{}, s.Playing()...)

	for i := 1; i < passes; i++ {
		assumption = append(assumption, maps.Clone(assumption[i-1]))
		utils.Shuffle(rnd, pids)
		for _, pid := range pids {
			assumption[i][pid] = optimizeForPlayer2(s, rnd, pid, search, evaluate, normalize, assumption)
		}
	}
	return assumption[len(assumption)-1][pid]
}

func beanMoves3ForAll(
	s *state.State,
	rnd *rand.Rand,
	passes int,
	search func(rnd *rand.Rand, move [][]int, measure func([][]int) float64) Solution,
	evaluate func(*state.State) []float64,
	normalize func([]float64) []float64,
) map[int][][]int {
	assumption := []map[int][][]int{beanMoves1ForAll(s, rnd, 2, search, evaluate, normalize)}
	pids := append([]int{}, s.Playing()...)

	for i := 1; i < passes; i++ {
		assumption = append(assumption, maps.Clone(assumption[i-1]))
		utils.Shuffle(rnd, pids)
		for _, pid := range pids {
			assumption[i][pid] = optimizeForPlayer2(s, rnd, pid, search, evaluate, normalize, assumption)
		}
	}
	return assumption[len(assumption)-1]
}

// ===== Bean4 ======

// passes2 is the number of found all-piece moves
// potential for monte carlo tree search!!
func BeanMoves4(
	actions int,
	passes int,
	search func(rnd *rand.Rand, move [][]int, measure func([][]int) float64) Solution,
	evaluate func(*state.State) []float64,
	normalize func([]float64) []float64,
	evaluate2 func(*state.State) []float64,
) func(s *state.State, rnd *rand.Rand, pid int) [][]int {
	return func(s *state.State, rnd *rand.Rand, pid int) [][]int {
		return beanMoves4(s, rnd, pid, actions, passes, search, evaluate, normalize, evaluate2)
	}
}

// passes2 is the number of found all-piece moves
func beanMoves4(
	s *state.State,
	rnd *rand.Rand,
	pid int,
	actions int,
	passes int,
	search func(rnd *rand.Rand, move [][]int, measure func([][]int) float64) Solution,
	evaluate func(*state.State) []float64,
	normalize func([]float64) []float64,
	evaluate2 func(*state.State) []float64,
) [][]int {
	moves := make([]map[int][][]int, actions)
	for i := range actions {
		moves[i] = beanMoves3ForAll(s, rnd, 4, search, evaluate, normalize)
	}

	pa := make(map[int][][]int)
	pids := append([]int{}, s.Playing()...)
	for _, pid := range pids {
		pa[pid] = moves[rnd.Intn(actions)][pid]
	}

	for range passes {
		utils.Shuffle(rnd, pids)
		for _, pid := range pids {
			bestIndex, bestScore := -1, -1.
			for i := range actions {
				pa[pid] = moves[i][pid]
				s := normalize(evaluate2(s.Move(pa)))[pid]
				if bestScore < s {
					bestScore = s
					bestIndex = i
				}
			}
			pa[pid] = moves[bestIndex][pid]
		}
	}
	return pa[pid]
}

// ===== Region optimization =====

func regionMoves(s *state.State, pid int) [][]int {
	// 1. optimize paths to the alone soldiers
	// 2. find regions
	// 3. optimize all regions in some manner
	panic("not implemented")
}

// this will optimize region, and will return result into assumption
func optimizeRegion(
	rnd *rand.Rand,
	s *state.State,
	nids []int,
	assumption [][]int,
	search func(rnd *rand.Rand, move [][]int, measure func([][]int) float64) Solution,
	evaluate func(*state.State) []float64,
	normalize func([]float64) []float64,
) {
	panic("not implemented")
}

// ===== HELPERS =====

// this was always worse then a random init
func makeNullMove(s *state.State) map[int][][]int {
	moves := map[int][][]int{}
	for nid, node := range s.Nodes {
		if node.Soldiers == 0 {
			continue
		}
		pid := node.Owner
		if _, ok := moves[pid]; !ok {
			moves[pid] = coreutils.NewMoveMatrix(len(s.Matrix))
		}
		moves[pid][nid][nid] = node.Soldiers
	}
	return moves
}

func makeRandomMove(s *state.State, rnd *rand.Rand) map[int][][]int {
	moves := map[int][][]int{}
	for nid, node := range s.Nodes {
		if node.Soldiers == 0 {
			continue
		}
		pid := node.Owner
		if _, ok := moves[pid]; !ok {
			moves[pid] = coreutils.NewMoveMatrix(len(s.Matrix))
		}
		for _, nid2 := range coreutils.CombinationWithRepetetion2(rnd, node.Neighbourhood, node.Soldiers) {
			moves[pid][nid][nid2]++
		}
	}
	return moves

}

// this function can be used for bean0, bean1, bean2.1, with slight change even for 2.2
// assumption HAS SELFLOOPS!
// goal is to
//  1. take all current moves of selected pid from assumption
//  2. make from them intermidiate representation for optimizer
//	3. lastly return moves of pid

func optimizeForPlayer(
	s *state.State,
	rnd *rand.Rand,
	pid int,
	search func(rnd *rand.Rand, move [][]int, measure func([][]int) float64) Solution,
	evaluate func(*state.State) []float64,
	normalize func([]float64) []float64,
	assumption map[int][][]int,
) [][]int {
	curr, indexToNid := currentSolutionToIntermidiateRepresetation(s, assumption[pid])

	best := search(
		rnd,
		curr,
		func(move [][]int) float64 {
			tmp := maps.Clone(assumption)
			tmp[pid] = intermidiateToPidMove(move, indexToNid, s.Nodes)
			return normalize(evaluate(s.Move(tmp)))[pid]
		},
	)

	// writing best solution to the matrix
	return intermidiateToPidMove(best.move, indexToNid, s.Nodes)
}

func currentSolutionToIntermidiateRepresetation(s *state.State, assumption [][]int) ([][]int, []int) {
	curr := [][]int{}     // these are moves, but encoded in a different way than a classical matrix
	indexToNid := []int{} // [2, 4, 5] -> solution[0] is about node 2, solution[1] is about node 4 ...

	// filling current solution of selected player
	for nid, node := range s.Nodes {
		if node.Soldiers == 0 {
			continue
		}
		nodeMove := make([]int, len(node.Neighbourhood))
		for i, nid2 := range node.Neighbourhood {
			nodeMove[i] = assumption[nid][nid2]
		}
		curr = append(curr, nodeMove)
		indexToNid = append(indexToNid, nid)
	}
	return curr, indexToNid
}

func intermidiateToPidMove(solution [][]int, indexToNid []int, nodes []state.Node) [][]int {
	moveMatrix := coreutils.NewMoveMatrix(len(nodes))
	for i, neighbourMove := range solution {
		nid1 := indexToNid[i]
		for neighIndex, soldiers := range neighbourMove {
			nid2 := nodes[nid1].Neighbourhood[neighIndex]
			moveMatrix[nid1][nid2] = soldiers
		}
	}
	return moveMatrix
}

func optimizeForPlayer2(
	s *state.State,
	rnd *rand.Rand,
	pid int,
	search func(rnd *rand.Rand, move [][]int, measure func([][]int) float64) Solution,
	evaluate func(*state.State) []float64,
	normalize func([]float64) []float64,
	assumption []map[int][][]int,
) [][]int {
	indx := len(assumption) - 1
	curr, indexToNid := currentSolutionToIntermidiateRepresetation(s, assumption[indx][pid])
	best := search(
		rnd,
		curr,
		func(move [][]int) float64 {
			sum := 0.
			for i := range assumption {
				tmp := maps.Clone(assumption[i])
				tmp[pid] = intermidiateToPidMove(move, indexToNid, s.Nodes)
				sum += normalize(evaluate(s.Move(tmp)))[pid]
			}
			return sum
		},
	)

	return intermidiateToPidMove(best.move, indexToNid, s.Nodes)
}
