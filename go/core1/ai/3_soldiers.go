package ai

import (
	state "gra/core1/state"
	"gra/coreutils"
	"gra/datstruct"
	"gra/utils"
	"math"
	"math/rand"
)

// ===== Random =====

func RandomSoldiers(s *state.State, rnd *rand.Rand, pid int) []int {
	return RandomSoldiersForAll(s, rnd)[pid]
}

// ===== Default =====

func DefaultSoldiersForAll(s *state.State, rnd *rand.Rand, stoppingCriterion byte) map[int][]int {
	ret := map[int][]int{}
	for _, pid := range s.Playing() {
		ret[pid] = defaultSoldiers(s, rnd, pid, stoppingCriterion)
	}
	return ret
}

func DefaultSoldiers(stoppingCriterion byte) func(s *state.State, rnd *rand.Rand, pid int) []int {
	return func(s *state.State, rnd *rand.Rand, pid int) []int {
		return defaultSoldiers(s, rnd, pid, stoppingCriterion)
	}
}

func defaultSoldiers(s *state.State, rnd *rand.Rand, pid int, stoppingCriterion byte) []int {
	soldiersToMake := s.Players[pid].MaxSoldiers - s.Players[pid].CurrSoldiers
	candidates := playerTerritories1(s)[pid]

	candidates2 := []int{}
	for _, nid := range candidates {
		if !isStupidChoice(s, pid, nid) {
			candidates2 = append(candidates2, nid)
		}
	}
	if len(candidates2) == 0 {
		return chooseFromCandidates(rnd, candidates, soldiersToMake)
	}

	candidates3 := []int{}
	minDist := math.MaxInt32
	for _, nid := range candidates2 {
		dist := distOfClosestNotYourTerritory(s, pid, nid, stoppingCriterion)
		if dist < minDist {
			minDist = dist
			candidates3 = []int{nid}
			continue
		}
		if dist == minDist {
			candidates3 = append(candidates3, nid)
		}
	}

	return chooseFromCandidates(rnd, candidates3, soldiersToMake)
}

func chooseFromCandidates(rnd *rand.Rand, candidates []int, number int) []int {
	ret := make([]int, number)
	indexes := coreutils.CombinationWithRepetetion(rnd, len(candidates), number)
	for i, inx := range indexes {
		ret[i] = candidates[inx]
	}
	return ret
}

func isStupidChoice(s *state.State, pid int, nid int) bool {
	proximityArmies := map[int]int{}

	for _, nid2 := range s.Nodes[nid].Neighbourhood {
		node2 := s.Nodes[nid2]
		if node2.Soldiers == 0 {
			continue
		}
		if _, ok := proximityArmies[node2.Owner]; !ok {
			proximityArmies[node2.Owner] = 0
		}
		proximityArmies[node2.Owner] += node2.Soldiers
	}
	for _, soldiers := range proximityArmies {
		if soldiers > proximityArmies[pid] {
			return true
		}
	}
	return false
}

func distOfClosestNotYourTerritory(s *state.State, pid, nid int, stoppingCriterion byte) int {
	type elem struct {
		nid  int
		dist int
	}
	seen := datstruct.NewSet[int]()
	seen.Add(nid)
	fifo := []elem{{nid: nid, dist: 0}}
	for i := 0; i < len(fifo); i++ {
		curr := fifo[i]
		for neighbour := 0; neighbour < len(s.Matrix); neighbour++ {
			if s.Matrix[curr.nid][neighbour] && !seen.Has(neighbour) {
				owner := s.Nodes[neighbour].Owner
				switch stoppingCriterion {
				case 0:
					if owner != pid {
						return curr.dist + 1
					}
				case 1:
					if owner != pid && owner != -1 {
						return curr.dist + 1
					}
				case 2:
					if owner != pid && s.Nodes[neighbour].Soldiers != 0 {
						return curr.dist + 1
					}
				default:
					panic("not an option")
				}

				fifo = append(fifo, elem{nid: neighbour, dist: curr.dist + 1})
				seen.Add(neighbour)
			}
		}
	}
	if stoppingCriterion == 2 {
		// this can happen when enemy has no soldiers
		return distOfClosestNotYourTerritory(s, pid, nid, 1)
	}
	panic("did not find ")
}

// ===== Bean =====

func BeanSoldiers(evaluate func(*state.State) []float64, strategyIters int, normalize func([]float64) []float64) func(s *state.State, rnd *rand.Rand, pid int) []int {
	return func(s *state.State, rnd *rand.Rand, pid int) []int {
		return predictSoldiers(s, rnd, pid, evaluate, strategyIters, normalize)
	}
}

// could be optimalized by keeping one soldier array and just updating the values insted of making the new soldier array allways
func predictSoldiers(s *state.State, rnd *rand.Rand, pid int, evaluate func(*state.State) []float64, strategyIters int, normalize func([]float64) []float64) []int {
	// assigning random move
	territories := playerTerritories1(s)
	choices := make([]playerSoldierChoice, 0)
	for _, pid := range s.Playing() {
		player := s.Players[pid]
		move0 := coreutils.CombinationWithRepetetion(rnd, player.Territory, player.MaxSoldiers-player.CurrSoldiers)
		// translation
		for i := range move0 {
			move0[i] = territories[pid][move0[i]]
		}
		choices = append(choices,
			playerSoldierChoice{
				pid:          pid,
				posibilities: territories[pid],
				selected:     move0,
			},
		)
	}

	// making the decision
	for i := 0; i < strategyIters; i++ {
		utils.Shuffle(rnd, choices)
		for _, ch := range choices {
			// =============================================
			// finding the local maximum (really dumb TODO!)
			utils.Shuffle(rnd, ch.selected)
			for i := range ch.selected {
				scores := []float64{}
				for _, pos := range ch.posibilities {
					ch.selected[i] = pos
					s := normalize(evaluate(s.Soldiers(playerSoldierChoicesToArray(choices))))[ch.pid]
					scores = append(scores, s)
				}
				bestValues, _ := utils.SliceAllMaxes(scores)
				ch.selected[i] = ch.posibilities[rnd.Intn(len(bestValues))]
			}
			// finding the local maximum (really dumb TODO!)
			// =============================================
		}
	}

	ret := map[int][]int{}
	for _, ch := range choices {
		ret[ch.pid] = ch.selected
	}
	return ret[pid]
}

type playerSoldierChoice struct {
	pid          int
	posibilities []int // all nids which can be used, it does not matter how you iterate through them
	selected     []int // selected nids, the index does not play role (always shufle it!)
}

func playerSoldierChoicesToArray(choices []playerSoldierChoice) map[int][]int {
	ret := map[int][]int{}
	for _, ch := range choices {
		ret[ch.pid] = ch.selected
	}
	return ret
}
