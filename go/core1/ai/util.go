package ai

import (
	state "gra/core1/state"
	"gra/datstruct"
)

// it filters neutral territories (Owner == -1)
func playerTerritories1(s *state.State) map[int][]int {
	territories := make(map[int][]int)
	for nid, n := range s.Nodes {
		if n.Owner != -1 {
			territories[n.Owner] = append(territories[n.Owner], nid)
		}
	}
	return territories
}

func allTerritoryPossibilities(s *state.State) map[int][]int {
	if s.MoveType != state.Territory {
		panic("wrong state")
	}

	moves := make(map[int][]int)
	seen := datstruct.NewSet[int]()
	for nid, n := range s.Nodes {
		if n.Owner != -1 && s.Players[n.Owner].Playing && !seen.Has(nid) {
			s.DfsByOwner(nid, seen, nil)
			if moves[n.Owner] == nil {
				moves[n.Owner] = make([]int, 0)
			}
			moves[n.Owner] = append(moves[n.Owner], nid)
		}
	}
	return moves
}

// does not filter out the neutral
func playerTerritories2(s *state.State) map[int][]int {
	territories := make(map[int][]int)
	for nid, n := range s.Nodes {
		territories[n.Owner] = append(territories[n.Owner], nid)
	}
	return territories
}
