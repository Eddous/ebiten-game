package ai

import (
	state "gra/core1/state"
	"gra/coreutils"
	"math/rand"
)

func RandomNegotationForAll(s *state.State, rnd *rand.Rand) map[int][]int {
	ret := map[int][]int{}
	alive := len(s.AlivePlayers())
	for _, pid := range s.Playing() {
		for {
			rndChoice := coreutils.RandomTuple(rnd, alive, len(s.Nodes))
			if s.CheckNegotationInput(pid, rndChoice) {
				ret[pid] = rndChoice
				break
			}
		}
	}
	return ret
}

func RandomMovesForAll(s *state.State, rnd *rand.Rand) map[int][][]int {
	moves := map[int][][]int{}
	for nid1, n := range s.Nodes {
		if n.Soldiers == 0 {
			continue
		}
		if _, ok := moves[n.Owner]; !ok {
			moves[n.Owner] = coreutils.NewMoveMatrix(len(s.Matrix))
		}
		for _, nid2 := range coreutils.CombinationWithRepetetion2(rnd, n.Neighbourhood, n.Soldiers) {
			moves[n.Owner][nid1][nid2]++
		}
	}
	return moves
}

func RandomTerritoryForAll(s *state.State, rnd *rand.Rand) map[int]int {
	if s.MoveType != state.Territory {
		panic("wrong state")
	}

	allTerritory := allTerritoryPossibilities(s)
	moves := map[int]int{}
	for _, pid := range s.Playing() {
		nids := allTerritory[pid]
		moves[pid] = nids[rnd.Intn(len(nids))]
	}
	return moves
}

func RandomSoldiersForAll(s *state.State, rnd *rand.Rand) map[int][]int {
	if s.MoveType != state.Soldiers {
		panic("wrong state")
	}
	territories := playerTerritories1(s)
	moves := map[int][]int{}
	for _, pid := range s.Playing() {
		p := s.Players[pid]
		soldiersToMake := p.MaxSoldiers - p.CurrSoldiers
		choices := coreutils.CombinationWithRepetetion(rnd, p.Territory, soldiersToMake)

		move := []int{}
		for _, index := range choices {
			move = append(move, territories[pid][index])
		}
		moves[pid] = move
	}
	return moves
}
