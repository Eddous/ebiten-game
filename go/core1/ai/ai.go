package ai

import (
	state "gra/core1/state"
	"math/rand"
)

var AIZoo []func() *AI = []func() *AI{
	func() *AI {
		return &AI{
			Pid: -1,
			Neg: RandomNegotation,
			Mov: BeanMoves4(3, 2, LocalSearch, EvalBasic4(), LinearNormalization, EvalBasic4()),
			Ter: DefaultTerritory2(1),
			Sol: DefaultSoldiers(1),
		}
	},
	func() *AI {
		return &AI{
			Pid: -1,
			Neg: RandomNegotation,
			Mov: BeanMoves4(3, 2, LocalSearch, EvalBasic4(), LinearNormalization, EvalMonteCarlo()),
			Ter: DefaultTerritory2(1),
			Sol: DefaultSoldiers(1),
		}
	},
}

// full baked AIs
var AIBattery []func() *AI = []func() *AI{
	func() *AI {
		return &AI{
			Pid: -1,
			Neg: RandomNegotation,
			Mov: BeanMoves1(2, LocalSearch, EvalBasic4(), LinearNormalization),
			Ter: DefaultTerritory2(1),
			Sol: DefaultSoldiers(1),
		}
	},
	func() *AI {
		return &AI{
			Pid: -1,
			Neg: RandomNegotation,
			Mov: BeanMoves3(4, LocalSearch, EvalBasic4(), LinearNormalization),
			Ter: DefaultTerritory2(1),
			Sol: DefaultSoldiers(1),
		}
	},
	func() *AI {
		return &AI{
			Pid: -1,
			Neg: RandomNegotation,
			Mov: BeanMoves3(4, DestroyAndRepair(true, 10, 0.9), EvalBasic4(), LinearNormalization),
			Ter: DefaultTerritory2(1),
			Sol: DefaultSoldiers(1),
		}
	},
}

// ================================================

type AI struct {
	Pid int
	Rnd *rand.Rand

	Neg func(*state.State, *rand.Rand, int) []int
	Mov func(*state.State, *rand.Rand, int) [][]int
	Ter func(*state.State, *rand.Rand, int) int
	Sol func(*state.State, *rand.Rand, int) []int
}

// =================================================

// pid can be set to arbitrary number - if you want to use AI object for multiple players
func (ai *AI) Init(pid int, seed int64) {
	ai.Pid = pid
	ai.Rnd = rand.New(rand.NewSource(seed))
}

func (ai *AI) Negotation(s *state.State) []int {
	return ai.Neg(s, ai.Rnd, ai.Pid)
}

func (ai *AI) Moves(s *state.State) [][]int {
	matrix := ai.Mov(s, ai.Rnd, ai.Pid)
	ai.checkMoveMatrix(s, matrix)
	return matrix
}

func (ai *AI) Territory(s *state.State) int {
	return ai.Ter(s, ai.Rnd, ai.Pid)
}

func (ai *AI) Soldiers(s *state.State) []int {
	return ai.Sol(s, ai.Rnd, ai.Pid)
}

// =================================================

func (ai *AI) NegotationForAll(s *state.State) map[int][]int {
	ret := map[int][]int{}
	for _, pid := range s.Playing() {
		ret[pid] = ai.Neg(s, ai.Rnd, pid)
	}
	return ret
}

func (ai *AI) MovesForAll(s *state.State) map[int][][]int {
	ret := map[int][][]int{}
	for _, pid := range s.Playing() {
		ret[pid] = ai.Mov(s, ai.Rnd, pid)
	}
	return ret
}

func (ai *AI) TerritoryForAll(s *state.State) map[int]int {
	ret := map[int]int{}
	for _, pid := range s.Playing() {
		ret[pid] = ai.Ter(s, ai.Rnd, pid)
	}
	return ret
}

func (ai *AI) SoldiersForAll(s *state.State) map[int][]int {
	ret := map[int][]int{}
	for _, pid := range s.Playing() {
		ret[pid] = ai.Sol(s, ai.Rnd, pid)
	}
	return ret
}

// =================================================

func AIsNegotation(ais []*AI, s *state.State) map[int][]int {
	moves := map[int][]int{}
	for _, ai := range ais {
		if s.Players[ai.Pid].Playing {
			moves[ai.Pid] = ai.Negotation(s)
		}
	}
	return moves
}

func AIsMoves(ais []*AI, s *state.State) map[int][][]int {
	moves := map[int][][]int{}
	for _, ai := range ais {
		if s.Players[ai.Pid].Playing {
			moves[ai.Pid] = ai.Moves(s)
		}
	}
	return moves
}

func AIsTerritory(ais []*AI, s *state.State) map[int]int {
	moves := map[int]int{}
	for _, ai := range ais {
		if s.Players[ai.Pid].Playing {
			moves[ai.Pid] = ai.Territory(s)
		}
	}
	return moves
}

func AIsSoldiers(ais []*AI, s *state.State) map[int][]int {
	moves := map[int][]int{}
	for _, ai := range ais {
		if s.Players[ai.Pid].Playing {
			moves[ai.Pid] = ai.Soldiers(s)
		}
	}
	return moves
}

// =================================================

func (ai *AI) checkMoveMatrix(s *state.State, matrix [][]int) {
	for i := range matrix {
		for j := range matrix {
			if matrix[i][j] != 0 && s.Nodes[i].Owner != ai.Pid {
				panic("WRONG OWNER!")
			}
			if matrix[i][j] < 0 {
				panic("minus in matrix")
			}
			if matrix[i][j] != 0 && !s.Matrix[i][j] && i != j {
				panic("wrong connection")
			}
		}
	}

	for nid, node := range s.Nodes {
		if node.Owner != ai.Pid {
			continue
		}
		sum := 0
		for _, soldiers := range matrix[nid] {
			sum += soldiers
		}
		if node.Soldiers != sum {
			panic("sum of row in the matrix is not EQ with number of soldiers")
		}
	}

}
