package ai

import (
	"fmt"
	"gra/coreutils"
	"gra/utils"
	"math"
	"math/rand"
	"slices"
)

func ProbeOfSearches(statistics *[][]float64, searches []func(*rand.Rand, [][]int, func([][]int) float64) Solution) func(*rand.Rand, [][]int, func([][]int) float64) Solution {
	return func(rnd *rand.Rand, move [][]int, measure func([][]int) float64) Solution {
		return probeOfSearches(rnd, move, measure, statistics, searches)
	}
}

func PrintSummaryOfStatistics(statistics [][]float64) {
	result := make([]int, len(statistics[0]))
	for i := range statistics {
		max := slices.Max(statistics[i])
		for j, score := range statistics[i] {
			if score == max {
				result[j]++
			}
		}
	}
	fmt.Println(result)
}

// will run every search and gather information about them, printing which one is the best
func probeOfSearches(rnd *rand.Rand, move [][]int, measure func([][]int) float64, statistics *[][]float64, searches []func(*rand.Rand, [][]int, func([][]int) float64) Solution) Solution {
	solutions := make([]Solution, 0)
	for _, s := range searches {
		solutions = append(solutions, s(rnd, move, measure))
	}

	best := solutions[0]
	stat := []float64{solutions[0].score}
	for i := 1; i < len(solutions); i++ {
		s := solutions[i]
		if s.score > best.score {
			best = s
		}
		stat = append(stat, s.score)
	}
	*statistics = append(*statistics, stat)

	return best
}

// this is a single Solution - all moves of a player
// for each node, which does have soldiers, it holds one list where is how many of soldiers goes to which neighbour
/*	player has 2 nodes with soldiers, from 1. 1 soldiers goes to first neighbour ...
	[
		[1,0,1],
		[5,0],
	]
*/
type Solution struct {
	score float64
	move  [][]int
}

func (s Solution) copy() [][]int {
	return utils.Copy2DSlice(s.move)
}

type soldierMove2 struct {
	nnid1 int
	nnid2 int
}

func MultipassLocalSearch(passes int) func(rnd *rand.Rand, move [][]int, measure func([][]int) float64) Solution {
	return func(rnd *rand.Rand, move [][]int, measure func([][]int) float64) Solution {
		return multiPassLocalSearch(rnd, move, measure, passes)
	}
}

func multiPassLocalSearch(rnd *rand.Rand, move [][]int, measure func([][]int) float64, passes int) Solution {
	sol := LocalSearch(rnd, move, measure)
	for i := 1; i < passes; i++ {
		sol = LocalSearch(rnd, sol.move, measure)
	}
	return sol
}

func LocalSearch(rnd *rand.Rand, move [][]int, measure func([][]int) float64) Solution {
	move = utils.Copy2DSlice(move)
	soldierMoves := make([]soldierMove2, 0)

	for i := range move {
		for j := range move[i] {
			for range move[i][j] {
				soldierMoves = append(soldierMoves, soldierMove2{i, j})
			}
		}
	}
	utils.Shuffle(rnd, soldierMoves)
	for _, sm := range soldierMoves {
		move[sm.nnid1][sm.nnid2]--
		bestNnids2 := []int{}
		bestScore := -1.0

		for nnid2 := range move[sm.nnid1] {
			move[sm.nnid1][nnid2]++
			score := measure(move)
			if score > bestScore {
				bestNnids2 = []int{nnid2}
				bestScore = score
			}
			if score == bestScore {
				bestNnids2 = append(bestNnids2, nnid2)
			}
			move[sm.nnid1][nnid2]--
		}

		nnid2 := bestNnids2[rnd.Intn(len(bestNnids2))]
		move[sm.nnid1][nnid2]++
	}
	return Solution{measure(move), move}
}

func RandomChangesSearch(iters int, changeRate float64, localSearchBefore bool, localSearchAfter bool) func(*rand.Rand, [][]int, func([][]int) float64) Solution {
	return func(rnd *rand.Rand, move [][]int, measure func([][]int) float64) Solution {
		return randomChangesSearch(rnd, move, measure, iters, changeRate, localSearchBefore, localSearchAfter)
	}
}

// iters are multiplied by number of users moves
func randomChangesSearch(rnd *rand.Rand, cand [][]int, measure func([][]int) float64, iters int, changeRate float64, localSearchBefore bool, localSearchAfter bool) Solution {
	var curr Solution
	if localSearchBefore {
		curr = LocalSearch(rnd, cand, measure)
	} else {
		curr = Solution{measure(cand), cand}
	}

	for range iters * len(cand) {
		cand = curr.copy()
		for range int(math.Ceil(changeRate * float64(len(cand)))) {
			i := rnd.Intn(len(cand))
			cand[i] = randomizeSingleNode(rnd, cand[i])
		}
		score := measure(cand)
		if score > curr.score {
			curr = Solution{score, cand}
		}
	}

	if localSearchAfter {
		cand = LocalSearch(rnd, cand, measure).move
	}
	return Solution{measure(cand), cand}
}

func randomizeSingleNode(rnd *rand.Rand, move []int) []int {
	ret := make([]int, len(move))
	soldiers := 0
	for i := range move {
		soldiers += move[i]
	}
	for _, j := range coreutils.CombinationWithRepetetion(rnd, len(move), soldiers) {
		ret[j]++
	}
	return ret
}

func AllPosibilities(_ *rand.Rand, move [][]int, measure func([][]int) float64) Solution {
	move = utils.Copy2DSlice(move)

	// all possibilities
	// node, all possible moves
	possibilities := make([][][]int, len(move))
	for i := range move {
		soldiers := 0
		for j := range move[i] {
			soldiers += move[i][j]
		}
		possibilities[i] = translateAllCombinationWithRepetetion(
			coreutils.AllCombinationsWithRepetetion(
				len(move[i]),
				soldiers,
			),
			len(move[i]),
		)
	}

	var bestScore = -1.0
	var best [][]int

	// [0,0,1] - means first option on first node, first option on second node and second option on the third node
	mask := make([]int, len(move))

	for {
		curr := maskToMove(mask, possibilities)

		score := measure(curr)
		if score > bestScore {
			best = curr
			bestScore = score
		}

		if !incrementMask(mask, possibilities) {
			return Solution{bestScore, best}
		}
	}
}

func maskToMove(mask []int, possibilities [][][]int) [][]int {
	move := make([][]int, len(possibilities))
	for nid, i := range mask {
		move[nid] = possibilities[nid][i]
	}
	return move
}

func incrementMask(mask []int, possibilities [][][]int) bool {
	for i := range mask {
		mask[i]++
		if mask[i] < len(possibilities[i]) {
			return true
		}
		mask[i] = 0
	}
	return false
}

func translateAllCombinationWithRepetetion(in [][]int, nodes int) [][]int {
	ret := make([][]int, len(in))
	for i := range in {
		ret[i] = make([]int, nodes)
		for _, j := range in[i] {
			ret[i][j]++
		}
	}
	return ret
}

// Next time do a reiterated local search... much better
func DestroyAndRepair(prePost bool, iters int, portion float64) func(*rand.Rand, [][]int, func([][]int) float64) Solution {
	return func(rnd *rand.Rand, move [][]int, measure func([][]int) float64) Solution {
		return destroyAndRepair(rnd, move, measure, prePost, iters, portion)
	}
}

func destroyAndRepair(rnd *rand.Rand, move [][]int, measure func([][]int) float64, preAndPost bool, iters int, portion float64) Solution {

	tmp := utils.Copy2DSlice(move)
	best := Solution{measure(tmp), tmp}
	if preAndPost {
		best = LocalSearch(rnd, best.move, measure)
	}

	for range iters {
		cand := best.copy()
		translateTable := []int{}
		limitedMove := [][]int{}
		for i := range best.move {
			if rnd.Float64() < portion {
				limitedMove = append(limitedMove, randomizeSingleNode(rnd, best.move[i]))
				translateTable = append(translateTable, i)
			}
		}
		// result is OF COURSE in limited space
		cand2 := LocalSearch(
			rnd,
			limitedMove,
			func(limMove [][]int) float64 {
				for i, j := range translateTable {
					cand[j] = limMove[i]
				}
				return measure(cand)
			},
		)
		if cand2.score > best.score {
			if measure(best.move) != best.score {
				// panic("WTF2")
			}
			for i, j := range translateTable {
				best.move[j] = cand2.move[i]
			}
			if measure(best.move) != cand2.score {
				// panic("WTF")
			}
			best.score = cand2.score

		}

	}

	if preAndPost {
		tmp := LocalSearch(rnd, best.move, measure)
		if tmp.score < best.score {
			// panic("no shit")
		}
		return tmp
	}
	return best
}
