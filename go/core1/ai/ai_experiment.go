package ai

import (
	state "gra/core1/state"
	"gra/coreutils"
	"gra/graphgen"
)

// Returns score, moves in string (for saving the game).

func RunAIExperiment(seed int64, nodes int, AIs []*AI, limit int) ([]coreutils.Result, []map[int]string) {
	gameMap := graphgen.GenerateGenerate(seed, nodes)

	for pid, ai := range AIs {
		ai.Init(pid, seed+int64(pid))
	}

	s := state.NewState(len(AIs), gameMap)

	history := []map[int]string{}
	for s.MoveType != state.End {
		his := map[int]string{}
		switch s.MoveType {
		case state.Negotation:
			moves := map[int][]int{}
			for _, ai := range AIs {
				pid := ai.Pid
				if s.Players[pid].Playing {
					move := ai.Negotation(s)
					moves[pid] = move
					his[pid] = coreutils.NidsToStr(move)
				}
			}
			s = s.Negotation(moves)
		case state.Moves:
			moves := map[int][][]int{}
			for _, ai := range AIs {
				pid := ai.Pid
				if s.Players[pid].Playing {
					move := ai.Moves(s)

					// delete this!!!!!!!!!!!!!!
					// this code was for testing performance of DecodeMovesArgs
					// TestDecoder(s, pid, move)
					/*
						if pid == 0 {
							desired := GetDesiredFromMoveMatrix(move)
							move, _, _ = DecodeMove(s, pid, desired, DecodeMovesArgs{Iters: 50, DestroyRate: 0.5, RecordToRecord: 1.00}, 0)
						}
					*/
					// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

					his[pid] = coreutils.MovesToStr(move)
					moves[pid] = move
				}
			}
			s = s.Move(moves)
		case state.Territory:
			moves := map[int]int{}
			for _, ai := range AIs {
				pid := ai.Pid
				if s.Players[pid].Playing {
					move := ai.Territory(s)
					his[pid] = coreutils.NidToStr(move)
					moves[pid] = move
				}
			}
			s = s.Territory(moves)
		case state.Soldiers:
			moves := map[int][]int{}
			for _, ai := range AIs {
				pid := ai.Pid
				if s.Players[pid].Playing {
					move := ai.Soldiers(s)
					his[pid] = coreutils.NidsToStr(move)
					moves[pid] = move
				}
			}
			s = s.Soldiers(moves)
		}
		history = append(history, his)

		if len(history) == limit {
			s = s.Kill(s.AlivePlayers(), "limit was reached")
		}
	}

	results := []coreutils.Result{}
	for _, p := range s.Players {
		results = append(results, p.DeathInfo.Result)
	}
	return results, history
}
