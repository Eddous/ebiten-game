package ai

import (
	state "gra/core1/state"
	"gra/datstruct"
	"gra/graphgen"
)

func EncodeEdges(s *state.State) [2][]int {
	ret := [2][]int{{}, {}}
	for i := range s.Matrix {
		for j := range s.Matrix {
			if s.Matrix[i][j] {
				ret[0] = append(ret[0], i)
				ret[1] = append(ret[1], j)
			}
		}
	}
	return ret
}

func EncodeNodesMoves(s *state.State) [][]float64 {
	if s.MoveType != state.Moves {
		panic("not moves")
	}
	ret := [][]float64{}
	for nid := range s.Nodes {
		ret = append(ret, encodeNodeMoves(s, nid))
	}
	return ret
}

func EncodeNodes24(s *state.State) [][]float64 {
	ret := [][]float64{}
	for nid := range s.Nodes {
		ret = append(ret, encodeNode(s, nid))
	}
	return ret
}

func EncodeNodesMinimal(s *state.State) [][]float64 {
	ret := [][]float64{}
	for nid := range s.Nodes {
		ret = append(ret, encodeNodeMinimal(s, nid))
	}
	return ret
}

func EncodeScores(s *state.State) map[int]string {
	if s.MoveType != state.End {
		panic("premature end is not implemented")
	}
	ret := map[int]string{}
	for pid, p := range s.Players {
		ret[pid] = p.DeathInfo.Result.String()
	}
	return ret
}

func NewState(nodes int, players int, seed int64) *state.State {
	gameMap := graphgen.GenerateGenerate(seed, nodes)
	return state.NewState(players, gameMap)
}

// only for 2 players
// game state is skipped because reinforcement algorithm is envoked only in moves state
func encodeNodeMinimal(s *state.State, nid int) []float64 {
	ret := make([]float64, 5)
	node := s.Nodes[nid]
	ret[node.Owner+1] = 1 // neutral territory has index 0
	ret[3] = float64(node.MakingSoldiers)
	ret[4] = float64(node.Soldiers)
	// ret[5] = float64(nodeDistances(s, node.Owner, nid)[1])
	return ret
}

/*
[0, 4]: gamestate
[5, 11]: ownership
12: making solier
13: num soldiers
14: dist from enemy territory
*/

func encodeNode(s *state.State, nid int) []float64 {
	// basing encoding
	ret := make([]float64, 14)
	switch s.MoveType {
	case state.Negotation:
		ret[0] = 1
	case state.Moves:
		ret[1] = 1
	case state.Territory:
		ret[2] = 1
	case state.Soldiers:
		ret[3] = 1
	case state.End:
		ret[4] = 1
	default:
		panic("something is missing")
	}

	node := s.Nodes[nid]
	ret[node.Owner+6] = 1 // neutral territory has index 5
	ret[12] = float64(node.MakingSoldiers)
	ret[13] = float64(node.Soldiers)

	// advanced stuff
	ret = append(ret, nodeDistances(s, node.Owner, nid)...)
	ret = append(ret, nodeCentralities(s, nid)...)
	ret = append(ret, globalInfo(s, node.Owner)...)

	return ret
}

func encodeNodeMoves(s *state.State, nid int) []float64 {
	// basing encoding
	ret := make([]float64, 9)

	node := s.Nodes[nid]
	ret[node.Owner+6] = 1 // neutral territory has index 0
	ret[7] = float64(node.MakingSoldiers)
	ret[8] = float64(node.Soldiers)

	// advanced stuff
	ret = append(ret, nodeDistances(s, node.Owner, nid)...)
	ret = append(ret, nodeCentralities(s, nid)...)
	ret = append(ret, globalInfo(s, node.Owner)...)

	return ret
}

func nodeDistances(s *state.State, pid, nid int) []float64 {
	ret := make([]float64, 3)
	ret[0], ret[1], ret[2] = distances(s, pid, nid)
	return ret
}

func nodeCentralities(s *state.State, nid int) []float64 {
	ret := make([]float64, 3)
	ret[0] = float64(s.GraphGenData.DegreeCentrality[nid])
	ret[1] = s.GraphGenData.ClosenessCentrality[nid]
	ret[2] = s.GraphGenData.EigenVectorCentrality[nid]
	return ret
}

// nid is only for knowing who owns the stuff
func globalInfo(s *state.State, pid int) []float64 {
	ret := make([]float64, 4)
	if pid == -1 {
		return ret
	}
	p := s.Players[pid]
	ret[0] = float64(p.Territory)
	ret[1] = float64(p.CurrSoldiers)
	ret[2] = float64(p.CreatingSoldiers)
	ret[3] = float64(p.TerritoryUntilNextSoldier)
	return ret
}

// little bit different implementation
// return -1 if such thing is not found
func distances(s *state.State, pid, nid int) (float64, float64, float64) {
	toNeutral := -1
	toEnemy := -1
	toEnemySoldier := -1

	type elem struct {
		nid  int
		dist int
	}
	seen := datstruct.NewSet[int]()
	seen.Add(nid)
	fifo := []elem{{nid: nid, dist: 0}}

	// edge case
	if s.Nodes[nid].Owner == -1 {
		toNeutral = 0
	}

	for i := 0; i < len(fifo); i++ {
		curr := fifo[i]
		for neighbour := 0; neighbour < len(s.Matrix); neighbour++ {
			if s.Matrix[curr.nid][neighbour] && !seen.Has(neighbour) {
				owner := s.Nodes[neighbour].Owner

				if owner == -1 && toNeutral == -1 {
					toNeutral = curr.dist + 1
				}
				if owner != pid && owner != -1 && toEnemy == -1 {
					toEnemy = curr.dist + 1
				}
				if owner != pid && s.Nodes[neighbour].Soldiers != 0 && toEnemySoldier == -1 {
					toEnemySoldier = curr.dist + 1
				}

				// early stopping
				if toNeutral != -1 && toEnemy != -1 && toEnemySoldier != -1 {
					return float64(toNeutral), float64(toEnemy), float64(toEnemySoldier)
				}

				fifo = append(fifo, elem{nid: neighbour, dist: curr.dist + 1})
				seen.Add(neighbour)
			}
		}
	}
	return float64(toNeutral), float64(toEnemy), float64(toEnemySoldier)
}

// [0, 6]: enemy territory, [7, 12] enemy soldier
// did not work
func distanceOfNodeToEverything(s *state.State, nid int) [13]int {
	result := [13]int{}
	for i := range result {
		result[i] = -1
	}

	type elem struct {
		nid  int
		dist int
	}
	seen := datstruct.NewSet[int]()
	seen.Add(nid)
	fifo := []elem{{nid: nid, dist: 0}}
	for i := 0; i < len(fifo); i++ {
		curr := fifo[i]

		n := s.Nodes[curr.nid]
		i1 := n.Owner + 1
		if result[i1] == -1 {
			result[i1] = curr.dist
		}
		i2 := n.Owner + 7
		if n.Soldiers != 0 && result[i2] == -1 {
			result[i2] = curr.dist
		}

		for neighbour := 0; neighbour < len(s.Matrix); neighbour++ {
			if s.Matrix[curr.nid][neighbour] && !seen.Has(neighbour) {
				fifo = append(fifo, elem{nid: neighbour, dist: curr.dist + 1})
				seen.Add(neighbour)
			}
		}
	}

	return result
}
