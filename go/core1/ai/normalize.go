package ai

import (
	"gra/utils"
	"math"
)

// the sum of the slice will be 1, if there are negative values it will shift them
func LinearNormalization(rawScore []float64) []float64 {
	ret := append([]float64{}, rawScore...)
	removeNegativeValues(ret)
	normalizeFloats(ret)
	return ret
}

func QuadraticNormalization(rawScore []float64) []float64 {
	ret := make([]float64, len(rawScore))
	removeNegativeValues(ret)
	for i, val := range rawScore {
		ret[i] = val * val
	}
	normalizeFloats(ret)
	return ret
}

func ExponentialNormalization(rawScore []float64) []float64 {
	ret := make([]float64, len(rawScore))
	for i, val := range rawScore {
		ret[i] = math.Exp(val)
	}
	normalizeFloats(ret)
	return ret
}

func removeNegativeValues(slice []float64) {
	_, minVal := utils.SliceMin(slice)

	// this is crucial! e.g. [4,5] => [0,1]
	if minVal >= 0 {
		return
	}
	offset := -minVal
	for i := range slice {
		slice[i] += offset
	}
}

// normalize floats in place
func normalizeFloats(slice []float64) {
	sum := 0.0
	for i := range slice {
		sum += slice[i]
	}
	for i := range slice {
		slice[i] = slice[i] / sum
	}
}
