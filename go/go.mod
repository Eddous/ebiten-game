module gra

go 1.23.1

require (
	github.com/PerformLine/go-stockutil v1.9.5
	github.com/coder/websocket v1.8.12
	github.com/davecgh/go-spew v1.1.2-0.20180830191138-d8f796af33cc
	github.com/hajimehoshi/ebiten/v2 v2.8.0
	github.com/redis/go-redis/v9 v9.6.1
	github.com/tonestuff/quadratic v0.0.0-20141117024252-b79de8af2377
	golang.org/x/crypto v0.27.0
	golang.org/x/image v0.20.0
	gonum.org/v1/gonum v0.15.1
)

require (
	github.com/ebitengine/gomobile v0.0.0-20240911145611-4856209ac325 // indirect
	github.com/ebitengine/hideconsole v1.0.0 // indirect
	github.com/go-text/typesetting v0.2.0 // indirect
	golang.org/x/exp v0.0.0-20240904232852-e7e105dedf7e // indirect
)

require (
	github.com/barweiss/go-tuple v1.1.2
	github.com/cespare/xxhash/v2 v2.3.0 // indirect
	github.com/dgryski/go-rendezvous v0.0.0-20200823014737-9f7001d12a5f // indirect
	github.com/ebitengine/purego v0.8.0 // indirect
	github.com/fatih/structs v1.1.0 // indirect
	github.com/google/uuid v1.6.0 // indirect
	github.com/hashicorp/errwrap v1.1.0 // indirect
	github.com/hashicorp/go-multierror v1.1.1 // indirect
	github.com/jbenet/go-base58 v0.0.0-20150317085156-6237cf65f3a6 // indirect
	github.com/jdkato/prose v1.2.1 // indirect
	github.com/jezek/xgb v1.1.1 // indirect
	golang.org/x/sync v0.8.0 // indirect
	golang.org/x/sys v0.25.0 // indirect
	golang.org/x/text v0.18.0 // indirect
	gopkg.in/neurosnap/sentences.v1 v1.0.7 // indirect
	k8s.io/apimachinery v0.31.0 // indirect
	sigs.k8s.io/json v0.0.0-20221116044647-bc3834ca7abd // indirect
)
