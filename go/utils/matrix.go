package utils

import (
	"math"
)

func MatrixMultiply(A [][]float64, vec []float64) []float64 {
	rowsA := len(A)
	colsA := len(A[0])
	if colsA != len(vec) {
		panic("Matrix and vector dimensions do not match")
	}
	result := make([]float64, rowsA)
	for i := 0; i < rowsA; i++ {
		sum := 0.0
		for j := 0; j < colsA; j++ {
			sum += A[i][j] * vec[j]
		}
		result[i] = sum
	}
	return result
}

func NewSquereMatrixF(size int) [][]float64 {
	ret := make([][]float64, size)
	for i := 0; i < size; i++ {
		ret[i] = make([]float64, size)
	}
	return ret
}

// inplace
func L2Norm(in []float64) {
	sum := 0.
	for _, val := range in {
		sum += val * val
	}
	sum = math.Sqrt(sum)
	for i := range in {
		in[i] = in[i] / sum
	}
}

// for calculation of the eigenvector coresponding to the largest eigenvalue
func PowerIteration(in [][]float64) []float64 {
	powerIterationTest(in)

	A := Copy2DSlice(in)
	vec := make([]float64, len(in))
	for i := range vec {
		vec[i] = 1
	}
	L2Norm(vec)

	const maxIterations = 1000
	const epsilon = 1e-6

	for range maxIterations {
		new := MatrixMultiply(A, vec)
		L2Norm(new)
		if converged(vec, new, epsilon) {
			return new
		}
		vec = new
	}
	return vec
}

func powerIterationTest(in [][]float64) {
	if in == nil {
		panic("in is nil")
	}
	dim := len(in)
	for i := range in {
		if len(in[i]) != dim {
			panic("in matrix is not squere")
		}
	}
}

func converged(old, new []float64, eps float64) bool {
	sum := 0.
	for i := range old {
		dif := new[i] - old[i]
		sum += dif * dif
	}
	return math.Sqrt(sum) < eps
}
