package utils

func SliceMin(slice []float64) (int, float64) {
	if len(slice) == 0 {
		panic("wrong input")
	}
	var index = 0
	var value = slice[0]
	for i, v := range slice {
		if v < value {
			index = i
			value = v
		}
	}
	return index, value
}

func SliceMax(slice []float64) (int, float64) {
	if len(slice) == 0 {
		panic("wrong input")
	}
	var index = 0
	var value = slice[0]
	for i, v := range slice {
		if v > value {
			index = i
			value = v
		}
	}
	return index, value
}

func SliceMean(slice []float64) float64 {
	sum := 0.0
	for _, val := range slice {
		sum += val
	}
	return sum / float64(len(slice))
}

func SliceAllMaxes(slice []float64) ([]int, float64) {
	if len(slice) == 0 {
		panic("wrong input")
	}
	indexes := []int{}
	var value = slice[0]
	for i, v := range slice {
		if v > value {
			indexes = []int{i}
			value = v
		} else if v == value {
			indexes = append(indexes, i)
		}
	}
	return indexes, value
}

func Copy2DSlice[K any](slice [][]K) [][]K {
	ret := make([][]K, len(slice))
	for i := range slice {
		ret[i] = append(ret[i], slice[i]...)
	}
	return ret
}

func MapToSlice[K comparable, T any](in map[K]T) []T {
	out := make([]T, 0)
	for _, t := range in {
		out = append(out, t)
	}
	return out
}
