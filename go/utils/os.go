package utils

import (
	"fmt"
	"log"
	"os"
	"os/exec"
	"runtime"
	"strconv"
)

// This will be deleted in a while
func OpenBrowser(url string) {
	var err error
	switch runtime.GOOS {
	case "linux":
		err = exec.Command("xdg-open", url).Start()
	case "windows":
		err = exec.Command("rundll32", "url.dll,FileProtocolHandler", url).Start()
	case "darwin":
		err = exec.Command("open", url).Start()
	case "js":
		/*TODO somehow redirect user on a webpage*/
	default:
		err = fmt.Errorf("unsupported platform: " + runtime.GOOS)
	}
	if err != nil {
		fmt.Println(err)
	}
}

// if there is no such directory it attemps to create it
// TODO it does not check whether the directory is writable
func CheckDirectory(path string) {
	_, err := os.Stat(path)
	if os.IsNotExist(err) {
		err = os.Mkdir(path, os.ModePerm)
	}
	if err != nil {
		log.Fatalln(err)
	}
}

// to find next number to name the file (counting from 1).
// Does not take into account the extensions (they will broke it).
// the function is no good
func NextFileNumber(dirPath string) int {
	files, err := os.ReadDir(dirPath)
	if err != nil {
		panic(err)
	}
	max := 0
	for _, f := range files {
		val, err := strconv.Atoi(f.Name())
		if err == nil && val > max {
			max = val
		}
	}
	return max + 1
}

func WriteBytesToFile(path string, bytes []byte) {
	CheckErr(os.WriteFile(path, bytes, 0644))
}

func ReadBytesFromFile(path string) []byte {
	data, err := os.ReadFile(path)
	CheckErr(err)
	return data
}
