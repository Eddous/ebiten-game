package utils

import "math"

type VecF struct {
	X, Y float64
}

func NewVecF(x, y float64) VecF { return VecF{x, y} }

func (v VecF) XY() (float64, float64) { return v.X, v.Y }

func (v1 VecF) Plus(v2 VecF) VecF {
	return NewVecF(v1.X+v2.X, v1.Y+v2.Y)
}

func (v1 VecF) Minus(v2 VecF) VecF {
	return NewVecF(v1.X-v2.X, v1.Y-v2.Y)
}

func (v VecF) Mult(num float64) VecF {
	return NewVecF(v.X*num, v.Y*num)
}

func (v VecF) Div(num float64) VecF {
	return NewVecF(v.X/num, v.Y/num)
}

func (v VecF) Floor() VecF {
	return VecF{math.Floor(v.X), math.Floor(v.Y)}
}

func (v1 VecF) Equals(v2 VecF, err float64) bool {
	return FloatEquals(v1.X, v2.X, err) && FloatEquals(v1.Y, v2.Y, err)
}

func (v1 VecF) Compare(v2 VecF, err float64) int {
	if v1.Equals(v2, err) {
		return 0
	}
	if v1.X < v2.X {
		return -1
	}
	if v1.X > v2.X {
		return 1
	}
	if v1.Y < v2.Y {
		return -1
	}
	if v1.Y > v2.Y {
		return 1
	}
	panic("never happen")
}

func (v VecF) Normalise() VecF {
	max := math.Max(math.Abs(v.X), math.Abs(v.Y))
	return NewVecF(v.X/max, v.Y/max)
}

func (v VecF) Round() VecF {
	return NewVecF(math.Round(v.X), math.Round(v.Y))
}

func (v VecF) IsIn(b1, b2 VecF, err float64) bool {
	return IsIn(v.X, b1.X, b2.X, err) && IsIn(v.Y, b1.Y, b2.Y, err)
}

func (v VecF) IsIn2(x1, y1, x2, y2, err float64) bool {
	return IsIn(v.X, x1, x2, err) && IsIn(v.Y, y1, y2, err)
}

func (v VecF) PerpendicularVec() VecF {
	return NewVecF(-v.Y, v.X)
}

func (v1 VecF) GetDistanceNS(v2 VecF) float64 {
	difX := v1.X - v2.X
	difY := v1.Y - v2.Y
	return difX*difX + difY*difY
}

func (v1 VecF) GetDistance(v2 VecF) float64 {
	return math.Pow(v1.GetDistanceNS(v2), 0.5)
}

func (v VecF) Clamp(lt, rb VecF) VecF {
	return NewVecF(ClampF(v.X, lt.X, rb.X), ClampF(v.Y, lt.Y, rb.Y))
}

// BETWEEN TWO POINTS!! (up to 2*pi)
func GetAngle(a, b VecF) float64 {
	theta := math.Acos((b.X - a.X) / a.GetDistance(b))
	if a.Y <= b.Y {
		return theta
	}
	return 2*math.Pi - theta
}

func PolarToCartesian(origin VecF, r, angle float64) VecF {
	return NewVecF(r*math.Cos(angle)+origin.X, r*math.Sin(angle)+origin.Y)
}
