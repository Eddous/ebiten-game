package utils

import (
	"encoding/json"
)

func CheckErr(err error) {
	if err != nil {
		panic(err)
	}
}

func ToBytes(a any) []byte {
	bytes, err := json.Marshal(a)
	if err != nil {
		panic(err)
	}
	return bytes
}

func PowInts(x, n int) int {
	if n < 0 {
		panic("cannot do negative powers")
	}
	return powInts(x, n)
}

func powInts(x, n int) int {
	if n == 0 {
		return 1
	}
	if n == 1 {
		return x
	}
	y := powInts(x, n/2)
	if n%2 == 0 {
		return y * y
	}
	return x * y * y
}

// little endian (cant it be replaced with IterateOverCoordinates)?
func IndexToTuple(counter, places, base int) []int {
	if counter < 0 {
		panic("counter cannot be less than 0")
	}
	ret := make([]int, places)
	for i := 0; i < places; i++ {
		ret[i] = counter % base
		counter = counter / base
	}
	return ret
}
