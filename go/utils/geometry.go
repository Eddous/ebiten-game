package utils

import (
	"math"
	"sort"
)

type Line struct {
	A, B VecF
}

func (l Line) GetVec() VecF {
	return l.B.Minus(l.A)
}

func NewLine(a, b VecF) Line {
	return Line{a, b}
}

func (l Line) PointDist(p VecF) float64 {
	a, b, c := p.GetDistance(l.A), p.GetDistance(l.B), l.Len()

	// the point lies between points => angleA = 0 && angleB = 0
	// the point lies in left on the line => angleA = pi && angleB = 0
	// the point lies in right on the line => angleA = 0 && angleB = 360
	angleA := math.Acos((a*a + c*c - b*b) / (2 * a * c))
	angleB := math.Acos((b*b + c*c - a*a) / (2 * b * c))
	if angleA > math.Pi/2 {
		return a
	}
	if angleB > math.Pi/2 {
		return b
	}

	// height of the triangle
	s := (a + b + c) / 2
	A := math.Sqrt(s * (s - a) * (s - b) * (s - c))
	return A / (c / 2)
}

func acuteTriangle(a, b, c float64) bool {
	sides := []float64{a, b, c}
	sort.Float64s(sides)
	return sides[2]*sides[2] < sides[0]*sides[0]+sides[1]*sides[1]
}

func (l Line) Len() float64 {
	return l.A.GetDistance(l.B)
}

func (l1 Line) CommonPoint(l2 Line, err float64) bool {
	return l1.A.Equals(l2.A, err) || l1.A.Equals(l2.B, err) || l1.B.Equals(l2.A, err) || l1.B.Equals(l2.B, err)
}

func PerpendicularBisector(a, b VecF) Line {
	vec := b.Minus(a).PerpendicularVec()
	mid := a.Plus(b).Div(2)

	return Line{mid, mid.Plus(vec)}
}

// for the parralel lines it returns nil
func (l1 Line) IntersectLine(l2 Line) *VecF {
	vec1 := l1.B.Minus(l1.A)
	vec2 := l2.B.Minus(l2.A)
	if vec1.Normalise().Equals(vec2.Normalise(), 0.00001) {
		return nil
	}
	// l1.X + k * vec1.X = l2.X + t * vec2.X
	// l1.Y + k * vec1.Y = l2.Y + t * vec2.Y
	// .
	// .
	k := (((l1.A.X - l2.A.X) * vec2.Y) - ((l1.A.Y - l2.A.Y) * vec2.X)) / (vec2.X*vec1.Y - vec2.Y*vec1.X)
	intersect := l1.A.Plus(vec1.Mult(k))

	if !intersect.IsIn(l1.A, l1.B, 0.00001) || !intersect.IsIn(l2.A, l2.B, 0.00001) {
		return nil
	}
	return &intersect
}

func (l Line) Vectorize(set func(x, y int, opacity float64)) {
	start := l.A.Round()
	stop := l.B.Round()
	if start.Compare(stop, 0) >= 0 {
		start, stop = stop, start
	}
	curr := start
	d := stop.Minus(start).Normalise()
	for curr.IsIn(start, stop, 0) {
		drawPixel(curr, set)
		curr = curr.Plus(d)
	}
	drawPixel(stop, set)
}

func (l Line) OnLeft(p VecF) bool {
	return ((l.B.X-l.A.X)*(p.Y-l.A.Y) - (l.B.Y-l.A.Y)*(p.X-l.A.X)) > 0
}

func (l Line) OnRight(p VecF) bool {
	return ((l.B.X-l.A.X)*(p.Y-l.A.Y) - (l.B.Y-l.A.Y)*(p.X-l.A.X)) < 0
}

// y = ax + b
func (l Line) getExplicitLineParams() (float64, float64) {
	p1, p2 := l.A, l.B
	a := (p1.Y + p2.Y) / (p1.X + p2.X)
	b := a*p1.X - p1.Y
	return a, b
}

// --------------
// Convex Polygon

type CPolygon struct {
	vertices []VecF
}

func (cp *CPolygon) GetLines() []Line {
	lines := []Line{}
	for i := range cp.vertices {
		lines = append(lines, NewLine(cp.vertices[i], cp.vertices[(i+1)%len(cp.vertices)]))
	}
	return lines
}

// -------
// Circle

type Circle struct {
	center VecF
	radius float64
}

func NewCircle(center VecF, radius float64) Circle {
	return Circle{center, radius}
}

func (c Circle) IsIn(point VecF) bool {
	return c.center.GetDistance(point) <= c.radius
}

func (circle Circle) ClipLine(l Line) *Line {
	A, vec := l.A, l.B.Minus(l.A)
	center := circle.center
	r := circle.radius

	a := vec.X*vec.X + vec.Y*vec.Y
	b := 2*A.X*vec.X - 2*vec.X*center.X + 2*A.Y*vec.Y - 2*vec.Y*center.Y
	c := -2*A.X*center.X + center.X*center.X + A.X*A.X - 2*A.Y*center.Y + center.Y*center.Y + A.Y*A.Y - r*r
	t1, t2, ok := SolveQuadratic(a, b, c)
	if !ok || t1 == t2 {
		return nil
	}

	var p1, p2 VecF
	var ok1, ok2 bool
	if circle.IsIn(l.A) {
		p1 = l.A
		ok1 = true
	}
	if circle.IsIn(l.B) {
		p2 = l.B
		ok2 = true
	}
	if IsIn(t1, 0, 1, 0) {
		p1 = A.Plus(vec.Mult(t1))
		ok1 = true
	}
	if IsIn(t2, 0, 1, 0) {
		p2 = A.Plus(vec.Mult(t2))
		ok2 = true
	}
	if !ok1 || !ok2 {
		return nil
	}
	ret := NewLine(p1, p2)
	return &ret
}

func (c *Circle) GetPoint(alpha float64) VecF {
	return NewVecF(c.radius*math.Cos(alpha), c.radius*math.Sin(alpha)).Plus(c.center)
}

func (c *Circle) vectorize(alpha, beta float64, set func(x, y int, opacity float64)) {
	if FloatEquals(alpha, beta, 0.00001) {
		return
	}
	if beta < alpha {
		beta += 2 * math.Pi
	}
	curr := alpha
	step := math.Abs(math.Acos((-2*c.radius*c.radius + 1) / (-2 * c.radius * c.radius)))
	for curr < beta {
		drawPixel(c.GetPoint(curr), set)
		curr += step
	}
	drawPixel(c.GetPoint(beta), set)
}

// -------------
// PolygonCircle

// Intersect of a circle and a polygon
// the voronoi cell

/*
type CuttedCircle struct {
	circle *Circle
	lines  []Line
}

func NewCuttedCircle(center VecF, radius float64, close []VecF) *CuttedCircle {
	circle := NewCircle(center, radius)

	wildLines := make([]*Line, 0)
	for _, cp := range close {
		if center.Equals(cp, 0) {
			continue
		}
		bisector := PerpendicularBisector(center, cp)
		line := circle.ClipLine(bisector)
		wildLines = append(wildLines, line) // can be nil!
	}

	for i, l1 := range wildLines {
		if l1 == nil {
			continue
		}
		if l1.OnRight(center) {
			l1.A, l1.B = l1.B, l1.A
		}
		for j, l2 := range wildLines {
			if i == j || l2 == nil {
				continue
			}
			pos1, pos2 := l1.OnLeft(l2.A), l1.OnLeft(l2.B) // OnLeft = ok
			if pos1 && pos2 {
				continue
			}
			if !pos1 && !pos2 {
				wildLines[j] = nil
				continue
			}
			intersect := l1.IntersectLine(l2)
			if pos1 && !pos2 {
				wildLines[j] = NewLine(l2.A, intersect, false)
			}
			if !pos1 && pos2 { // if is for readability
				wildLines[j] = NewLine(intersect, l2.B, false)
			}
		}
	}
	tamedLines := make([]Line, 0, len(wildLines))
	for _, line := range wildLines {
		if line != nil {
			tamedLines = append(tamedLines, line)
		}
	}
	sort.Slice(tamedLines, func(i, j int) bool {
		return GetAngle(*center, *tamedLines[i].A) < GetAngle(*center, *tamedLines[j].A)
	})
	return &CuttedCircle{circle, tamedLines}
}

func (pc *CuttedCircle) Vectorize(set func(x, y int, opacity float64)) {
	if len(pc.lines) == 0 {
		pc.circle.vectorize(0, 2*math.Pi, set)
		return
	}
	for i := 0; i < len(pc.lines); i++ {
		l1, l2 := pc.lines[i], pc.lines[(i+1)%len(pc.lines)]
		l1.Vectorize(set)
		alpha := GetAngle(*pc.circle.center, *l1.B)
		beta := GetAngle(*pc.circle.center, *l2.A)
		pc.circle.vectorize(alpha, beta, set)
	}
}
*/
// -------
// helpers

func drawPixel(curr VecF, set func(x, y int, opacity float64)) {
	p := curr.Round()
	set(int(p.X), int(p.Y), 1)
}
