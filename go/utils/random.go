package utils

import "math/rand"

func Shuffle[T any](rnd *rand.Rand, slice []T) {
	for i := len(slice) - 1; i > 0; i-- { // Fisher–Yates shuffle
		j := rnd.Intn(i + 1)
		slice[i], slice[j] = slice[j], slice[i]
	}
}

func Sample[T any](rnd *rand.Rand, slice []T) T {
	return slice[rnd.Intn(len(slice))]
}

func NextSeed(seed int64) int64 {
	return rand.NewSource(seed).Int63()
}

// returns an index, assumes that sum(probs) = 1
func SampleFromProbs(rnd *rand.Rand, probs []float64) int {
	val := rnd.Float64()
	for i, p := range probs {
		val -= p
		if val <= 0 {
			return i
		}
	}
	panic("the sum of the probs is >1")
}

func RandomInt(r *rand.Rand, min, max int) int {
	if min >= max {
		panic("wrong args")
	}
	return r.Intn(max) + min
}

func RandomFloat(r *rand.Rand, min, max float64) float64 {
	if min >= max {
		panic("wrong args")
	}
	return min + r.Float64()*(max-min)
}

func RandomFloat2(min, max float64) float64 {
	if min >= max {
		panic("wrong args")
	}
	return min + rand.Float64()*(max-min)
}
