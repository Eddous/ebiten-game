package utils

import (
	"math"
	"sort"

	"github.com/tonestuff/quadratic"
)

func Get1Transformator(fromA, toA, fromB, toB float64) func(float64) float64 {
	a := (toA - toB) / (fromA - fromB)
	b := (toA) - (fromA)*a
	return func(in float64) float64 {
		return in*a + b
	}
}

func Get2Transformator(fromA, toA, fromB, toB VecF) func(VecF) VecF {
	t1 := Get1Transformator(fromA.X, toA.X, fromB.X, toB.X)
	t2 := Get1Transformator(fromA.Y, toA.Y, fromB.Y, toB.Y)
	return func(in VecF) VecF {
		return NewVecF(
			t1(in.X),
			t2(in.Y),
		)
	}
}

func FloatEquals(f1, f2, err float64) bool {
	return math.Abs(f1-f2) <= err
}

func Abs(val int) int {
	if val >= 0 {
		return val
	}
	return -val
}

func Min(a, b int) int {
	if a < b {
		return a
	}
	return b
}

func Max(a, b int) int {
	if a < b {
		return b
	}
	return a
}

func Mod(num, mod float64) float64 {
	ret := math.Mod(num, mod)
	if ret < 0 {
		ret += mod
	}
	return ret
}

func ClampI(num, left, right int) int {
	if num < left {
		return left
	}
	if right < num {
		return right
	}
	return num
}

func ClampF(val, min, max float64) float64 {
	if val < min {
		return min
	}
	if max < val {
		return max
	}
	return val
}

// both borders are included
func IsInInt(num, b1, b2 int) bool {
	return b1 <= num && num <= b2
}

func IsIn(num, bound1, bound2, err float64) bool {
	return isIn(num, bound1, bound2, err) || isIn(num, bound2, bound1, err)
}

func isIn(num, bound1, bound2, err float64) bool {
	return bound1-err <= num && num <= bound2+err
}

func SolveQuadratic(a, b, c float64) (float64, float64, bool) {
	x1c, x2c := quadratic.Solve(complex(a, 0), complex(b, 0), complex(c, 0))
	if imag(x1c) != 0 || imag(x2c) != 0 {
		return math.NaN(), math.NaN(), false
	}
	return real(x1c), real(x2c), true
}

// angles in radians, the input will be edited, can return number > 2*pi
func MaxEmptyAngleInCircle(angles []float64) float64 {
	sort.Float64s(angles)
	angles = append(angles, angles[0]+2*math.Pi)

	anglesBetween := make([]float64, 0)
	actualAngle := make([]float64, 0)
	for i := 0; i < len(angles)-1; i++ {
		anglesBetween = append(anglesBetween, angles[i+1]-angles[i])
		actualAngle = append(actualAngle, (angles[i+1]+angles[i])/2)
	}

	max := math.Inf(-1)
	index := -1
	for i, a := range anglesBetween {
		if max < a {
			max = a
			index = i
		}
	}
	return actualAngle[index]
}

func L1Error(s1 []float64, s2 []float64) float64 {
	if s1 == nil || s2 == nil || len(s1) != len(s2) {
		panic("wrong input")
	}

	sum := 0.0
	for i := range s1 {
		sum += math.Abs(s1[i] - s2[i])
	}
	return sum
}
