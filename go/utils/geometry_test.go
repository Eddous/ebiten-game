package utils

import "testing"

// Testing lines left to line, right to line, in between points, on points
func TestPointLineDistance(t *testing.T) {
	line := NewLine(NewVecF(1, 0), NewVecF(3, 0))
	if line.PointDist(NewVecF(0, 0)) != 1 {
		t.FailNow()
	}

	if line.PointDist(NewVecF(4, 0)) != 1 {
		t.FailNow()
	}

	if line.PointDist(NewVecF(2, 0)) != 0 {
		t.FailNow()
	}

	if line.PointDist(NewVecF(1, 0)) != 0 {
		t.FailNow()
	}
}
