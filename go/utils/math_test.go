package utils

import (
	"testing"
)

func TestAngle(t *testing.T) {
	center := NewVecF(0, 0)
	points := []VecF{
		NewVecF(1, 0),
		NewVecF(1, 1),
		NewVecF(0, 1),
		NewVecF(-1, 1),
		NewVecF(-1, 0),
		NewVecF(-1, -1),
		NewVecF(0, -1),
		NewVecF(1, -1),
	}
	for i := 0; i < len(points)-1; i++ {
		a1 := GetAngle(center, points[i])
		a2 := GetAngle(center, points[i+1])
		if a1 >= a2 {
			t.Fatal("p1, a1: ", points[i], ", ", a1, "   p2, a2: ", points[i+1], ", ", a2)
		}
	}
}

func TestIndexesMax(t *testing.T) {
	input := []float64{1, 7, 1, 2, 5, 5, 7}
	indexes, value := SliceAllMaxes(input)
	if indexes[0] != 1 || indexes[1] != 6 || value != 7 {
		t.Fatal("find me")
	}
}
