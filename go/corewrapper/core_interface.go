package corewrapper

import (
	"gra/coreutils"

	"github.com/barweiss/go-tuple"
)

type CoreInterface interface {
	KillPlayers(ids []int, deathText string) (playing []int) // ids are guaranteed to be correct
	InputCheck(id int, data string) bool
	Move(map[int]string) (playing []int)

	// will return nil if the AI players are not playing
	// it returns pid, move
	// it will do 1 move
	StartAI() <-chan tuple.T2[int, string]

	// points, reason of death
	// 1 win, 0.5 draw, 0 defeat
	Result() map[int]coreutils.DeathInfo
}
