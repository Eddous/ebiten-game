package corewrapper

import (
	"gra/coreutils"
	"gra/utils"
)

type PlayerFeelings byte

const (
	WantBlood PlayerFeelings = iota
	Draw
	Surrender
)

func (f PlayerFeelings) String() string {
	switch f {
	case WantBlood:
		return "WantBlood"
	case Draw:
		return "Draw"
	case Surrender:
		return "Surrender"
	}
	panic("no such feelings")
}

type PlayerState byte

const (
	Playing PlayerState = iota
	PlayingReady
	NotPlaying
	Dead
)

type Player struct {
	game *Wrapper

	Id   int
	Name string
	Elo  float64

	// Setted by player (DON'T SET IT DIRECTLY)
	Feelings PlayerFeelings
	Buffer   string

	// Setted by wrapper
	State PlayerState
	Time  int

	// only valid when PlayerState == Dead
	DeathInfo  coreutils.DeathInfo
	UpdatedElo float64
}

func newPlayer(game *Wrapper, id int, name string, elo float64, time int) *Player {
	return &Player{
		game: game,
		Id:   id,
		Time: time,
		Name: name,
		Elo:  elo,
	}
}

// =========
// # Setters

func (p *Player) SetFeelings(state PlayerFeelings) (playerMsg []byte) {
	if !p.Alive() {
		return nil
	}
	p.Feelings = state

	return utils.ToBytes(
		PlayerMsg{
			T:       FeelingT,
			Turn:    p.game.turnCounter,
			Feeling: state,
		},
	)
}

func (p *Player) SetBuffer(buffer string) (playerMsg []byte) {
	if p.Ready() || !p.game.TGame.InputCheck(p.Id, buffer) {
		return nil
	}
	p.Buffer = buffer
	p.State = PlayingReady
	return utils.ToBytes(
		PlayerMsg{
			T:      BufferT,
			Turn:   p.game.turnCounter,
			Buffer: buffer,
		},
	)
}

// # Setters
// =========

func (p *Player) Ready() bool {
	return p.State != Playing
}

func (p *Player) Alive() bool {
	return p.State != Dead
}
