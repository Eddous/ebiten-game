package corewrapper

import (
	"encoding/json"
	"gra/utils"
	"strconv"
)

// =======
// # types

type PlayerMsgType byte

const (
	FeelingT PlayerMsgType = iota
	BufferT
)

type PlayerMsg struct {
	Turn int

	T       PlayerMsgType
	Feeling PlayerFeelings
	Buffer  string
}

type WrapperMsgT byte

const (
	InfoT WrapperMsgT = iota
	AsyncUpdateT
	SyncUpdateT
	ServerKilledPlayersT
)

type WrapperMsg struct {
	T WrapperMsgT

	SubmitedMove  []string                  // InfoT
	Time          map[string]int            // InfoT
	Feel          map[string]PlayerFeelings // InfoT, AsyncUpdateT
	Actions       map[string]string         // SyncUpdateT, send only PlayingReady players
	Killed        []string                  // ServerKilledPlayersT
	KillingReason string                    // ServerKilledPlayersT
}

func (w *Wrapper) getTimesAndFeels() (map[string]int, map[string]PlayerFeelings) {
	times := make(map[string]int)
	feels := make(map[string]PlayerFeelings)
	for _, p := range w.alivePlayers {
		times[p.Name] = p.Time
		feels[p.Name] = p.Feelings
	}
	return times, feels
}

func (w *Wrapper) getInfoMsg() WrapperMsg {
	msg := WrapperMsg{
		T:            InfoT,
		SubmitedMove: []string{},
	}
	for _, p := range w.alivePlayers {
		if p.State == PlayingReady {
			msg.SubmitedMove = append(msg.SubmitedMove, p.Name)
		}
	}
	msg.Time, msg.Feel = w.getTimesAndFeels()
	return msg
}

func (w *Wrapper) getAsyncUpdateMsg() WrapperMsg {
	msg := WrapperMsg{T: AsyncUpdateT}
	_, msg.Feel = w.getTimesAndFeels()
	return msg
}

func (w *Wrapper) getSyncUpdateMsg() WrapperMsg {
	msg := WrapperMsg{
		T:       SyncUpdateT,
		Actions: map[string]string{},
	}
	for _, p := range w.alivePlayers {
		if p.State == PlayingReady {
			msg.Actions[p.Name] = p.Buffer
		}
	}
	return msg
}

type serialized struct {
	GameMode  GameMode
	MapMode   int
	Seed      int64
	Names     []string
	Eloes     []float64
	TimeMode  TimeMode
	Time      int
	Increment int
	History   []WrapperMsg
}

func (w *Wrapper) Serialize() []byte {
	players := len(w.AllPlayers)
	serialized := serialized{
		GameMode:  w.gameMode,
		MapMode:   w.mapMode,
		Seed:      w.seed,
		Names:     make([]string, players),
		Eloes:     make([]float64, players),
		TimeMode:  w.timeMode,
		Time:      w.time,
		Increment: w.increment,
		History:   append(w.reconnectMsgBuffer, w.getInfoMsg()),
	}
	for i, p := range w.AllPlayers {
		serialized.Names[i] = p.Name
		serialized.Eloes[i] = p.Elo
	}
	return utils.ToBytes(serialized)
}

func Deserialize(bytes []byte) *Wrapper {
	var serialized serialized
	utils.CheckErr(json.Unmarshal(bytes, &serialized))
	w := newWrapper(
		serialized.GameMode,
		serialized.MapMode,
		serialized.Seed,
		serialized.Names,
		serialized.Eloes,
		make([]int, len(serialized.Names)),
		serialized.TimeMode,
		serialized.Time,
	)
	for _, msg := range serialized.History {
		w.msgUpdate(msg, "")
	}
	return w
}

// safer is to never skip the player
func (w *Wrapper) msgUpdate(msg WrapperMsg, playerToSkip string) bool {
	setFeelings := func(remoteFeelings map[string]PlayerFeelings, skipYou bool) {
		// skipping is to seem less lagy for soft updates: SetFeeling(), SetFeelings(),... update1, update2
		// not skipping is important for hard updates: SetFeelings(), SetFeelings(), update1 (the game already ended on server, but not for client)
		for name, feels := range remoteFeelings {
			if name == playerToSkip && skipYou {
				continue
			}
			w.NameToPlayer[name].Feelings = feels
		}
	}

	switch msg.T {
	case ServerKilledPlayersT:
		w.KillPlayers(msg.Killed, msg.KillingReason)
		return true
	case InfoT:
		// updating time
		for name, time := range msg.Time {
			w.NameToPlayer[name].Time = time
		}
		// updating feelings
		setFeelings(msg.Feel, true)
		for _, name := range msg.SubmitedMove {
			// no need to skip you
			w.NameToPlayer[name].State = PlayingReady
		}
		return false
	case SyncUpdateT:
		for name, buffer := range msg.Actions {
			p := w.NameToPlayer[name]
			p.Buffer = buffer
			p.State = PlayingReady
		}
		w.TryStep()
		return true
	case AsyncUpdateT:
		setFeelings(msg.Feel, false)
		w.TryStep()
		return true
	}
	panic("no such option")
}

// for ai experiments which does not use wrapper but want to use its Serialization/Deserialization capabilities
func SerializeFromMoves(gameMode GameMode, mapMode int, seed int64, players int, history []map[int]string) []byte {
	wrapper := NewOffline(gameMode, seed, mapMode, make([]int, players))
	for i, moves := range history {
		for pid, move := range moves {
			if wrapper.AllPlayers[pid].SetBuffer(move) == nil {
				panic("wrong buffer for player: " + strconv.Itoa(pid) + " at history move: " + strconv.Itoa(i))
			}
		}
		_, ok := wrapper.TryStep()
		if !ok {
			panic("wrapper was not able to perform a step at move: " + strconv.Itoa(i))
		}
	}
	return wrapper.Serialize()
}
