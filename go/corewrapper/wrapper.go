package corewrapper

import (
	core1 "gra/core1/board"
	core2 "gra/core2/board"
	"gra/coreutils"
	"gra/utils"
	"math/rand"
)

type TimeMode byte

const (
	PerRound TimeMode = iota
	Inf
)

type GameMode byte

const (
	Classical GameMode = iota
	Cities
)

type Wrapper struct {
	TGame CoreInterface

	AllPlayers   []*Player
	NameToPlayer map[string]*Player
	alivePlayers []*Player

	timeMode  TimeMode
	time      int
	increment int

	reconnectMsgBuffer []WrapperMsg

	// online game
	turnCounter int

	// arguments
	gameMode GameMode
	seed     int64
	mapMode  int
}

func NewOffline(gameMode GameMode, seed int64, nodes int, playerTypes []int) *Wrapper {
	names, eloes := getNotInitedNames(len(playerTypes))

	// non-inf time can hit a bug in scn_offline_game (it is labeled POTENTIAL BUG)
	return newWrapper(gameMode, nodes, seed, names, eloes, playerTypes, Inf, 0)
}

// not deterministic!
func getNotInitedNames(players int) ([]string, []float64) {
	names := []string{"Turing", "Neumann", "Dijkstra", "GOTO", "BigONotation", "Algorithms", "Ritchie", "C", "Capablanca", "MagnusCarlsen", "AdamOndra", "Chess", "Dumbledore", "IfStatement", "FordPrefect", "Gandalf", "Smaug"}
	rand.Shuffle(len(names), func(i, j int) { names[i], names[j] = names[j], names[i] })
	eloes := make([]float64, players)
	for i := range eloes {
		eloes[i] = utils.RandomFloat2(-100, 100)
	}
	return names[:players], eloes[:players]
}

func newWrapper(gameMode GameMode, nodes int, seed int64, names []string, eloes []float64, playerTypes []int, timeMode TimeMode, time int) *Wrapper {
	if len(names) != len(eloes) || len(names) != len(playerTypes) {
		panic("wrong len of the names, eloes, or aiMask")
	}
	var board CoreInterface
	switch gameMode {
	case Classical:
		board = core1.NewBoard(seed, playerTypes, nodes)
	case Cities:
		board = core2.NewBoard(seed, playerTypes, nodes)
	}

	if board == nil {
		return nil
	}
	t := time
	if timeMode == Inf {
		t = -1
	}
	w := &Wrapper{
		TGame: board,

		AllPlayers:   []*Player{},
		alivePlayers: []*Player{},
		NameToPlayer: map[string]*Player{},

		timeMode: timeMode,
		time:     t,

		turnCounter:        1,
		reconnectMsgBuffer: make([]WrapperMsg, 0),

		seed:     seed,
		mapMode:  nodes,
		gameMode: gameMode,
	}
	for i := 0; i < len(names); i++ {
		p := newPlayer(w, i, names[i], eloes[i], w.time)
		w.AllPlayers = append(w.AllPlayers, p)
		w.alivePlayers = append(w.alivePlayers, p)
		w.NameToPlayer[p.Name] = p
	}
	return w
}

func (w *Wrapper) End() bool {
	return len(w.alivePlayers) == 0
}

// this lowers time and when time == 0, then it will generate default move
func (w *Wrapper) TimeStep() {
	if w.End() {
		panic("End of game")
	}

	if w.timeMode != Inf {
		for _, p := range w.alivePlayers {
			if p.Ready() {
				continue
			}
			p.Time--
			if p.Time == 0 {
				p.SetBuffer(w.TGame.DefaultMove(p.Id))
			}
		}
	}
}

// returns the whether there was a "hard update"
func (w *Wrapper) TryStep() (wrapperMsg []byte, nextState bool) {
	if w.End() {
		panic("End of game")
	}

	info, async, sync := w.getInfoMsg(), w.getAsyncUpdateMsg(), w.getSyncUpdateMsg()
	if w.surrender() || w.draw() {
		w.reconnectMsgBuffer = append(w.reconnectMsgBuffer, async)
		return utils.ToBytes(async), true
	} else if w.ready() {
		w.reconnectMsgBuffer = append(w.reconnectMsgBuffer, sync)
		return utils.ToBytes(sync), true
	} else {
		return utils.ToBytes(info), false
	}
}

// I keep it here if I would like to reimplement other time mechanisms. HINT: it is same as w.draw() or w.surrender()
func (w *Wrapper) timeOut() bool {
	buffer := []*Player{}
	for _, p := range w.alivePlayers {
		if p.Time == 0 {
			buffer = append(buffer, p)
		}
	}
	if len(buffer) == 0 {
		return false
	}

	// step
	w.killPlayersFromTop(buffer, "TimedOut")
	return true
}

func (w *Wrapper) surrender() bool {
	var tmp *Player
	for _, p := range w.alivePlayers {
		if p.Feelings == Surrender {
			tmp = p
		}
	}
	if tmp == nil {
		return false
	}

	// step
	w.killPlayersFromTop([]*Player{tmp}, "Surrendered")
	return true
}

func (w *Wrapper) draw() bool {
	for _, p := range w.alivePlayers {
		if p.Feelings != Draw {
			return false
		}
	}

	// step
	rest := make([]*Player, 0)
	rest = append(rest, w.alivePlayers...)
	w.killPlayersFromTop(rest, "Drew")
	return true
}

func (w *Wrapper) ready() bool {
	for _, p := range w.alivePlayers {
		if !p.Ready() {
			return false
		}
	}

	// step
	m := map[int]string{}
	for _, p := range w.alivePlayers {
		if p.State == PlayingReady {
			m[p.Id] = p.Buffer
		}
	}
	w.afterStep1(w.TGame.Move(m))

	return true
}

// =========
// # Helpers

func (w *Wrapper) killPlayersFromTop(ps []*Player, deathText string) {
	ids := []int{}
	for _, p := range ps {
		ids = append(ids, p.Id)
	}

	w.afterStep1(w.TGame.KillPlayers(ids, deathText))

}

// will finish state of players and will correct alivePlayers
func (w *Wrapper) afterStep1(playing []int) {
	w.turnCounter++

	// reset players
	for _, p := range w.alivePlayers {
		if w.timeMode == PerRound {
			p.Time = w.time
		}
		p.Buffer = ""
		p.Feelings = WantBlood
		p.State = NotPlaying
	}

	// set playing players
	for _, id := range playing {
		w.AllPlayers[id].State = Playing
	}

	// set dead players
	dead := w.TGame.Result()

	winners := 0.0
	losers := 0.0
	for _, r := range dead {
		if r.Result == coreutils.Win || r.Result == coreutils.Draw {
			winners++
		} else {
			losers++
		}
	}

	newAlive := []*Player{}
	for _, p := range w.alivePlayers {
		info, ok := dead[p.Id]
		if !ok {
			newAlive = append(newAlive, p)
			continue
		}
		p.State = Dead
		p.DeathInfo = info
		if info.Result == coreutils.Win || info.Result == coreutils.Draw {
			p.UpdatedElo = p.Elo + (losers/(winners+losers))/winners
		} else {
			p.UpdatedElo = p.Elo - 1/float64(len(w.AllPlayers))
		}
	}
	w.alivePlayers = newAlive
}
