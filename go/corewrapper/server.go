package corewrapper

import (
	"encoding/json"
	"gra/server"
	"gra/utils"
)

type WrapperArgs struct {
	GameMode GameMode
	MapNodes int
	Seed     int64
	Players  int

	TimeMode TimeMode
	Time     int
}

type Factory struct{}

func (Factory) NewGame(generatorArgs []byte) (game server.GenericGameInterface, players int, eloType string, defaultElo float64) {
	var args WrapperArgs
	if json.Unmarshal(generatorArgs, &args) != nil {
		return nil, 0, "", 0
	}
	names, eloes := getNotInitedNames(args.Players)
	w := newWrapper(args.GameMode, args.MapNodes, args.Seed, names, eloes, make([]int, args.Players), args.TimeMode, args.Time)
	if w == nil {
		return nil, 0, "", 0
	}
	return w, args.Players, "elo1", 0
}

// ======================
// # GenericGameInterface

func (w *Wrapper) Init(names []string, eloes []float64) {
	if len(names) != len(w.AllPlayers) || len(eloes) != len(names) {
		panic("wrong len of the names and eloes")
	}
	for i, p := range w.AllPlayers {
		p.Name = names[i]
		p.Elo = eloes[i]
		w.NameToPlayer[p.Name] = p
	}
}

func (w *Wrapper) Time() (gameMsg []byte) {
	if w.End() {
		return nil
	}
	w.TimeStep()
	outMsg, _ := w.TryStep()
	return outMsg
}

func (w *Wrapper) PlayerMsg(name string, playerMsg []byte) (gameMsg []byte) {
	p := w.NameToPlayer[name]
	var msg PlayerMsg
	err := json.Unmarshal(playerMsg, &msg)
	if !p.Alive() || err != nil || msg.Turn != w.turnCounter {
		return nil
	}

	switch msg.T {
	case FeelingT:
		if p.SetFeelings(msg.Feeling) == nil {
			return nil
		}
	case BufferT:
		if p.SetBuffer(msg.Buffer) == nil {
			return nil
		}
	}

	outMsg, _ := w.TryStep()
	return outMsg
}

func (w *Wrapper) KillPlayers(names []string, text string) []byte {
	newDead := []*Player{}
	for _, name := range names {
		if p := w.NameToPlayer[name]; p != nil && p.Alive() {
			newDead = append(newDead, p)
		}
	}
	if len(newDead) == 0 {
		return nil
	}

	w.killPlayersFromTop(newDead, text)

	msg := WrapperMsg{
		T:             ServerKilledPlayersT,
		Killed:        names,
		KillingReason: text,
	}
	w.reconnectMsgBuffer = append(w.reconnectMsgBuffer, msg)
	return utils.ToBytes(msg)
}

func (g *Wrapper) PlayerElo(name string) float64 {
	return g.NameToPlayer[name].UpdatedElo
}

// # GenericGameInterface
// ======================

// ============
// # for client

// returns if there was an hard update (for GUI)
func (w *Wrapper) ClientUpdate(encodedMsg []byte, you string) bool {
	var msg WrapperMsg
	utils.CheckErr(json.Unmarshal(encodedMsg, &msg))
	return w.msgUpdate(msg, you)
}
