package gulrot

import (
	"github.com/hajimehoshi/ebiten/v2"
)

// very simple Layout, it has its size and childs must obey
type Layout struct {
	childs []*component
	opt    Opt
	Bopt   *Bopt
	you    *component
}

func NewLayout(bopt *Bopt) *Layout {
	return &Layout{
		childs: []*component{},
		Bopt:   bopt,
	}
}

func (r *Layout) AddLeaf(l LeafInterface) {
	r.childs = append(r.childs, &component{leaf: l})
}

func (r *Layout) AddLayout(l *Layout) {
	c := &component{layout: l}
	l.you = c
	r.childs = append(r.childs, c)
}

func (r *Layout) IsMeOrMyDescandantFocused() bool {
	return r.you.isMeOrMyDescendantFocused
}

func (l *Layout) updateSize(x1, y1, x2, y2 int) Opt {
	l.opt = l.Bopt.Layout(x2-x1+1, y2-y1+1)
	x, y := anchoredXY(l.opt.X, l.opt.Y, l.opt.Anchor, l.opt.Width, l.opt.Height)
	x1 = x1 + x
	y1 = y1 + y
	x2 = x1 + l.opt.Width - 1
	y2 = y1 + l.opt.Height - 1
	for _, ch := range l.childs {
		ch.updateSize(x1+l.opt.Border, y1+l.opt.Border, x2-l.opt.Border, y2-l.opt.Border)
	}
	return l.opt
}

func (l *Layout) update(touches []*GTouch, isMouseAbove bool) {
	updateChilds(l.childs, touches, isMouseAbove)
}

func (l *Layout) draw(screen *ebiten.Image) {
	for _, ch := range l.childs {
		ch.draw(screen)
	}
}
