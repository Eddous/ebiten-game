package gulrot

type Anchor byte

const (
	LT Anchor = iota
	LM
	LB
	MT
	MM
	MB
	RT
	RM
	RB
)

// returns new x, y
func anchoredXYFloat(x, y float64, a Anchor, w, h float64) (float64, float64) {
	switch a {
	case LT:
		return x, y
	case LM:
		return x, y - h/2
	case LB:
		return x, y - h
	case MT:
		return x - w/2, y
	case MM:
		return x - w/2, y - h/2
	case MB:
		return x - w/2, y - h
	case RT:
		return x - w, y
	case RM:
		return x - w, y - h/2
	case RB:
		return x - w, y - h
	default:
		panic("Dont know this anchor")
	}
}

// returns new x, y
func anchoredXY(x, y int, a Anchor, w, h int) (int, int) {
	switch a {
	case LT:
		return leftOrTop(x, w), leftOrTop(y, h)
	case LM:
		return leftOrTop(x, w), mid(y, h)
	case LB:
		return leftOrTop(x, w), rightOrBot(y, h)
	case MT:
		return mid(x, w), leftOrTop(y, h)
	case MM:
		return mid(x, w), mid(y, h)
	case MB:
		return mid(x, w), rightOrBot(y, h)
	case RT:
		return rightOrBot(x, w), leftOrTop(y, h)
	case RM:
		return rightOrBot(x, w), mid(y, h)
	case RB:
		return rightOrBot(x, w), rightOrBot(y, h)
	default:
		panic("Dont know this anchor")
	}
}

func leftOrTop(coord, _ int) int {
	return coord
}

func mid(coord, size int) int {
	tmp := size / 2
	if size%2 == 1 {
		return coord - tmp
	}
	return coord - tmp + 1

}

func rightOrBot(coord, size int) int {
	return coord - size + 1
}
