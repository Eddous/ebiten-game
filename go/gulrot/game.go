package gulrot

import (
	"fmt"
	"gra/utils"
	_ "image/png"
	"math"

	"github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/ebitenutil"
	"github.com/hajimehoshi/ebiten/v2/inpututil"
)

// this just holds some function to call the framework back
type GulrotController struct {
	g *Game
}

// for restarting the focus, just send nil
func (c *GulrotController) SetFocused(a any) {
	c.g.root.restartFocused()
	if a != nil {
		c.g.root.setFocused(a)
	}
}

// closing is done outside
func (c *GulrotController) OpenKeyboard(value string, password bool) {
	if c.g.openKeyboard != nil {
		c.g.openKeyboard(value, password)
	}
}

// 0: no change, 1: something, 2: nothing
func (c *GulrotController) IncomingMsg() (string, byte) {
	switch c.g.incomingMsg {
	case 0:
		return "", 0
	case 1:
		msg := c.g.charsBuffer
		c.g.incomingMsg = 0
		c.g.charsBuffer = ""
		return msg, 1
	case 2:
		c.g.incomingMsg = 0
		return "", 2
	}
	panic("not an option")
}

type Game struct {
	controller   ControllerInterface
	openKeyboard func(string, bool)

	root   *Root
	inited bool

	mouse   *GTouch
	touches []touch

	// 0 = no change, 1 = something, 2 = nothing
	incomingMsg byte
	charsBuffer string

	cursorEnabled bool
}

type touch struct {
	id ebiten.TouchID
	t  *GTouch
}

func NewGame(controller ControllerInterface, openKeyboard func(string, bool)) *Game {
	return &Game{
		controller:    controller,
		openKeyboard:  openKeyboard,
		touches:       make([]touch, 0),
		cursorEnabled: true,
	}
}

func (g *Game) SendOuterChars(something bool, chars string) {
	if something {
		g.incomingMsg = 1
		g.charsBuffer = chars
	} else {
		g.incomingMsg = 2
	}
}

// because of a bug in ebiten
func (g *Game) DisableCursorPosition() {
	g.cursorEnabled = false
}

// ================
// # Game interface

func (g *Game) Layout(ow int, oh int) (int, int) {
	s := ebiten.Monitor().DeviceScaleFactor()
	w, h := int(float64(ow)*s), int(float64(oh)*s)

	w, h = size(w, h)

	if g.root != nil {
		g.root.layout(w, h)
	}
	return w, h
}

// dont allow more then specified ration e.g. 16:9 or 9:16
func size(w, h int) (int, int) {
	const ratio1 = 16
	const ratio2 = 9

	max, min := utils.Max(w, h), utils.Min(w, h)
	if max/ratio1 <= min/ratio2 {
		return w, h
	}
	newMax := (min * ratio1) / ratio2

	if w == max {
		return newMax, h
	}
	return w, newMax
}

func (g *Game) Update() error {
	if g.root == nil {
		g.root = g.controller.Init(GulrotController{g: g})
	}

	touches := []*GTouch{}
	g.handleMouse()
	if g.mouse != nil {
		if g.mouse.Counter == 0 {
			touches = append(touches, g.mouse)
		}
	}
	touches = append(touches, g.handleTouches()...)

	g.inited = true
	g.root.update(touches, g.cursorEnabled)
	if new := g.controller.Update(); new != nil {
		g.root = new
		g.inited = false
	}
	return nil
}

func (g *Game) Draw(screen *ebiten.Image) {
	if !g.inited {
		return
	}
	g.root.draw(screen)

	tps := int(math.Round(ebiten.ActualTPS()))
	fps := int(math.Round(ebiten.ActualFPS()))
	if !utils.IsInInt(tps, 58, 62) || !utils.IsInInt(fps, 55, 65) {
		debug := fmt.Sprintf("%d/%d", tps, fps)
		ebitenutil.DebugPrint(screen, debug)
	}

}

func (g *Game) handleMouse() {
	x, y := ebiten.CursorPosition()
	if g.mouse == nil {
		if inpututil.IsMouseButtonJustPressed(0) {
			g.mouse = &GTouch{
				GlobX: x,
				GlobY: y,
			}
		}
		return
	}

	g.mouse.GlobX = x
	g.mouse.GlobY = y
	g.mouse.Counter += 1
	if inpututil.IsMouseButtonJustReleased(0) {
		g.mouse.Released = true
		g.mouse = nil
	}
	if !ebiten.IsMouseButtonPressed(0) && g.mouse != nil {
		panic("The mouse is not released even though it should! ")
	}
}

func (g *Game) handleTouches() []*GTouch {
	// removing realesed touches within last tick
	unreleased := []touch{}
	for _, t := range g.touches {
		if !t.t.Released {
			unreleased = append(unreleased, t)
		}
	}
	g.touches = unreleased

	// adding new touches
	justPressed := inpututil.AppendJustPressedTouchIDs(nil)
	for _, id := range justPressed {
		g.touches = append(g.touches, touch{id: id, t: &GTouch{Counter: -1}}) // counter will be imidiately incremented
	}

	// updating touches
	for _, t := range g.touches {
		t.t.Counter++
		if inpututil.IsTouchJustReleased(t.id) {
			t.t.Released = true
		} else {
			// if the touch is realesed, then this returns (0,0)
			t.t.GlobX, t.t.GlobY = ebiten.TouchPosition(t.id)
		}
	}

	// returning just pressed touches
	// counter == 0 is little bit hacky
	ret := []*GTouch{}
	for _, t := range g.touches {
		if t.t.Counter == 0 {
			ret = append(ret, t.t)
		}
	}
	return ret
}
