package gulrot

import (
	"gra/utils"
)

// counter == 0 && released cannot happen
type GTouch struct {
	GlobX, GlobY int
	Counter      int
	Released     bool

	focusingTouch bool
}

type LTouch struct {
	*GTouch
	c *component
}

// The LTouch is bounded with a component
// The component has it's bounderies, thus it is possible to ask
// if the touch did not drift away
func (t LTouch) InMe() bool {
	return utils.IsInInt(t.GlobX, t.c.x1, t.c.x2) && utils.IsInInt(t.GlobY, t.c.y1, t.c.y2)
}

func (t LTouch) X() int {
	return t.GlobX - t.c.x1
}

func (t LTouch) Y() int {
	return t.GlobY - t.c.y1
}

func (t LTouch) VecF() utils.VecF {
	return utils.NewVecF(float64(t.X()), float64(t.Y()))
}

type Input struct {
	touches      []*GTouch
	c            *component
	isMouseAbove bool
}

func NewInput(touches []*GTouch, c *component, isMouseAbove bool) Input {
	return Input{touches: touches, c: c, isMouseAbove: isMouseAbove}
}

func (i Input) Touches() []LTouch {
	ret := []LTouch{}
	for _, t := range i.touches {
		lt := LTouch{GTouch: t, c: i.c}
		ret = append(ret, lt)
	}
	return ret
}

func (i Input) IsMouseAbove() bool {
	return i.isMouseAbove
}

func (i Input) IsFocused() bool {
	return i.c.isMeOrMyDescendantFocused
}
