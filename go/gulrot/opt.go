package gulrot

import "image/color"

type Opt struct {
	X, Y          int // relative on a parent
	Width, Height int // including border
	Anchor        Anchor
	Border        int
	FillColor     color.Color // due to the limitation of img.Fill(color) this can be nil - indicating that there is no color
	Visible       bool
}

type Bopt struct {
	Geom         func(ow int, oh int) (x int, y int, w int, h int)
	Anchor       Anchor
	Border       int
	FillColor    color.Color // due to the limitation of img.Fill(color) this can be nil - indicating that there is no color
	Visible      bool
	LastW, LastH int
}

func NewBopt(geom func(ow int, oh int) (int, int, int, int), anchor Anchor, border int, fillColor color.Color, visible bool) *Bopt {
	return &Bopt{
		Geom:      geom,
		Anchor:    anchor,
		Border:    border,
		FillColor: fillColor,
		Visible:   visible,
	}
}

// gulrot.Layouter
func (b *Bopt) Layout(width, height int) Opt {
	opt := Opt{}
	opt.X, opt.Y, opt.Width, opt.Height = b.Geom(width, height)
	opt.Anchor = b.Anchor
	opt.Border = b.Border
	opt.FillColor = b.FillColor
	opt.Visible = b.Visible
	b.LastW = opt.Width
	b.LastH = opt.Height

	return opt
}
