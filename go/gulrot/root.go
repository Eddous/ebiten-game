package gulrot

import (
	"image/color"

	"github.com/hajimehoshi/ebiten/v2"
)

type Root struct {
	color  color.Color
	childs []*component
}

func NewRoot(fillColor color.Color) *Root {
	return &Root{color: fillColor, childs: []*component{}}
}

func (r *Root) AddLeaf(l LeafInterface) {
	r.childs = append(r.childs, &component{leaf: l})
}

func (r *Root) AddLayout(l *Layout) {
	c := &component{layout: l}
	l.you = c
	r.childs = append(r.childs, c)
}

func (r *Root) restartFocused() {
	for _, c := range r.childs {
		c.restartFocused()
	}
}

// will set into focus layout or leaf and it's all parents
func (r *Root) setFocused(a any) {
	for _, c := range r.childs {
		c.setFocused(a)
	}
}

// this will update gui Opts
func (r *Root) layout(w, h int) {
	for _, ch := range r.childs {
		ch.updateSize(0, 0, w-1, h-1)
	}
}

// it will detect new touch and if there is one, it will select the first with counter == 0 (there can be more)
func (r *Root) update(touches []*GTouch, isMouseEnabled bool) {
	for _, gt := range touches {
		if gt.Counter == 0 { // just pressed
			r.restartFocused()
			gt.focusingTouch = true
			break
		}
	}
	updateChilds(r.childs, touches, isMouseEnabled)
}

func (r *Root) draw(screen *ebiten.Image) {
	screen.Fill(r.color)
	for _, ch := range r.childs {
		ch.draw(screen)
	}
}

// this function is also for layout
func updateChilds(childs []*component, touches []*GTouch, isMouseAbove bool) {
	for i := len(childs) - 1; i >= 0; i-- {
		keep := make([]*GTouch, 0)
		push := make([]*GTouch, 0)
		ch := childs[i]
		if !ch.lastOpt.Visible {
			continue
		}
		for _, t := range touches {
			if ch.isIn(t.GlobX, t.GlobY) {
				ch.isMeOrMyDescendantFocused = ch.isMeOrMyDescendantFocused || t.focusingTouch // tracing where the focusing touch goes
				push = append(push, t)
			} else {
				keep = append(keep, t)
			}
		}

		isMouseAboveCurr := isMouseAbove && ch.isIn(ebiten.CursorPosition())
		if isMouseAboveCurr {
			isMouseAbove = false
		}

		touches = keep
		ch.update(push, isMouseAboveCurr)
	}
}
