package gulrot

import (
	"math"
)

func StandardScaler(wScale, hScale float64) func(int, int) (int, int) {
	return func(w, h int) (int, int) {
		return int(wScale * float64(w)), int(hScale * float64(h))
	}
}

func FixedRationScalerW(wScale float64, rw, rh float64) func(int, int) (int, int) {
	return func(outW, outH int) (int, int) {
		w := float64(outW) * wScale
		return int(w), int((rh / rw) * w)
	}

}

func FixedRationScalerH(hScale float64, rw, rh float64) func(int, int) (int, int) {
	return func(outW, outH int) (int, int) {
		h := float64(outH) * hScale
		return int((rw / rh) * h), int(h)
	}
}

// wscale: maximum width it can take from outwidth,
// hscale: maximum height it can take from outheigh,
// w2h: width:height ratio, eq: (5:1) w2h = 0.2
func NiceScaler(wscale, hscale, w2h float64) func(int, int) (int, int) {
	return func(width, height int) (int, int) {
		maxW := float64(width) * wscale
		maxH := float64(height) * hscale
		maxW2 := maxH / w2h
		w := math.Min(maxW, maxW2)
		return int(w), int(w * w2h)
	}
}

// component takes centrain portion of a parent space
func QuadraticScaler(takenSpace, w2h float64) func(int, int) (int, int) {
	return func(width, height int) (int, int) {
		wSquered := float64(width) * float64(height) * takenSpace / w2h
		w := math.Sqrt(wSquered)
		return int(w), int(w * w2h)
	}
}

// == text scalers ==

func TextHeightScaler(factor float64) func(ow, oh int) float64 {
	return func(ow, oh int) float64 {
		return factor * float64(oh)
	}
}

func TextNiceScaler(wscale, hscale float64) func(int, int) float64 {
	return func(width, height int) float64 {
		maxW := float64(width) * wscale
		maxH := float64(height) * hscale
		return math.Min(maxW, maxH)
	}
}
