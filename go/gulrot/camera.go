package gulrot

import (
	"gra/utils"

	"github.com/hajimehoshi/ebiten/v2"
)

type CameraTouch struct {
	c         *Camera
	initTouch utils.VecF
	lt        LTouch
}

func NewCameraTouch(c *Camera, lt LTouch) *CameraTouch {
	x, y := c.PixelsToGame1(float64(lt.X()), float64(lt.Y()))
	return &CameraTouch{
		c:         c,
		initTouch: utils.NewVecF(x, y),
		lt:        lt,
	}
}

// true -> continue, false -> remove the touch
func (ct *CameraTouch) Update() bool {
	if ct.lt.Released {
		return false
	}
	ct.c.Move(ct.initTouch, ct.lt.VecF())
	return true
}

type Camera struct {
	game, pixels utils.VecF
	zoom         float64

	w, h int
}

// fontScale is fontSize in 1:1
func NewCamera(game, pixels utils.VecF, zoom float64) *Camera {
	return &Camera{
		game:   game,
		pixels: pixels,
		zoom:   zoom,
	}
}

// position of game, and what pixel it is projected to
func (c *Camera) Jump(game, pixels utils.VecF) {
	c.game = game
	c.pixels = pixels
}

func (c *Camera) Zoom(speed float64) {
	if c.w != 0 && c.h != 0 {
		pixels := utils.NewVecF(float64(c.w)/2, float64(c.h)/2)
		game := c.PixelsToGame2(pixels)
		c.game = game
		c.pixels = pixels
	}
	c.zoom *= speed
}

func (c *Camera) Move(firstGamePosition, currentPixelPosition utils.VecF) {
	c.game = firstGamePosition
	c.pixels = currentPixelPosition
}

// from game to pixels
func (c *Camera) GameToPixelsSize(gameSize float64) float64 { return c.zoom * gameSize }

// from pixels to game
func (c *Camera) PixelsToGameSize(pixelSize float64) float64 { return pixelSize / c.zoom }

// from game to pixels
func (c *Camera) GameToPixels1(x, y float64) (float64, float64) {
	a, bx, by := c.getABxBy()
	return a*x + bx, a*y + by
}

func (c *Camera) GameToPixels2(pos utils.VecF) utils.VecF {
	return utils.NewVecF(c.GameToPixels1(pos.X, pos.Y))
}

// from pixels to game
func (c *Camera) PixelsToGame1(x, y float64) (float64, float64) {
	a, bx, by := c.getABxBy()
	return (x - bx) / a, (y - by) / a
}

func (c *Camera) PixelsToGame2(pos utils.VecF) utils.VecF {
	return utils.NewVecF(c.PixelsToGame1(pos.X, pos.Y))
}

func (c *Camera) GetZoom() float64 {
	return c.zoom
}

// for drawing then use basic Draw
func (c *Camera) Dio(img *ebiten.Image, x, y float64, anchor Anchor, w, h, r float64) *ebiten.DrawImageOptions {
	x, y = c.GameToPixels1(x, y)
	w, h = c.GameToPixelsSize(w), c.GameToPixelsSize(h)
	return Dio(img, x, y, anchor, w, h, r)
}

func (c *Camera) Draw(dst *ebiten.Image, img *ebiten.Image, x, y float64, anchor Anchor, w, h, r float64) {
	c.w, c.h = ImgSize(dst)
	Draw1(dst, img, c.Dio(img, x, y, anchor, w, h, r))
}

// normaly Draw sets the ouput dimensions
// if the Draw is not called (only Dio is called), then the zoom behaves wierdly
func (c *Camera) SetOutDim(dst *ebiten.Image) {
	c.w, c.h = ImgSize(dst)
}

// =========
// # Helpers

// camera(x, y) = a*x + bx, a*y + by
func (c *Camera) getABxBy() (float64, float64, float64) {
	return c.zoom, c.pixels.X - c.zoom*c.game.X, c.pixels.Y - c.zoom*c.game.Y
}
