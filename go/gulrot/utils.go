package gulrot

import (
	"embed"
	"errors"
	"image"
	"image/color"
	"image/jpeg"
	"image/png"
	"io/fs"
	"log"
	"os"

	"github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/text/v2"
)

// =========
// # Drawing

func Dio(img *ebiten.Image, x, y float64, anchor Anchor, w, h, r float64) *ebiten.DrawImageOptions {
	geom := ebiten.GeoM{}
	geom.Scale(imageToMxN(img, w, h))
	geom.Translate(-w/2, -h/2)
	geom.Rotate(r)
	geom.Translate(w/2, h/2)
	geom.Translate(anchoredXYFloat(x, y, anchor, w, h))
	return &ebiten.DrawImageOptions{GeoM: geom, Filter: ebiten.FilterLinear}
}

func Draw1(dst, img *ebiten.Image, dio *ebiten.DrawImageOptions) {
	min := dst.Bounds().Min
	dio.GeoM.Translate(float64(min.X), float64(min.Y))
	dst.DrawImage(img, dio)
}

// TODO float64 -> int
func Draw2(dst, img *ebiten.Image, x, y float64, anchor Anchor, w, h, r float64) {
	Draw1(dst, img, Dio(img, x, y, anchor, w, h, r))
}

// TODO float64 -> int
func Draw3(dst, img *ebiten.Image, x, y float64, anchor Anchor, w, h float64) {
	Draw2(dst, img, x, y, anchor, w, h, 0)
}

// draw image exactly on the destination image
func Draw4(dst, img *ebiten.Image) {
	w, h := ImgSize(dst)
	Draw3(dst, img, 0, 0, LT, float64(w), float64(h))
}

// pads are in pixels
func Text(screen *ebiten.Image, str string, font *text.GoTextFaceSource, size float64, x, y int, anchor Anchor, padX, padY int, color color.Color) {
	face := &text.GoTextFace{
		Source: font,
		Size:   size,
	}
	w, h := text.Measure(str, face, GetFontSpacing(face))
	x, y = anchoredXY(x, y, anchor, int(w), int(h))
	lt := screen.Bounds().Min

	geom := ebiten.GeoM{}
	geom.Translate(float64(x+padX+lt.X), float64(y+padY+lt.Y))

	colorScale := ebiten.ColorScale{}
	colorScale.ScaleWithColor(color)

	opt := &text.DrawOptions{
		LayoutOptions: text.LayoutOptions{
			LineSpacing:  GetFontSpacing(face),
			PrimaryAlign: text.AlignStart,
		},
		DrawImageOptions: ebiten.DrawImageOptions{
			GeoM:       geom,
			ColorScale: colorScale,
		},
	}
	text.Draw(screen, str, face, opt)
}

func GetFontSpacing(face *text.GoTextFace) float64 {
	m := face.Metrics()
	return m.HAscent + m.HDescent + m.HLineGap
}

func TextSize(txt string, face *text.GoTextFace) (float64, float64) {
	return text.Measure(txt, face, GetFontSpacing(face))
}

// ====================
// # Getting Image/Font

func LoadImage2(path string) *ebiten.Image {
	file, err := os.Open(path)
	if err != nil {
		panic(err)
	}
	image, _, err := image.Decode(file)
	if err != nil {
		panic(err)
	}
	return ebiten.NewImageFromImage(image)
}

func LoadImage(fs embed.FS, name string) *ebiten.Image {
	file := open(fs, name)
	defer file.Close()

	image, _, err := image.Decode(file)
	if err != nil {
		panic(err)
	}
	return ebiten.NewImageFromImage(image)
}

func LoadFont(fs embed.FS, name string) *text.GoTextFaceSource {
	file := open(fs, name)
	defer file.Close()
	s, err := text.NewGoTextFaceSource(file)
	if err != nil {
		log.Fatal(err)
	}
	return s
}

// ========
// # Others

func ImgSize(img *ebiten.Image) (int, int) {
	s := img.Bounds().Size()
	return s.X, s.Y
}

func SavePng(in *ebiten.Image, path string) {
	if _, err := os.Stat(path); errors.Is(err, os.ErrNotExist) {
		f, err := os.Create(path)
		if err != nil {
			panic(err)
		}
		png.Encode(f, in)
	}
}

func SaveJpeg(in *ebiten.Image, path string, quality int) {
	if _, err := os.Stat(path); errors.Is(err, os.ErrNotExist) {
		f, err := os.Create(path)
		if err != nil {
			panic(err)
		}
		jpeg.Encode(f, in, &jpeg.Options{Quality: quality})
	}
}

func ColorPixel(c color.Color) *ebiten.Image {
	img := ebiten.NewImage(1, 1)
	img.Set(0, 0, c)
	return img
}

// =========
// # helpers

// how to scale the image in drawing
func imageToMxN(image *ebiten.Image, m, n float64) (float64, float64) {
	x, y := ImgSize(image)
	return float64(m) / float64(x), float64(n) / float64(y)
}

func open(fs embed.FS, name string) fs.File {
	dirs, err := fs.ReadDir(".")
	if err != nil {
		panic(err)
	}
	path := dirs[0].Name() + "/" + name
	f, err := fs.Open(path)
	if err != nil {
		panic(err)
	}
	return f
}
