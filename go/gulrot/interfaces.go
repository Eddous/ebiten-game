package gulrot

import (
	"github.com/hajimehoshi/ebiten/v2"
)

type ControllerInterface interface {
	Init(GulrotController) *Root
	Update() *Root
}

type LeafInterface interface {
	Layout(int, int) Opt // parent w, parent h => how to show myself
	Update(Input)
	Draw(*ebiten.Image)
}
