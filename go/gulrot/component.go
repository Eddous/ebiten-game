package gulrot

import (
	"gra/utils"
	"image"
	"image/color"

	"github.com/hajimehoshi/ebiten/v2"
)

type component struct {
	leaf   LeafInterface
	layout *Layout

	// absolute value, for calculating input, including bounds
	x1, y1, x2, y2 int

	// also I could implement isFocused that would mark only one component,
	// rather then whole path, but I do not see any use-case
	// the text field group makes more sense (tab, enter), then sending the keys to the focused element
	isMeOrMyDescendantFocused bool

	// options for drawing
	lastOpt Opt
}

// for touches
func (c *component) isIn(x, y int) bool {
	return utils.IsInInt(x, c.x1, c.x2) && utils.IsInInt(y, c.y1, c.y2)
}

func (c *component) restartFocused() {
	c.isMeOrMyDescendantFocused = false
	if c.layout != nil {
		for _, ch := range c.layout.childs {
			ch.restartFocused()
		}
	}
}

// it is important to find the Leaf/Layout since the component struct is hidden from other code
// also if the child of layout is focused, the layout is focused as well
func (c *component) setFocused(a any) bool {
	if c.layout != nil {
		if l, ok := a.(*Layout); ok && l == c.layout {
			c.isMeOrMyDescendantFocused = true
			return true
		}
		for _, ch := range c.layout.childs {
			if ch.setFocused(a) {
				c.isMeOrMyDescendantFocused = true
				return true
			}
		}
		return false
	}

	// the component is leaf
	if a == c.leaf { // this should work
		c.isMeOrMyDescendantFocused = true
	}
	return c.isMeOrMyDescendantFocused
}

func (c *component) updateSize(x1, y1, x2, y2 int) {
	if c.leaf != nil {
		c.lastOpt = c.leaf.Layout(x2-x1+1, y2-y1+1)
	}
	if c.layout != nil {
		c.lastOpt = c.layout.updateSize(x1, y1, x2, y2)
	}

	x, y := anchoredXY(
		c.lastOpt.X,
		c.lastOpt.Y,
		c.lastOpt.Anchor,
		c.lastOpt.Width,
		c.lastOpt.Height,
	)
	c.x1 = x1 + x
	c.y1 = y1 + y
	c.x2 = c.x1 + c.lastOpt.Width - 1
	c.y2 = c.y1 + c.lastOpt.Height - 1
}

func (c *component) update(touches []*GTouch, isMouseAbove bool) {
	if c.leaf != nil {
		c.leaf.Update(NewInput(touches, c, isMouseAbove))
	}
	if c.layout != nil {
		c.layout.update(touches, isMouseAbove)
	}
}

func (c *component) draw(dst *ebiten.Image) {
	if !c.lastOpt.Visible {
		return
	}

	c.drawBorder(dst)

	this := dst.SubImage(
		image.Rect(
			c.x1+c.lastOpt.Border,
			c.y1+c.lastOpt.Border,
			c.x2+1-c.lastOpt.Border,
			c.y2+1-c.lastOpt.Border,
		)).(*ebiten.Image)

	if c.lastOpt.FillColor != nil {
		this.Fill(c.lastOpt.FillColor)
	}

	if c.leaf != nil {
		c.leaf.Draw(this)
	}
	if c.layout != nil {
		c.layout.draw(this)
	}
}

func (c *component) drawBorder(dst *ebiten.Image) {

	/*dst.SubImage(
		image.Rect(
			c.x1,
			c.y1,
			c.x2+1,
			c.y2+1,
		),
	).(*ebiten.Image).Fill(color.Black)*/
	b := c.lastOpt.Border

	dst.SubImage(
		image.Rect(
			c.x1,
			c.y1,
			c.x1+b,
			c.y2+1,
		),
	).(*ebiten.Image).Fill(color.Black)

	dst.SubImage(

		image.Rect(
			c.x1,
			c.y1,
			c.x2+1,
			c.y1+b,
		),
	).(*ebiten.Image).Fill(color.Black)

	dst.SubImage(

		image.Rect(
			c.x2+1-b,
			c.y1,
			c.x2+1,
			c.y2+1,
		),
	).(*ebiten.Image).Fill(color.Black)

	dst.SubImage(

		image.Rect(
			c.x1,
			c.y2+1-b,
			c.x2+1,
			c.y2+1,
		),
	).(*ebiten.Image).Fill(color.Black)
}
