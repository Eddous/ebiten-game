package main

import (
	"gra/gui"
	"gra/gulrot"
	"log"
	"syscall/js"
	"time"

	"github.com/hajimehoshi/ebiten/v2"
)

var game *gulrot.Game

func main() {
	game = gulrot.NewGame(
		gui.NewController(
			gui.ControllerArgs{
				Seed:           time.Now().UnixMicro(),
				ServerAdress:   "grrra.org:54775",
				SavedGamesPath: "",
				TestingEnv:     false,
				WebUrl:         "https://grrra.org",
				OpenBrowser:    openBrowser,
			},
		),
		openKeyboard,
	)

	js.Global().Set("sendCharsToGame", js.FuncOf(sendCharsToGame))
	js.Global().Set("sendNothingToGame", js.FuncOf(sendNothingToGame))
	js.Global().Set("disableCursorPosition", js.FuncOf(disableCursorPosition))

	err := ebiten.RunGame(game)
	if err != nil {
		log.Fatal(err)
	}
}

func openBrowser(url string) {
	// since this is called just once it is ok to retrive it here
	js.Global().Get("changeUrl").Invoke(url)
}

func openKeyboard(currentVal string, password bool) {
	js.Global().Get("openKeyboard").Invoke(currentVal, password)
}

func sendCharsToGame(this js.Value, args []js.Value) any {
	chars := args[0].String()
	game.SendOuterChars(true, chars)
	return js.Null()
}

func sendNothingToGame(this js.Value, args []js.Value) any {
	game.SendOuterChars(false, "")
	return js.Null()
}

func disableCursorPosition(this js.Value, args []js.Value) any {
	game.DisableCursorPosition()
	return js.Null()
}
