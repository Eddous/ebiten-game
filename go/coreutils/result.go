package coreutils

type Result byte

const (
	Lost Result = iota
	Draw
	Win
)

func (r Result) String() string {
	switch r {
	case Lost:
		return "Lost"
	case Draw:
		return "Draw"
	case Win:
		return "Win"
	}
	panic("does not happen")
}

type DeathInfo struct {
	Result Result
	Text   string
}

func NewDeathInfo(result Result, text string) DeathInfo {
	return DeathInfo{Result: result, Text: text}
}

func ResultsToFloat(in []Result) []float64 {
	ret := make([]float64, len(in))
	drawingPlayers := []int{}

	for pid, res := range in {
		if res == Win {
			ret[pid] = 1
			return ret
		}
		if res == Draw {
			drawingPlayers = append(drawingPlayers, pid)
		}
	}

	for _, pid := range drawingPlayers {
		ret[pid] = 1.0 / float64(len(drawingPlayers))
	}
	return ret
}
