package coreutils

// returns (soldiers, territories until next soldier)

func MaxSoldiers1(territory int) (int, int) {
	return territory / 3, 3 - territory%3
}

func MaxSoldiers2(territory int) (int, int) {
	if territory == 0 {
		return 0, 1
	}
	if territory < 3 {
		return 1, 3 - territory
	}
	if territory < 6 {
		return 2, 6 - territory
	}
	if territory < 10 {
		return 3, 10 - territory
	}
	return (territory / 5) + 2, 5 - (territory % 5)
}
