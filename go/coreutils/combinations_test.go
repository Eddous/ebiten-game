package coreutils

import (
	"fmt"
	"gra/utils"
	"math/rand"
	"testing"

	"gonum.org/v1/gonum/stat/combin"
)

func TestCombinationWithRepetetion(t *testing.T) {
	const n = 2
	const k = 4
	iters := 1000000
	rnd := rand.New(rand.NewSource(0))

	m := map[[k]int]int{}
	var arr [k]int
	for i := 0; i < iters; i++ {
		tmp := CombinationWithRepetetion(rnd, n, k)
		copy(arr[:], tmp)
		m[arr]++
	}

	expected := combin.Binomial(n+k-1, k)
	if len(m) != expected {
		t.Log("expected len is:", expected, ", real is:", len(m))
		t.FailNow()
	}
	average := iters / expected
	fivePercent := (average / 100) * 5
	for k, v := range m {
		if !utils.IsInInt(v, average-fivePercent, average+fivePercent) {
			t.Log("k =", k, " is out of 5% threshold, having:", v, "which is not in [", average-fivePercent, ",", average+fivePercent, "]")
			t.FailNow()
		}
	}

	// to see printed result run: go test -timeout 30s -run ^TestCombinationWithRepetetion$ gra/coreutils -v
	fmt.Print("[")
	for _, count := range m {
		fmt.Print(count, ",")
	}
	fmt.Print("]")
}
