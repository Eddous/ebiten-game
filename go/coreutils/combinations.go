package coreutils

import (
	"gra/utils"
	"math/rand"
	"slices"
)

// for n=2, k=3, fuction can return [0,0,0] [0,0,1] [0,1,1] [1,1,1]
// n: number of distinct items in a bag, k: number you wish to draw out of a bag
func CombinationWithRepetetion(rnd *rand.Rand, n int, k int) []int {
	if k == 0 {
		return []int{}
	}
	bag := make([]int, n+k-1)
	counter := -1
	for i := range bag {
		if i < n {
			bag[i] = i
		} else {
			bag[i] = counter
			counter--
		}
	}
	utils.Shuffle(rnd, bag)

	selection := bag[:k]
	slices.Sort(selection)

	slc1 := []int{}
	slc2 := []int{}
	for _, item := range selection {
		if item >= 0 {
			slc1 = append(slc1, item)
		} else {
			slc2 = append(slc2, -item)
		}
	}
	// reversing because [-2, -1, 0, 1] -> [0, 1] [2, 1]
	slices.Reverse(slc2)

	ret := make([]int, k)
	p1 := 0
	p2 := 0
	lastItem := 80085
	for i := range ret {
		// this will never happen first
		if p2 < len(slc2) && slc2[p2] == i {
			ret[i] = lastItem
			p2++
		} else {
			ret[i] = slc1[p1]
			lastItem = slc1[p1]
			p1++
		}
	}
	return ret
}

// can be generic but I dont have usage
func CombinationWithRepetetion2(rnd *rand.Rand, bag []int, k int) []int {
	if k == 0 {
		return []int{}
	}
	ret := make([]int, k)
	for i, j := range CombinationWithRepetetion(rnd, len(bag), k) {
		ret[i] = bag[j]
	}
	return ret
}

// n is number of items in the bag and k is a number you wish to take out
func AllCombinationsWithRepetetion(n, k int) [][]int {
	// just copied safeguard from above, idk if it makes sense
	if n == 0 && k == 0 || n < 0 || k < 0 {
		panic("wrong k or n")
	}
	ret := make([][]int, 0)
	curr := make([]int, k)

	for {
		cpy := make([]int, k)
		copy(cpy, curr)
		ret = append(ret, cpy)
		if !getNextCombinationWithRepetetion(n, curr) {
			break
		}
	}
	return ret
}

func getNextCombinationWithRepetetion(max int, curr []int) bool {
	for i := range curr {
		curr[i]++
		if curr[i] < max {
			for j := i - 1; j >= 0; j-- {
				curr[j] = curr[i]
			}
			return true
		}
		curr[i] = 0
	}
	return false

}
