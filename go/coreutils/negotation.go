package coreutils

import (
	"gra/graphgen"
	"gra/utils"
	"math"
	"math/rand"
	"sort"
)

func MinDistanceBetweenNodes(gameMap graphgen.Data, maxPlayers int) []int {
	cdf := distanceDistributionFunctionOfRandomTuples(
		rand.New(rand.NewSource(0)),
		gameMap.MinimalPathDistance,
		gameMap.ClosenessCentrality,
		maxPlayers,
		1000,
	)
	distances := getMinDistanceBetweenPlayerNodeChoices(50, cdf)
	for i := range distances {
		if distances[i] > 8 {
			distances[i] = 8
		}
	}
	return distances
}

func distanceDistributionFunctionOfRandomTuples(rnd *rand.Rand, distance [][]int, closenessCentrality []float64, maxPlayers int, steps int) [][]int {
	// for 1 player I want 0 size, for 2 players I want 1 size
	cdf := make([][]int, maxPlayers-1)
	for i := range cdf {
		cdf[i] = make([]int, 0)
	}

	// this WORKS, but I dont see a reason for it.
	// tmp := slices.Clone(closenessCentrality)
	//slices.Sort(tmp)
	//threshold := tmp[utils.Min(40, len(tmp)-1)]

	candidates := []int{}
	for nid := range distance {
		//if closenessCentrality[nid] <= threshold {
		candidates = append(candidates, nid)
		//}
	}

	// better sampling based on variation without repetition (the normalization will be different)
	// other sampling methods are bullshit because they break the normalization
	for range steps {
		min := math.MaxInt32
		choices := []int{}
		for i := range maxPlayers {
			choices = append(choices, newChoice(rnd, candidates, choices))
			for j := range i {
				dist := distance[choices[i]][choices[j]]
				if dist < min {
					min = dist
				}
			}
			if i != 0 {
				cdf[i-1] = addToCdf(cdf[i-1], min)
			}
		}
	}

	// cdf is cumulative distribution function
	for i := range cdf {
		for j := len(cdf[i]) - 1; j > 0; j-- {
			cdf[i][j-1] = cdf[i][j-1] + cdf[i][j]
		}
	}

	// normalization
	// the nTuples[0] is for two players
	nTuples := []int{len(candidates) * (len(candidates) - 1)}
	for i := range maxPlayers - 2 {
		nTuples = append(nTuples, nTuples[i]*(len(candidates)-2-i))
	}
	for i := range cdf {
		samples := cdf[i][0]
		for j := range cdf[i] {
			tmp := float64(cdf[i][j]) / float64(samples)
			tmp = tmp * float64(nTuples[i])
			tmp = math.Round(tmp)
			cdf[i][j] = int(tmp)
		}
	}
	cdf = append([][]int{nil, nil}, cdf...)
	return cdf
}

func addToCdf(cdf []int, val int) []int {
	for val >= len(cdf) {
		cdf = append(cdf, 0)
	}
	cdf[val]++
	return cdf
}

func newChoice(rnd *rand.Rand, candidates []int, choices []int) int {
start:
	nid := candidates[rnd.Intn(len(candidates))]
	for _, nid2 := range choices {
		if nid == nid2 {
			goto start
		}
	}
	return nid
}

func getMinDistanceBetweenPlayerNodeChoices(threshold int, distanceDistributionFunction [][]int) []int {
	minDists := make([]int, len(distanceDistributionFunction))
	for n := 2; n < len(distanceDistributionFunction); n++ {
		cdf := distanceDistributionFunction[n]
		selectedK := 1
		for k := range cdf {
			if cdf[k] >= threshold {
				selectedK = k
			}
		}
		minDists[n] = selectedK
	}
	return minDists
}

func distOfTuple(tuple []int, distance [][]int) int {
	min := math.MaxInt32
	for i := 0; i < len(tuple)-1; i++ {
		for j := i + 1; j < len(tuple); j++ {
			ch1, ch2 := tuple[i], tuple[j]
			if min > distance[ch1][ch2] {
				min = distance[ch1][ch2]
			}
		}
	}
	return min
}

// selects the best nodes combinations MUST BE DETERMINISTIC
func MaxMinDistance(choices map[int][]int, dist [][]int) map[int]int {
	matrix := make([][]int, 0)
	keys := []int{}
	for p := range choices {
		keys = append(keys, p)
	}
	sort.Ints(keys)
	for _, key := range keys {
		matrix = append(matrix, choices[key])
	}
	ret := map[int]int{}
	for i, n := range maxMinDistance(matrix, dist) {
		ret[keys[i]] = n
	}
	return ret
}

func maxMinDistance(choices [][]int, dist [][]int) []int {
	p := len(choices)

	bestDist := -1
	var bestGeometricMean uint64 = 0
	var best []int
	for counter := 0; counter < utils.PowInts(p, p); counter++ {
		curr := make([]int, p)
		for i, ch := range utils.IndexToTuple(counter, p, p) {
			curr[i] = choices[i][ch]
		}
		currDist := distOfTuple(curr, dist)
		if currDist < bestDist {
			continue
		}
		gm := geometricMean(curr, dist)
		if currDist > bestDist {
			best = curr
			bestDist = currDist
			bestGeometricMean = gm
			continue
		}
		// the distances are equal
		if gm > bestGeometricMean {
			best = curr
			bestGeometricMean = gm
		}
	}
	return best
}

func geometricMean(tuple []int, dist [][]int) uint64 {
	var ret uint64 = 1

	for i := 0; i < len(tuple)-1; i++ {
		for j := i + 1; j < len(tuple); j++ {
			n1, n2 := tuple[i], tuple[j]
			ret = ret * uint64(dist[n1][n2])
		}
	}
	return ret
}
