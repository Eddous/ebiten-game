package coreutils

import (
	"fmt"
	"gra/utils"
	"math/rand"
)

func NewMoveMatrix(size int) [][]int {
	ret := make([][]int, size)
	for i := 0; i < size; i++ {
		ret[i] = make([]int, size)
	}
	return ret
}

func ConcatMatrix(moves map[int][][]int) [][]int {
	var size int
	for _, m := range moves {
		size = len(m)
	}

	ret := NewMoveMatrix(size)
	for _, matrix := range moves {
		for i := range matrix {
			for j := range matrix {
				if matrix[i][j] != 0 {
					ret[i][j] = matrix[i][j]
				}
			}
		}
	}
	return ret
}

func AddToMatrix(a, b [][]int) {
	for i := range a {
		for j := range a {
			a[i][j] += b[i][j]
		}
	}
}

func CopyMatrix(matrix [][]int) [][]int {
	new := make([][]int, len(matrix))
	for i := range matrix {
		new[i] = make([]int, len(matrix))
		copy(new[i], matrix[i])
	}
	return new
}

// [0;max)
func RandomTuple(rnd *rand.Rand, lenght, max int) []int {
	ret := make([]int, lenght)
	for i := 0; i < lenght; i++ {
		ret[i] = rnd.Intn(max)
	}
	return ret
}

func PrintMatrix(matrix [][]int) {
	for i := 0; i < len(matrix); i++ {
		for j := 0; j < len(matrix); j++ {
			fmt.Print(matrix[i][j], " ")
		}
		fmt.Println()
	}
}

func IterateOverAllCoords(coords []int, dims []int) bool {
	for i := range dims {
		coords[i]++
		if coords[i] < dims[i] {
			return true
		}
		coords[i] = 0
	}
	return false
}

// returns map[nid][]pid
func NegotationToTrans(move map[int][]int) map[int][]int {
	nidsPids := make(map[int][]int)
	for pid, nids := range move {
		for _, nid := range nids {
			if nidsPids[nid] == nil {
				nidsPids[nid] = []int{pid}
			} else {
				nidsPids[nid] = append(nidsPids[nid], pid)
			}
		}
	}
	return nidsPids
}

func HelperArrowsPositions(nodes []utils.VecF, matrix [][]bool) []float64 {
	ret := make([]float64, len(nodes))
	for i, n1 := range nodes {
		angles := []float64{}
		for j, linked := range matrix[i] {
			if linked {
				angles = append(angles, utils.GetAngle(n1, nodes[j]))
			}
		}
		ret[i] = utils.MaxEmptyAngleInCircle(angles)
	}
	return ret
}
