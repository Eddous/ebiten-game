package coreutils

import (
	"encoding/json"
	"gra/utils"
	"strconv"
	"strings"
)

// this is for sending and receiving moves to and from server

// ===== To Buffer =====

func NidToStr(num int) string {
	return strconv.Itoa(num)
}

func NidsToStr(in []int) string {
	ret := ""
	for _, n := range in {
		ret += strconv.Itoa(n) + ";"
	}
	return ret
}

// it is used when translating moves in matrix from ONE player
func MovesToStr(moves [][]int) string {
	return string(utils.ToBytes(moves))
}

// ===== From Buffer =====

func StrToNid(buffer string) (int, bool) {
	n, err := strconv.Atoi(buffer)
	return n, err == nil
}

func StrToNids(str string) ([]int, bool) {
	ret := make([]int, 0)
	split := strings.Split(str, ";")
	for i := 0; i < len(split)-1; i++ {
		strNum := split[i]
		num, err := strconv.Atoi(strNum)
		if err != nil {
			return ret, false
		}
		ret = append(ret, num)
	}
	return ret, true
}

func StrToMoves(buffer string) ([][]int, bool) {
	var matrix [][]int
	err := json.Unmarshal([]byte(buffer), &matrix)
	return matrix, err == nil
}

// ===== To Buffer Batch =====

func ToStrBatch[T any](actions map[int]T, trans func(T) string) map[int]string {
	ret := map[int]string{}
	for pid, a := range actions {
		ret[pid] = trans(a)
	}
	return ret
}

func FromStrBatch[T any](actions map[int]string, trans func(string) (T, bool)) map[int]T {
	ret := map[int]T{}
	for pid, a := range actions {
		var ok bool
		ret[pid], ok = trans(a)
		if !ok {
			return nil
		}
	}
	return ret
}
