package main

import (
	"encoding/json"
	"fmt"
	ai1 "gra/core1/ai"
	state1 "gra/core1/state"
	ai2 "gra/core2/ai"
	state2 "gra/core2/state"
	"gra/coreutils"
	"gra/utils"
	"syscall/js"
)

// this is copied because I need to leave out the import of ebiten
type InMsg1 struct {
	State  state1.State
	AIType int
	Pid    int
}

type InMsg2 struct {
	State  state2.State
	AIType int
	Pid    int
}

type OutMsg struct {
	Pid    int
	Action string
}

func remoteAI(this js.Value, args []js.Value) any {
	gameType := args[0].Int()
	msgStr := args[1].String()
	switch gameType {
	case 1:
		var msg InMsg1
		if err := json.Unmarshal([]byte(msgStr), &msg); err != nil {
			panic(err)
		}
		ret := OutMsg{
			Pid:    msg.Pid,
			Action: core1AI(&msg.State, msg.AIType, msg.Pid),
		}
		return string(utils.ToBytes(ret))
	case 2:
		var msg InMsg2
		if err := json.Unmarshal([]byte(msgStr), &msg); err != nil {
			panic(err)
		}
		ret := OutMsg{
			Pid:    msg.Pid,
			Action: core2AI(&msg.State, msg.AIType, msg.Pid),
		}
		return string(utils.ToBytes(ret))
	default:
		panic("not an option")
	}
}

func core1AI(s *state1.State, aiType int, pid int) string {
	ai := ai1.AIBattery[aiType]()
	ai.Init(pid, 0)
	if !s.Players[pid].Playing {
		panic("player is not playing")
	}
	switch s.MoveType {
	case state1.Negotation:
		return coreutils.NidsToStr(ai.Negotation(s))
	case state1.Moves:
		return coreutils.MovesToStr(ai.Moves(s))
	case state1.Territory:
		return coreutils.NidToStr(ai.Territory(s))
	case state1.Soldiers:
		return coreutils.NidsToStr(ai.Soldiers(s))
	default:
		panic("not an option or end")
	}
}

func core2AI(s *state2.State, aiType int, pid int) string {
	ai := ai2.NewAI(pid, 0, aiType)
	str, ok := ai.Action(s)
	if !ok {
		panic("player is not playing")
	}
	return str
}

func main() {
	fmt.Println("AI WASM inited")
	js.Global().Set("remoteAI", js.FuncOf(remoteAI))
	select {}
}
