package main

import (
	"fmt"
	"gra/graphgen"
	"gra/utils"
	"syscall/js"
	"time"
)

var seed = time.Now().UnixMilli()

func seedGenerator(this js.Value, args []js.Value) any {
	seed = time.Now().UnixMilli()
	return js.Null()
}

func generate(this js.Value, args []js.Value) any {
	if len(args) != 9 {
		fmt.Println("wrong number of args")
		return js.Null()
	}
	fmt.Println("generating")
	gargs := graphgen.NewGenArgs(
		args[0].Int(),
		args[1].Float(),
		args[2].Float(),
		args[3].Float(),
		args[4].Float(),
		args[5].Float(),
		args[6].Float(),
		args[7].Float(),
		args[8].Float(),
	)

	out, ok := graphgen.Generate(seed, gargs)
	if !ok {
		return js.Null()
	}
	return string(utils.ToBytes(out))
}

func main() {
	fmt.Println("WASM inited")
	js.Global().Set("generate", js.FuncOf(generate))
	js.Global().Set("seedGenerator", js.FuncOf(seedGenerator))

	// keep running
	select {}
}
