package main

import (
	"gra/gui"
	"gra/gulrot"
	"gra/utils"
	"log"
	"time"

	"github.com/hajimehoshi/ebiten/v2"
)

func main() {
	w, h := 540, 540

	ebiten.SetWindowSize(w, h)
	ebiten.SetWindowResizingMode(ebiten.WindowResizingModeEnabled)
	ebiten.SetWindowTitle("Gra")

	err := ebiten.RunGame(
		gulrot.NewGame(
			gui.NewController(
				gui.ControllerArgs{
					Seed:           time.Now().UnixMicro(),
					ServerAdress:   "grrra.org:54775",
					SavedGamesPath: "",
					TestingEnv:     false,
					WebUrl:         "https://grrra.org",
					OpenBrowser:    utils.OpenBrowser,
				},
			),
			nil,
		),
	)
	if err != nil {
		log.Fatal(err)
	}
}
