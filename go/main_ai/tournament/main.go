package main

import (
	"fmt"
	ai1 "gra/core1/ai"
	ai2 "gra/core2/ai"
	"gra/coreutils"
	"gra/corewrapper"
	"gra/utils"
	"strconv"
	"time"
)

func main() {
	gameMode := corewrapper.Cities
	ais1 := []*ai1.AI{
		ai1.AIZoo[0](),
		ai1.AIZoo[1](),
	}
	ais2 := []*ai2.AI{
		ai2.NewAI(0, 0, 2),
		ai2.NewAI(1, 0, 3),
	}

	nodes := 20
	savedGames := "/tmp/gra_savedgames/"
	seed := time.Now().UnixMicro()
	trials := 100
	checkpointsAfter := 0
	limit := 100

	utils.CheckDirectory(savedGames)

	players := 0
	if gameMode == corewrapper.Classical {
		players = len(ais1)
	} else {
		players = len(ais2)
	}

	var result []coreutils.Result
	var history []map[int]string
	score := make([]float64, players)
	for i := 1; i < trials+1; i++ {
		fmt.Printf("iter %d | seed %019d", i, seed)

		if gameMode == corewrapper.Classical {
			result, history = ai1.RunAIExperiment(seed, nodes, ais1, limit)
		} else {
			result, history = ai2.RunAIExperiment(seed, nodes, ais2, limit)
		}

		fmt.Println(" | result", result)
		for j, val := range coreutils.ResultsToFloat(result) {
			score[j] += val
		}
		if checkpointsAfter != 0 && i%checkpointsAfter == 0 {
			bytes := corewrapper.SerializeFromMoves(gameMode, nodes, seed, players, history)
			utils.WriteBytesToFile(savedGames+"/"+strconv.Itoa(i), bytes)
		}
		seed = utils.NextSeed(seed)
	}
	printScore(score, trials)
}

func printScore(score []float64, num int) {
	for j := range score {
		fmt.Println(j, ":", score[j]/float64(num))
	}
}
