package main

import (
	"encoding/json"
	"fmt"
	"gra/core1/ai"
	"gra/core1/state"
	"os"
	"time"

	"math/rand"
)

type point struct {
	X         [][]float64
	EdgeIndex [2][]int
	Action    []float64
}

func main() {
	const nodes = 20
	const players = 3
	const size = 10000
	const path = "/tmp/gra_dataset_actor.json"
	const safeguard = 200

	rnd := rand.New(rand.NewSource(int64(time.Now().Unix())))

	// creating AIs
	ais := []*ai.AI{
		ai.AIBattery[0](),
		ai.AIBattery[0](),
		ai.AIBattery[0](),
	}
	for pid, ai := range ais {
		ai.Init(pid, rnd.Int63())
	}

	data := make([]point, 0)
	for i := range size {
		fmt.Println(i)

		counter := safeguard
		s := ai.NewState(nodes, players, rnd.Int63())
		for s.MoveType != state.End {
			switch s.MoveType {
			case state.Negotation:
				s = s.Negotation(ai.AIsNegotation(ais, s))
			case state.Moves:
				moves := ai.AIsMoves(ais, s)

				if s.Players[0].Playing {
					data = append(data,
						point{
							X:         ai.EncodeNodes24(s),
							EdgeIndex: ai.EncodeEdges(s),
							Action:    ai.GetDesiredFromMoveMatrix(moves[0]),
						},
					)
				}

				s = s.Move(moves)
			case state.Territory:
				s = s.Territory(ai.AIsTerritory(ais, s))
			case state.Soldiers:
				s = s.Soldiers(ai.AIsSoldiers(ais, s))
			}
			counter--
			if counter == 0 {
				fmt.Println("safeguard")
				break
			}
		}
	}

	file, err := os.Create(path)
	if err != nil {
		fmt.Println("Error creating file:", err)
		return
	}
	defer file.Close()

	encoder := json.NewEncoder(file)
	err = encoder.Encode(data)
	if err != nil {
		fmt.Println("Error encoding JSON:", err)
		return
	}
}
