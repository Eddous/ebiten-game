package main

import (
	"encoding/json"
	"fmt"
	"gra/core1/ai"
	"gra/core1/state"
	"gra/coreutils"
	"os"
	"time"

	"math/rand"
)

// for generating datasets without playout
// this is worse approach

type point struct {
	X         [][]float64
	EdgeIndex [2][]int
	Result    [6]float64 // 0 - lost, 1 - win
}

func encodeResult(s *state.State) [6]float64 {
	ret := [6]float64{}
	for pid, p := range s.Players {
		if p.DeathInfo.Result == coreutils.Win {
			ret[pid] = 1
		}
	}
	return ret
}

func main() {
	const size = 1000
	const path = "/tmp/gra_dataset_classifier.json"
	const nodes = 20
	const limit = 1000 // if the level is to small, this has to be set

	seed := time.Now().Unix()
	rnd := rand.New(rand.NewSource(int64(seed)))

	// creating AIs
	ais := []*ai.AI{
		ai.AIBattery[2](),
		ai.AIBattery[2](),
		ai.AIBattery[2](),
	}
	for pid, ai := range ais {
		ai.Init(pid, rnd.Int63())
	}

	data := make([]point, 0)
	for i := range size {
		fmt.Println(i)

		s := ai.NewState(nodes, len(ais), rnd.Int63())
		tmpData := make([]point, 0)
		counter := limit
		for s.MoveType != state.End && counter != 0 {

			switch s.MoveType {
			case state.Negotation:
				moves := map[int][]int{}
				for _, pid := range s.Playing() {
					moves[pid] = ais[pid].Negotation(s)
				}
				s = s.Negotation(moves)
			case state.Moves:
				tmpData = append(tmpData,
					point{
						X:         ai.EncodeNodesMoves(s),
						EdgeIndex: ai.EncodeEdges(s),
					},
				)
				moves := map[int][][]int{}
				for _, pid := range s.Playing() {
					moves[pid] = ais[pid].Moves(s)
				}
				s = s.Move(moves)
			case state.Territory:
				moves := map[int]int{}
				for _, pid := range s.Playing() {
					moves[pid] = ais[pid].Territory(s)
				}
				s = s.Territory(moves)
			case state.Soldiers:
				moves := map[int][]int{}
				for _, pid := range s.Playing() {
					moves[pid] = ais[pid].Soldiers(s)
				}
				s = s.Soldiers(moves)
			}
			counter--
		}

		if counter == 0 {
			fmt.Println("safeguard!")
			continue
		}
		res := encodeResult(s)
		for _, d := range tmpData {
			d.Result = res
			data = append(data, d)
		}
	}

	file, err := os.Create(path)
	if err != nil {
		fmt.Println("Error creating file:", err)
		return
	}
	defer file.Close()

	encoder := json.NewEncoder(file)
	err = encoder.Encode(data)
	if err != nil {
		fmt.Println("Error encoding JSON:", err)
		return
	}
}
