package main

import (
	"encoding/json"
	"fmt"
	"gra/core1/ai"
	"gra/core1/state"
	"gra/coreutils"
	"gra/utils"
	"math/rand"
	"net"
)

// for reinforcement learning
// the external script will decide when to stop

func rewards(oldStates map[int]*state.State, currState *state.State, action, actionUsed map[int][]float64) map[int]float64 {
	ret := map[int]float64{}
	for pid := range oldStates {
		ret[pid] = ai.GetReward13(oldStates[pid], currState, action[pid], actionUsed[pid], pid)
	}
	return ret
}

func encodeNodes(s *state.State) [][]float64 {
	return ai.EncodeNodes24(s)
}

func main() {
	const nodes = 20
	const players = 3
	const address = "localhost:54775"
	const savedGamesDir = "/tmp/gra_savedgames/"
	const limit = 300
	progress := 1
	clasicalAI := ai.AIBattery[0]()

	// starting the server
	listener, err := net.Listen("tcp", address)
	utils.CheckErr(err)
	fmt.Println("mode:", nodes, " players:", players)

	conn, err := listener.Accept()
	utils.CheckErr(err)

	decoder := json.NewDecoder(conn)
	encoder := json.NewEncoder(conn)

	for i := 0; ; i++ {
		in := getMsg(decoder)
		if in.T == 2 {
			break
		}

		seed := in.Seed
		classicalAINoise := in.Noise
		selfPlay := in.SelfPlay
		history := []map[int]string{}
		s := ai.NewState(nodes, players, in.Seed)

		// creating AIs
		clasicalAI.Init(-1, seed)
		rnd := rand.New(rand.NewSource(seed))

		// these two serve for feedback calculation
		// oldStates can differ player to player because the rewards are calculated from (lastStateThePlayerPlayed, currentState)
		oldStates := map[int]*state.State{}
		decodedMoves := map[int][]float64{}
		sentActions := map[int][]float64{}

		for s.MoveType != state.End {
			switch s.MoveType {
			case state.Negotation:
				moves := clasicalAI.NegotationForAll(s)
				s = s.Negotation(moves)
				history = append(history, coreutils.ToStrBatch(moves, coreutils.NidsToStr))
			case state.Moves:
				external, internal := getPlayingExternalInternal(s, selfPlay)
				moves := getMovesForInternal(s, rnd, internal, classicalAINoise, clasicalAI)
				if len(external) != 0 {
					externalMoves := communication(s, external, oldStates, sentActions, decodedMoves, rnd, encoder, decoder)

					// loading the moves
					for pid, move := range externalMoves {
						moves[pid] = move
					}
				}
				s = s.Move(moves)
				history = append(history, coreutils.ToStrBatch(moves, coreutils.MovesToStr))
			case state.Territory:
				moves := clasicalAI.TerritoryForAll(s)
				s = s.Territory(moves)
				history = append(history, coreutils.ToStrBatch(moves, coreutils.NidToStr))
			case state.Soldiers:
				moves := clasicalAI.SoldiersForAll(s)
				s = s.Soldiers(moves)
				history = append(history, coreutils.ToStrBatch(moves, coreutils.NidsToStr))
			}
			if len(history) == limit {
				// the Kill is not serialized, but whatever.
				s = s.Kill(s.AlivePlayers(), "Reached Limit")
			}
		}

		// sending the result
		msg := outMsg{
			T:           2,
			State:       4,
			X:           encodeNodes(s),                                   // dummy, will be never used!! but it is important to keep the dimensions
			ActionsUsed: decodedMoves,                                     // all not yet used decodedMoves
			Rewards:     rewards(oldStates, s, sentActions, decodedMoves), // all not yet used oldStates

			Done:   true,
			Scores: ai.EncodeScores(s),
		}
		sendMsg(encoder, msg)

		// saving the game
		if progress != 0 && i%int(progress) == 0 {
			// commented out because of the dependency to corewrapper -> core1/board (core2/board) -> ebiten
			/*
				utils.CheckDirectory(savedGamesDir)
				bytes := corewrapper.SerializeFromMoves(corewrapper.CityLess, mode, seed, players, history)
				fileName := strconv.Itoa(utils.NextFileNumber(savedGamesDir))
				utils.WriteBytesToFile(savedGamesDir+"/"+fileName, bytes)
			*/
		}

	}
	utils.CheckErr(conn.Close())
}

func getPlayingExternalInternal(s *state.State, selfPlay bool) ([]int, []int) {
	if selfPlay {
		return s.Playing(), []int{}
	}

	external := []int{}
	internal := []int{}
	for _, pid := range s.Playing() {
		if pid == 0 {
			external = append(external, pid)
		} else {
			internal = append(internal, pid)
		}
	}
	return external, internal
}

func getRandomActionsForExternal(s *state.State, rnd *rand.Rand, external []int) map[int][]float64 {
	ret := map[int][]float64{}
	actions := ai.RandomMovesForAll(s, rnd)
	for _, pid := range external {
		ret[pid] = ai.GetDesiredFromMoveMatrix(actions[pid])
	}
	return ret
}

func getMovesForInternal(s *state.State, rnd *rand.Rand, internal []int, noise float64, ai *ai.AI) map[int][][]int {
	ret := map[int][][]int{}
	for _, pid := range internal {
		ret[pid] = insertNoise(rnd, ai.Mov(s, rnd, pid), s.Nodes, noise)
	}
	return ret
}

/*
Python	|	Go
----------------
Init	>
		<	State (without feedback)
Move	>
		<	State (with feedback)
Move	>

		.

Move	>
		<	Result (with feedback)
Init	>

		.

		<	Result (with feedback)
Close	>
*/

type outMsg struct {
	// 0: state, 1: state with feedback, 2: result
	T int

	// 0 - send only once
	EdgeIndex [2][]int

	// 0, 1
	PlayingPlayers []int             // list of playing pids
	RandomActions  map[int][]float64 // this is for exploration

	// state - 0, 1, 2
	// the end state is also passed even though it is not used
	State int         // 0: Negotation (not used), 1: Moves, 2: Territory, 3: Soldiers, 4: End
	X     [][]float64 // [nodeIndex][featureIndex]

	// feedback - 1, 2
	Rewards     map[int]float64
	ActionsUsed map[int][]float64

	// result - 2
	Done   bool // this is encoded already twice (existence of scores, and T==2)
	Scores map[int]string
}

type inMsg struct {
	// 0: init, 1: action, 2: close env
	T int

	// init
	Seed     int64
	SelfPlay bool
	Noise    float64

	// action (depends on the game state) (raw values for preprocessing)
	Moves      map[int][]float64 // the desired number of soldiers
	Negotation map[int][]float64 // score for each node, postprocessing will make a sum and deside which component to take
	Soldiers   map[int][]float64
}

func sendMsg(encoder *json.Encoder, msg any) {
	utils.CheckErr(encoder.Encode(msg))
}

func getMsg(decoder *json.Decoder) inMsg {
	in := inMsg{}
	utils.CheckErr(decoder.Decode(&in))
	return in
}

func communication(
	curr *state.State,
	externalPlaying []int,
	oldStates map[int]*state.State,
	sentActions map[int][]float64,
	decodedMoves map[int][]float64,
	rnd *rand.Rand,
	encoder *json.Encoder,
	decoder *json.Decoder,
) map[int][][]int {
	randomActions := getRandomActionsForExternal(curr, rnd, externalPlaying)

	var msg outMsg
	if len(oldStates) == 0 {
		// this should happen only at the begining of the game
		msg = outMsg{
			T:              0,
			EdgeIndex:      ai.EncodeEdges(curr),
			PlayingPlayers: externalPlaying,
			State:          1,
			X:              encodeNodes(curr),
			RandomActions:  randomActions,
		}
	} else {
		oldStatesForPlaying := map[int]*state.State{}
		decodedMovesForPlaying := map[int][]float64{}
		sentActionForPlaying := map[int][]float64{}
		for _, pid := range externalPlaying {
			oldStatesForPlaying[pid] = oldStates[pid]
			decodedMovesForPlaying[pid] = decodedMoves[pid]
			sentActionForPlaying[pid] = sentActions[pid]

			// this is unnecessary, since it is always replaced for a new buffer
			delete(oldStates, pid)
			delete(decodedMoves, pid)
			delete(sentActions, pid)
		}
		msg = outMsg{
			T:              1,
			PlayingPlayers: externalPlaying,
			State:          1,
			X:              encodeNodes(curr),
			RandomActions:  randomActions,
			ActionsUsed:    decodedMovesForPlaying,
			Rewards:        rewards(oldStatesForPlaying, curr, sentActionForPlaying, decodedMovesForPlaying),
		}
	}
	sendMsg(encoder, msg)

	in := getMsg(decoder)

	externalMoves := map[int][][]int{}
	for pid := range in.Moves {
		move, usedMove, _ := ai.DecodeMove(curr, pid, in.Moves[pid], ai.DecodeMovesArgs{Iters: 50, DestroyRate: 0.5, RecordToRecord: 1.00}, rnd.Int63())
		externalMoves[pid] = move
		oldStates[pid] = curr
		sentActions[pid] = in.Moves[pid]
		decodedMoves[pid] = usedMove
	}
	return externalMoves
}

func insertNoise(rnd *rand.Rand, move [][]int, nodes []state.Node, noise float64) [][]int {
	ret := coreutils.NewMoveMatrix(len(move))
	for i, node := range nodes {
		for j, soldiers := range move[i] {
			for range soldiers {
				if rnd.Float64() < noise {
					newJ := utils.Sample(rnd, node.Neighbourhood)
					ret[i][newJ]++
				} else {
					ret[i][j]++
				}
			}
		}
	}
	return ret
}
