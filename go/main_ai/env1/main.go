package main

// ai environment for testing learned evaluation function

import (
	"encoding/json"
	"fmt"
	ai "gra/core1/ai"
	"gra/core1/state"
	"gra/coreutils"
	"gra/corewrapper"
	"gra/utils"
	"net"
	"strconv"
	"time"
)

// for testing the trained classifier

type outMsg struct {
	EdgeIndex [2][]int
	X         [][]float64
}

func main() {
	const nodes = 20
	const address = "localhost:54775"
	const savedGamesDir = "/tmp/gra_savedgames/"
	const trials = 100
	progress := 0
	seed := time.Now().Unix()

	utils.CheckDirectory(savedGamesDir)

	// starting the server
	listener, err := net.Listen("tcp", address)
	utils.CheckErr(err)
	fmt.Println("ready")

	conn, err := listener.Accept()
	utils.CheckErr(err)
	defer conn.Close()

	encoder := json.NewEncoder(conn)
	decoder := json.NewDecoder(conn)

	ais := []*ai.AI{
		{
			Pid: -1,
			Neg: ai.RandomNegotation,
			Mov: ai.BeanMoves1(2, ai.LocalSearch, ai.EvalWrapper2(getExternalEval(encoder, decoder)), ai.LinearNormalization),
			Ter: ai.DefaultTerritory2(1),
			Sol: ai.DefaultSoldiers(1),
		},
		ai.AIZoo[1](),
	}

	score := make([]float64, len(ais))
	for i := 1; i < trials+1; i++ {
		fmt.Printf("iter %d | seed %019d", i, seed)
		result, history := ai.RunAIExperiment(seed, nodes, ais, 200)
		fmt.Println(" | result", result)
		for j := range result {
			if result[j] == coreutils.Win {
				score[j]++
			}
		}
		if progress != 0 && i%progress == 0 {
			bytes := corewrapper.SerializeFromMoves(corewrapper.Classical, nodes, seed, len(ais), history)
			utils.WriteBytesToFile(savedGamesDir+"/"+strconv.Itoa(i), bytes)
		}
		seed = utils.NextSeed(seed)
	}
	printScore(score, trials)
}

func printScore(score []float64, num int) {
	for j := range score {
		fmt.Println(j, ":", score[j]/float64(num))
	}
}

func getExternalEval(encoder *json.Encoder, decoder *json.Decoder) func(s *state.State) []float64 {
	return func(s *state.State) []float64 {
		err := encoder.Encode(
			outMsg{
				X: ai.EncodeNodesMoves(s),
				// X:         ai.EncodeNodes24(s),
				EdgeIndex: ai.EncodeEdges(s),
			},
		)
		utils.CheckErr(err)

		ret := [6]float64{}
		err = decoder.Decode(&ret)
		utils.CheckErr(err)
		return ret[:len(s.Players)]
	}
}
