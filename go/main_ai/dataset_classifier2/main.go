package main

import (
	"encoding/json"
	"fmt"
	"gra/core1/ai"
	"gra/core1/state"
	"gra/coreutils"
	"os"
	"time"

	"math/rand"
)

// for generating datasets with playout

type point struct {
	X         [][]float64
	EdgeIndex [2][]int
	Result    [6]float64 // 0 - lost, 1 - win
}

func main() {
	const size = 100
	const path = "/tmp/dataset.json"
	const games = 30
	const nodes = 20
	const limit = 1000 // if the level is to small, this has to be set
	const onlyMoves = true

	seed := time.Now().Unix()
	rnd := rand.New(rand.NewSource(int64(seed)))

	// creating AIs
	ais := []*ai.AI{
		ai.AIZoo[1](),
		ai.AIZoo[1](),
	}
	for pid, ai := range ais {
		ai.Init(pid, rnd.Int63())
	}

	data := make([]point, 0)
	for i := range size {
		fmt.Println(i)

		s := ai.NewState(nodes, len(ais), rnd.Int63())
		for s.MoveType != state.End {

			switch s.MoveType {
			case state.Negotation:
				moves := map[int][]int{}
				for _, pid := range s.Playing() {
					moves[pid] = ais[pid].Negotation(s)
				}
				s = s.Negotation(moves)
			case state.Moves:
				moves := map[int][][]int{}
				for _, pid := range s.Playing() {
					moves[pid] = ais[pid].Moves(s)
				}
				s = s.Move(moves)
			case state.Territory:
				moves := map[int]int{}
				for _, pid := range s.Playing() {
					moves[pid] = ais[pid].Territory(s)
				}
				s = s.Territory(moves)
			case state.Soldiers:
				moves := map[int][]int{}
				for _, pid := range s.Playing() {
					moves[pid] = ais[pid].Soldiers(s)
				}
				s = s.Soldiers(moves)
			}

			if onlyMoves && s.MoveType != state.Moves {
				continue
			}

			// if any playout will happen to be infinite, then jump out from this game
			// this is not ideal, but at larger levels it does not happen often

			ok, result := playout(s, ais, games, limit)
			if !ok {
				fmt.Println("safeguard!")
				break
			}

			var encoded [][]float64
			if onlyMoves {
				encoded = ai.EncodeNodesMoves(s)
			} else {
				encoded = ai.EncodeNodes24(s)
			}

			data = append(data,
				point{
					X:         encoded,
					EdgeIndex: ai.EncodeEdges(s),
					Result:    result,
				},
			)
		}
	}

	file, err := os.Create(path)
	if err != nil {
		fmt.Println("Error creating file:", err)
		return
	}
	defer file.Close()

	encoder := json.NewEncoder(file)
	err = encoder.Encode(data)
	if err != nil {
		fmt.Println("Error encoding JSON:", err)
		return
	}
}

func encodeResult(s *state.State) [6]float64 {
	ret := [6]float64{}
	for pid, p := range s.Players {
		if p.DeathInfo.Result == coreutils.Win {
			ret[pid] = 1
		}
	}
	return ret
}

func playout(s0 *state.State, ais []*ai.AI, games, limit int) (bool, [6]float64) {
	res := [6]float64{}
	for range games {
		s := s0
		c := 0
		for s.MoveType != state.End {
			switch s.MoveType {
			case state.Negotation:
				moves := map[int][]int{}
				for _, pid := range s.Playing() {
					moves[pid] = ais[pid].Negotation(s)
				}
				s = s.Negotation(moves)
			case state.Moves:
				moves := map[int][][]int{}
				for _, pid := range s.Playing() {
					moves[pid] = ais[pid].Moves(s)
				}
				s = s.Move(moves)
			case state.Territory:
				moves := map[int]int{}
				for _, pid := range s.Playing() {
					moves[pid] = ais[pid].Territory(s)
				}
				s = s.Territory(moves)
			case state.Soldiers:
				moves := map[int][]int{}
				for _, pid := range s.Playing() {
					moves[pid] = ais[pid].Soldiers(s)
				}
				s = s.Soldiers(moves)
			}

			// safeguard
			c++
			if c > limit {
				return false, [6]float64{}
			}
		}
		for pid, v := range encodeResult(s) {
			res[pid] += v
		}
	}
	for pid := range res {
		res[pid] = res[pid] / float64(games)
	}
	return true, res
}
