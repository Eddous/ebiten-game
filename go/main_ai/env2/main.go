package main

import (
	"encoding/json"
	"fmt"
	"gra/core1/ai"
	"gra/core1/state"
	"gra/coreutils"
	"gra/corewrapper"
	"gra/utils"
	"net"
	"strconv"
	"time"
)

/*
AI environment for learning evaluation function via genetic algorithm
*/

type outMsg struct {
	// 0: observation, 1: result
	T int

	// observation
	EdgeIndex [2][]int
	X         [][]float64 // [nodeIndex][featureIndex]

	// result
	Score float64
}

func main() {
	const nodes = 20
	const address = "localhost:54775"
	const savedGamesDir = "/tmp/gra_savedgames/"
	seed := time.Now().Unix()
	progress := 0
	maxMoves := 500

	utils.CheckDirectory(savedGamesDir)

	// starting the server
	listener, err := net.Listen("tcp", address)
	utils.CheckErr(err)
	fmt.Println("ready")

	for i := 0; ; i++ {
		conn, err := listener.Accept()
		utils.CheckErr(err)
		go game(i, conn, nodes, seed, progress, maxMoves, savedGamesDir)
		seed = utils.NextSeed(seed)
	}
}

func game(i int, conn net.Conn, nodes int, seed int64, progress int, maxMoves int, path string) {
	encoder := json.NewEncoder(conn)
	decoder := json.NewDecoder(conn)
	externalEval := func(s *state.State) []float64 {
		err := encoder.Encode(
			outMsg{
				T:         0,
				X:         ai.EncodeNodes24(s),
				EdgeIndex: ai.EncodeEdges(s),
			},
		)
		utils.CheckErr(err)

		ret := [6]float64{}
		err = decoder.Decode(&ret)
		utils.CheckErr(err)
		return ret[:len(s.Players)]
	}
	ais := []*ai.AI{
		{
			Pid: -1,
			Neg: ai.RandomNegotation,
			Mov: ai.BeanMoves1(2, ai.LocalSearch, ai.EvalWrapper(externalEval), ai.LinearNormalization),
			Ter: ai.DefaultTerritory2(1),
			Sol: ai.DefaultSoldiers(1),
		},
		ai.AIBattery[0](),
	}

	result, history := ai.RunAIExperiment(seed, nodes, ais, maxMoves)
	fmt.Println(i, result)

	var score int
	if result[0] == coreutils.Win {
		score = maxMoves - len(history)
	} else if result[0] == coreutils.Lost {
		score = -maxMoves + len(history)
	}

	utils.CheckErr(
		encoder.Encode(
			outMsg{
				T:     1,
				Score: float64(score),
			},
		),
	)
	conn.Close()

	if progress != 0 && i%progress == 0 {
		bytes := corewrapper.SerializeFromMoves(corewrapper.Classical, nodes, seed, len(ais), history)
		utils.WriteBytesToFile(path+"/"+strconv.Itoa(i), bytes)
	}
}
