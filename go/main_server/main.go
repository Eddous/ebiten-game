package main

import (
	"encoding/json"
	"flag"
	"gra/corewrapper"
	"gra/server"
	"log"
	"os"
	"path/filepath"
	"runtime"
)

func main() {
	_, filename, _, ok := runtime.Caller(0)
	if !ok {
		panic("something wrong")
	}

	configPathFlag := flag.String("config", filepath.Dir(filename)+"/config.json", "path to a config file")
	flag.Parse()

	configFile, err := os.Open(*configPathFlag)
	if err != nil {
		log.Fatal(err)
	}
	jsonParser := json.NewDecoder(configFile)
	config := server.Config{}
	if err = jsonParser.Decode(&config); err != nil {
		log.Fatal(err)
	}
	server.RunServer(config, corewrapper.Factory{})
}
