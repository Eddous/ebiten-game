package main

import (
	"encoding/json"
	"flag"
	"gra/corewrapper"
	"gra/server"
	"log"
	"os"
)

func main() {
	configPathFlag := flag.String("config", "", "path to a config file")
	flag.Parse()
	if *configPathFlag == "" {
		log.Fatalln("Server needs config file")
	}
	configFile, err := os.Open(*configPathFlag)
	if err != nil {
		log.Fatal(err)
	}
	jsonParser := json.NewDecoder(configFile)
	config := server.Config{}
	if err = jsonParser.Decode(&config); err != nil {
		log.Fatal(err)
	}
	server.RunServer(config, corewrapper.Factory{})
}
