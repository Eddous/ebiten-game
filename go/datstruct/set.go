package datstruct

type Set[K comparable] struct {
	m map[K]struct{}
}

func NewSet[K comparable]() Set[K] {
	return Set[K]{
		m: make(map[K]struct{}),
	}
}

func NewSetFromSlice[K comparable](slice []K) Set[K] {
	s := NewSet[K]()
	for _, k := range slice {
		s.Add(k)
	}
	return s
}

func (s Set[K]) Add(val K) {
	s.m[val] = struct{}{}
}

func (s Set[K]) Has(val K) bool {
	_, ok := s.m[val]
	return ok
}

func (s Set[K]) Remove(val K) {
	delete(s.m, val)
}

func (s Set[K]) Size() int {
	return len(s.m)
}

func (s1 Set[K]) Minus(s2 Set[K]) Set[K] {
	ret := NewSet[K]()
	for k := range s1.m {
		if !s2.Has(k) {
			ret.Add(k)
		}
	}
	return ret
}

func (s1 Set[K]) Union(s2 Set[K]) Set[K] {
	ret := NewSet[K]()
	for k := range s1.m {
		ret.Add(k)
	}
	for k := range s2.m {
		ret.Add(k)
	}
	return ret
}

func (s Set[K]) Iterator() <-chan K {
	ch := make(chan K)
	go func() {
		for k := range s.m {
			ch <- k
		}
		close(ch)
	}()
	return ch
}

func (s Set[K]) Copy() Set[K] {
	new := NewSet[K]()
	for k := range s.m {
		new.Add(k)
	}
	return new
}

func (s Set[K]) ToSlice() []K {
	ret := make([]K, 0)
	for item := range s.m {
		ret = append(ret, item)
	}
	return ret
}

// if set contains only one element, this will get it
func (s Set[K]) GetSingleton() K {
	if s.Size() != 1 {
		panic("wrong size for getting singleton")
	}
	for e := range s.m {
		return e
	}
	panic("dont happen")
}
