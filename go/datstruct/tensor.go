package datstruct

type NDArray[T any] struct {
	Data []T
	Dims []int

	// for calculating indexes
	rightProduct []int
}

func NewNDArray[T any](dims []int) NDArray[T] {
	rightProduct := make([]int, len(dims)+1)
	rightProduct[len(dims)] = 1
	for i := len(dims) - 1; i >= 0; i-- {
		if dims[i] <= 0 {
			panic("cannot have negative dimension")
		}
		rightProduct[i] = rightProduct[i+1] * dims[i]
	}
	return NDArray[T]{
		Data:         make([]T, rightProduct[0]),
		Dims:         dims,
		rightProduct: rightProduct,
	}
}

func (a NDArray[T]) Set(coords []int, value T) {
	i := a.getLinearIndex(coords)
	a.Data[i] = value
}

func (a NDArray[T]) Get(coords []int) T {
	return a.Data[a.getLinearIndex(coords)]
}

func (a NDArray[T]) getLinearIndex(coords []int) int {
	if len(coords) != len(a.Dims) {
		panic("wrong dimensionality of coords")
	}
	pos := 0
	for i, c := range coords {
		if c < 0 {
			panic("negative coordinate")
		}
		pos += c * a.rightProduct[i+1]
	}
	return pos
}
