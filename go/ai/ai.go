package ai

import (
	"gra/utils"
	"maps"
	"math/rand"
	"slices"
)

type NodeActionTuple[NodeAction any] struct {
	Nid        int
	NodeAction NodeAction
}

func randomActions[State, NodeAction any](si StateInterface[State, NodeAction], s State, rnd *rand.Rand) map[int][]NodeActionTuple[NodeAction] {
	ret := map[int][]NodeActionTuple[NodeAction]{}
	for _, pid := range si.Playing(s) {
		ret[pid] = si.RandomActions(s, pid, rnd)
	}
	return ret
}

func Bean1[State any, NodeAction any](
	s StateInterface[State, NodeAction],
	passes int,
	search Search[NodeAction],
	evaluate func(State) []float64,
) func(state State, pid int, rnd *rand.Rand) []NodeActionTuple[NodeAction] {
	return func(state State, pid int, rnd *rand.Rand) []NodeActionTuple[NodeAction] {
		return bean1(s, passes, search, evaluate, state, pid, rnd)
	}
}

func bean1[State any, NodeAction any](
	si StateInterface[State, NodeAction],
	passes int,
	search Search[NodeAction],
	evaluate func(State) []float64,

	s State,
	pid int,
	rnd *rand.Rand,
) []NodeActionTuple[NodeAction] {
	actions := bean1ForAll(si, passes, search, evaluate, s, rnd)
	return actions[pid]
}

func bean1ForAll[State, NodeAction any](
	si StateInterface[State, NodeAction],
	passes int,
	search Search[NodeAction],
	evaluate func(State) []float64,

	s State,
	rnd *rand.Rand,
) map[int][]NodeActionTuple[NodeAction] {
	actions := randomActions(si, s, rnd)
	pids := si.Playing(s)
	for range passes {
		utils.Shuffle(rnd, pids)
		for _, pid := range pids {
			actions[pid] = search(
				searchFunctions[NodeAction]{
					randomActions:       randomActionsFunc(si, s, pid),
					neighbourhood:       neighbourhoodFunc(si, s),
					globalNeighbourhood: globalNeighbourhoodFunc(si, s),
					measure:             bean1Measure(si, evaluate, s, actions, pid),
				},
				slices.Clone(actions[pid]),
				rnd,
			)
		}
	}
	return actions
}

func bean1Measure[State, NodeAction any](
	si StateInterface[State, NodeAction],
	evaluate func(State) []float64,
	s State,
	actions map[int][]NodeActionTuple[NodeAction],
	pid int,
) func([]NodeActionTuple[NodeAction]) float64 {
	return func(testedAction []NodeActionTuple[NodeAction]) float64 {
		orig := actions[pid]
		actions[pid] = testedAction
		score := evaluate(si.Step(s, actions))[pid]
		actions[pid] = orig
		return score
	}
}

func Bean3[State any, NodeAction any](
	s StateInterface[State, NodeAction],
	passes int,
	search Search[NodeAction],
	evaluate func(State) []float64,
) func(state State, pid int, rnd *rand.Rand) []NodeActionTuple[NodeAction] {
	return func(state State, pid int, rnd *rand.Rand) []NodeActionTuple[NodeAction] {
		return bean3(s, passes, search, evaluate, state, pid, rnd)
	}
}

func bean3[State any, NodeAction any](
	si StateInterface[State, NodeAction],
	passes int,
	search Search[NodeAction],
	evaluate func(State) []float64,

	s State,
	pid int,
	rnd *rand.Rand,
) []NodeActionTuple[NodeAction] {
	actions := bean3ForAll(si, passes, search, evaluate, s, rnd)
	return actions[pid]
}

func bean3ForAll[State, NodeAction any](
	si StateInterface[State, NodeAction],
	passes int,
	search Search[NodeAction],
	evaluate func(State) []float64,

	s State,
	rnd *rand.Rand,
) map[int][]NodeActionTuple[NodeAction] {
	actions := []map[int][]NodeActionTuple[NodeAction]{bean1ForAll(si, 2, search, evaluate, s, rnd)}
	pids := si.Playing(s)

	for i := 1; i < passes; i++ {
		actions = append(actions, maps.Clone(actions[i-1]))
		utils.Shuffle(rnd, pids)
		for _, pid := range pids {
			actions[i][pid] = search(
				searchFunctions[NodeAction]{
					randomActions:       randomActionsFunc(si, s, pid),
					neighbourhood:       neighbourhoodFunc(si, s),
					globalNeighbourhood: globalNeighbourhoodFunc(si, s),
					measure:             bean3Measure(si, evaluate, s, actions, pid),
				},
				slices.Clone(actions[i][pid]),
				rnd,
			)
		}
	}
	return actions[passes-1]
}

func bean3Measure[State, NodeAction any](
	si StateInterface[State, NodeAction],
	evaluate func(State) []float64,
	s State,
	actionss []map[int][]NodeActionTuple[NodeAction],
	pid int,
) func([]NodeActionTuple[NodeAction]) float64 {
	return func(testedAction []NodeActionTuple[NodeAction]) float64 {
		sum := 0.
		for _, actions := range actionss {
			orig := actions[pid]
			actions[pid] = testedAction
			sum += evaluate(si.Step(s, actions))[pid]
			actions[pid] = orig
		}
		return sum / float64(len(actionss))
	}
}
