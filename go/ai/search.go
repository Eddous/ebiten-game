package ai

import (
	"gra/utils"
	"math/rand"
)

func randomActionsFunc[State, NodeAction any](
	si StateInterface[State, NodeAction],
	s State,
	pid int,
) func(rnd *rand.Rand) []NodeActionTuple[NodeAction] {
	return func(rnd *rand.Rand) []NodeActionTuple[NodeAction] {
		return si.RandomActions(s, pid, rnd)
	}
}

func neighbourhoodFunc[State, NodeAction any](
	si StateInterface[State, NodeAction],
	s State,
) func(int, NodeAction) []NodeAction {
	return func(nid int, na NodeAction) []NodeAction {
		return si.Neighbourhood(s, nid, na)
	}
}

func globalNeighbourhoodFunc[State, NodeAction any](
	si StateInterface[State, NodeAction],
	s State,
) func([]NodeActionTuple[NodeAction]) [][]NodeActionTuple[NodeAction] {
	return func(all []NodeActionTuple[NodeAction]) [][]NodeActionTuple[NodeAction] {
		return si.GlobalNeighbourhood(s, all)
	}
}

type searchFunctions[NodeAction any] struct {
	// these two are always the same
	randomActions       func(rnd *rand.Rand) []NodeActionTuple[NodeAction]
	neighbourhood       func(int, NodeAction) []NodeAction
	globalNeighbourhood func([]NodeActionTuple[NodeAction]) [][]NodeActionTuple[NodeAction] // this assumes that the input contains all possible nodes

	// this differ
	measure func([]NodeActionTuple[NodeAction]) float64
}

type Search[NodeAction any] func(searchFunctions[NodeAction], []NodeActionTuple[NodeAction], *rand.Rand) []NodeActionTuple[NodeAction]

// nodeActions are all nodeActions the player can make
func LocalSearch[NodeAction any](fn searchFunctions[NodeAction], nodeActions []NodeActionTuple[NodeAction], rnd *rand.Rand) []NodeActionTuple[NodeAction] {
	bestVal := fn.measure(nodeActions)
	utils.Shuffle(rnd, nodeActions)
	for i := range nodeActions {
		foundBetter := true
		for foundBetter {
			foundBetter = false
			neighbourhood := fn.neighbourhood(nodeActions[i].Nid, nodeActions[i].NodeAction)
			utils.Shuffle(rnd, neighbourhood)

			best := nodeActions[i].NodeAction
			for _, action := range neighbourhood {
				nodeActions[i].NodeAction = action
				if curr := fn.measure(nodeActions); curr > bestVal {
					bestVal = curr
					best = action
					foundBetter = true
				}
			}
			nodeActions[i].NodeAction = best
		}
	}

	foundBetter := true
	for foundBetter {
		foundBetter = false
		neighbourhood := fn.globalNeighbourhood(nodeActions)
		utils.Shuffle(rnd, neighbourhood)

		for _, candidate := range neighbourhood {
			if curr := fn.measure(candidate); curr > bestVal {
				bestVal = curr
				nodeActions = candidate
				foundBetter = true
			}
		}
	}
	return nodeActions
}
