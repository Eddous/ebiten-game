package ai

import "math/rand"

// it is struct to keep implementation of the interface in ai package
type StateInterface[State any, NodeAction any] struct {
	Playing func(State) []int

	RandomActions       func(State, int, *rand.Rand) []NodeActionTuple[NodeAction]
	Neighbourhood       func(State, int, NodeAction) []NodeAction
	GlobalNeighbourhood func(State, []NodeActionTuple[NodeAction]) [][]NodeActionTuple[NodeAction]

	Step func(State, map[int][]NodeActionTuple[NodeAction]) State
}
