package graphgen

import (
	"gra/utils"
	"math/rand"
)

func GenerateGenerate(seed int64, mode int) Data {
	genArgs, ok := GraphgenArgs(seed, mode)
	if !ok {
		panic("wrong arguments")
	}
	gameMap, ok2 := Generate(seed, genArgs)
	if !ok2 {
		panic("wrong arguments!")
	}
	return gameMap
}

func GraphgenArgs(seed int64, mode int) (GenArgs, bool) {
	rnd := rand.New(rand.NewSource(seed))

	var nodes int
	var edges float64
	var crossEdges float64
	var radius float64

	minNE := 1.
	minNN := 2.
	maxNN := 3.
	maxBE := 4.
	maxCE := 4.5

	switch mode {
	case 0:
		nodes = 20
		edges = utils.RandomFloat(rnd, 0.5, 0.9)
		crossEdges = utils.RandomFloat(rnd, 0, 0.2)
		radius = .05
	case 1:
		nodes = 45
		edges = utils.RandomFloat(rnd, 0.5, 0.9)
		crossEdges = utils.RandomFloat(rnd, 0, .2)
		radius = 0.5
	case 2:
		nodes = 80
		edges = utils.RandomFloat(rnd, 0.5, 0.9)
		crossEdges = utils.RandomFloat(rnd, 0, .1)
		radius = 1
	case 3:
		nodes = 125
		edges = utils.RandomFloat(rnd, 0.5, 0.9)
		crossEdges = utils.RandomFloat(rnd, 0, .1)
		radius = 1
	case 10:
		nodes = 10
		edges = utils.RandomFloat(rnd, 0.5, 0.9)
		crossEdges = utils.RandomFloat(rnd, 0, 0.2)
		radius = .01
	default:
		return GenArgs{}, false
	}
	return NewGenArgs(nodes, edges, crossEdges, minNE, minNN, maxNN, maxBE, maxCE, radius), true
}

/*
1. calculate the radius of the smallest circle r = sqrt(20/pi)
2. choose how much bigger the level should be
3. nodes = pi * (r * scale)^2

20, 45, 80, 125, 180, 245, 320, 405, 500... ()
*/
