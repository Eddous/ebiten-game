package graphgen

import (
	"gra/utils"
	"math/rand"
)

func GenerateGenerate(seed int64, nodes int) Data {
	genArgs, ok := GraphgenArgs(seed, nodes)
	if !ok {
		panic("wrong arguments")
	}
	gameMap, ok2 := Generate(seed, genArgs)
	if !ok2 {
		panic("wrong arguments!")
	}
	return gameMap
}

func GraphgenArgs(seed int64, nodes int) (GenArgs, bool) {
	if nodes < 6 {
		return GenArgs{}, false
	}

	rnd := rand.New(rand.NewSource(seed))

	minNE := 1.
	minNN := 2.
	maxNN := 3.
	maxBE := 4.
	maxCE := 4.5

	edges := utils.RandomFloat(rnd, 0.5, 0.8)
	crossEdges := utils.RandomFloat(rnd, 0, 0.2)

	radius := 0.00017535*float64(nodes*nodes) - 0.00170206*float64(nodes) + 0.01389961
	if radius > 1 {
		radius = 1
	}

	return NewGenArgs(nodes, edges, crossEdges, minNE, minNN, maxNN, maxBE, maxCE, radius), true
}

/*
1. calculate the radius of the smallest circle r = sqrt(20/pi)
2. choose how much bigger the level should be
3. nodes = pi * (r * scale)^2

20, 45, 80, 125, 180, 245, 320, 405, 500... ()
*/
