package graphgen

import (
	"fmt"
	"testing"
)

var defaultArgs = NewGenArgs(0, 0.6, 0.15, 1, 2, 3, 4, 4.5, 1)

func BenchmarkTest(b *testing.B) {
	for _, size := range []int{100, 200, 300, 400, 500, 600, 700, 800, 900, 1000} {
		b.Run(
			fmt.Sprintf("generate_%d", size),
			func(b *testing.B) {
				defaultArgs.NodesNum = size
				for i := 0; i < b.N; i++ {
					Generate(0, defaultArgs)
				}
			},
		)
	}
}
