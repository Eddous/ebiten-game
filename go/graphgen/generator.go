package graphgen

import (
	"fmt"
	"gra/utils"
	"math"
	"math/rand"
)

type edge struct {
	n1Index, n2Index int
	line             utils.Line
}

func newEdge(n1Index, n2Index int, n1, n2 utils.VecF) edge {
	return edge{
		line:    utils.NewLine(n1, n2),
		n1Index: n1Index,
		n2Index: n2Index,
	}
}

type Data struct {
	GenArgs
	Nodes               []utils.VecF
	Matrix              [][]bool
	MinimalPathDistance [][]int

	Radius float64
	BENum  int
	BEMax  int
	BCNum  int
	BCMax  int

	random *rand.Rand
	edges  []edge

	DegreeCentrality      []int
	ClosenessCentrality   []float64
	EigenVectorCentrality []float64
}

func Generate(seed int64, args GenArgs) (Data, bool) {
	if !args.Check() {
		return Data{}, false
	}

	data := Data{
		GenArgs: args,
		Nodes:   []utils.VecF{utils.NewVecF(0, 0)},
		edges:   make([]edge, 0),
		random:  rand.New(rand.NewSource(seed)),
	}

	data.Matrix = make([][]bool, data.NodesNum)
	data.MinimalPathDistance = make([][]int, data.NodesNum)
	for i := range data.Matrix {
		data.Matrix[i] = make([]bool, data.NodesNum)
		data.MinimalPathDistance[i] = make([]int, data.NodesNum)
	}

	data.generateSpanningTree()
	data.generateBonusEdges1()
	data.generateBonusEdges2()
	data.calculateMinimalPathDistance()

	data.calculateDegreeCentrality()
	data.calculateClosenessCentralities()
	data.calculateEigenVectorCentrality()

	return data, true
}

func (d *Data) addEdge(e edge) {
	d.edges = append(d.edges, e)
	d.Matrix[e.n1Index][e.n2Index] = true
	d.Matrix[e.n2Index][e.n1Index] = true
}

func (d *Data) generateSpanningTree() {
	for len(d.Nodes) != d.NodesNum {
		n1Index := d.random.Intn(len(d.Nodes))
		n2Index := len(d.Nodes)
		n1 := d.Nodes[n1Index]
		n2 := newPosition(n1, d.MinNN, d.MaxNN, d.random)

		if !d.nodeTooCloseToNodes(n2) || !d.nodeTooCloseToEdges(n2) {
			continue
		}
		edge := newEdge(n1Index, n2Index, n1, n2)
		if !d.edgeTooCloseToNodes(edge) || !edgeNotIntersectsEdge(d.edges, edge) {
			continue
		}

		newRadius := math.Sqrt(n2.X*n2.X + n2.Y*n2.Y)
		if newRadius > d.Radius {
			if d.random.Float64() < d.RadiusProb {
				d.Radius = newRadius
			} else {
				continue
			}

		}
		d.addEdge(edge)
		d.Nodes = append(d.Nodes, n2)
	}
}

func (d *Data) getAllPotentialEdges(maxLen float64) []edge {
	potential := make([]edge, 0)
	for n1Index := range d.Matrix {
		for n2Index := n1Index + 1; n2Index < len(d.Matrix); n2Index++ {
			n1, n2 := d.Nodes[n1Index], d.Nodes[n2Index]
			if d.Matrix[n1Index][n2Index] || n1.GetDistance(n2) > maxLen {
				continue
			}
			edge := newEdge(n1Index, n2Index, n1, n2)
			if !d.edgeTooCloseToNodes(edge) {
				continue
			}
			potential = append(potential, edge)
		}
	}
	d.random.Shuffle(len(potential), func(i, j int) { potential[i], potential[j] = potential[j], potential[i] })
	return potential
}

func (d *Data) generateBonusEdges1() {
	potential := d.getAllPotentialEdges(d.MaxBE)
	new := make([]edge, 0)
	for _, e := range potential {
		if edgeNotIntersectsEdge(new, e) && edgeNotIntersectsEdge(d.edges, e) {
			new = append(new, e)
		}
	}
	d.BEMax = len(new)
	d.BENum = d.addNewEdges(new, d.BonusE)
}

func (d *Data) generateBonusEdges2() {
	potential := d.getAllPotentialEdges(d.MaxCE)
	new := make([]edge, 0)
	for _, e := range potential {
		if !edgeNotIntersectsEdge(d.edges, e) {
			new = append(new, e)
		}
	}
	d.BCMax = len(new)
	d.BCNum = d.addNewEdges(new, d.CrossE)
}

func (d *Data) addNewEdges(new []edge, percentage float64) int {
	maxI := int(math.Ceil(float64(len(new)) * percentage))
	for i := 0; i < maxI; i++ {
		d.addEdge(new[i])
	}
	return maxI
}

func (d *Data) nodeTooCloseToNodes(new utils.VecF) bool {
	for _, n := range d.Nodes {
		if new.GetDistance(n) < d.MinNN {
			return false
		}
	}
	return true
}

func (d *Data) nodeTooCloseToEdges(new utils.VecF) bool {
	for _, e := range d.edges {
		if e.line.PointDist(new) < d.MinNE {
			return false
		}
	}
	return true
}

func (d *Data) edgeTooCloseToNodes(new edge) bool {
	for i, n := range d.Nodes {
		if i == new.n1Index || i == new.n2Index {
			continue
		}
		if new.line.PointDist(n) < d.MinNE {
			return false
		}
	}
	return true
}

func edgeNotIntersectsEdge(edges []edge, new edge) bool {
	for _, e := range edges {
		if new.n1Index == e.n1Index || new.n1Index == e.n2Index || new.n2Index == e.n1Index || new.n2Index == e.n2Index {
			continue
		}
		if e.line.IntersectLine(new.line) != nil {
			return false
		}
	}
	return true
}

func newPosition(pos utils.VecF, min, max float64, random *rand.Rand) utils.VecF {
	phi := random.Float64() * 2 * math.Pi
	r := min + random.Float64()*(max-min)
	return utils.NewVecF(r*math.Cos(phi)+pos.X, r*math.Sin(phi)+pos.Y)
}

func (d *Data) calculateMinimalPathDistance() {
	for i := 0; i < d.NodesNum; i++ {
		for j := 0; j < d.NodesNum; j++ {
			if i == j {
				continue
			}
			if d.Matrix[i][j] {
				d.MinimalPathDistance[i][j] = 1
			} else {
				d.MinimalPathDistance[i][j] = math.MaxInt32
			}
		}
	}
	for k := 0; k < d.NodesNum; k++ {
		for i := 0; i < d.NodesNum; i++ {
			for j := 0; j < d.NodesNum; j++ {
				if d.MinimalPathDistance[i][j] > d.MinimalPathDistance[i][k]+d.MinimalPathDistance[k][j] {
					d.MinimalPathDistance[i][j] = d.MinimalPathDistance[i][k] + d.MinimalPathDistance[k][j]
				}
			}
		}
	}
}

func (d *Data) calculateDegreeCentrality() {
	d.DegreeCentrality = make([]int, d.NodesNum)
	for nid, row := range d.Matrix {
		sum := 0
		for _, val := range row {
			if val {
				sum += 1
			}
		}
		d.DegreeCentrality[nid] = sum
	}
}

// assumes computed minimal path distance
func (d *Data) calculateClosenessCentralities() {
	d.ClosenessCentrality = make([]float64, d.NodesNum)
	for nid1 := range d.Nodes {
		sum := 0.0
		for nid2 := range d.Nodes {
			sum += float64(d.MinimalPathDistance[nid1][nid2])
		}
		d.ClosenessCentrality[nid1] = float64(d.NodesNum-1) / sum
	}
}

func (d *Data) calculateEigenVectorCentrality() {
	A := utils.NewSquereMatrixF(d.NodesNum)
	for i := range d.NodesNum {
		for j := range d.NodesNum {
			if d.Matrix[i][j] {
				A[i][j] = 1
			}
		}
	}
	d.EigenVectorCentrality = utils.PowerIteration(A)
}

func (d *Data) String() string {
	return fmt.Sprintf(
		"Nodes: %d\nBonusE: %.2f(%d/%d)\nCrossE: %.2f(%d/%d)\nMinNE: %.2f\nMinNN: %.2f\nMaxNN: %.2f\nMaxBE: %.2f\nMaxCE: %.2f\nRadiusProb: %.2f",
		d.NodesNum,

		d.BonusE,
		d.BENum,
		d.BEMax,

		d.CrossE,
		d.BCNum,
		d.BCMax,

		d.MinNE,
		d.MinNN,
		d.MaxNN,
		d.MaxBE,
		d.MaxCE,
		d.RadiusProb,
	)
}
