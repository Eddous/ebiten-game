package graphgen

import "gra/utils"

type GenArgs struct {
	NodesNum int

	BonusE float64
	CrossE float64

	MinNE float64
	MinNN float64
	MaxNN float64
	MaxBE float64
	MaxCE float64

	RadiusProb float64
}

func (a GenArgs) Check() bool {
	return a.NodesNum > 0 &&
		utils.IsIn(a.BonusE, 0, 1, 0) &&
		utils.IsIn(a.CrossE, 0, 1, 0) &&
		a.MinNE < a.MinNN &&
		a.MinNN < a.MaxNN &&
		a.MinNN < a.MaxBE &&
		utils.IsIn(a.RadiusProb, 0.01, 1, 0)
}

func NewGenArgs(NodesNum int, BonusE float64, CrossE float64, MinNE float64, MinNN float64, MaxNN float64, MaxBE float64, MaxCE float64, RadiusProb float64) GenArgs {
	return GenArgs{
		NodesNum:   NodesNum,
		BonusE:     BonusE,
		CrossE:     CrossE,
		MinNN:      MinNN,
		MaxNN:      MaxNN,
		MinNE:      MinNE,
		MaxBE:      MaxBE,
		MaxCE:      MaxCE,
		RadiusProb: RadiusProb,
	}
}
