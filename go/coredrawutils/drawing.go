package coredrawutils

import (
	"gra/gulrot"
	"gra/resources"
	"gra/utils"
	"image/color"
	"math"
	"strconv"

	"github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/vector"
)

const NodeSize = 1.
const edgeThickness = 0.1
const citySize = 1.3
const outlineOutline = 1.4
const onEdgeImageSize = 0.6
const logicArrowCenter = 0.35
const physicArrowGap = 0.17
const selectedNodeArrowSize = 0.5

const nodeSoldierFontSize = 0.7
const nodeCreatingSoldierFontSize = 0.4
const marchingSoldierFontSize = 0.5

// ===== GETTERS =====

// ===== DRAWING =====

func DrawEdges(screen *ebiten.Image, camera *gulrot.Camera, nodes []utils.VecF, matrix [][]bool) {
	for i := range nodes {
		for j := i + 1; j < len(nodes); j++ {
			if !matrix[i][j] {
				continue
			}
			n1 := nodes[i]
			n2 := nodes[j]

			x1, y1 := camera.GameToPixels1(n1.X, n1.Y)
			x2, y2 := camera.GameToPixels1(n2.X, n2.Y)
			stroke := camera.GameToPixelsSize(edgeThickness)

			vector.StrokeLine(
				screen,
				float32(x1),
				float32(y1),
				float32(x2),
				float32(y2),
				float32(stroke),
				color.Black,
				true,
			)
			/*
				previous way
				camera.Draw(
					screen,
					imgBlackPixel,
					(n1.X+n2.X)/2,
					(n1.Y+n2.Y)/2,
					gulrot.MM,
					n1.GetDistance(n2),
					edgeThickness,
					utils.GetAngle(n1, n2),
				)
			*/
		}
	}
}

type DrawNodeOptions struct {
	Pos                utils.VecF
	Pid                int
	Outline            byte    // 0: none, 1: full, 2: lined,3: dotted
	OutlineAlpha       float64 // for building + destroying... may be changed
	Soldiers           int     // this is the printed number (preprocessing will be removed)
	SuperScript        int     // will get +/- sign before
	Selected           bool    // is it darkened?
	SelectedMoreTimes  int     // making soldiers in classical
	Banned             bool
	Outgoing           []Outgoing
	WhoChosePlayers    []int
	PointingArrowAngle float64
	ShowArrow          bool
}

type Outgoing struct {
	Dest utils.VecF
	Num  int
}

func NewOutgoing(dest utils.VecF, num int) Outgoing {
	return Outgoing{Dest: dest, Num: num}
}

func DrawNode(screen *ebiten.Image, camera *gulrot.Camera, opt DrawNodeOptions) {
	camera.SetOutDim(screen)

	if opt.Outline != 0 {
		x, y := camera.GameToPixels1(opt.Pos.X, opt.Pos.Y)
		size := camera.GameToPixelsSize(outlineOutline)
		vector.DrawFilledCircle(screen, float32(x), float32(y), float32(size/2), color.White, true)

		dio := camera.Dio(resources.OutlineFull, opt.Pos.X, opt.Pos.Y, gulrot.MM, citySize, citySize, 0)
		dio.ColorScale.ScaleAlpha(float32(opt.OutlineAlpha))
		switch opt.Outline {
		case 1:
			gulrot.Draw1(screen, resources.OutlineFull, dio)
		case 2:
			gulrot.Draw1(screen, resources.OutlineLined, dio)
		case 3:
			gulrot.Draw1(screen, resources.OutlineDotted, dio)
		}
	}

	// this is because I want to make the nodes grayer
	img := resources.ColoredFrames[opt.Pid]
	dio := camera.Dio(img, opt.Pos.X, opt.Pos.Y, gulrot.MM, NodeSize, NodeSize, 0)
	if opt.Selected {
		// I think it could be smarter, but it works ...
		var val float32 = 0.7
		dio.ColorScale.SetR(val)
		dio.ColorScale.SetG(val)
		dio.ColorScale.SetB(val)
	}
	gulrot.Draw1(screen, img, dio)

	if opt.Banned {
		camera.Draw(screen, resources.Nok, opt.Pos.X, opt.Pos.Y, gulrot.MM, NodeSize/2, NodeSize/2, 0)
	}
	if opt.ShowArrow && opt.SelectedMoreTimes == 0 {
		drawLittleArrowToNode(screen, camera, opt.Pos, opt.PointingArrowAngle)
	}
	if opt.SelectedMoreTimes != 0 {
		pos := utils.PolarToCartesian(opt.Pos, NodeSize/2+selectedNodeArrowSize/2, opt.PointingArrowAngle)
		screenX, screenY := camera.GameToPixels1(pos.X, pos.Y)
		gulrot.Text(
			screen,
			strconv.Itoa(opt.SelectedMoreTimes),
			resources.NumberFont,
			camera.GetZoom()*marchingSoldierFontSize,
			int(math.Round(screenX)),
			int(math.Round(screenY)),
			gulrot.MM,
			0,
			0,
			color.Black,
		)
	}

	for _, out := range opt.Outgoing {
		drawReadyToMarch(screen, camera, opt.Pid, opt.Pos, out.Dest, out.Num)
	}

	writeOnNodeSoldiers(screen, camera, opt.Pos, opt.Soldiers, opt.SuperScript)

	if opt.WhoChosePlayers != nil {
		l := float64(len(opt.WhoChosePlayers))
		tr := utils.Get1Transformator(0, 0, l, 2*math.Pi)
		for i := 0; float64(i) < l; i++ {
			p := utils.PolarToCartesian(opt.Pos, NodeSize/2, tr(float64(i)))
			camera.Draw(screen, resources.ColoredFrames[opt.WhoChosePlayers[i]], p.X, p.Y, gulrot.MM, onEdgeImageSize, onEdgeImageSize, 0)
		}
	}
}

func DrawHelpArrows(screen *ebiten.Image, camera *gulrot.Camera, nodes []utils.VecF, moveMatrix [][]int) {
	for a := 0; a < len(moveMatrix)-1; a++ {
		for b := a + 1; b < len(moveMatrix); b++ {
			if moveMatrix[a][b] != 0 && moveMatrix[b][a] != 0 {
				drawHelpArrows(screen, camera, logicArrowCenter, nodes[a], nodes[b], moveMatrix[a][b])
				drawHelpArrows(screen, camera, logicArrowCenter, nodes[b], nodes[a], moveMatrix[b][a])
			} else if moveMatrix[a][b] != 0 {
				drawHelpArrows(screen, camera, 0.5, nodes[a], nodes[b], moveMatrix[a][b])
			} else if moveMatrix[b][a] != 0 {
				drawHelpArrows(screen, camera, 0.5, nodes[b], nodes[a], moveMatrix[b][a])
			}
		}
	}
}

// -----------------------------------

func writeOnNodeSoldiers(screen *ebiten.Image, camera *gulrot.Camera, pos utils.VecF, soldiers, creating int) {
	if soldiers == 0 && creating == 0 {
		return
	}

	screenX, screenY := camera.GameToPixels1(pos.X, pos.Y)
	x := int(math.Round(screenX))
	y := int(math.Round(screenY))

	var soldiersAnchor, creatingAnchor gulrot.Anchor

	if soldiers == 0 && creating != 0 {
		creatingAnchor = gulrot.MM
	} else if soldiers != 0 && creating == 0 {
		soldiersAnchor = gulrot.MM
	} else {
		soldiersAnchor = gulrot.RM
		creatingAnchor = gulrot.LB
	}

	if soldiers != 0 {
		gulrot.Text(
			screen,
			strconv.Itoa(soldiers),
			resources.NumberFont,
			camera.GetZoom()*nodeSoldierFontSize,
			x,
			y,
			soldiersAnchor,
			0,
			0,
			color.Black,
		)
	}
	if creating != 0 {
		pre := ""
		if creating > 0 {
			pre = "+"
		}
		gulrot.Text(
			screen,
			pre+strconv.Itoa(creating),
			resources.NumberFont,
			camera.GetZoom()*nodeCreatingSoldierFontSize,
			x,
			y,
			creatingAnchor,
			0,
			0,
			color.Black,
		)
	}
}

func drawHelpArrows(screen *ebiten.Image, camera *gulrot.Camera, center float64, n1, n2 utils.VecF, num int) {
	physicCenter := center * n1.GetDistance(n2)
	middle := float64(num-1) / 2
	tr := utils.Get1Transformator(middle, physicCenter, middle+1, physicCenter+physicArrowGap)
	for i := 0; i < num; i++ {
		drawBetweenNodes(screen, camera, resources.Arrow1, n1, n2, tr(float64(i)), onEdgeImageSize)
	}
}

func drawLittleArrowToNode(screen *ebiten.Image, camera *gulrot.Camera, node utils.VecF, angle float64) {
	pos := utils.PolarToCartesian(node, outlineOutline/2+selectedNodeArrowSize/2, angle)
	camera.Draw(screen, resources.Arrow2, pos.X, pos.Y, gulrot.MM, selectedNodeArrowSize, selectedNodeArrowSize, angle)
}

func drawReadyToMarch(screen *ebiten.Image, camera *gulrot.Camera, pid int, n1, n2 utils.VecF, num int) {
	drawBetweenNodes(
		screen,
		camera,
		resources.ColoredFrames[pid],
		n1,
		n2,
		0.5,
		onEdgeImageSize,
	)
	writeBetweenNodes(screen, camera, n1, n2, num, 0.5)
}

func drawBetweenNodes(screen *ebiten.Image, camera *gulrot.Camera, image *ebiten.Image, n1, n2 utils.VecF, pos float64, size float64) {
	len := n1.GetDistance(n2)
	camera.Draw(
		screen,
		image,
		utils.Get1Transformator(0, n1.X, len, n2.X)(pos),
		utils.Get1Transformator(0, n1.Y, len, n2.Y)(pos),
		gulrot.MM,
		size,
		size,
		utils.GetAngle(n1, n2),
	)
}

func writeBetweenNodes(screen *ebiten.Image, camera *gulrot.Camera, n1, n2 utils.VecF, num int, pos float64) {
	len := n1.GetDistance(n2)
	gameX, gameY := utils.Get1Transformator(0, n1.X, len, n2.X)(pos), utils.Get1Transformator(0, n1.Y, len, n2.Y)(pos)
	screenX, screenY := camera.GameToPixels1(gameX, gameY)

	gulrot.Text(
		screen,
		strconv.Itoa(num),
		resources.NumberFont,
		camera.GetZoom()*marchingSoldierFontSize,
		int(math.Round(screenX)),
		int(math.Round(screenY)),
		gulrot.MM,
		0,
		0,
		color.Black,
	)
}
