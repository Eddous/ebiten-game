package resources

import (
	"embed"
	"gra/gulrot"
	"image/color"

	"github.com/PerformLine/go-stockutil/colorutil"
	"github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/text/v2"
)

//go:embed files/*
var resourcesFS embed.FS

func loadImage(path string) *ebiten.Image {
	return gulrot.LoadImage(resourcesFS, path)
}

func loadFont(path string) *text.GoTextFaceSource {
	return gulrot.LoadFont(resourcesFS, path)
}

func hslToRgba(h, s, l float64) color.RGBA {
	r, g, b := colorutil.HslToRgb(h, s, l)
	return color.RGBA{r, g, b, 255}
}

func makeFilledImage(image *ebiten.Image, c color.RGBA) *ebiten.Image {
	// threshold := uint32(65535) // complete white
	threshold := uint32(20000)

	w, h := gulrot.ImgSize(image)
	new := ebiten.NewImageFromImage(image)
	for x := 0; x < w; x++ {
		for y := 0; y < h; y++ {
			r, g, b, a := new.At(x, y).RGBA()
			if r >= threshold && g >= threshold && b >= threshold && a == 65535 {
				new.Set(x, y, c)
			}
		}
	}
	return new
}

var RgbsOwners = []color.RGBA{
	hslToRgba(0, 1, 0.7),
	hslToRgba(200, 1, 0.5),
	hslToRgba(120, 0.7, 0.6),
	hslToRgba(60, 0.9, 0.6),
	hslToRgba(30, 1, 0.6),
	hslToRgba(300, 1, 0.7),
}

var (
	BlackPixel,
	WhitePixel, // because of draw triangles (IDK why)
	BackArrow,
	NotReady,
	Ready,
	ButtonOpenMenu,
	ZoomMinus,
	ZoomPlus,
	Help,
	Next,
	Previous,
	Equals,
	WantDraw,
	Ok,
	Nok,
	Node,
	Arrow1,
	Arrow2,
	Arrow3,
	City,
	Destroy,
	Disband,
	Farm,
	Reset,
	Spawn,
	Tower,
	OutlineFull,
	OutlineLined,
	OutlineDotted,
	Players *ebiten.Image

	ColoredFrames map[int]*ebiten.Image
	ColoredImages map[int]*ebiten.Image

	GuiFont,
	TextFont,
	TextFontBold,
	NumberFont *text.GoTextFaceSource
)

// just safeguard
var inited bool

// it is called once, from controller
// it is not init because of ebiten problems
func Init() {
	if inited {
		panic("already inited")
	}

	BlackPixel = ebiten.NewImage(1, 1)
	WhitePixel = ebiten.NewImage(1, 1)
	BlackPixel.Fill(color.Black)
	WhitePixel.Fill(color.White)

	BackArrow = loadImage("imgs/back_arrow.png")
	NotReady = loadImage("imgs/not_ready.png")
	Ready = loadImage("imgs/ready.png")
	ButtonOpenMenu = loadImage("imgs/menu.png")
	ZoomMinus = loadImage("imgs/zoom_minus.png")
	ZoomPlus = loadImage("imgs/zoom_plus.png")
	Help = loadImage("imgs/help.png")
	Next = loadImage("imgs/next.png")
	Previous = loadImage("imgs/previous.png")
	Equals = loadImage("imgs/equals.png")
	WantDraw = loadImage("imgs/want_draw.png")
	Ok = loadImage("imgs/ok.png")
	Nok = loadImage("imgs/nok.png")
	Arrow1 = loadImage("imgs/arrow1.png")
	Arrow2 = loadImage("imgs/arrow2.png")
	Arrow3 = loadImage("imgs/arrow3.png")
	Node = loadImage("imgs/node.png")
	City = loadImage("imgs/city.png")
	Destroy = loadImage("imgs/destroy.png")
	Disband = loadImage("imgs/disband.png")
	Farm = loadImage("imgs/farm.png")
	Reset = loadImage("imgs/reset.png")
	Spawn = loadImage("imgs/spawn.png")
	Tower = loadImage("imgs/tower.png")
	OutlineFull = loadImage("imgs/outline_full.png")
	OutlineLined = loadImage("imgs/outline_lined.png")
	OutlineDotted = loadImage("imgs/outline_dotted.png")
	Players = loadImage("imgs/players.png")

	GuiFont = loadFont("fonts/dejavu_sans_mono_1.ttf")
	TextFont = GuiFont
	TextFontBold = loadFont("fonts/dejavu_sans_mono_2.ttf")
	NumberFont = loadFont("fonts/pixograd.ttf")

	ColoredFrames = map[int]*ebiten.Image{
		-1: Node,
		0:  makeFilledImage(Node, RgbsOwners[0]),
		1:  makeFilledImage(Node, RgbsOwners[1]),
		2:  makeFilledImage(Node, RgbsOwners[2]),
		3:  makeFilledImage(Node, RgbsOwners[3]),
		4:  makeFilledImage(Node, RgbsOwners[4]),
		5:  makeFilledImage(Node, RgbsOwners[5]),
	}

	ColoredImages = map[int]*ebiten.Image{}
	for pid, rgb := range RgbsOwners {
		img := ebiten.NewImage(40, 40)
		img.Fill(rgb)
		ColoredImages[pid] = img
	}
}
