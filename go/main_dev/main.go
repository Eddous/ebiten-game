package main

import (
	"flag"
	"gra/gui"
	"gra/gulrot"
	"gra/utils"
	"log"
	"time"

	"github.com/hajimehoshi/ebiten/v2"
)

func main() {
	addressFlag := flag.String("address", "localhost:54775", "address of the server")
	savedgamesFlag := flag.String("savedgames", "/tmp/gra_savedgames/", "directory for saved games")
	seedFlag := flag.Int64("seed", time.Now().UnixMicro(), "seed for everything")
	flag.Parse()

	ebiten.SetWindowSize(540, 540)
	ebiten.SetWindowResizingMode(ebiten.WindowResizingModeEnabled)
	ebiten.SetWindowTitle("Gra")

	var err error

	// profiling
	// go tool pprof -http=:8080 prof
	/*
		f, err := os.Create("prof")
		if err != nil {
			fmt.Println(err)
			return
		}
		pprof.StartCPUProfile(f)
		defer pprof.StopCPUProfile()
	*/

	err = ebiten.RunGame(
		gulrot.NewGame(
			gui.NewController(
				gui.ControllerArgs{
					Seed:           *seedFlag,
					ServerAdress:   *addressFlag,
					SavedGamesPath: *savedgamesFlag,
					TestingEnv:     true,
					WebUrl:         "http://localhost",
					OpenBrowser:    utils.OpenBrowser,
				},
			),
			nil,
		),
	)
	if err != nil {
		log.Fatal(err)
	}
}
