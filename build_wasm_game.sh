#!/bin/bash

cd go/main_web || { echo "you are in a wrong path"; exit 1; }
env GOOS=js GOARCH=wasm go build -o gra_web.wasm
mv gra_web.wasm ../../web/wasm/

cd ../main_wasm_ai
env GOOS=js GOARCH=wasm go build -o remote_ai.wasm
mv remote_ai.wasm ../../web/wasm/