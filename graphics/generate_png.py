import os
import cairosvg

# Define input and output folder
input_folder = "."
output_folder = "../go/resources/files/imgs/"
size = 100

os.makedirs(output_folder, exist_ok=True)

# Loop through SVG files
for filename in os.listdir(input_folder):
    if filename.endswith(".svg"):
        input_path = os.path.join(input_folder, filename)
        output_path = os.path.join(output_folder, filename.replace(".svg", ".png"))
        
        # Convert SVG to PNG
        cairosvg.svg2png(url=input_path, write_to=output_path, output_width=size, output_height=size)
        print(f"Converted: {filename} -> {output_path}")

print("Conversion completed!")
