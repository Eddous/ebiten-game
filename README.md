# Gra

Gra is a simple, simultaneous, turn-based strategy game for up to 6 players.
The goal of the game is to gain power over the generated map.
The game is available at [grrra.org](https://grrra.org).

![Gra](assets/screenshot.png)

## Development

The game is written in Go using the minimalistic game engine [Ebitengine](https://ebitengine.org).
The game is in active development.

## Licence

Released under the Commons Clause License.