#!/bin/bash

mkdir -p build
rm build/gra_server 2>/dev/null
cd go/main_server || { echo "you are in a wrong path"; exit 1; }
go build -o gra_server
mv gra_server ../../build
