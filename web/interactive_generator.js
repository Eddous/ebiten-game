import "https://d3js.org/d3.v6.js"
import "./wasm/wasm_exec.js"

// global variable
var globalData = null

// HTML ELEMENTS
const histElem = document.getElementById("histogram")

// INITIALIZATION OF MAP
const mapSize = document.getElementById("map").clientWidth
const mapSvg = d3.select("#map")
    .append("svg")
    .attr("width", mapSize)
    .attr("height", mapSize)
    .append("g")

// INITIALIZTION OF A HISTOGRAM
const histMarginX = 50
const histMarginY = 40
const histWidth = histElem.clientWidth - 2 * histMarginX
const histHeigth = histElem.clientHeight - 2 * histMarginY
const histSvg = d3.select("#histogram")
    .append("svg")
    .attr("width", histElem.clientWidth)
    .attr("height", histElem.clientHeight)
    .append("g")
    .attr("transform", "translate(" + histMarginX + "," + histMarginY + ")")
const yAxis = histSvg.append("g")
const xAxis = histSvg.append("g")
    .attr("transform", "translate(0," + histHeigth + ")")

const xLabel = histSvg.append("text")
    .attr("x", histWidth / 2)
    .attr("y", histHeigth + 32)
    .text("x-label")
    .attr("text-anchor", "middle")
    .style("font-size", "12px")

const labelX = -30
const labelY = histHeigth / 2
histSvg.append("text")
    .attr("transform", "rotate(-90," + labelX + "," + labelY + ")")
    .attr("x", labelX)
    .attr("y", labelY)
    .text("Count")
    .attr("text-anchor", "middle")
    .style("font-size", "12px")

// MAKING HISTOGRAM DISAPPEARS
histElem.style.display = "none"

// ===========================
// == BINDING THE FUNCTIONS ==

const goWasm = new Go()
WebAssembly.instantiateStreaming(fetch("wasm/generator.wasm"), goWasm.importObject)
    .then((result) => {
        goWasm.run(result.instance)
        document.getElementById("seed-and-generate").addEventListener("click", () => {
            seedGenerator()
            globalData = getData()
            updateSvgs()
        })
        document.getElementById("generate").addEventListener("click", () => {
            globalData = getData()
            updateSvgs()
        })
    })
document.getElementById("default0").addEventListener("click", () => { setDefaultValues(0) })
document.getElementById("default1").addEventListener("click", () => { setDefaultValues(1) })
document.getElementById("default2").addEventListener("click", () => { setDefaultValues(2) })
document.getElementById("default3").addEventListener("click", () => { setDefaultValues(3) })
document.getElementById("download-button").addEventListener("click", downloadMap)
document.getElementById("statistics").addEventListener("change", updateSvgs)

// ======================    
// == UPDATE FUNCTIONS ==

// function that updates both mapSvg and histSvg
function updateSvgs() {
    let statToShow = document.getElementById("statistics").value

    // showing correct elements
    if (statToShow == "none") {
        histElem.style.display = "none"
    } else {
        histElem.style.display = "block"
    }

    // if data are null then just clean the graphics
    if (globalData == null) {
        mapSvg.selectAll("*").remove()
        histSvg.selectAll("rect").remove()
        return
    }

    // select correct statistics to visualize
    switch (statToShow) {
        case "none":
            updateMap(null)
            histSvg.selectAll("rect").remove()
            break
        case "degree":
            updateMap((d) => d.degree)
            updateHist(degreesForHist(globalData))
            xLabel.text("Degree Centrality")
            break
        case "closeness":
            updateMap((d) => d.closeness)
            updateHist(contDataForHist(globalData, (node) => node.closeness))
            xLabel.text("Closseness Centrality")
            break
        case "eigenvector":
            updateMap((d) => d.eigenvector)
            updateHist(contDataForHist(globalData, (node) => node.eigenvector))
            xLabel.text("EigenVector Centrality")
            break
        default:
            console.log("Unknown value selected")
    }
}

function updateMap(getMetric) {

    function getTranform(logicalMid, realMid, zoomFactor) {
        return x => zoomFactor * x + realMid - zoomFactor * logicalMid
    }

    let minX = 0
    let minY = 0
    let maxX = 0
    let maxY = 0
    for (let node of globalData.nodes) {
        minX = Math.min(minX, node.x)
        minY = Math.min(minY, node.y)
        maxX = Math.max(maxX, node.x)
        maxY = Math.max(maxY, node.y)
    }
    minX -= 1
    maxX += 1
    minY -= 1
    maxY += 1

    const zoom = mapSize / Math.max(maxX - minX, maxY - minY)
    const ax = (maxX + minX) / 2
    const ay = (maxY + minY) / 2
    const middle = mapSize / 2
    const xmap = getTranform(ax, middle, zoom)
    const ymap = getTranform(ay, middle, zoom)

    mapSvg.selectAll("*").remove()

    // background
    mapSvg.append("rect")
        .attr("width", "100%")
        .attr("height", "100%")
        .attr("fill", "white")

    // Initialize the links
    mapSvg.selectAll("line")
        .data(globalData.links)
        .join("line")
        .style("stroke", "black")
        .style("stroke-width", 0.1 * zoom)
        .attr("x1", d => xmap(d.x1))
        .attr("y1", d => ymap(d.y1))
        .attr("x2", d => xmap(d.x2))
        .attr("y2", d => ymap(d.y2))

    let min = Infinity
    let max = -Infinity
    if (getMetric != null) {
        for (let node of globalData.nodes) {
            let m = getMetric(node)
            min = Math.min(min, m)
            max = Math.max(max, m)
        }
    }

    // Initialize the nodes
    mapSvg.selectAll("circle")
        .data(globalData.nodes)
        .join("circle")
        .attr("r", 0.5 * zoom)
        .attr("cx", d => xmap(d.x))
        .attr("cy", d => ymap(d.y))
        .style("stroke-width", 0.1 * zoom)
        .style("stroke", "black")
        .style("fill", d => {
            if (getMetric == null) {
                return "white"
            }
            return d3.interpolatePlasma((getMetric(d) - min) / (max - min))
        })
        .on("mouseover", function (event, d) {
            d3.select("#map").append("div")
                .attr("class", "tooltip")
                .style("position", "absolute")
                .style("left", 0)
                .style("top", 0)
                .style("background-color", "white")
                .style("padding", "10px")
                .style("border-right", "1px solid black")
                .style("border-bottom", "1px solid black")
                .style("font-size", "0.8em")
                .html("Index: " + d.index + "<br>Degree: " + d.degree + "<br>Closeness: " + d.closeness.toFixed(4) + "<br>Eigenvector: " + d.eigenvector.toFixed(4))
        })
        .on("mouseout", function () {
            d3.select(".tooltip").remove()
        })

    if (getMetric == null) {
        return
    }

    var gradient = mapSvg.append("defs")
        .append("linearGradient")
        .attr("id", "rectGradient")
        .attr("x1", "0%")
        .attr("y1", "0%")
        .attr("x2", "100%")
        .attr("y2", "0%")


    gradient.append("stop")
        .attr("offset", "0%")
        .style("stop-color", d3.interpolatePlasma(0))

    gradient.append("stop")
        .attr("offset", "50%")
        .style("stop-color", d3.interpolatePlasma(0.5))

    gradient.append("stop")
        .attr("offset", "100%")
        .style("stop-color", d3.interpolatePlasma(1))

    const width = 200
    const height = 20
    mapSvg.append("rect")
        .attr("width", width)
        .attr("height", height)
        .attr("x", mapSize - width - 10)
        .attr("y", mapSize - height - 5)
        .style("stroke-width", 2)
        .style("stroke", "black")
        .style("fill", "url(#rectGradient)") // Reference the gradient using its id

    mapSvg.append("text")
        .attr("x", mapSize - width - 10)
        .attr("y", mapSize - height - 8)
        .text(min.toFixed(4))
        .attr("text-anchor", "start")
        .style("font-size", "12px")

    mapSvg.append("text")
        .attr("x", mapSize - 10)
        .attr("y", mapSize - height - 8)
        .text(max.toFixed(4))
        .attr("text-anchor", "end")
        .style("font-size", "12px")
}

// function that get data like [{group: "A", value: 10}, {group: "B", value: 5}]
function updateHist(histData) {

    var x = d3.scaleBand()
        .range([0, histWidth])
        .padding(0.2)
    x.domain(histData.map(function (d) { return d.group }))
    xAxis.call(d3.axisBottom(x))

    var y = d3.scaleLinear()
        .range([histHeigth, 0])
    y.domain([0, d3.max(histData, function (d) { return d.value })])
    yAxis.transition().duration(1000).call(d3.axisLeft(y))

    var u = histSvg.selectAll("rect").data(histData)
    u
        .enter()
        .append("rect") // Add a new rect for each new elements
        .merge(u) // get the already existing elements as well
        .transition() // and apply changes to all of them
        .duration(1000)
        .attr("x", function (d) { return x(d.group) })
        .attr("y", function (d) { return y(d.value) })
        .attr("width", x.bandwidth())
        .attr("height", function (d) { return histHeigth - y(d.value) })
        .attr("fill", "#1a7d9c")
        .style("stroke", "black")

    // If less group in the new dataset, I delete the ones not in use anymore
    u
        .exit()
        .remove()
}

// =======================
// == DATA MANIPULATION ==

function getData() {
    const nodes = parseInt(document.getElementById("Nodes").innerHTML)
    const bonusE = parseFloat(document.getElementById("BonusE").innerHTML)
    const crossE = parseFloat(document.getElementById("CrossE").innerHTML)
    const minNE = parseFloat(document.getElementById("MinNE").innerHTML)
    const minNN = parseFloat(document.getElementById("MinNN").innerHTML)
    const maxNN = parseFloat(document.getElementById("MaxNN").innerHTML)
    const maxBE = parseFloat(document.getElementById("MaxBE").innerHTML)
    const maxBC = parseFloat(document.getElementById("MaxBC").innerHTML)
    const radius = parseFloat(document.getElementById("Radius").innerHTML)

    let obj = JSON.parse(generate(nodes, bonusE, crossE, minNE, minNN, maxNN, maxBE, maxBC, radius))
    if (obj == null) {
        return null
    }

    let data = { nodes: [], links: [] }
    for (let i = 0; i < obj.NodesNum; i++) {
        data.nodes.push(
            {
                x: obj.Nodes[i].X,
                y: obj.Nodes[i].Y,
                index: i,
                degree: obj.DegreeCentrality[i],
                closeness: obj.ClosenessCentrality[i],
                eigenvector: obj.EigenVectorCentrality[i]
            }
        )
        for (let j = i + 1; j < obj.NodesNum; j++) {
            if (obj.Matrix[i][j]) {
                data.links.push(
                    {
                        x1: obj.Nodes[i].X,
                        y1: obj.Nodes[i].Y,
                        x2: obj.Nodes[j].X,
                        y2: obj.Nodes[j].Y
                    }
                )
            }
        }
    }
    return data
}

function degreesForHist(data) {
    let map = new Map()
    let min = 1000
    let max = 0
    for (let node of data.nodes) {
        let d = node.degree
        if (!map.has(d)) {
            map.set(d, 0)
        }
        map.set(d, map.get(d) + 1)
        if (d < min) {
            min = d
        }
        if (d > max) {
            max = d
        }
    }
    let ret = []
    for (let i = min; i <= max; i++) {
        if (!map.has(i)) {
            map.set(i, 0)
        }
        ret.push(
            {
                group: i,
                value: map.get(i)
            }
        )
    }
    return ret
}

// data is non empty list of numbers
function iqr(data) {
    let values, q1, q3

    values = data.slice().sort((a, b) => a - b)

    if ((values.length / 4) % 1 === 0) {
        q1 = 1 / 2 * (values[(values.length / 4)] + values[(values.length / 4) + 1])
        q3 = 1 / 2 * (values[(values.length * (3 / 4))] + values[(values.length * (3 / 4)) + 1])
    } else {
        q1 = values[Math.floor(values.length / 4 + 1)]
        q3 = values[Math.ceil(values.length * (3 / 4) + 1)]
    }

    return q3 - q1
}

// data is just list of values, returns number of bins
function freedmanDiaconis(data) {
    let min = Math.min(...data)
    let max = Math.max(...data)
    let width = 2 * iqr(data) * Math.pow(data.length, -0.333)
    return Math.round((max - min) / width)
}

// data is just list of values, returns number of bins
function square(data) {
    return Math.ceil(Math.sqrt(data.length))
}

// data is just list of values, returns number of bins
function sturges(data) {
    return Math.ceil(Math.log2(data.length))
}

// data is just list of values, returns number of bins
function rice(data) {
    return Math.ceil(2 * Math.pow(data.length, 1 / 3))
}

function contDataForHist(data, getter) {
    let stripped = []
    for (let node of data.nodes) {
        stripped.push(getter(node))
    }
    let min = Math.min(...stripped)
    let max = Math.max(...stripped)

    let bins = freedmanDiaconis(stripped)
    // let bins = square(stripped)
    // let bins = sturges(stripped)
    // let bins = rice(stripped)

    let width = (max - min) / bins

    let ret = []
    for (let i = 0; i < bins; i++) {
        ret.push(
            {
                group: (min + (i + 0.5) * width).toFixed(4),
                value: 0
            }
        )
    }

    for (let x of stripped) {
        let bin = Math.floor((x - min) / width)
        if (bin == ret.length) {
            bin -= 1
        }
        ret[bin].value += 1
    }
    return ret
}

// ===============================
// == SETTING DEFAULT ARGUMENTS ==

const defaultArgs = [
    { Nodes: 20, BonusE: 0.5, CrossE: 0.2, MinNE: 1, MinNN: 2, MaxNN: 3, MaxBE: 4, MaxBC: 4.5, Radius: 0.05 },
    { Nodes: 45, BonusE: 0.5, CrossE: 0.2, MinNE: 1, MinNN: 2, MaxNN: 3, MaxBE: 4, MaxBC: 4.5, Radius: 0.5 },
    { Nodes: 80, BonusE: 0.5, CrossE: 0.2, MinNE: 1, MinNN: 2, MaxNN: 3, MaxBE: 4, MaxBC: 4.5, Radius: 1 },
    { Nodes: 125, BonusE: 0.5, CrossE: 0.2, MinNE: 1, MinNN: 2, MaxNN: 3, MaxBE: 4, MaxBC: 4.5, Radius: 1 }
]

function setDefaultValues(val) {
    document.getElementById("Nodes").innerHTML = defaultArgs[val].Nodes
    document.getElementById("NodesSlider").value = defaultArgs[val].Nodes

    document.getElementById("BonusE").innerHTML = defaultArgs[val].BonusE
    document.getElementById("BonusESlider").value = defaultArgs[val].BonusE

    document.getElementById("CrossE").innerHTML = defaultArgs[val].CrossE
    document.getElementById("CrossESlider").value = defaultArgs[val].CrossE

    document.getElementById("MinNE").innerHTML = defaultArgs[val].MinNE
    document.getElementById("MinNESlider").value = defaultArgs[val].MinNE

    document.getElementById("MinNN").innerHTML = defaultArgs[val].MinNN
    document.getElementById("MinNNSlider").value = defaultArgs[val].MinNN

    document.getElementById("MaxNN").innerHTML = defaultArgs[val].MaxNN
    document.getElementById("MaxNNSlider").value = defaultArgs[val].MaxNN

    document.getElementById("MaxBE").innerHTML = defaultArgs[val].MaxBE
    document.getElementById("MaxBESlider").value = defaultArgs[val].MaxBE

    document.getElementById("MaxBC").innerHTML = defaultArgs[val].MaxBC
    document.getElementById("MaxBCSlider").value = defaultArgs[val].MaxBC

    document.getElementById("Radius").innerHTML = defaultArgs[val].Radius
    document.getElementById("RadiusSlider").value = defaultArgs[val].Radius
}

// =====================
// == DOWNLOADING MAP ==

function downloadMap() {
    const imageSize = 1240

    const svg = document.querySelector('svg')
    const data = (new XMLSerializer()).serializeToString(svg)
    const svgBlob = new Blob([data], {
        type: 'image/svg+xml;charset=utf-8'
    })

    const url = URL.createObjectURL(svgBlob)

    const img = new Image()
    img.addEventListener('load', () => {
        const canvas = document.createElement('canvas')
        canvas.width = imageSize
        canvas.height = imageSize

        const context = canvas.getContext('2d')
        context.drawImage(img, 0, 0, imageSize, imageSize)

        URL.revokeObjectURL(url)

        const a = document.createElement('a')
        a.download = Date.now() + '.png'
        document.body.appendChild(a)
        a.href = canvas.toDataURL()
        a.click()
        a.remove()
    })
    img.src = url
}