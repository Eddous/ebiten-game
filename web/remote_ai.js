importScripts('wasm/wasm_exec.js')

// RACE condition
// if the user is faster then loading of wasm, it dies horrible death
// punishment for too fast players


const go = new Go();
WebAssembly.instantiateStreaming(fetch("wasm/remote_ai.wasm"), go.importObject).then(result => {
    go.run(result.instance)
    self.onmessage = async function (e) {
        const { id, gameType, data } = e.data
        const action = remoteAI(gameType, data)
        postMessage({id, action})  // id is not PID!!!
    }
})
