import os
import sys
from PIL import Image, ImageDraw, ImageFont, ImageColor

COLOR = "#ef9c00"
PAD_RATIO = 0.03
TEXT_POS = 0.035
SQUERE = 0.1
FONT_SIZE = 0.05
IN = "imgs/tutorial/"
OUT = "imgs/generated/"


def proccess_img(img1, number):
    width1, _ = img1.size  # images will be squeres

    pad = int(width1 * PAD_RATIO)
    width2 = width1 + 2 * pad
    img2 = Image.new(
        mode="RGB", size=(width2, width2), color=ImageColor.getcolor(COLOR, "RGB")
    )
    img2.paste(img1, (pad, pad))

    draw = ImageDraw.Draw(img2)

    squere_pos = int(SQUERE * width2)
    draw.rectangle([(0, 0), (squere_pos, squere_pos)], fill=COLOR)

    font = ImageFont.truetype("fonts/dejavu_sans_mono_2.ttf", int(FONT_SIZE * width2))
    text_pos = int(TEXT_POS * width2)
    draw.text((text_pos, text_pos), number, (0, 0, 0), font=font, anchor="lt")

    return img2


for name in os.listdir(IN):
    img1 = Image.open(IN + name)
    number = name.split(".")[0].split("_")[-1]
    img2 = proccess_img(img1, number)
    img2.save(OUT + name)
