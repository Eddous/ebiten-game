const CACHE_NAME = 'grrra:v2';
const CACHE_ASSETS = [
    '/play.html',
    '/manifest.json',
    '/imgs/logo.svg',
    '/wasm/wasm_exec.js',
    '/wasm/gra_web.wasm',
];

self.addEventListener('install', (event) => {
    console.log('Service Worker: Installing...');

    event.waitUntil(
        caches.open(CACHE_NAME)
            .then(cache => {
                console.log('Service Worker: Caching Files...');
                return cache.addAll(CACHE_ASSETS);
            })
            .then(() => self.skipWaiting())
    );
});

// Call activate event
self.addEventListener('activate', (event) => {
    console.log('Service Worker: Activating...');

    // Remove old caches
    event.waitUntil(
        caches.keys().then(cacheNames => {
            return Promise.all(
                cacheNames.map(cache => {
                    if (cache !== CACHE_NAME) {
                        console.log('Service Worker: Clearing Old Cache...');
                        return caches.delete(cache);
                    }
                })
            );
        })
    );

    self.clients.claim();
});

// Call fetch event
self.addEventListener('fetch', (event) => {
    console.log('Service Worker: Fetching...');

    event.respondWith(
        fetch(event.request)
            .then(response => {
                const resClone = response.clone();
                caches.open(CACHE_NAME)
                    .then(cache => {
                        cache.put(event.request, resClone);
                    });
                return response;
            }).catch(err => caches.match(event.request).then(response => response))
    );
});