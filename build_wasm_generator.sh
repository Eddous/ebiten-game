#!/bin/bash

cd go/main_gen_web || { echo "you are in a wrong path"; exit 1; }
env GOOS=js GOARCH=wasm go build -o generator.wasm
mv generator.wasm ../../web/wasm/
